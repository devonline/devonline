/**
 * 删除嵌套数据的脚本
 */

/**
 * parameter
 */
DELETE FROM `parameter` WHERE parent_id IN (
	SELECT t2.id FROM (
		SELECT id FROM `parameter` WHERE parent_id IN (
			SELECT t.id FROM `parameter` AS t WHERE parent_id IS NULL
		)
	) AS t2
);

DELETE FROM `parameter` WHERE parent_id IN (
	SELECT t2.id FROM (
		SELECT id FROM `parameter` WHERE parent_id IS NULL
	) AS t2
);

DELETE FROM `parameter` WHERE parent_id IS NULL;
