-- --------------------------------------------------------
-- 主机:                           192.168.20.199
-- 服务器版本:                        5.7.42 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- auth 数据库 identity 相关的设置

/**
 * 修改本地地址为服务器地址

    https://localhost:9527 -> https://auth.devonline.cn
    https://localhost:8080 -> https://app.devonline.cn

*/


-- 正在导出表  auth.ApiResourceClaims 的数据：~9 rows (大约)
INSERT INTO `ApiResourceClaims` (`Id`, `ApiResourceId`, `Type`) VALUES
	(1, 1, 'id'),
	(2, 1, 'name'),
	(3, 1, 'userName');

-- 正在导出表  auth.ApiResources 的数据：~3 rows (大约)
INSERT INTO `ApiResources` (`Id`, `Enabled`, `Name`, `DisplayName`, `Description`, `AllowedAccessTokenSigningAlgorithms`, `ShowInDiscoveryDocument`, `Created`, `Updated`, `LastAccessed`, `NonEditable`) VALUES
	(1, 1, 'hmf.admin', NULL, NULL, NULL, 1, '2023-08-22 01:15:42.491566', NULL, NULL, 0);

-- 正在导出表  auth.ApiResourceScopes 的数据：~3 rows (大约)
INSERT INTO `ApiResourceScopes` (`Id`, `Scope`, `ApiResourceId`) VALUES
	(1, 'hmf.admin', 1);

-- 正在导出表  auth.ApiResourceSecrets 的数据：~3 rows (大约)
INSERT INTO `ApiResourceSecrets` (`Id`, `ApiResourceId`, `Description`, `Value`, `Expiration`, `Type`, `Created`) VALUES
	(1, 1, NULL, '7E898mdv0+FhTMg2C1wBiT7FOHNgt7eG2OasTeqwDnw=', NULL, 'SharedSecret', '2023-08-22 01:15:42.492120');

-- 正在导出表  auth.ApiScopes 的数据：~3 rows (大约)
INSERT INTO `ApiScopes` (`Id`, `Enabled`, `Name`, `DisplayName`, `Description`, `Required`, `Emphasize`, `ShowInDiscoveryDocument`) VALUES
	(1, 1, 'hmf.admin', 'hmf admin service api', NULL, 0, 0, 1);

-- 正在导出表  auth.ClientCorsOrigins 的数据：~3 rows (大约)
INSERT INTO `ClientCorsOrigins` (`Id`, `Origin`, `ClientId`) VALUES
	(1, 'https://localhost:44301', 1);

-- 正在导出表  auth.ClientGrantTypes 的数据：~3 rows (大约)
INSERT INTO `ClientGrantTypes` (`Id`, `GrantType`, `ClientId`) VALUES
	(1, 'authorization_code', 1),
	(2, 'password', 1);

-- 正在导出表  auth.ClientPostLogoutRedirectUris 的数据：~3 rows (大约)
INSERT INTO `ClientPostLogoutRedirectUris` (`Id`, `PostLogoutRedirectUri`, `ClientId`) VALUES
	(1, 'https://localhost:44301/signout-callback-oidc', 1);

-- 正在导出表  auth.ClientRedirectUris 的数据：~3 rows (大约)
INSERT INTO `ClientRedirectUris` (`Id`, `RedirectUri`, `ClientId`) VALUES
	(1, 'https://localhost:44301/signin-oidc', 1);

-- 正在导出表  auth.Clients 的数据：~3 rows (大约)
INSERT INTO `Clients` (`Id`, `Enabled`, `ClientId`, `ProtocolType`, `RequireClientSecret`, `ClientName`, `Description`, `ClientUri`, `LogoUri`, `RequireConsent`, `AllowRememberConsent`, `AlwaysIncludeUserClaimsInIdToken`, `RequirePkce`, `AllowPlainTextPkce`, `RequireRequestObject`, `AllowAccessTokensViaBrowser`, `FrontChannelLogoutUri`, `FrontChannelLogoutSessionRequired`, `BackChannelLogoutUri`, `BackChannelLogoutSessionRequired`, `AllowOfflineAccess`, `IdentityTokenLifetime`, `AllowedIdentityTokenSigningAlgorithms`, `AccessTokenLifetime`, `AuthorizationCodeLifetime`, `ConsentLifetime`, `AbsoluteRefreshTokenLifetime`, `SlidingRefreshTokenLifetime`, `RefreshTokenUsage`, `UpdateAccessTokenClaimsOnRefresh`, `RefreshTokenExpiration`, `AccessTokenType`, `EnableLocalLogin`, `IncludeJwtId`, `AlwaysSendClientClaims`, `ClientClaimsPrefix`, `PairWiseSubjectSalt`, `Created`, `Updated`, `LastAccessed`, `UserSsoLifetime`, `UserCodeType`, `DeviceCodeLifetime`, `NonEditable`) VALUES
	(1, 1, 'hmf.admin', 'oidc', 1, 'hmf admin', NULL, 'https://localhost:44301', NULL, 0, 1, 1, 1, 0, 0, 1, NULL, 1, NULL, 1, 0, 300, NULL, 3600, 300, NULL, 2592000, 1296000, 1, 0, 1, 0, 1, 1, 0, 'client_', NULL, '2023-08-22 01:15:42.703484', NULL, NULL, NULL, NULL, 300, 0);

-- 正在导出表  auth.ClientScopes 的数据：~9 rows (大约)
INSERT INTO `ClientScopes` (`Id`, `Scope`, `ClientId`) VALUES
	(1, 'openid', 1),
	(2, 'profile', 1),
	(3, 'hmf.admin', 1);

-- 正在导出表  auth.ClientSecrets 的数据：~3 rows (大约)
INSERT INTO `ClientSecrets` (`Id`, `ClientId`, `Description`, `Value`, `Expiration`, `Type`, `Created`) VALUES
	(1, 1, NULL, '7E898mdv0+FhTMg2C1wBiT7FOHNgt7eG2OasTeqwDnw=', NULL, 'SharedSecret', '2023-08-22 01:15:42.704073');

-- 正在导出表  auth.DeviceCodes 的数据：~0 rows (大约)

-- 正在导出表  auth.IdentityResourceClaims 的数据：~8 rows (大约)
INSERT INTO `IdentityResourceClaims` (`Id`, `IdentityResourceId`, `Type`) VALUES
	(1, 1, 'id'),
	(2, 1, 'sub'),
	(3, 1, 'userId'),
	(4, 2, 'name'),
	(5, 2, 'alias'),
	(6, 2, 'image'),
	(7, 2, 'type'),
	(8, 2, 'userName');

-- 正在导出表  auth.IdentityResources 的数据：~2 rows (大约)
INSERT INTO `IdentityResources` (`Id`, `Enabled`, `Name`, `DisplayName`, `Description`, `Required`, `Emphasize`, `ShowInDiscoveryDocument`, `Created`, `Updated`, `NonEditable`) VALUES
	(1, 1, 'openid', 'Your user identifier', NULL, 1, 0, 1, '2023-08-22 01:15:42.206885', NULL, 0),
	(2, 1, 'profile', 'User profile', 'Your user profile information (first name, last name, etc.)', 0, 1, 1, '2023-08-22 01:15:42.266156', NULL, 0);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
