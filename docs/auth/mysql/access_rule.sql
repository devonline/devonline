-- --------------------------------------------------------
-- 主机:                           app.devonline.cn
-- 服务器版本:                        8.3.0 - MySQL Community Server - GPL
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- 正在导出表  auth.access_rule 的数据：~9 rows (大约)
INSERT INTO `access_rule` (`id`, `row_version`, `state`, `created_on`, `created_by`, `updated_on`, `updated_by`, `description`, `resource_id`, `identity_id`, `identity_type`, `code`, `is_allow`, `priority`, `expire_time`, `access_count`, `condition`) VALUES
	('3235850708901167104', '3235850717314940928', 'Available', '2024-06-12 08:34:17.206915', 'developer', '2024-06-12 08:34:17.207354', 'developer', NULL, '314206411103928321', NULL, 'All', '#AS0_All-', 'Forbid', 0, NULL, 0, NULL),
	('3235850708989247488', '3235850717528850432', 'Available', '2024-06-12 08:34:17.258076', 'developer', '2024-06-12 08:34:17.258085', 'developer', NULL, '319714208797163521', NULL, 'All', '#AS0_All+AccessDenied', 'Allow', 9, NULL, 0, NULL),
	('3235850709068939264', '3235850717528850433', 'Available', '2024-06-12 08:34:17.258290', 'developer', '2024-06-12 08:34:17.258291', 'developer', NULL, '324063208073592833', NULL, 'All', '#AS0_All+Error', 'Allow', 9, NULL, 0, NULL),
	('3235850709140242432', '3235850717528850434', 'Available', '2024-06-12 08:34:17.258314', 'developer', '2024-06-12 08:34:17.258315', 'developer', NULL, '343316503463985153', NULL, 'All', '#AS0_All+SendCaptcha', 'Allow', 9, NULL, 0, NULL),
	('3235850709169602560', '3235850717528850435', 'Available', '2024-06-12 08:34:17.258333', 'developer', '2024-06-12 08:34:17.258334', 'developer', NULL, '343316503463985155', '3235834974917623808', 'Role', '#AS0_Authenticator+BindPhoneNumber', 'Allow', 9, NULL, 0, NULL),
	('3235850709207351296', '3235850717528850436', 'Available', '2024-06-12 08:34:17.258352', 'developer', '2024-06-12 08:34:17.258352', 'developer', NULL, '457038300318793739', '3235834974917623808', 'Role', '#AS0_Authenticator+ConfirmPhoneNumber', 'Allow', 9, NULL, 0, NULL),
	('3235850709245100032', '3235850717528850437', 'Available', '2024-06-12 08:34:17.258374', 'developer', '2024-06-12 08:34:17.258375', 'developer', NULL, '322971014801653761', '3235834974917623808', 'Role', '#AS0_Authenticator+GetUserInfo', 'Allow', 9, NULL, 0, NULL),
	('3235850709282848768', '3235850717528850438', 'Available', '2024-06-12 08:34:17.258391', 'developer', '2024-06-12 08:34:17.258392', 'developer', NULL, '314206411103928321', '537101188135387136', 'Role', '#AS0_Admin+', 'Allow', 99, NULL, 0, NULL),
	('3235850709320597504', '3235850717528850439', 'Available', '2024-06-12 08:34:17.258408', 'developer', '2024-06-12 08:34:17.258409', 'developer', NULL, '314206411103928321', '537101188538040320', 'Role', '#AS0_Developer+', 'Allow', 99, NULL, 0, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
