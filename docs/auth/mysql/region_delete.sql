/**
 * 删除嵌套数据的脚本
 */

/**
 * delete region
 */
DELETE FROM `region` WHERE parent_id IN (
	SELECT t6.id FROM (
		SELECT id FROM `region` WHERE parent_id IN (
			SELECT t5.id FROM (
				SELECT id FROM `region` WHERE parent_id IN (
					SELECT t4.id FROM (
						SELECT id FROM `region` WHERE parent_id IN (
							SELECT t3.id FROM (
								SELECT id FROM `region` WHERE parent_id IN (
									SELECT t2.id FROM (
										SELECT id FROM `region` WHERE parent_id IN (
											SELECT t.id FROM (
												SELECT id FROM `region` WHERE parent_id IS NULL
											) AS t
										)
									) AS t2
								)
							) AS t3
						)
					) AS t4
				)
			) AS t5
		)
	) AS t6
);

DELETE FROM `region` WHERE parent_id IN (
	SELECT t5.id FROM (
		SELECT id FROM `region` WHERE parent_id IN (
			SELECT t4.id FROM (
				SELECT id FROM `region` WHERE parent_id IN (
					SELECT t3.id FROM (
						SELECT id FROM `region` WHERE parent_id IN (
							SELECT t2.id FROM (
								SELECT id FROM `region` WHERE parent_id IN (
									SELECT t.id FROM (
										SELECT id FROM `region` WHERE parent_id IS NULL
									) AS t
								)
							) AS t2
						)
					) AS t3
				)
			) AS t4
		)
	) AS t5
);

DELETE FROM `region` WHERE parent_id IN (
	SELECT t4.id FROM (
		SELECT id FROM `region` WHERE parent_id IN (
			SELECT t3.id FROM (
				SELECT id FROM `region` WHERE parent_id IN (
					SELECT t2.id FROM (
						SELECT id FROM `region` WHERE parent_id IN (
							SELECT t.id FROM (
								SELECT id FROM `region` WHERE parent_id IS NULL
							) AS t
						)
					) AS t2
				)
			) AS t3
		)
	) AS t4
);

DELETE FROM `region` WHERE parent_id IN (
	SELECT t3.id FROM (
		SELECT id FROM `region` WHERE parent_id IN (
			SELECT t2.id FROM (
				SELECT id FROM `region` WHERE parent_id IN (
					SELECT t.id FROM (
						SELECT id FROM `region` WHERE parent_id IS NULL
					) AS t
				)
			) AS t2
		)
	) AS t3
);

DELETE FROM `region` WHERE parent_id IN (
	SELECT t2.id FROM (
		SELECT id FROM `region` WHERE parent_id IN (
			SELECT t.id FROM (
				SELECT id FROM `region` WHERE parent_id IS NULL
			) AS t
		)
	) AS t2
);

DELETE FROM `region` WHERE parent_id IN (
	SELECT t.id FROM (
		SELECT id FROM `region` WHERE parent_id IS NULL
	) AS t
);

DELETE FROM `region` WHERE parent_id IS NULL;
