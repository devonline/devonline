-- --------------------------------------------------------
-- 主机:                           www.devonline.cn
-- 服务器版本:                        PostgreSQL 16.1 (Debian 16.1-1.pgdg120+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 12.2.0-14) 12.2.0, 64-bit
-- 服务器操作系统:                      
-- HeidiSQL 版本:                  12.6.0.6765
-- --------------------------------------------------------

INSERT INTO `user` (`id`, `row_version`, `state`, `created_on`, `created_by`, `updated_on`, `updated_by`, `description`, `name`, `alias`, `image`, `type`, `level_id`, `lockout_end`, `two_factor_enabled`, `phone_number_confirmed`, `phone_number`, `concurrency_stamp`, `security_stamp`, `password_hash`, `email_confirmed`, `normalized_email`, `email`, `normalized_user_name`, `user_name`, `lockout_enabled`, `access_failed_count`) VALUES
	('537101194422648832', '537101194422648833', 'Available', '2024-01-22 05:27:10.608296', 'system', '2024-01-22 05:27:10.608298', 'system', '系统本身', '系统', '系统', NULL, 'System', NULL, NULL, 0, 0, NULL, '8a83292c-0c9f-4fa0-9803-50f0ae2346ef', 'T5O34AEQFNVFPWMQBLXCTP34WMRVZ2XR', NULL, 0, NULL, NULL, 'SYSTEM', 'system', 1, 0),
	('537101195819352064', '537101195819352065', 'Available', '2024-01-22 05:27:10.941812', 'system', '2024-01-22 05:27:10.941815', 'system', '匿名用户', '匿名用户', '匿名用户', NULL, 'Anonymous', NULL, NULL, 0, 0, NULL, 'e3eb6a7c-26fe-4dfa-a474-5fe42009949a', 'CXAEILT6IGQN7HKQAOLMMSR7RWTAVY6L', NULL, 0, NULL, NULL, 'ANONYMOUS', 'anonymous', 1, 0),
	('537101192296136704', '537101192296136705', 'Available', '2024-01-22 21:27:10.101517', 'system', '2024-01-22 21:27:10.101523', 'system', '系统开发者', '开发者', '开发者', NULL, 'Developer', NULL, NULL, 0, 0, NULL, '7dc20ed8-51f0-4ab5-a7a4-aa754389da4a', 'CKXBJQTLZSFN26JREIDQV4V2OO62EVRI', 'AQAAAAIAAYagAAAAEEwbbKG+Nde9uOpyu0+6Xw5h5OQqtLpYTEfsoYuBFMzE723LLlEvKYPE9EOdOCV6Bw==', 0, NULL, NULL, 'DEVELOPER', 'developer', 1, 0),
	('537101189305597952', '537101189318180864', 'Available', '2024-01-22 05:27:09.390194', 'system', '2024-01-22 05:27:09.391118', 'system', '超级管理员', '超级管理员', '超级管理员', NULL, 'Administrator', NULL, NULL, 0, 0, NULL, 'bda21cc2-51c6-43d9-8f60-6eef4e4d0c6c', 'OAO3ZENBZDBSFQUX7A5KYPXSECWE6XQ6', 'AQAAAAIAAYagAAAAEAngdZCJ916geAJE0VV50QHwQt8OEvOfHF0C/A1cc8Sp71vyIYtMvPBgZ6X4ek+evA==', 0, NULL, NULL, 'ADMINISTRATOR', 'administrator', 1, 0);


INSERT INTO `role` (`id`, `name`, `alias`, `image`, `type`, `row_version`, `state`, `created_on`, `created_by`, `updated_on`, `updated_by`, `description`, `normalized_name`, `concurrency_stamp`) VALUES
	('537101184482148352', 'systems', '系统', NULL, 'System', '537101184486342656', 'Available', '2024-01-22 05:27:08.238777', 'system', '2024-01-22 05:27:08.239318', 'system', '系统本身', 'SYSTEMS', '537101184482148352'),
	('537101188135387136', 'administrators', '超级管理员', NULL, 'Administrator', '537101188135387137', 'Available', '2024-01-22 05:27:09.109182', 'system', '2024-01-22 05:27:09.109202', 'system', '超级管理员', 'ADMINISTRATORS', '537101188135387136'),
	('537101188538040320', 'developers', '开发者', NULL, 'Developer', '537101188538040321', 'Available', '2024-01-22 05:27:09.205312', 'system', '2024-01-22 05:27:09.205313', 'system', '系统开发者', 'DEVELOPERS', '537101188538040320'),
	('537101188898750464', 'anonymous', '匿名用户', NULL, 'Anonymous', '537101188898750464', 'Available', '2024-01-22 05:27:09.291395', 'system', '2024-01-22 05:27:09.291397', 'system', '匿名用户', 'ANONYMOUS', '537101188898750464'),
	('537101188898750465', 'authorizers', '授权者', NULL, 'Authorizer', '537101188898750465', 'Available', '2024-01-22 05:27:09.291395', 'system', '2024-01-22 05:27:09.291397', 'system', '授权者', 'AUTHORIZERS', '537101188898750465'),
	('537101188898750467', 'external_administrators', '外部管理员', NULL, 'ExternalAdministrator', '3235834974133288960', 'Available', '2024-06-12 07:31:43.739709', 'developer', '2024-06-12 07:31:43.740605', 'developer', '外部管理员', 'EXTERNAL_ADMINISTRATORS', '3235834974124900352'),
	('537101188898750468', 'authenticators', '认证用户', NULL, 'Authenticator', '3235834974917623809', 'Available', '2024-06-12 07:31:43.927880', 'developer', '2024-06-12 07:31:43.927890', 'developer', '认证用户', 'AUTHENTICATORS', '3235834974917623808');


INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	('537101189305597952', '537101188135387136'),
	('537101192296136704', '537101188538040320'),
	('537101194422648832', '537101184482148352'),
	('537101195819352064', '537101188898750464');