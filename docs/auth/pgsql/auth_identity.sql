-- auth 数据库 identity 相关的设置

--- IdentityResources 为固定配置
INSERT INTO "IdentityResources" ("Id", "Enabled", "Name", "DisplayName", "Description", "Required", "Emphasize", "ShowInDiscoveryDocument", "Created", "Updated", "NonEditable") VALUES
	(1, 'true', 'openid', 'Your user identifier', 'Your user identifier infomation (id, userId, etc..)', 'true', 'false', 'true', '2024-01-01 00:00:00', NULL, 'false'),
	(2, 'true', 'profile', 'User profile', 'Your user profile information (first name, last name, etc..)', 'false', 'true', 'true', '2024-01-01 00:00:00', NULL, 'false');
	
INSERT INTO "IdentityResourceClaims" ("Id", "IdentityResourceId", "Type") VALUES
	(1, 1, 'id'),
	(2, 1, 'sub'),
	(3, 1, 'userId'),
	(4, 2, 'name'),
	(5, 2, 'alias'),
	(6, 2, 'image'),
	(7, 2, 'type'),
	(8, 2, 'userName');



--- 以下根据应用设置

/**
 * 修改本地地址为服务器地址

    https://localhost:44301 -> https://app.devonline.cn

*/

INSERT INTO "ApiResources" ("Id", "Enabled", "Name", "DisplayName", "Description", "AllowedAccessTokenSigningAlgorithms", "ShowInDiscoveryDocument", "Created", "Updated", "LastAccessed", "NonEditable") VALUES
	(101, 'true', 'ft.dms', NULL, NULL, NULL, 'true', '2024-01-01 00:00:00', NULL, NULL, 'false');

INSERT INTO "ApiResourceClaims" ("Id", "ApiResourceId", "Type") VALUES
	(101, 101, 'id'),
	(102, 101, 'name'),
	(103, 101, 'userName');

INSERT INTO "ApiResourceScopes" ("Id", "Scope", "ApiResourceId") VALUES
	(101, 'ft.dms', 101);

INSERT INTO "ApiResourceSecrets" ("Id", "ApiResourceId", "Description", "Value", "Expiration", "Type", "Created") VALUES
	(101, 101, NULL, '7E898mdv0+FhTMg2C1wBiT7FOHNgt7eG2OasTeqwDnw=', NULL, 'SharedSecret', '2024-01-01 00:00:00');


INSERT INTO "ApiScopes" ("Id", "Enabled", "Name", "DisplayName", "Description", "Required", "Emphasize", "ShowInDiscoveryDocument") VALUES 
	(101, 'true', 'ft.dms', 'foreign trade dms service api', NULL, 'true', 'false', 'true');

INSERT INTO "ApiScopeClaims" ("Id", "ScopeId", "Type") VALUES 
	(101, 101, 'name'),
	(102, 101, 'role'),
	(103, 101, 'userName');


INSERT INTO "Clients" ("Id", "Enabled", "ClientId", "ProtocolType", "RequireClientSecret", "ClientName", "Description", "ClientUri", "LogoUri", "RequireConsent", "AllowRememberConsent", "AlwaysIncludeUserClaimsInIdToken", "RequirePkce", "AllowPlainTextPkce", "RequireRequestObject", "AllowAccessTokensViaBrowser", "FrontChannelLogoutUri", "FrontChannelLogoutSessionRequired", "BackChannelLogoutUri", "BackChannelLogoutSessionRequired", "AllowOfflineAccess", "IdentityTokenLifetime", "AllowedIdentityTokenSigningAlgorithms", "AccessTokenLifetime", "AuthorizationCodeLifetime", "ConsentLifetime", "AbsoluteRefreshTokenLifetime", "SlidingRefreshTokenLifetime", "RefreshTokenUsage", "UpdateAccessTokenClaimsOnRefresh", "RefreshTokenExpiration", "AccessTokenType", "EnableLocalLogin", "IncludeJwtId", "AlwaysSendClientClaims", "ClientClaimsPrefix", "PairWiseSubjectSalt", "Created", "Updated", "LastAccessed", "UserSsoLifetime", "UserCodeType", "DeviceCodeLifetime", "NonEditable") VALUES
	(101, 'true', 'ft.dms', 'oidc', 'true', 'foreign trade dms system', NULL, 'https://localhost:44301', NULL, 'false', 'true', 'true', 'true', 'false', 'false', 'true', NULL, 'true', NULL, 'true', 'false', 300, NULL, 3600, 300, NULL, 2592000, 1296000, 1, 'false', 1, 0, 'true', 'true', 'false', 'client_', NULL, '2024-01-01 00:00:00', NULL, NULL, NULL, NULL, 300, 'false');

INSERT INTO "ClientCorsOrigins" ("Id", "Origin", "ClientId") VALUES
	(101, 'https://localhost:44301', 101);

INSERT INTO "ClientGrantTypes" ("Id", "GrantType", "ClientId") VALUES
	(101, 'authorization_code', 101),
	(102, 'password', 101);

INSERT INTO "ClientPostLogoutRedirectUris" ("Id", "PostLogoutRedirectUri", "ClientId") VALUES
	(101, 'https://localhost:44301/signout-callback-oidc', 101);

INSERT INTO "ClientRedirectUris" ("Id", "RedirectUri", "ClientId") VALUES
	(101, 'https://localhost:44301/signin-oidc', 101);

INSERT INTO "ClientScopes" ("Id", "ClientId", "Scope") VALUES
	(101, 101, 'id'),
	(102, 101, 'sub'),
	(103, 101, 'userId'),
	(104, 101, 'name'),
	(105, 101, 'alias'),
	(106, 101, 'image'),
	(107, 101, 'type'),
	(108, 101, 'userName'),
	(109, 101, 'openid'),
	(110, 101, 'profile'),
	(111, 101, 'ft.dms');

INSERT INTO "ClientSecrets" ("Id", "ClientId", "Description", "Value", "Expiration", "Type", "Created") VALUES
	(101, 101, NULL, '7E898mdv0+FhTMg2C1wBiT7FOHNgt7eG2OasTeqwDnw=', NULL, 'SharedSecret', '2024-01-01 00:00:00');

