# dotnet core 基础类库

### 介绍
dotnet core 基础类库, 用于各类 .net core 项目的快速开发和功能增强
开发和维护中的只有基于最新版本 .net 的基础框架类库

### 软件架构
软件架构说明:
#### core 文件夹: 核心功能项目部分
- Devonline.Core: 通用基础功能类库和通用扩展方法类库, 提供了基础常量定义, 枚举定义, 特性定义, 扩展方法定义, 读取配置文件等基础功能
- Devonline.Entity: 实体对象模型基础基类模型定义, 用于定义可映射至数据库的基本抽象基类定义和字符串主键的实现
- Devonline.Http: 提供 asp.net core web 项目中的通用功能和扩张方法, 受限于类库项目无法使用 web 基础类库 AspNetCore 进行扩展, 因此目前此项目功能有限
- Devonline.Utils: 工具类集合, 提供 Excel 读写, 图片处理, 文件操作, 邮件发送, 简易日志等功能

#### dapr 文件夹: dapr 测试项目

#### http 文件夹: web 示例项目

#### identity 文件夹: 身份认证与授权部分, 基于 IdentityFramework 和 Ids4 的功能扩展
- Devonline.Identity: 基础身份认证相关数据对象模型定义, Store 定义, 上下文定义等
- Devonline.Identity.Advance: 高级身份认证相关数据对象模型定义, Store 定义, 上下文定义等
- Devonline.Identity.Security: 安全身份认证相关数据对象模型定义, Store 定义, 上下文定义等
- Devonline.Identity.Admin: 身份认证接口服务器端(包含基于 asp.net core MVC 的 UI), 正在开发中
- Devonline.identity.ui: 纯前端技术开发的身份认证 UI 端, 正在开发中

#### im 文件夹: 分布式消息中心服务器端和客户端
- Devonline.IMServer: 消息中心服务器端 正在开发中
- Devonline.IMClient: 消息中心客户端 正在开发中

#### tests 测试文件夹: 各类项目测试和单元测试文件夹, 后期项目完善后会补充所有单元测试
#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用的组件清单 附版本号, 开源协议, 作者和地址, 排名不分先后, 部分组件并未完全开源, 使用时请遵守出品方的协议
1.  EntityFrameworkCore: 5.0.10; Apache-2.0; Microsoft; https://github.com/dotnet/efcore
2. Newtonsoft.Json: 13.0.1; MIT License; James Newton-King; https://www.newtonsoft.com/json
3.  Mapster: 7.2.0; MIT License; chaowlert,eric_swann; https://github.com/MapsterMapper/Mapster
4.  IdentityServer4: 4.1.2; Apache-2.0 License; Brock Allen,Dominick Baier; https://github.com/IdentityServer/IdentityServer4
5.  EPPlus: 57.5; Polyform Noncommercial as the community license and a commercial license for commercial businesses; EPPlus Software AB; https://epplussoftware.com/
6.  DynamicLinq:  Apache-2.0 License; ZZZ Projects,Stef Heyenrath 等; https://dynamic-linq.net/
7.  Serilog: 2.10.0; Apache-2.0 License; Serilog Contributors; https://serilog.net/
8.  Serilog.Sinks.Grafana.Loki: 7.1.0; MIT License; Mykhailo Shevchuk, Contributors; https://github.com/serilog-contrib/serilog-sinks-grafana-loki
9.  Npgsql: 5.0.10; PostgreSQL License; Shay Rojansky,Yoh Deadfall,Brar Piening等; https://github.com/npgsql/npgsql
10. Pomelo.EntityFrameworkCore.MySql: 5.0.1; MIT License; Laurents Meyer, Caleb Lloyd, Yuko Zheng; https://github.com/PomeloFoundation/Pomelo.EntityFrameworkCore.MySql
11. 其余未列出的包含: AspNetCore 官方扩展库, 安全认证相关库, efcore 相关扩展库, SignalR, Redis, OData, Serilog 等组件及其相关扩展库, 均属开源软件
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


© 2014-2021 devonline.cn 版权所有 ICP 证:  <a href="https://beian.miit.gov.cn/" target="_blank">陕ICP备14006366号-1</a>
