﻿rm -rf ./bin
mkdir bin
unzip -oq dcs_linux_x64_v6.0.*.zip -d bin/
rm -f bin/appsettings*.json
cp appsettings*.json bin/
sudo docker stop dcs
sudo docker rm dcs
sudo docker image remove dcs:6.0
sudo docker build -t dcs:6.0 .
sudo docker run -d --name dcs -p 9527:9527 -e TZ=Asia/Shanghai -e ASPNETCORE_ENVIRONMENT=Docker -e ASPNETCORE_URLS=http://*:9527 --restart=always dcs:6.0
sudo docker ps -l