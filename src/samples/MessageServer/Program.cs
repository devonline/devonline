using Devonline.AspNetCore;
using Devonline.Communication.Server;
using Devonline.Database.PostgreSQL;
using Devonline.Logging;

var builder = WebApplication.CreateBuilder(args);
builder.Host.AddLogging();
var services = builder.Services;
var appSetting = builder.AddSetting<HostSetting>();
services.AddLogging();
services.AddHttpContextAccessor();
services.AddResponseCompression();
services.AddCommunicationServer(appSetting);
services.AddDbContext<ApplicationDbContext>(appSetting);

var app = builder.Build();
app.UseRouting();
app.UseResponseCompression();
app.UseCommunicationServer(appSetting);
app.Run();