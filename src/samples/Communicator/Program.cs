﻿using System.Text;
using Devonline.AspNetCore;
using Devonline.Communication.Messages;
using Devonline.Core;
using Devonline.Entity;
using Devonline.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

var builder = WebApplication.CreateBuilder(args);
builder.Host.AddLogging();
builder.AddCommunicator();
builder.AddSetting<HttpSetting>();

var services = builder.Services;
services.AddLogging();
services.AddTransient<IFileService, FileService>();

var app = builder.Build();
await app.StartAsync();

var logger = app.Services.GetService<ILogger<Program>>();
var fileService = app.Services.GetService<IFileService>();
var communicator = app.Services.GetService<IMessageCommunicator>();
if (logger is not null && fileService is not null && communicator is not null)
{
    try
    {
        var commandLine = new CommandLineArgumentParser(args);
        var printReturnValue = commandLine.Has("-p") || commandLine.GetValue<bool>("--printReturnValue");
        var getOnlineUsers = commandLine.Has("-o") || commandLine.GetValue<bool>("--getOnlineUsers");
        var defaultRecever = commandLine.GetValue<string>("-r") ?? commandLine.GetValue<string>("--receiver");
        var queryString = commandLine.GetValue<string>("-q") ?? commandLine.GetValue<string>("--queryString");

        Message? _message = null;
        communicator.Connected += (sender, e) =>
        {
            logger.LogWarning("通讯器已经启动, 当前版本: {version}", "v8.0.10.1011");
            logger.LogInformation("参数 {arg} 发出消息将显示返回结果", "-p | --printReturnValue");
            logger.LogInformation("参数 {arg} 将在启动时输出在线用户", "-o | --getOnlineUsers");
            logger.LogInformation("参数 {arg} 设定消息默认消息接收者", "-r | --receiver");
            logger.LogInformation("参数 {arg} 将在启动时追加查询选项", "-q | --queryString");
            if (printReturnValue)
            {
                logger.LogInformation("已启用发出消息显示返回结果!");
            }

            if (getOnlineUsers)
            {
                logger.LogInformation("已启用启动时显示在线用户!");
            }

            if (defaultRecever is not null)
            {
                logger.LogInformation("已设定默认消息接收者为: {defaultRecever}!", defaultRecever);
            }

            logger.LogInformation("输入 {arg} 退出", "quit");
            logger.LogInformation("输入 {type}#{content}@{receiver} 形式将发送指定类型的消息内容给指定接收者", "消息类型", "消息内容", "接收者");
            logger.LogInformation("输入以上三种形式的两种组合, 剩余一种将采用缺省值; 仅输入一种, 则为 {type} 或 {content}, 未输入 {receiver} 将视为默认接收者", "消息类型", "消息内容", "接收者");
            logger.LogInformation("输入 {type} 将最后一条收到的消息标记为已读", "read");
            logger.LogInformation("输入 {type} 类型, 则 {content} 需要为文件的全路径", "file", "消息内容");
            logger.LogInformation("输入其余任意内容将视为文本内容消息或命令发送给默认接收者");
        };

        Action<Message> onReturnMessage = (message) => logger.LogInformation("收到了来自消息中心服务器的返回内容: {sender} 从: {client} 发来的 {type} 类型的消息, 内容为: {content}", message.Sender, message.From, message.Type, message.Content);

        communicator.Receive += async (sender, message) =>
        {
            _message = message;
            if (message.Type == MessageType.File)
            {
                var fileName = $"/home/app/dcs/file-{KeyGenerator.GetKey()}.zip";
                var data = Encoding.ASCII.GetBytes(message.Content ?? string.Empty);
                logger.LogInformation("收到了来自用户: {sender} 从: {client} 发来的文件, 内容长度: {contentLength}, 文件大小: {fileLength}, 将保存文件到默认目录: {file}", message.Sender, message.From, message.Content?.Length ?? 0, data.Length, fileName);
                await File.WriteAllBytesAsync(fileName, data);
            }
            else
            {
                logger.LogInformation("收到了来自用户: {sender} 从: {client} 发来的 {type} 类型的消息, 内容为: {content}", message.Sender, message.From, message.Type, message.Content);
            }
        };

        if (!string.IsNullOrWhiteSpace(queryString))
        {
            var queryStrings = queryString.ToKeyValuePairs(AppSettings.DEFAULT_URL_OUTER_SPLITER, AppSettings.DEFAULT_URL_INNER_SPLITER);
            communicator.QueryString.AddRange(queryStrings);
        }

        await communicator.StartAsync();

        if (getOnlineUsers)
        {
            var onlineMessage = await communicator.GetOnlineUsersAsync();
            if (onlineMessage is not null)
            {
                logger.LogInformation($"当前在线用户: {onlineMessage.Content}");
            }
        }

        var input = Console.ReadLine();
        while (input != "quit")
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                try
                {
                    var array = input.Split(new char[] { AppSettings.CHAR_SHARP, AppSettings.CHAR_AT }, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
                    var type = MessageType.Text;
                    string? content = null;
                    string? receiver = defaultRecever;

                    //length 目前只支持 1,2,3
                    if (Enum.TryParse(array[0], true, out type))
                    {
                        if (array.Length == 3)
                        {
                            //type#?content@?receiver 形式
                            content = array[1];
                            receiver = array[2];
                        }
                        else if (array.Length == 2)
                        {
                            if (input.Contains(AppSettings.CHAR_AT))
                            {
                                //type@receiver 形式
                                receiver = array[1];
                            }
                            else
                            {
                                //type#content 形式
                                content = array[1];
                            }
                        }
                    }
                    else
                    {
                        //未指定type的情况下, 按 type = Text 处理
                        content = array[0];
                        if (array.Length >= 2 && input.Contains(AppSettings.CHAR_AT))
                        {
                            //content@receiver 形式
                            receiver = array[1];
                        }
                    }

                    switch (type)
                    {
                        //向对方发送文件
                        case MessageType.File:
                            if (File.Exists(content))
                            {
                                try
                                {
                                    fileService.Compress(content, async fileName =>
                                    {
                                        logger.LogInformation($"将发送文件 {fileName} 到 {receiver}, 文件大小: {fileName.Length}!");
                                        var fileResult = await communicator.SendFileAsync(fileName);
                                        if (fileResult is not null)
                                        {
                                            fileResult.Content = fileName;
                                            onReturnMessage(fileResult);
                                        }
                                    });
                                }
                                catch (Exception ex)
                                {
                                    await communicator.WarningAsync("发送文件时出错: " + ex.GetMessage());
                                }
                            }
                            else
                            {
                                logger.LogInformation($"文件 {content} 不存在!");
                            }
                            break;
                        case MessageType.Read:
                            if (_message != null)
                            {
                                //消息已读形式
                                await communicator.ReadAsync(_message.Id);
                                _message = null;
                            }
                            break;
                        case MessageType.Text:
                            //文本模式
                            if (!string.IsNullOrWhiteSpace(content))
                            {
                                var sendResult = await communicator.SendAsync(content!, receiver);
                                if (sendResult is not null)
                                {
                                    onReturnMessage(sendResult);
                                }
                            }
                            else
                            {
                                logger.LogWarning("文本模式请输入发送内容!");
                            }

                            break;
                        default:
                            //其余情况当作命令模式
                            var executeResult = await communicator.ExecuteAsync(type, receiver, content);
                            if (executeResult is not null)
                            {
                                onReturnMessage(executeResult);
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    logger.LogWarning("通讯器输入处理异常: " + ex.GetMessage());
                }
            }

            input = Console.ReadLine();
        }

        await communicator.StopAsync();
    }
    catch (Exception ex)
    {
        logger.LogError(ex, "客户端链接出错, {message}", ex.GetInnerException());
    }

    await app.StopAsync();
    logger.LogInformation("客户端退出, 消息中心连接结束!");
}