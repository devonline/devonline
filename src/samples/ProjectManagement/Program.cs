global using Devonline.AspNetCore;
global using Devonline.Core;
global using Devonline.Entity;
using Devonline.AspNetCore.OData;
using Devonline.Communication.Messages;
using Devonline.Database.MySQL;
using Devonline.Identity;
using Devonline.Logging;
using OfficeOpenXml;
using ProjectManagement.Models;

ExcelPackage.LicenseContext = LicenseContext.Commercial;
var builder = WebApplication.CreateBuilder(args);
builder.Host.AddLogging();
builder.AddCommunicator();
var services = builder.Services;
var appSetting = builder.AddSetting<HttpSetting>();
var mvcBuilder = services.AddControllersWithViews(appSetting).AddDefaultOData<ApplicationDbContext>();

#if DEBUG
mvcBuilder.AddRazorRuntimeCompilation();
#endif

services.AddDbContext<IdentityDbContext>(appSetting.IdentityDbContext);
services.AddDbContext<ApplicationDbContext>(appSetting);
services.AddDefaultIdentityServices(appSetting);

var app = builder.Build();
app.UseDefaultBuilder(appSetting);
app.Run();