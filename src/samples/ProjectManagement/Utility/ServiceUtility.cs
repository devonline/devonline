﻿using System;
using Devonline.Core;
using Devonline.Entity;
using Microsoft.OData.Edm;
using Microsoft.OData.ModelBuilder;
using ProjectManagement.Models;

namespace ProjectManagement
{
    public static class ServiceUtility
    {
        /// <summary>
        /// 获取 odata 的 edm model
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEdmModel BuildEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<Parameter>(nameof(ApplicationDbContext.Parameters));
            builder.EntitySet<Item>(nameof(ApplicationDbContext.Items));
            builder.EntitySet<Issue>(nameof(ApplicationDbContext.Issues));
            builder.EntityType<Item>().CollectionProperty(x => x.Additionals).IsNavigable();
            builder.EntityType<Issue>().CollectionProperty(x => x.Additionals).IsNavigable();
            builder.EntityType<Issue>().CollectionProperty(x => x.Attachments).IsNavigable();
            //return builder.EnableLowerCamelCase().GetEdmModel();
            return builder.GetEdmModel();
        }
        /// <summary>
        /// CDN 分主, 备 和本地三种, 主 CDN ping 不通则 ping 备份 CDN, 依然不通, 则暂时使用本地, 一分钟切换一次
        /// </summary>
        /// <returns></returns>
        public static void AutoCheckCdn(Action<string> action)
        {
            var timer = new System.Timers.Timer
            {
                Enabled = true,
                Interval = AppSettings.UNIT_SECONDS_A_MINUTE * AppSettings.UNIT_THOUSAND
            };

            //timer.Elapsed += new ElapsedEventHandler(async (o, x) =>
            //{
            //    Console.WriteLine("auto check cdn start!");
            //    var master = AppSetting.Current.CdnEndpoints.FirstOrDefault(x => x.ResourceType == EndpointType.Main);

            //    try
            //    {
            //        var httpClient = new HttpClient();
            //        var result = await httpClient.GetStringAsync(master.Host);
            //        if (result.IsNotNullOrEmpty())
            //        {
            //            Console.WriteLine("master cdn normal!");
            //            action(master.Name);
            //            return;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine($"请求 CDN ${master.Name} 失败, ${ex.Message}");
            //    }

            //    foreach (var cdn in AppSetting.Current.CdnEndpoints)
            //    {
            //        try
            //        {
            //            var httpClient = new HttpClient();
            //            var result = await httpClient.GetStringAsync(cdn.Host);
            //            if (result.IsNotNullOrEmpty())
            //            {
            //                Console.WriteLine($"cdn {cdn.Name} normal!");
            //                action(cdn.Name);
            //                return;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.WriteLine($"请求 cdn ${cdn.Name} 失败, ${ex.Message}");
            //        }
            //    }

            //    Console.WriteLine($"所有 cdn 节点请求失败, 切换到本地源!");
            //    action(EndpointType.Local.ToString());
            //});

            timer.Disposed += new EventHandler((x, y) => Console.WriteLine("auto check cdn timer disposed!"));
            timer.Start();
            Console.WriteLine("auto check cdn timer started!");
        }
    }
}
