﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using ProjectManagement.Models;

namespace ProjectManagement.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
public class IssuesController : ODataController
{
    private readonly IDataService<ApplicationDbContext, Issue> _dataService;
    public IssuesController(IDataService<ApplicationDbContext, Issue> dataService)
    {
        _dataService = dataService;
    }

    [HttpGet, EnableQuery]
    public IActionResult Get() => Ok(_dataService.GetQueryable());

    public async Task<IActionResult> Post([FromBody] Issue entitySet) => Ok(await _dataService.AddAsync(entitySet));

    public async Task<IActionResult> Put([FromBody] Issue entitySet) => Ok(await _dataService.UpdateAsync(entitySet));

    public async Task<IActionResult> Delete(string id)
    {
        await _dataService.DeleteAsync(id);
        return Ok();
    }
}
