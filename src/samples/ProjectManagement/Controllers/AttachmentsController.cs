﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Models;

namespace ProjectManagement.Controllers;

/// <summary>
/// 附件及文件操作类服务通用接口
/// </summary>
[ApiController]
[Route("api/[controller]")]
[Authorize]
public class AttachmentsController(
    ILogger<AttachmentsController> logger,
    IDataService<ApplicationDbContext, Attachment> dataService,
    IAttachmentService<ApplicationDbContext> attachmentService,
    IFileService fileService) :
    AttachmentServiceController<ApplicationDbContext>(logger, dataService, attachmentService, fileService)
{ }