﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using ProjectManagement.Models;

namespace ProjectManagement.Controllers;

/// <summary>
/// 基础数据管理
/// </summary>    
[Route("api/[controller]")]
[ApiController]
[Authorize(Roles = AppSettings.GROUP_MAINTAINERS)]
public class ParametersController : ControllerBase
{
    private readonly IDataService<ApplicationDbContext, Parameter> _dataService;
    public ParametersController(IDataService<ApplicationDbContext, Parameter> dataService)
    {
        _dataService = dataService;
    }

    [HttpGet, EnableQuery]
    public IActionResult Get() => Ok(_dataService.GetQueryable());

    public async Task<IActionResult> Post([FromBody] Parameter entitySet) => Ok(await _dataService.AddAsync(entitySet));

    public async Task<IActionResult> Put([FromBody] Parameter entitySet) => Ok(await _dataService.UpdateAsync(entitySet));

    public async Task<IActionResult> Delete(string id)
    {
        await _dataService.DeleteAsync(id);
        return Ok();
    }
}
