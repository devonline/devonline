﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using ProjectManagement.Models;

namespace ProjectManagement.Controllers;

[Route("api/[controller]")]
[ApiController]
[Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
public class ItemsController : ODataController
{
    private readonly IDataService<ApplicationDbContext, Item> _dataService;
    public ItemsController(IDataService<ApplicationDbContext, Item> dataService)
    {
        _dataService = dataService;
    }

    [HttpGet, EnableQuery]
    public IActionResult Get() => Ok(_dataService.GetQueryable());

    public async Task<IActionResult> Post([FromBody] Item entitySet) => Ok(await _dataService.AddAsync(entitySet));

    public async Task<IActionResult> Put([FromBody] Item entitySet) => Ok(await _dataService.UpdateAsync(entitySet));

    public async Task<IActionResult> Delete(string id)
    {
        await _dataService.DeleteAsync(id);
        return Ok();
    }
}
