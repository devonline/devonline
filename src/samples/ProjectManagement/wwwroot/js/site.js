﻿var Account = new function () {
    'use strict';

    this.onLoginSuccess = function (el, data, xhr) {
        if (data) {
            app.cache.global.set('user', data);
        }

        location.href = "/Home/Index";
    }

    this.onResetPassword = function (el, data, xhr) {
        location.href = "/Home/Login";
    }

    this.onLogout = function () {
        $.api.get({
            action: 'onLink',
            url: '/api/Accounts/Logout',
            complete: function () {
                localStorage.clear();
                location.href = "/Home/Login"
            }
        });
    };
}
