﻿/**
 * 本地数据缓存
 */
var cache = new (class _Cache_ {
    constructor() {
        this.parameters = [];
    }

    set(data) {
        cache.parameters = data;
        cache.tops = cache.parameters.where(x => x.Key === 'Root' || x.Parent && x.Parent.Key === 'Root');
        cache.dataStates = cache.parameters.where(x => x.Parent && x.Parent.Key === 'DataState');
        cache.yesOrNo = cache.parameters.where(x => x.Parent && x.Parent.Key === 'YesOrNo');
        cache.haveOrNot = cache.parameters.where(x => x.Parent && x.Parent.Key === 'HaveOrNot');
        cache.agreeOrNot = cache.parameters.where(x => x.Parent && x.Parent.Key === 'AgreeOrNot');
        cache.itemTypes = cache.parameters.where(x => x.Parent && x.Parent.Key === 'ItemType');
        cache.itemStates = cache.parameters.where(x => x.Parent && x.Parent.Key === 'ItemState');
        cache.itemAdditionalType = cache.parameters.where(x => x.Parent && x.Parent.Key === 'ItemAdditionalType');
        cache.issueTypes = cache.parameters.where(x => x.Parent && x.Parent.Key === 'IssueType');
        cache.issueStates = cache.parameters.where(x => x.Parent && x.Parent.Key === 'IssueState');
        cache.issueAdditionalType = cache.parameters.where(x => x.Parent && x.Parent.Key === 'IssueAdditionalType');
    }

    load() {
        let parameters = localCache.global.get('parameters');
        if ($.com.isArray(parameters)) {
            this.set(parameters);
        } else {
            return axios.get('/odata/Parameters?$select=Id,Index,Key,Value,Text,ParentId&$expand=Parent($select=Id,Key)&$orderby=Index')
                .then(resp => resp.data)
                .then(data => {
                    if ($.com.isArray(data.value)) {
                        localCache.global.set('parameters', data.value)
                        this.set(data.value);
                    }
                });
        }
    }
})();