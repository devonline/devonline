### 组件相关修改
1. 增加了 fetch, Promise, download.js, map.js, app.js 的支持文件, 并修改了 shared 布局页中 ie 相关, 启动顺序相关代码
1. config.js 增加了部分功能
3. common.js 增加了文件操作相关等一些底层通用方法
4. commonExtensions.js 增加了 Array 和 Map 相关的一些扩展方法, 及 jQuery 相关扩展方法, 修改了 Array.select 方法逻辑, 现在返回的对象数组中不会存在 undefined
5. plugin 组件基类增加了两个方法, 一个判断组件值是否为 true, 一个获取组件某个属性的值
6. button 组件针对独立上传 upload 增加了支持; 修改了下载逻辑
7. form 组件修改了部分逻辑
8. input 组件修改了部分逻辑
9. popup 组件最新更新内容: 
	1). 增加了传入对象机制和传出 showbox 机制, 实现原理为: 原来使用 parameter 数组作为传入传出参数的方式保持不变; 
	2). 新增 inout 参数(格式为: "#:{Id:'EmployeeId', Name:'PopupField4'}", 其中键值对分别代表内外字段名, 外部字段名可用 #:传入值类型 )
	3). 新增 openerProperty 参数, 指示外部 form 中存储选择数据的字段, 参数传递机制变为对象方式
	4). 在旧模式下, 传入值任然由外部 form 得到; 对象模式下, 传入参数的值将从 openerProperty 指定的列读出;
	5). 在旧模式下, 传出值任然设置到外部 form ; 对象模式下, 传出值将设置到 openerProperty 指定的列, 并由 input 中列名构成显示到界面的值;
