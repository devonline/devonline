﻿if (typeof jQuery === 'undefined') {
    throw new Error('jquery.plugin JavaScript requires jQuery');
}


var Plugin = function () { };

Plugin.prototype = {
    getData: function () {
        return this;
    },
    getValue: function (property) {
        var value = null;
        if (typeof property === 'string') {
            value = $.com.getValue(this, property);
            if (!$.com.hasValue(value) && !property.includes('.')) {
                property = 'options.' + property;
                value = $.com.getValue(this, property);
            }
        }

        return $.com.hasValue(value) ? value : null;
    },
    setValue: function (property, value, mode) {
        if ($.com.hasValue(property) && typeof property === 'string') {
            $.com.setValue(this, property, value, mode);
        }

        return this.getValue(property);
    },
    getNamedPrefix: function (field) {
        if (!$.com.hasValue(field) && $.com.hasValue(this.options)) {
            if ($.com.hasValue(this.options.field)) {
                field = 'field';
            } else if ($.com.hasValue(this.options.name)) {
                field = 'name';
            } else if ($.com.hasValue(this.options.type)) {
                field = 'type';
            }
        }

        if (this.$el.is('[id]')) {
            this.options.id = this.$el.attr('id');
        } else if ($.com.hasValue(this.options[field])) {
            if ($.com.isJqueryObject(this.$popup) && !$.com.isJqueryObject(this.$form)) {
                //有父级 popupWindow 没有父级 form, 则为 popupWindow 的顶级组件
                this.options.id = this.$popup.attr('id') + '_' + this.options[field];
            } else if ($.com.isJqueryObject(this.$form)) {
                //有父级 form
                var form = this.$form.data(_config_.form.namespace);
                let id = form.options.id + '_' + this.options[field];
                if (this.$form.is(_config_.selector.formArray) && $.com.isJqueryObject(form.$forms)) {
                    let lastId = Number(form.options.lastId || 0);
                    lastId++;
                    form.options.lastId = lastId;
                    id = id + '_' + lastId;
                }

                this.options.id = id;
            }
        } else {
            var namedPrefix = this.$el.parents(_config_.selector.namedPrefix);
            if ($.com.isJqueryObject(namedPrefix)) {
                var prefix = namedPrefix.attr(_config_.attribute.namedPrefix);
                if (!$.com.hasValue(prefix)) {
                    prefix = namedPrefix.attr(_config_.attribute.dataNamedPrefix);;
                }

                if ($.com.hasValue(prefix) && $.com.hasValue(this.options[field])) {
                    this.options.id = prefix + '_' + this.options[field];
                }
            }
        }

        if ($.com.hasValue(this.options.id)) {
            this.options.id = this.options.id.replace(/[\s|\.]/g, ',').replace(/,+/g, '_');
        }
        this.$el.attr('id', this.options.id);
        return this.options.id;
    },
    authorizeButton: function () {
        //处理button授权
        if ($.com.isJqueryObject(this.$buttons)) {
            this.$buttons.button();
            var $unAuthed = this.$buttons.filter('[isAuthenticated="false"]');
            while ($.com.isJqueryObject($unAuthed)) {
                $unAuthed.button('destroy');
                $unAuthed = this.$buttons.filter('[isAuthenticated="false"]');
            }
        }
    },
    execute: function (method) {
        if (!$.com.isNullOrEmpty(this.$el) && $.isFunction(method)) {
            var args = Array.prototype.slice.call(arguments);
            args[0] = this;

            return method.apply(this.$el, args);
        }
    },
    trigger: function (event, name) {
        if (!$.com.isNullOrEmpty(this.$el) && !$.com.isNullOrEmpty(this.events)) {
            var args = Array.prototype.slice.call(arguments);
            //var args = [event, this];
            //args = Array.prototype.slice.call(arguments, 2);
            args[1] = this;
            name = name + '.' + this.namespace;
            if ($.com.hasValue(this.events[name]) && $.isFunction(this.options[this.events[name]])) {
                this.options[this.events[name]].apply(this.$el, args);
            }

            this.$el.trigger($.Event(name), args);
        }
    },
    isTrue: function (attr) {
        var result = false;
        var value = this.getPropertyValue(attr);
        if ($.com.hasValue(value)) {
            value = value.toString().toUpperCase();
            result = value === "TRUE";
        }

        return result;
    },
    getPropertyValue: function (attr) {
        if (!$.com.hasValue(attr)) {
            attr = 'value';
        }

        var value = this.$el.attr(attr);
        if (!$.com.hasValue(value)) {
            value = this.$el.attr('data-' + attr);
        }

        if (!$.com.hasValue(value)) {
            value = this.options[attr];
        }

        return value;
    },
    setup: function (PLUGIN, options) {
        //此方法暂时不使用
        var that = this;
        var args = Array.prototype.slice.call(arguments, 2);
        var value = undefined;
        this.$el.each(function () {
            var $this = $(this);
            var data = $this.data(that.namespace);
            // initial if nover
            if ($.com.isNullOrEmpty(data)) {
                data = new PLUGIN(this, typeof options === 'object' && options);
                $this.data(that.namespace, data);
            }

            if (typeof options === 'string') {
                //if options is not a method
                if ($.inArray(options, that.methods) < 0) {
                    throw 'Unknown method: ' + options;
                }

                //if options is a plugin method, to execute
                value = that.execute(options, args);
            }
        });

        return typeof value === 'undefined' ? this.$el : value;
    }
};