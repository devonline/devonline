﻿
if (typeof jQuery === 'undefined') {
    throw new Error('jquery.showbox JavaScript requires jQuery');
}

if (typeof Plugin === 'undefined') {
    throw new Error('jquery.showbox JavaScript requires Plugin');
}

(function ($) {
    'use strict';

    var ShowBox = function (el, options) {
        this.$el = $(el);
        this.defaults = _config_.showbox.defaults;
        this.methods = _config_.showbox.methods;
        this.events = _config_.showbox.events;
        this.initials = $.com.getDataBindFromElement(this.$el);
        this.originals = $.extend(true, {}, this.defaults, this.initials);
        this.options = $.extend(true, {}, this.originals, options);
        this.options.serial = $.com.getSerial(16);
        this.init();
    };

    ShowBox.prototype = new Plugin();

    ShowBox.prototype.init = function () {
        this.$form = this.$el.parents(_config_.selector.form).first();
        if (!this.$el.isBelongToForm(this.$form)) {
            this.$form = $(_config_.selector.default);
        };

        this.initShowBox();
        this.refresh();
    };

    ShowBox.prototype.initEvent = function () {
        var that = this;
        this.$container.find('a.showbox-remove').off('click.' + this.namespace).on('click.' + this.namespace, function (event) {
            that.onClick(event);
        });
    };

    ShowBox.prototype.initShowBox = function () {
        this.$container = $(this.options.template).children().clone();
        this.$el.before(this.$container);
        this.$container.find('.title').append(this.$el);
        this.$container.find('.control-label').html(this.options.label);
        this.$container.find('input:hidden').attr('data-field', this.options.valueField);
        this.$el.css({'cursor': 'pointer' });
        this.$el.data('button', this.options.button);
        if ($.com.hasValue(this.options.popupParameter)) {
            var that = this;
            var fun = that.options.popupParameter.confirmed;
            this.options.popupParameter.confirmed = function (el, event) {
                if ($.isFunction(fun)) {
                    el.execute(fun, event);
                }

                //TBC ***** 如果 popup 组件正常回写数据到 form 时, 可去掉 refresh 方法参数
                that.refresh(el.options.data);
            };

            var prefix = this.options[_config_.default.parameterPrefix] || _config_.default.parameterPrefixValue;
            var parameter = this.$el.attr(_config_.attribute.popupParameter);
            if ($.com.hasValue(parameter)) {
                parameter = parameter.substring(prefix.length);
                if ($.com.hasValue(parameter)) {
                    var $parameter = $(parameter);
                    if ($.com.isJqueryObject($parameter)) {
                        $parameter.data('confirmed', this.options.popupParameter.confirmed);
                    }
                }
            }
        }
    }

    ShowBox.prototype.onClick = function (event) {
        var args = {};
        var ret = this.execute(this.options.clicking, event, args);
        if ($.com.hasValue(ret) && ret === false) {
            return;
        }

        if ($.com.isJqueryObject(this.$form)) {
            var form = this.$form.data(_config_.form.namespace);
            if ($.com.hasValue(form)) {
                var $tag = $(event.currentTarget).parent(".tag");
                var data = form.options.data[this.options.popupParameter.openerProperty];
                if ($.com.isArray(data)) {
                    var id = $tag.attr('id');
                    form.options.data[this.options.popupParameter.openerProperty] = data.filter(function (x) {
                        return x.Id !== id;
                    });
                }

                this.$tags.removeItem($tag);
                $tag.remove();
            }
        }

        this.execute(this.options.clicked, event, args);
    };

    ShowBox.prototype.refresh = function (data) {
        if (!$.com.isArray(data)) {
            var form = this.$form.data(_config_.form.namespace);
            if ($.com.hasValue(form)) {
                data = form.options.data[this.options.popupParameter.openerProperty];
            }
        }

        if ($.com.isArray(data)) {
            var that = this;

            this.$container.find('.showbox-tags').empty();
            var column = this.options.showField || "Name";
            $.each(data, function (i, value) {
                var $tag = $(that.options.tagTemplate).children().clone();
                if (!$.com.isJqueryObject($("#" + value.Id))) {
                    $tag.attr("id", value.Id);
                    $tag.find('.showbox-content').text(value[column]);
                    that.$container.find('.showbox-tags').append($tag);
                }
            });

            this.$tags = this.$container.find('.showbox-tags .tag');
            this.initEvent();
        }
    }

    // plug-in
    $.fn.showbox = function (options) {
        var args = Array.prototype.slice.call(arguments, 1);
        var value = undefined;
        this.each(function () {
            var $this = $(this);
            var data = $this.data(_config_.showbox.namespace);
            // initial if nover
            if ($.com.isNullOrEmpty(data)) {
                data = new ShowBox(this, typeof options === 'object' && options);
                $this.data(_config_.showbox.namespace, data);
            }

            if (typeof options === 'string') {
                //if options is not a method
                if ($.inArray(options, data.methods) < 0) {
                    throw 'Unknown method: ' + options;
                }

                //if options is a plugin method, to execute
                value = data[options].apply(data, args);
            }
        });

        return typeof value === 'undefined' ? this : value;
    };
})(jQuery);