﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Devonline.Identity;

namespace ProjectManagement.Models
{
    /// <summary>
    /// 工作事项
    /// </summary>
    [Table("item"), DisplayName("工作事项")]
    public class Item : EntitySetWithCreateAndUpdate
    {
        /// <summary>
        /// 工作事项名称
        /// </summary>
        [Column("name"), DisplayName("工作事项名称"), Required, MaxLength(128), Excel]
        public string Name { get; set; }
        /// <summary>
        /// 工作事项编号
        /// </summary>
        [Column("code"), DisplayName("工作事项编号"), Required, MaxLength(16), Excel]
        public string Code { get; set; }
        /// <summary>
        /// 工作事项类型
        /// </summary>
        [Column("item_type"), DisplayName("工作事项类型"), Required, MaxLength(16), Excel]
        public string ItemType { get; set; }
        /// <summary>
        /// 工作事项状态
        /// </summary>
        [Column("item_state"), DisplayName("工作事项状态"), Required, MaxLength(16), Excel]
        public string ItemState { get; set; }
        /// <summary>
        /// 分配给
        /// </summary>
        [Column("assign_id"), DisplayName("分配编号"), MaxLength(36), Excel]
        public string AssignId { get; set; }

        /// <summary>
        /// 工作事项分配到的人
        /// </summary>
        public virtual User Assign { get; set; }
        /// <summary>
        /// 附加信息
        /// </summary>
        [Column("additionals"), DisplayName("附加信息"), MaxLength(1024), Excel]
        public virtual ICollection<Additional> Additionals { get; set; }
    }
}