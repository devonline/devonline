﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectManagement.Models
{
    /// <summary>
    /// 问题
    /// </summary>
    [Table("issue"), DisplayName("问题")]
    public class Issue : EntitySetWithCreateAndUpdate
    {
        /// <summary>
        /// 项目事项编号
        /// </summary>
        [Column("item_id"), DisplayName("项目事项编号"), Required, MaxLength(36), Excel]
        public string ItemId { get; set; }
        /// <summary>
        /// 问题名称
        /// </summary>
        [Column("name"), DisplayName("问题名称"), Required, MaxLength(128), Excel]
        public string Name { get; set; }
        /// <summary>
        /// 问题编号
        /// </summary>
        [Column("code"), DisplayName("问题编号"), Required, MaxLength(16), Excel]
        public string Code { get; set; }
        /// <summary>
        /// 问题类型
        /// </summary>
        [Column("issue_type"), DisplayName("问题类型"), Required, MaxLength(16), Excel]
        public string IssueType { get; set; }
        /// <summary>
        /// 问题状态
        /// </summary>
        [Column("issue_state"), DisplayName("问题状态"), Required, MaxLength(16), Excel]
        public string IssueState { get; set; }

        /// <summary>
        /// 项目事项
        /// </summary>
        public virtual Item Item { get; set; }

        /// <summary>
        /// 附加信息
        /// </summary>
        [Column("additionals"), DisplayName("附加信息"), MaxLength(1024), Excel]
        public virtual ICollection<Additional> Additionals { get; set; }
        /// <summary>
        /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
        /// </summary>
        [NotMapped]
        public virtual ICollection<Attachment> Attachments { get; set; }
    }
}