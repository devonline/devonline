﻿using System.ComponentModel.DataAnnotations;
using Devonline.AspNetCore.Security;
using Devonline.Identity;
using Microsoft.EntityFrameworkCore;

namespace ProjectManagement.Models;

[Display(Name = "数据库上下文")]
public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : BaseDbContext(options)
{
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        //modelBuilder.Entity<Credential>().OwnsMany(x => x.Additionals, x => x.ToJson("additionals"));
        //modelBuilder.Entity<Personal>().OwnsMany(x => x.Additionals, x => x.ToJson("additionals"));
        modelBuilder.Entity<Item>().OwnsMany(x => x.Additionals, x => x.ToJson("additionals"));
        modelBuilder.Entity<Issue>().OwnsMany(x => x.Additionals, x => x.ToJson("additionals"));
    }

    #region business Models
    public virtual DbSet<User> Users { get; set; }
    public virtual DbSet<Item> Items { get; set; }
    public virtual DbSet<Issue> Issues { get; set; }
    #endregion
}