# 发布步骤, 以 odas 为例
1. 创建目录
mkdir odas/bin -p
2. 创建 startup.sh 写入启动脚本, 或复制启动文件, 给执行权限
vim odas/startup.sh, 
chmod +x odas/startup.sh
3. 创建服务文件 /etc/systemd/system/odas.service 并给权限 755
4. 启动服务并设置自启动 systemctl start odas && systemctl enable odas
### 注意 服务器的退出命令必须是 kill -s SIGTERM 否则将不能由服务自主执行安全退出

## 服务文件内容

[Unit]
Description=transfer 文件处理服务
After=network.target

[Service]
Type=simple
PIDFile=/home/app/transfer/transfer.pid
ExecStart=/home/app/transfer/startup.sh
ExecStop=/bin/kill -s SIGTERM $MAINPID
PrivateTmp=true
User=app
Group=app

[Install]
WantedBy=default.target