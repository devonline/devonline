rm -rf bin/* logs/*
unzip -oq *.zip -d bin/
rm -rf bin/app*.json
cp app*.json bin/
chmod +x bin/ProjectManagement
# 服务器上要执行一次下面的命令给app用户应用授权, 已解决 1024 以下端口使用 https 协议的问题
# sudo setcap cap_net_bind_service=+eip /home/app/pm/bin/ProjectManagement
# sudo cp pm.service /etc/systemd/system/
sudo systemctl stop pm
sudo systemctl start pm