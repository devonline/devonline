﻿using Devonline.Core;
using Microsoft.AspNetCore.Mvc;

namespace EspDevices.Controllers;

[Route("api/[controller]")]
[ApiController]
public class DevicesController(ILogger<DevicesController> logger) : ControllerBase
{
    private readonly ILogger<DevicesController> _logger = logger;
    private static readonly Dictionary<int, bool> _lights = [];

    /// <summary>
    /// 获取设备状态
    /// </summary>
    /// <returns></returns>
    [HttpGet("Lights")]
    public IActionResult GetLights()
    {
        _logger.LogInformation($"获取当前设备状态: {_lights.ToJsonString()}");
        return Ok(_lights);
    }

    /// <summary>
    /// 切换设备状态
    /// </summary>
    /// <param name="light">设备编号</param>
    /// <param name="on">开关状态</param>
    /// <returns></returns>
    [HttpGet("Switch/{light}/{on}")]
    public IActionResult Switch(int light, bool on)
    {
        _logger.LogInformation($"切换 {light} 号设备状态为: {on}");
        if (!_lights.TryAdd(light, on))
        {
            _lights[light] = on;
        }

        return Ok();
    }
}