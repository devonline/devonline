using Devonline.AspNetCore;
using Devonline.Logging;

var builder = WebApplication.CreateBuilder(args);
builder.Host.AddLogging();
var services = builder.Services;
var appSetting = builder.AddSetting<HttpSetting>();
services.AddControllers(appSetting);
services.AddLogging();
services.AddHttpContextAccessor();
services.AddResponseCompression();

if (appSetting.HttpLogging is not null)
{
    services.AddHttpLogging(options => options = appSetting.HttpLogging);
}

var app = builder.Build();
app.UseException();
if (appSetting.HttpLogging is not null)
{
    app.UseHttpLogging();
}

app.UseRouting();
app.UseResponseCompression();
app.MapControllers();
app.MapDefaultControllerRoute();
app.Services.GetRequiredService<ILogger<Program>>().LogWarning($"ESP 设备服务平台启动于: {appSetting.Urls}");

app.Run();