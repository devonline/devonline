﻿var path1 = args[0];
var path2 = args[1];

Console.WriteLine($"输入目录1: {path1}");
Console.WriteLine($"输入目录2: {path2}");
var files = Directory.GetFiles(path1).OrderBy(x => x).ToArray();
var length = files.Length.ToString().Length;
for (int index = 0; index < files.Length; index++)
{
    var file = files[index];
    File.Move(file, Path.Combine(path2, (index + 1).ToString().PadLeft(length, '0') + Path.GetExtension(file)));
}