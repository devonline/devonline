﻿using Devonline.Core;

var arguments = CommandLineArgumentParser.Parse(args);
var inputPath = arguments.Get("-i")?.Take() ?? AppSettings.StartupPath;
var outPath = arguments.Get("-o")?.Take() ?? inputPath;
var format = arguments.Get("-f")?.Take() ?? "D/R/F";
var subDir = arguments.Get("-s") != null;

var consoleColor = Console.ForegroundColor;
Console.ForegroundColor = ConsoleColor.Yellow;
Console.WriteLine($"输入目录: {inputPath}");
Console.WriteLine($"输出目录: {outPath}");
Console.WriteLine($"格式: {format}");
Console.WriteLine($"子目录查找: {subDir}");

var exts = "*.png,*.jpg,*.jpeg,*.gif,*.bmp,*.webp";
var files = new List<string>();
foreach (var ext in exts.Split(AppSettings.CHAR_COMMA))
{
    files.AddRange(Directory.GetFiles(inputPath, ext, subDir ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
}

if (!files.Any())
{
    Console.WriteLine("No files found");
    return;
}

Console.WriteLine($"文件: {files.Count} 个");
Console.ForegroundColor = consoleColor;
var fileQueue = files.ToQueue();
do
{
    var file = fileQueue.Dequeue();
    if (file != null)
    {
        try
        {
            var fileName = await Devonline.Utils.ImageUtility.RenameAsync(file, outPath, format);
            Console.WriteLine($"File {file} renamed to {fileName}");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.GetMessage());
        }
    }
}
while (fileQueue.Count > 0);

files.Clear();
Console.ForegroundColor = ConsoleColor.Red;
Console.WriteLine("File rename completed");
Console.ForegroundColor = consoleColor;