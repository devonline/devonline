using CombinationCommunicator;
using Devonline.AspNetCore;
using Devonline.Communication.Messages;
using Devonline.Communication.SerialPorts;
using Devonline.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
builder.Host.AddLogging();

//注入第一个通讯器
builder.AddCommunicator<Communicator>();

//注入第二个通讯器
builder.AddSetting<SerialPortOptions>(nameof(System.IO.Ports.SerialPort));
builder.Services.AddSingleton<Devonline.Communication.Abstractions.ICommunicator, SerialPortCommunicator>();

// Add services to the container.
var services = builder.Services;
services.AddLogging();
var app = builder.Build();
await app.Services.GetRequiredService<IMessageCommunicator>().StartAsync();
app.Run();