﻿using Devonline.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Security;

var builder = WebApplication.CreateBuilder(args);
builder.Host.AddLogging();
var services = builder.Services;
services.AddLogging();

var app = builder.Build();
await app.StartAsync();

Console.WriteLine($"the benchmark of {nameof(DataSecurity)} start now!");
#if DEBUG
var dataSecurity = new DataSecurity("/home/app/benchmark/storage/");
dataSecurity.RunSM2();
await dataSecurity.RunAsync();
await dataSecurity.RunStringAsync();
#else
BenchmarkDotNet.Running.BenchmarkRunner.Run<DataSecurity>(args: ["/home/app/benchmark/storage/"]);
#endif

Console.WriteLine($"the benchmark of {nameof(DataSecurity)} complete!");