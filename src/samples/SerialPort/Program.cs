using Devonline.AspNetCore;
using Devonline.Communication.Abstractions;
using Devonline.Communication.SerialPorts;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
services.AddLogging();
var appSetting = builder.AddSetting<SerialPortOptions>(nameof(System.IO.Ports.SerialPort));
if (appSetting.UseStream)
{
    services.AddSingleton<IStreamCommunicator, SerialPortStreamCommunicator>();
}
else
{
    services.AddSingleton<ICommunicator, SerialPortCommunicator>();
}

var app = builder.Build();
if (appSetting.UseStream)
{
    await app.Services.GetRequiredService<IStreamCommunicator>().StartAsync();
}
else
{
    await app.Services.GetRequiredService<ICommunicator>().StartAsync();
}

app.Run();