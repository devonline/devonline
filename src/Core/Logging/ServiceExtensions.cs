﻿using Microsoft.Extensions.Hosting;
using Serilog;

namespace Devonline.Logging;

public static class ServiceExtensions
{
    /// <summary>
    /// 注入 Serilog
    /// </summary>
    /// <param name="builder"></param>
    public static void AddLogging(this IHostBuilder builder)
    {
        builder.UseSerilog((context, logger) => logger.ReadFrom.Configuration(context.Configuration));
    }
}