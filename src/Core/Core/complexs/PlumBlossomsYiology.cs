﻿namespace Devonline.Core;

/// <summary>
/// 梅花易数
/// </summary>
public class PlumBlossomsYiology
{
    public PlumBlossomsYiology(int number)
    {
        Value = number.ToString().PadLeft(4, '0');
        Thousands = Convert.ToByte(Value[0..1]);
        Hundreds = Convert.ToByte(Value[1..2]);
        Tens = Convert.ToByte(Value[2..3]);
        Units = Convert.ToByte(Value[3..4]);
        var up = (Thousands + Hundreds) % 8;
        var down = (Tens + Units) % 8;
        Up = up.GetHexTrigram();
        Down = down.GetHexTrigram();
    }

    /// <summary>
    /// 千位
    /// </summary>
    public byte Thousands { get; }
    /// <summary>
    /// 百位
    /// </summary>
    public byte Hundreds { get; }
    /// <summary>
    /// 十位
    /// </summary>
    public byte Tens { get; }
    /// <summary>
    /// 个位
    /// </summary>
    public byte Units { get; }
    /// <summary>
    /// 数字值
    /// </summary>
    public string Value { get; }
    /// <summary>
    /// 上卦
    /// </summary>
    public Trigram Up { get; }
    /// <summary>
    /// 下卦
    /// </summary>
    public Trigram Down { get; }
    /// <summary>
    /// 卦象
    /// </summary>
    public Hexagram Hexagram => (Up, Down).GetHexagram();
}