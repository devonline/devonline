﻿namespace Devonline.Core;

/// <summary>
/// 地理位置经纬度
/// </summary>
/// <param name="Longitude">经度</param>
/// <param name="Latitude">纬度</param>
public record GeographicalLocation(double Longitude, double Latitude);

/// <summary>
/// 地理位置经纬度标准文本形式
/// </summary>
/// <param name="Longitude">经度</param>
/// <param name="Latitude">纬度</param>
public record StandardGeographicalLocation(string Longitude, string Latitude);