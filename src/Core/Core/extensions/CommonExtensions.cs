﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq.Expressions;
using System.Numerics;
using System.Reflection;
using System.Text.RegularExpressions;
using Mapster;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Devonline.Core;

public static class CommonExtensions
{
    #region 类型转换, 设置/获取值
    /// <summary>
    /// 值类型直转
    /// </summary>
    /// <param name="value">原始值</param>
    /// <param name="type">转换类型</param>
    /// <param name="format">格式化格式</param>
    /// <returns></returns>
    public static object? To([DisallowNull] this IConvertible value, Type type, string? format = default)
    {
        if (value.GetType().GetCoreType().IsFromType(type))
        {
            return value;
        }

        try
        {
            object? result = null;
            if (value is string strValue)
            {
                if (type.IsEnum)
                {
                    //转换枚举类型
                    result = Enum.Parse(type, strValue);
                }
                else if (Type.GetTypeCode(type) == TypeCode.DateTime)
                {
                    //转换时间类型
                    if (string.IsNullOrWhiteSpace(format))
                    {
                        //无 format 的情况下
                        try
                        {
                            //先按默认情况自然转换
                            value = Convert.ToDateTime(strValue);
                        }
                        catch
                        {
                            //在按时间戳转换
                            var timestamp = Convert.ToInt64(strValue);
                            if (timestamp <= int.MaxValue)
                            {
                                //以秒为单位的 32 位时间戳
                                value = DateTime.UnixEpoch.AddSeconds(timestamp);
                            }
                            else
                            {
                                //以毫秒为单位的64位时间戳
                                value = DateTime.UnixEpoch.AddMilliseconds(timestamp);
                            }
                        }
                    }
                    else
                    {
                        //有 format 的情况下, 按 format 转换
                        value = DateTime.ParseExact(strValue, format, CultureInfo.InvariantCulture);
                    }

                    result = value;
                }
                else
                {
                    //其余类型直接转换
                    result = Convert.ChangeType(strValue, type, CultureInfo.CurrentCulture);
                }
            }
            else
            {
                //值非字符串类型直接转换
                result = Convert.ChangeType(value, type, CultureInfo.CurrentCulture);
            }

            return result;
        }
        catch
        {
            return default;
        }
    }
    /// <summary>
    /// 值类型直转
    /// </summary>
    /// <typeparam name="T">转换类型</typeparam>
    /// <param name="value">原始值</param>
    /// <param name="format">格式化格式</param>
    /// <returns></returns>
    public static T? To<T>([DisallowNull] this IConvertible value, string? format = default)
    {
        if (value is T t)
        {
            return t;
        }

        var result = value.To(typeof(T), format);
        return result is null ? default : (T)result;
    }
    /// <summary>
    /// 获取属性的值, 此处只处理 公共成员属性
    /// </summary>
    /// <param name="obj">The object that have the property</param>
    /// <param name="propertyName">property name, support the format of "A.B.C"</param>
    /// <param name="ignoreCase">是否忽略大小写, 默认: 是</param>
    /// <returns></returns>
    public static object? GetPropertyValue([DisallowNull] this object obj, string propertyName, bool ignoreCase = true)
    {
        var bindingFlags = BindingFlags.Public | BindingFlags.Instance;
        if (ignoreCase)
        {
            bindingFlags |= BindingFlags.IgnoreCase;
        }

        if (obj is IEnumerable enumerable)
        {
            //TODO TBC 模式匹配改造
            //(obj as ICollection).Cast<object>().ToList().ForEach(x => { x = x.GetPropertyValue(current); });
            object[] array = enumerable.Cast<object>().ToArray();
            var result = new object[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                var current = array[i].GetPropertyValue(propertyName, ignoreCase);
                if (current is not null)
                {
                    result[i] = current;
                }
            }

            return result;
        }
        else
        {
            int position = propertyName.IndexOf(AppSettings.CHAR_DOT, StringComparison.CurrentCulture);
            string current = position > 0 ? propertyName[..position] : propertyName;
            var propertyInfo = obj.GetType().GetProperty(current, bindingFlags);
            if (propertyInfo is not null && propertyInfo.CanRead)
            {
                var inner = propertyInfo.GetValue(obj);
                if (inner is not null && position > 0)
                {
                    string next = propertyName[(position + 1)..];
                    inner = inner.GetPropertyValue(next, ignoreCase);
                }

                return inner;
            }
        }

        return default;
    }
    /// <summary>
    /// 获取对象中属性的值, 此处只处理 公共成员属性
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="t"></param>
    /// <param name="propertyName"></param>
    /// <param name="ignoreCase">是否忽略大小写, 默认: 是</param>
    /// <returns></returns>
    public static TValue? GetPropertyValue<T, TValue>([DisallowNull] this T t, string propertyName, bool ignoreCase = true)
    {
        var bindingFlags = BindingFlags.Public | BindingFlags.Instance;
        if (ignoreCase)
        {
            bindingFlags |= BindingFlags.IgnoreCase;
        }

        //typeof(T) : 获取T的当前类型, 可能是个基类, 接口
        //t.GetType() : 获取实例 t 的当前类型, 是真实的对象类型
        var value = t.GetType().GetProperty(propertyName, bindingFlags)?.GetValue(t);
        return value is null ? default : (TValue)value;
    }
    /// <summary>
    /// 为对象的属性 property 设置 value 值, 此处只处理 公共成员属性
    /// </summary>
    /// <param name="t">要设置属性的对象</param>
    /// <param name="propertyName">属性名称</param>
    /// <param name="value">要设置的值</param>
    /// <param name="ignoreCase">是否忽略大小写, 默认: 是</param>
    /// <returns>是否设置成功</returns>
    public static void SetPropertyValue<T>([DisallowNull] this T t, string propertyName, object? value = default, bool ignoreCase = true, string? format = default)
    {
        var bindingFlags = BindingFlags.Public | BindingFlags.Instance;
        if (ignoreCase)
        {
            bindingFlags |= BindingFlags.IgnoreCase;
        }

        //typeof(T) : 获取T的当前类型, 可能是个基类, 接口
        //t.GetType() : 获取实例 t 的当前类型, 是真实的对象类型
        var propertyInfo = t.GetType().GetProperty(propertyName, bindingFlags);
        if (propertyInfo is not null && propertyInfo.CanWrite)
        {
            //value 有值, 可转换且类型和 PropertyInfo.PropertyType 不相同, 则先转换类型在赋值
            var propertyType = propertyInfo.PropertyType.GetCoreType();
            if (value is not null && value is IConvertible convertibleValue)
            {
                value = convertibleValue.To(propertyType, format);
            }

            propertyInfo.SetValue(t, value);
        }
    }

    /// <summary>
    /// 从缓存中读取值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="cache"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static T? GetValue<T>([DisallowNull] this IDistributedCache cache, string key)
    {
        var value = cache.GetString(key);
        return value == null ? default : value.ToJsonObject<T>();
    }
    /// <summary>
    /// 从缓存中读取值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="cache"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static async Task<T?> GetValueAsync<T>([DisallowNull] this IDistributedCache cache, string key)
    {
        var value = await cache.GetStringAsync(key);
        return value == null ? default : value.ToJsonObject<T>();
    }
    /// <summary>
    /// 数据写入缓存中
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="cache"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="options"></param>
    public static void SetValue<T>([DisallowNull] this IDistributedCache cache, string key, [DisallowNull] T value, DistributedCacheEntryOptions? options = default)
    {
        cache.SetString(key, value.ToJsonString(), options ?? new DistributedCacheEntryOptions());
    }
    /// <summary>
    /// 数据写入缓存中
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="cache"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="options"></param>
    public static async Task SetValueAsync<T>([DisallowNull] this IDistributedCache cache, string key, [DisallowNull] T value, DistributedCacheEntryOptions? options = default)
    {
        await cache.SetStringAsync(key, value.ToJsonString(), options ?? new DistributedCacheEntryOptions());
    }

    /// <summary>
    /// 从配置文件配置项读取并初始化制定类型
    /// </summary>
    /// <typeparam name="TSetting">配置项类型</typeparam>
    /// <returns></returns>
    public static TSetting GetSetting<TSetting>([DisallowNull] this IConfiguration configuration) where TSetting : class, new()
    {
        var setting = configuration.GetSetting<TSetting>(nameof(AppSetting)) ?? new TSetting();
        if (setting is AppSetting appSetting)
        {
            appSetting.Hosts ??= configuration.GetValue<string>(nameof(appSetting.Hosts));
            appSetting.Urls ??= configuration.GetValue<string>(nameof(appSetting.Urls));
            appSetting.AllowedHosts ??= configuration.GetValue<string>(nameof(appSetting.AllowedHosts));
            appSetting.CorsOrigins ??= configuration.GetValue<string>(nameof(appSetting.CorsOrigins));
            appSetting.ApplicationDbContext ??= configuration.GetConnectionString(nameof(appSetting.ApplicationDbContext));
        }

        return setting;
    }
    /// <summary>
    /// 将配置文件中 section 节点的值转换为指定类型
    /// 如果配置项中包含: 字符, 则直接返回配置项的确定值而不是整个配置节点的转换值
    /// </summary>
    /// <typeparam name="TSetting">配置项类型</typeparam>
    /// <param name="configuration">配置文件</param>
    /// <param name="key">配置项</param>
    /// <returns></returns>
    public static TSetting? GetSetting<TSetting>([DisallowNull] this IConfiguration configuration, string key) => key.Contains(AppSettings.CHAR_COLON) ? configuration.GetValue<TSetting>(key) : configuration.GetSection(key).Get<TSetting>();

    /// <summary>
    /// 对象转 json 字符串, 泛型用法
    /// </summary>
    /// <typeparam name="T">需要序列化的对象类型</typeparam>
    /// <param name="t">需要序列化的对象</param>
    /// <param name="settings">序列化设置</param>
    /// <returns></returns>
    public static string ToJsonString<T>([DisallowNull] this T t, JsonSerializerSettings? settings = default) => JsonConvert.SerializeObject(t, settings ?? AppSettings.JsonSerializerSettings);
    #endregion

    #region 复制对象
    /// <summary>
    /// 可以处理复杂映射
    /// 用法 source.Mapper(s => new Target { PropertyName = s.PropertyName });
    /// </summary>
    /// <typeparam name="TSource">输入类</typeparam>
    /// <typeparam name="TTarget">输出类</typeparam>
    /// <param name="expression">表达式目录树,可以为null</param>
    /// <param name="source">输入实例</param>
    /// <returns></returns>
    public static TTarget Mapper<TSource, TTarget>([DisallowNull] this TSource source, Expression<Func<TSource, TTarget>>? expression = default)
    {
        var sourceType = typeof(TSource);
        var targetType = typeof(TTarget);
        var memberBindingList = new List<MemberBinding>();
        var parameterExpression = Expression.Parameter(sourceType, AppSettings.DEFAULT_LAMBDA_PARAMETER);
        if (expression is not null)
        {
            parameterExpression = expression.Parameters[AppSettings.UNIT_ZERO];
            if (expression.Body != null && expression.Body is MemberInitExpression member)
            {
                memberBindingList.AddRange(member.Bindings);
            }
        }

        foreach (var item in targetType.GetProperties())
        {
            var propertyInfo = sourceType.GetProperty(item.Name);
            if (propertyInfo is not null)
            {
                var property = Expression.Property(parameterExpression, propertyInfo);
                memberBindingList.Add(Expression.Bind(item, property));
            }
        }

        foreach (var item in targetType.GetFields())
        {
            var fieldInfo = sourceType.GetField(item.Name);
            if (fieldInfo is not null)
            {
                var property = Expression.Field(parameterExpression, fieldInfo);
                memberBindingList.Add(Expression.Bind(item, property));
            }
        }

        var memberInitExpression = Expression.MemberInit(Expression.New(targetType), memberBindingList.ToArray());
        var lambda = Expression.Lambda<Func<TSource, TTarget>>(memberInitExpression, new ParameterExpression[] { parameterExpression });
        return lambda.Compile().Invoke(source);
    }

    /// <summary>
    /// 复制对象
    /// </summary>
    /// <typeparam name="T">目标类型</typeparam>
    /// <param name="t">原始类型对象</param>
    /// <returns>复制得到的目标类型对象</returns>
    public static T Copy<T>([DisallowNull] this T t, TypeAdapterConfig? config = default) => t.Adapt<T>(config ?? TypeAdapterConfig.GlobalSettings);
    /// <summary>
    /// 将原始对象复制到新的目标类型对象
    /// </summary>
    /// <param name="value">原始对象</param>
    /// <typeparam name="T">目标类型</typeparam>
    /// <returns></returns>
    public static T CopyTo<T>([DisallowNull] this object value, TypeAdapterConfig? config = default) => value.Adapt<T>(config ?? TypeAdapterConfig.GlobalSettings);
    /// <summary>
    /// 将原始对象复制到新的目标类型对象
    /// </summary>
    /// <param name="value">原始对象</param>
    /// <typeparam name="T">目标类型</typeparam>
    /// <returns></returns>
    public static TTarget CopyTo<TSource, TTarget>([DisallowNull] this TSource source, TypeAdapterConfig? config = default) => source.Adapt<TSource, TTarget>(config ?? TypeAdapterConfig.GlobalSettings);
    /// <summary>
    /// 将原始对象复制到新的目标类型对象
    /// </summary>
    /// <param name="source">原始对象</param>
    /// <param name="target">目标对象</param>
    /// <typeparam name="TSource">原始类型</typeparam>
    /// <typeparam name="TTarget">目标类型</typeparam>
    /// <returns></returns>
    public static TTarget CopyTo<TSource, TTarget>([DisallowNull] this TSource source, TTarget target, TypeAdapterConfig? config = default) => source.Adapt(target, config ?? TypeAdapterConfig.GlobalSettings);
    /// <summary>
    /// 将原始对象复制到新的目标类型对象
    /// </summary>
    /// <param name="source">原始对象</param>
    /// <param name="targetType">目标对象类型</param>
    /// <returns></returns>
    public static object? CopyTo([DisallowNull] this object value, Type type, TypeAdapterConfig? config = default) => value.Adapt(value.GetType().GetCoreType(), type, config ?? TypeAdapterConfig.GlobalSettings);
    /// <summary>
    /// 将原始对象复制到新的目标类型对象
    /// </summary>
    /// <param name="source">原始对象</param>
    /// <param name="targetType">目标对象类型</param>
    /// <returns></returns>
    public static object? CopyTo([DisallowNull] this object source, object target, TypeAdapterConfig? config = default) => source.Adapt(source.GetType().GetCoreType(), target.GetType().GetCoreType(), config ?? TypeAdapterConfig.GlobalSettings);
    /// <summary>
    /// 将目标对象列表中的属性值合并到源对象中
    /// </summary>
    /// <param name="source">原始对象</param>
    /// <param name="targets">待合并对象列表</param>
    /// <returns></returns>
    public static object? Merge([DisallowNull] this object source, params object[] targets)
    {
        if (targets.Length != 0)
        {
            var sourceType = source.GetType().GetCoreType();
            foreach (var target in targets)
            {
                source.Adapt(target, sourceType, target.GetType().GetCoreType());
            }
        }

        return source;
    }
    #endregion

    #region 特性相关操作
    /// <summary>
    /// 获取对象类型特性的指定属性值
    /// </summary>
    /// <param name="member">待获取对象</param>
    /// <param name="propertyName">用于获取值的特性上的属性名称</param>
    /// <returns>显示名称</returns>
    public static TValue? GetAttributeValue<TAttribute, TValue>([DisallowNull] this object obj, string propertyName) where TAttribute : Attribute => obj.GetType().GetAttributeValue<TAttribute, TValue>(propertyName);
    /// <summary>
    /// 获取对象类型成员特性的指定属性值
    /// </summary>
    /// <param name="member">待获取对象</param>
    /// <param name="memberName">成员名称, 可以是属性或者字段</param>
    /// <param name="propertyName">用于获取值的特性上的属性名称</param>
    /// <returns>显示名称</returns>
    public static TValue? GetAttributeValue<TAttribute, TValue>([DisallowNull] this object obj, string memberName, string propertyName) where TAttribute : Attribute
    {
        var member = obj.GetType().GetMember(memberName)?.FirstOrDefault();
        return member is not null ? member.GetAttributeValue<TAttribute, TValue>(propertyName) : default;
    }

    /// <summary>
    /// 获取对象的类型上可显示名字
    /// 可用于获取枚举值的可显示名字
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string GetDisplayName([DisallowNull] this object obj) => obj.GetType().GetDisplayName();
    /// <summary>
    /// 获取对象的属性上可显示名字
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="memberName">成员名称, 可以是属性或者字段</param>
    /// <returns></returns>
    public static string? GetDisplayName([DisallowNull] this object obj, string memberName) => obj.GetType().GetDisplayName(memberName);
    /// <summary>
    /// 获取对象上的隐射到数据库的字段名字(Column 特性的 Name 值)
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="memberName">成员名称, 可以是属性或者字段</param>
    /// <returns></returns>
    public static string? GetColumnName([DisallowNull] this object obj, string memberName) => obj.GetType().GetMember(memberName).FirstOrDefault()?.GetColumnName();
    /// <summary>
    /// 获取对象上的隐射到数据库的表名(Table 特性的 Name 值)
    /// </summary>
    /// <returns></returns>
    public static string? GetTableName([DisallowNull] this object obj) => obj.GetType().GetTableName();
    /// <summary>
    /// 获取对象设置的 json 
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="memberName"></param>
    /// <returns></returns>
    public static string? GetJsonPropertyName([DisallowNull] this object obj, string memberName) => obj.GetType().GetJsonPropertyName(memberName);
    /// <summary>
    /// 获取对象成员设定的缩写值
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string GetAbbreviation([DisallowNull] this object obj) => obj.GetType().GetAbbreviation();
    /// <summary>
    /// 获取对象成员设定的缩写值
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="memberName">成员名称, 可以是属性或者字段</param>
    /// <returns></returns>
    public static string? GetAbbreviation([DisallowNull] this object obj, string memberName) => obj.GetType().GetAbbreviation(memberName);
    /// <summary>
    /// 获取类型成员设定的农历值
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string GetLunisolarCalendar([DisallowNull] this object obj) => obj.GetType().GetLunisolarCalendar();
    /// <summary>
    /// 获取类型成员设定的农历值
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="memberName">成员名称, 可以是属性或者字段</param>
    /// <returns></returns>
    public static string? GetLunisolarCalendar([DisallowNull] this object obj, string memberName) => obj.GetType().GetLunisolarCalendar(memberName);
    #endregion

    #region 日期和时间相关扩展方法
    /// <summary>
    /// 获取时间差的详情, 精确到 kind 指定值
    /// </summary>
    /// <param name="timeSpan"></param>
    /// <returns></returns>
    public static string GetTimeDetail([DisallowNull] this TimeSpan timeSpan, TimeKind kind = TimeKind.MilliSecond)
    {
        var result = new List<string>();
        if (timeSpan.Days > 0 && kind >= TimeKind.Day)
        {
            if (timeSpan.Days > AppSettings.UNIT_DAYS_A_YEAR && kind >= TimeKind.Year)
            {
                int diff = timeSpan.Days % AppSettings.UNIT_DAYS_A_YEAR;
                result.Add($"{timeSpan.Days / AppSettings.UNIT_DAYS_A_YEAR} 年");
                if (diff > 0)
                {
                    result.Add($"{diff} 天");
                }
            }
            else
            {
                result.Add($"{timeSpan.Days} 天");
            }
        }

        if (timeSpan.Hours > 0 && kind >= TimeKind.Hour)
        {
            result.Add($"{timeSpan.Hours} 小时");
        }

        if (timeSpan.Minutes > 0 && kind >= TimeKind.Minute)
        {
            result.Add($"{timeSpan.Minutes} 分钟");
        }

        if (timeSpan.Seconds > 0 && kind >= TimeKind.Second)
        {
            result.Add($"{timeSpan.Seconds} 秒");
        }

        if (timeSpan.Milliseconds >= 0 && kind >= TimeKind.MilliSecond)
        {
            result.Add($"{timeSpan.Milliseconds} 毫秒");
        }

        return string.Join(AppSettings.CHAR_SPACE, result);
    }
    /// <summary>
    /// 获取相对于当前时间时间差的详情, 精确到 kind 指定值
    /// </summary>
    /// <param name="dateTime"></param>
    /// <param name="kind"></param>
    /// <returns></returns>
    public static string GetTimeDetail([DisallowNull] this DateTime dateTime, TimeKind kind = TimeKind.MilliSecond) => (DateTime.Now - dateTime).GetTimeDetail(kind);

    /// <summary>
    /// 获取时间类型的日期部分
    /// </summary>
    /// <param name="dateTime"></param>
    /// <returns></returns>
    public static DateOnly GetDate([DisallowNull] this DateTime dateTime) => DateOnly.FromDateTime(dateTime);
    /// <summary>
    /// 获取时间类型的时间部分
    /// </summary>
    /// <param name="dateTime"></param>
    /// <returns></returns>
    public static TimeOnly GetTime([DisallowNull] this DateTime dateTime) => TimeOnly.FromDateTime(dateTime);
    /// <summary>
    /// 将日期转换为时间部分为默认时间的时间类型
    /// </summary>
    /// <param name="dateOnly"></param>
    /// <returns></returns>
    public static DateTime ToDateTime([DisallowNull] this DateOnly dateOnly) => dateOnly.ToDateTime(TimeOnly.MinValue);
    #endregion

    #region 反射获取对象和调用方法
    /// <summary>
    /// 反射调用对象的泛型方法
    /// </summary>
    /// <param name="caller">调用对象</param>
    /// <param name="methodName">方法名</param>
    /// <param name="parameters">参数列表</param>
    /// <param name="returnValue">返回值</param>
    /// <param name="genericTypes">泛型类型</param>
    public static void InvokeGenericMethod([DisallowNull] this object caller, string methodName, object[]? parameters, out object? returnValue, params Type[] genericTypes)
    {
        returnValue = null;
        var methodInfo = caller.GetType().GetMethods().FirstOrDefault(x => x.IsGenericMethod && x.Name == methodName && x.GetParameters().Length == (parameters?.Length ?? 0));
        if (methodInfo is not null)
        {
            returnValue = methodInfo.MakeGenericMethod(genericTypes).Invoke(caller, parameters);
        }
    }
    /// <summary>
    /// 反射调用对象的泛型方法
    /// </summary>
    /// <typeparam name="T">返回值类型</typeparam>
    /// <param name="caller">调用对象</param>
    /// <param name="methodName">方法名</param>
    /// <param name="parameters">参数列表</param>
    /// <param name="genericTypes">泛型类型</param>
    /// <returns>返回调用结果</returns>
    public static T? InvokeGenericMethod<T>([DisallowNull] this object caller, string methodName, object[]? parameters, params Type[] genericTypes) where T : class
    {
        var methodInfo = caller.GetType().GetMethods().FirstOrDefault(x => x.IsGenericMethod && x.Name == methodName && x.GetParameters().Length == (parameters?.Length ?? 0));
        if (methodInfo is not null)
        {
            var returnValue = methodInfo.MakeGenericMethod(genericTypes).Invoke(caller, parameters);
            return returnValue is null ? default : (T)returnValue;
        }

        return default;
    }

    /// <summary>
    /// get string includes variables value from string variables expression
    /// usage (eg in user model):
    /// string value = "this is template string that contains Name: {Name} and Department: {Department.Name}";
    /// value = user.GetVariablesExpression(value);
    /// then value will be "this is template string that contains Name: Alex and Department: Huatek"
    /// </summary>
    /// <param name="obj">which object will be calculated to get value</param>
    /// <param name="value">which value expression will be calculated</param>
    /// <param name="prefix">veriable prrfox, default is "{"</param>
    /// <param name="suffix">veriable suffix, default is "}"</param>
    /// <param name="separator">veriable separator, default is "."</param>
    /// <returns></returns>
    public static string GetVariablesExpression([DisallowNull] this object obj, string value, string prefix = "{", string suffix = "}", string separator = ".")
    {
        if (!string.IsNullOrWhiteSpace(value) && value.Contains(prefix, StringComparison.InvariantCultureIgnoreCase) && value.Contains(suffix, StringComparison.InvariantCultureIgnoreCase))
        {
            string exp = prefix + @"(\w+" + separator + "?)+?" + suffix;
            return Regex.Replace(value, exp, new MatchEvaluator(match =>
            {
                var result = string.Empty;
                if (match.Value.Length > prefix.Length + suffix.Length)
                {
                    var propertyName = match.Value.Replace(prefix, string.Empty, StringComparison.InvariantCultureIgnoreCase).Replace(suffix, string.Empty, StringComparison.InvariantCultureIgnoreCase);
                    var value = obj.GetPropertyValue(propertyName);
                    if (value is not null)
                    {
                        result = value switch
                        {
                            string str => str,
                            DateTime dateTime => dateTime.ToString(AppSettings.DEFAULT_DATETIME_FORMAT, CultureInfo.CurrentCulture),
                            DateTimeOffset dateTimeOffset => dateTimeOffset.ToString(AppSettings.DEFAULT_DATETIME_FORMAT, CultureInfo.CurrentCulture),
                            IEnumerable enumerable => enumerable.Cast<object>().ToList().ToString<object>(),
                            _ => value.ToString(),
                        };
                    }
                }

                return result ?? string.Empty;
            }), RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
        }

        return string.Empty;
    }
    #endregion

    #region Lambda 表达式生成
    /// <summary>
    /// 获取成员
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <typeparam name="TResult">成员类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static MemberInfo GetMemberInfo<T, TResult>(this string memberName)
    {
        var resultType = typeof(TResult);
        var memberInfo = typeof(T).GetMember(memberName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase).FirstOrDefault();
        if (memberInfo is null || memberInfo.GetMemberType() != resultType)
        {
            throw new ArgumentException($"Member is not exist or member type not match result type: {resultType.Name}!");
        }

        return memberInfo;
    }
    /// <summary>
    /// 获取字符串类型成员
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    public static MemberInfo GetStringMemberInfo<T>(this string memberName) => memberName.GetMemberInfo<T, string>();

    /// <summary>
    /// 获取类型的成员选择表达式, 形如: arg => arg.memberName
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <typeparam name="TResult">成员类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    public static Expression<Func<T, TResult>> GetMemberExpression<T, TResult>(this string memberName) => memberName.GetMemberInfo<T, TResult>().GetMemberExpression<T, TResult>();
    /// <summary>
    /// 获取类型的成员相等表达式, 形如: arg => arg.memberName == value
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <typeparam name="TResult">成员类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    public static Expression<Func<T, bool>> GetMemberEqualExpression<T, TResult>(this string memberName, TResult value) => memberName.GetMemberInfo<T, TResult>().GetMemberEqualExpression<T, TResult>(value);
    /// <summary>
    /// 获取类型的成员赋值表达式, 形如: arg => arg.memberName = value
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <typeparam name="TResult">成员类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    public static Expression<Action<T>> GetMemberAssignExpression<T, TResult>(this string memberName, TResult value) => memberName.GetMemberInfo<T, TResult>().GetMemberAssignExpression<T, TResult>(value);

    /// <summary>
    /// 获取类型的字符串类型成员选择表达式, 形如: arg => arg.memberName
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    public static Expression<Func<T, string>> GetMemberExpression<T>(this string memberName) => memberName.GetStringMemberInfo<T>().GetMemberExpression<T>();
    /// <summary>
    /// 获取类型的字符串类型成员相等表达式, 形如: arg => arg.memberName == value
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    public static Expression<Func<T, bool>> GetMemberEqualExpression<T>(this string memberName, string value) => memberName.GetStringMemberInfo<T>().GetMemberEqualExpression<T>(value);
    /// <summary>
    /// 获取类型的字符串类型成员赋值表达式, 形如: arg => arg.memberName = value
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <param name="memberName">成员名</param>
    /// <returns></returns>
    public static Expression<Action<T>> GetMemberAssignExpression<T>(this string memberName, string value) => memberName.GetStringMemberInfo<T>().GetMemberAssignExpression<T>(value);
    #endregion

    #region 地理坐标转换
    /// <summary>
    /// 数字经纬度和度分秒经纬度转换 (Digital degree of latitude and longitude and vehicle to latitude and longitude conversion)
    /// </summary>
    /// <param name="digitalDegree">数字经纬度</param>
    /// <return>度分秒经纬度</return>
    public static string ToDegrees(this double digitalDegree)
    {
        const double unit = 60;
        var degree = (int)digitalDegree;
        var tmp = (digitalDegree - degree) * unit;
        var minute = (int)tmp;
        var second = Math.Round((tmp - minute) * unit, 4);
        return string.Empty + degree + "°" + minute + "′" + second + "″";
    }
    /// <summary>
    /// 数字经纬度和度分秒经纬度转换 (Digital degree of latitude and longitude and vehicle to latitude and longitude conversion)
    /// </summary>
    /// <param name="digitalDegree">数字经纬度</param>
    /// <return>度分秒经纬度</return>
    public static string ToDegrees(this decimal digitalDegree)
    {
        const decimal unit = 60;
        var degree = (int)digitalDegree;
        var tmp = (digitalDegree - degree) * unit;
        var minute = (int)tmp;
        var second = Math.Round((tmp - minute) * unit, 4);
        return string.Empty + degree + "°" + minute + "′" + second + "″";
    }
    /// <summary>
    /// 度分秒经纬度和数字经纬度转换 (Digital degree of latitude and longitude and vehicle to latitude and longitude conversion)
    /// </summary>
    /// <param name="degrees">度分秒经纬度</param>
    /// <return>数字经纬度</return>
    public static double ToDegrees(this string degrees)
    {
        var digitalDegree = 0.0;
        var indexDegree = degrees.IndexOf('°');           //度的符号对应的 Unicode 代码为：00B0[1]（六十进制），显示为°。
        if (indexDegree < 0)
        {
            return digitalDegree;
        }

        var degree = degrees[..indexDegree];
        digitalDegree += Convert.ToDouble(degree);

        var indexMinute = degrees.IndexOf('′');           //分的符号对应的 Unicode 代码为：2032[1]（六十进制），显示为′。
        if (indexMinute < 0)
        {
            return digitalDegree;
        }

        const double unit = 60;
        var minute = degrees[(indexDegree + 1)..indexMinute];
        digitalDegree += Convert.ToDouble(minute) / unit;

        var indexSecond = degrees.IndexOf('″');           //秒的符号对应的 Unicode 代码为：2033[1]（六十进制），显示为″。
        if (indexSecond < 0)
        {
            return digitalDegree;
        }

        var second = degrees[(indexMinute + 1)..indexSecond];
        digitalDegree += Convert.ToDouble(second) / (unit * unit);

        return digitalDegree;
    }

    /// <summary>
    /// 将包含经纬度信息的字符串转换为文本形式的地理位置表示法
    /// 30°44'56.64" / 114°6'9.33" 转换为文本形式的经纬度值 (30°44'56.64", 114°6'9.33")
    /// </summary>
    /// <param name="standardGeographicalLocation">包含经纬度信息的字符串</param>
    /// <returns>文本形式的地理位置表示法</returns>
    public static StandardGeographicalLocation GetStandardGeographicalLocation(this string standardGeographicalLocation)
    {
        //这是兼容中文标点符号
        standardGeographicalLocation = standardGeographicalLocation.Replace('\'', AppSettings.CHAR_QUOTE).Replace('\"', AppSettings.CHAR_DOUBLE_QUOTE);
        var geographicalLocation = standardGeographicalLocation.Split(AppSettings.CHAR_SLASH, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        if (geographicalLocation.Length == AppSettings.UNIT_TWO)
        {
            return new(geographicalLocation[AppSettings.UNIT_ZERO], geographicalLocation[AppSettings.UNIT_ONE]);
        }

        return new(string.Empty, string.Empty);
    }
    /// <summary>
    /// 从文本形式的地理位置表示法转换为数字形式的地理位置表示法
    /// </summary>
    /// <param name="geographicalLocation">文本形式的地理位置表示法, 形如: (30°44'56.64", 114°6'9.33")</param>
    /// <returns>数字形式的地理位置表示法</returns>
    public static GeographicalLocation GetGeographicalLocation(this StandardGeographicalLocation geographicalLocation) => new(geographicalLocation.Longitude.ToDegrees(), geographicalLocation.Latitude.ToDegrees());
    /// <summary>
    /// 将包含经纬度信息的字符串转换为数字形式的地理位置表示法
    /// </summary>
    /// <param name="standardGeographicalLocation">包含经纬度信息的字符串</param>
    /// <returns>数字形式的地理位置表示法</returns>
    public static GeographicalLocation GetGeographicalLocation(this string standardGeographicalLocation) => standardGeographicalLocation.GetStandardGeographicalLocation().GetGeographicalLocation();
    #endregion

    #region 其他扩展方法
    /// <summary>
    /// 获取数据库字段名分隔符
    /// </summary>
    /// <param name="databaseType"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public static char[] GetDatabaseSeparator(this DatabaseType databaseType) => databaseType switch
    {
        DatabaseType.SqlServer => ['[', ']'],
        DatabaseType.MySQL => ['`'],
        DatabaseType.PostgreSQL or DatabaseType.Oracle => ['"'],
        _ => throw new Exception("不支持的数据库类型!")
    };
    /// <summary>
    /// 获取制定类型的字段构造 sql 语句时的字符串表达式
    /// </summary>
    /// <typeparam name="TEntitySet">数据对象模型的类型</typeparam>
    /// <param name="entitySet">当前成员</param>
    /// <param name="propertyInfo">属性列表</param>
    /// <param name="databaseType">数据库类型</param>
    /// <exception cref="Exception"></exception>
    public static string? GetSqlStringValue<TEntitySet>(this TEntitySet entitySet, PropertyInfo propertyInfo, DatabaseType databaseType = DatabaseType.PostgreSQL)
    {
        var value = propertyInfo.GetValue(entitySet);
        var propertyType = propertyInfo.PropertyType.GetCoreType();
        if (value is null && (propertyInfo.PropertyType.HasAttribute<RequiredAttribute>() || (propertyType != typeof(string) && !propertyInfo.PropertyType.IsNullable())))
        {
            throw new Exception($"The type of {typeof(TEntitySet).GetDisplayName()} property {propertyInfo.Name} can not be null!");
        }

        if (value is null)
        {
            return "NULL";
        }

        if (propertyType.IsEnum)
        {
            var columnType = propertyInfo.GetAttributeValue<ColumnAttribute, string>(nameof(ColumnAttribute.TypeName));
            if (!string.IsNullOrWhiteSpace(columnType) && columnType.StartsWith("varchar", StringComparison.InvariantCultureIgnoreCase))
            {
                return AppSettings.CHAR_QUOTE + Enum.Format(propertyType, value, "F") + AppSettings.CHAR_QUOTE;
            }
            else
            {
                return Enum.Format(propertyType, value, "D");
            }
        }

        switch (value)
        {
            case bool boolValue: return databaseType == DatabaseType.MySQL ? (boolValue ? "1" : "0") : boolValue.ToString().ToLowerInvariant();
            case DateTime datetime: return AppSettings.CHAR_QUOTE + datetime.ToString(AppSettings.DEFAULT_DATETIME_FORMAT) + AppSettings.CHAR_QUOTE;
            case DateTimeOffset datetimeOffset: return AppSettings.CHAR_QUOTE + datetimeOffset.ToString(AppSettings.DEFAULT_ISO_DATETIME_WITH_TIMEZONE_FORMAT) + AppSettings.CHAR_QUOTE;
            case DateOnly dateOnly: return AppSettings.CHAR_QUOTE + dateOnly.ToString(AppSettings.DEFAULT_DATE_FORMAT) + AppSettings.CHAR_QUOTE;
            case TimeOnly timeOnly: return AppSettings.CHAR_QUOTE + timeOnly.ToString(AppSettings.DEFAULT_TIME_FORMAT) + AppSettings.CHAR_QUOTE;
            case string stringValue: return AppSettings.CHAR_QUOTE + stringValue + AppSettings.CHAR_QUOTE;
            case IEnumerable objects: return AppSettings.CHAR_QUOTE + objects.ToJsonString(AppSettings.JsonSerializerSettings) + AppSettings.CHAR_QUOTE;
            default:
                break;
        }

        return Type.GetTypeCode(propertyType) switch
        {
            TypeCode.SByte or TypeCode.Byte or TypeCode.Int16 or TypeCode.UInt16 or TypeCode.Int32 or TypeCode.UInt32 or TypeCode.Int64 or TypeCode.UInt64 or TypeCode.Single or TypeCode.Double or TypeCode.Decimal => value.ToString(),
            TypeCode.String or TypeCode.Char => AppSettings.CHAR_QUOTE + value.ToString() + AppSettings.CHAR_QUOTE,
            _ => throw new Exception("The type of value can not convert to string!")
        };
    }

    /// <summary>
    /// 获取字节大小的详情, 多少 GB, 多少 MB, 最大单位: TB
    /// </summary>
    /// <param name="size"></param>
    /// <returns></returns>
    public static string GetByteSizeDetail<TSize>(this TSize size) where TSize : INumber<TSize>
    {
        var (limit, unit) = size switch
        {
            >= AppSettings.UNIT_TERA => (AppSettings.UNIT_TERA, "TB"),
            < AppSettings.UNIT_TERA and >= AppSettings.UNIT_GIGA => (AppSettings.UNIT_GIGA, "GB"),
            < AppSettings.UNIT_GIGA and >= AppSettings.UNIT_MEGA => (AppSettings.UNIT_MEGA, "MB"),
            < AppSettings.UNIT_MEGA and >= AppSettings.UNIT_KILO => (AppSettings.UNIT_KILO, "KB"),
            _ => (AppSettings.UNIT_ONE, "B")
        };

        return Math.Round(Convert.ToDouble(size) / limit, AppSettings.UNIT_TWO) + unit;
    }

    /// <summary>
    /// get inner exception message of a exception
    /// </summary>
    /// <param name="ex">Exception instance</param>
    /// <returns>inner exception message</returns>
    public static string GetInnerException([DisallowNull] this Exception ex)
    {
        if (ex.InnerException is not null)
        {
            return ex.InnerException.GetInnerException();
        }

        return ex.Message;
    }
    /// <summary>
    /// 错误信息和堆栈一起返回
    /// </summary>
    /// <param name="ex"></param>
    /// <returns></returns>
    public static string GetMessage([DisallowNull] this Exception ex) => $"错误信息: {ex.GetInnerException()}, 错误堆栈: {ex.StackTrace}";

    /// <summary>
    /// 固定时间启动定时器, 并间隔执行方法
    /// </summary>
    /// <param name="timer">定时器</param>
    /// <param name="action">定时执行的方法</param>
    /// <param name="startTime">启动时间, 如果启动时间早于当前时间, 则将启动时间前后延迟 24 小时</param>
    /// <param name="interval">执行间隔, 单位: 秒, 默认执行间隔: 24小时, 也即每天执行一次</param>
    public static void Start(this System.Timers.Timer timer, Func<Task> action, DateTime startTime, int interval = AppSettings.UNIT_SECONDS_A_DAY)
    {
        var now = DateTime.Now;
        if (now > startTime)
        {
            startTime = startTime.AddDays(AppSettings.UNIT_ONE);
        }

        interval = interval * AppSettings.UNIT_THOUSAND;
        timer.Interval = (startTime - now).TotalMilliseconds;
        timer.Elapsed += async (sender, args) =>
        {
            if (sender is not null && sender is System.Timers.Timer t && t.Interval != interval)
            {
                t.Interval = interval;
            }

            await action();
        };
        timer.Start();
    }
    #endregion
}