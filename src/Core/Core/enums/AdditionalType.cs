﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 附加信息类别
/// </summary>
[Description("附加信息类别")]
public enum AdditionalType
{
    /// <summary>
    /// 别名
    /// </summary>
    [Display(Name = "别名")]
    Alias,
    /// <summary>
    /// 编号
    /// </summary>
    [Display(Name = "编号")]
    Code,
    /// <summary>
    /// 身份
    /// </summary>
    [Display(Name = "身份")]
    Identity,
    /// <summary>
    /// 组织机构
    /// </summary>
    [Display(Name = "组织机构")]
    Organization,
    /// <summary>
    /// 联系人
    /// </summary>
    [Display(Name = "联系人")]
    Contact,
    /// <summary>
    /// 电话号码
    /// </summary>
    [Display(Name = "电话号码")]
    PhoneNumber,
    /// <summary>
    /// 区域
    /// </summary>
    [Display(Name = "区域")]
    Region,
    /// <summary>
    /// 范围
    /// </summary>
    [Display(Name = "范围")]
    Scope,
    /// <summary>
    /// 位置
    /// </summary>
    [Display(Name = "位置")]
    Position,
    /// <summary>
    /// 地址
    /// </summary>
    [Display(Name = "地址")]
    Address,
    /// <summary>
    /// 证件
    /// </summary>
    [Display(Name = "证件")]
    Credential,
    /// <summary>
    /// 协议
    /// </summary>
    [Display(Name = "协议")]
    Protocol,
    /// <summary>
    /// 面积
    /// </summary>
    [Display(Name = "面积")]
    Area,
    /// <summary>
    /// 金额
    /// </summary>
    [Display(Name = "金额")]
    Amount,
    /// <summary>
    /// 设施
    /// </summary>
    [Display(Name = "设施")]
    Facility,
    /// <summary>
    /// 时间
    /// </summary>
    [Display(Name = "时间")]
    DateTime,
    /// <summary>
    /// 关系
    /// </summary>
    [Display(Name = "关系")]
    Relationship,
    /// <summary>
    /// 状态
    /// </summary>
    [Display(Name = "状态")]
    State,
    /// <summary>
    /// 性质
    /// </summary>
    [Display(Name = "性质")]
    Nature,
    /// <summary>
    /// 质量
    /// </summary>
    [Display(Name = "质量")]
    Quality,
    /// <summary>
    /// 距离
    /// </summary>
    [Display(Name = "距离")]
    Distance,
    /// <summary>
    /// 尺寸
    /// </summary>
    [Display(Name = "尺寸")]
    Size,
    /// <summary>
    /// 单位
    /// </summary>
    [Display(Name = "单位")]
    Unit,

    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "其他")]
    Other = 999
}