﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.Core;

/// <summary>
/// 逻辑操作类型
/// </summary>
[Description("逻辑操作类型")]
public enum OperatorType
{
    /// <summary>
    /// = 等于 equal
    /// </summary>
    [JsonProperty("eq")]
    [JsonPropertyName("eq")]
    [Display(Name = "等于")]
    Equal,
    /// <summary>
    /// != 等于 not equal
    /// </summary>
    [JsonProperty("ne")]
    [JsonPropertyName("ne")]
    [Display(Name = "不等于")]
    NotEqual,
    /// <summary>
    /// > 大于 greater than
    /// </summary>
    [JsonProperty("gt")]
    [JsonPropertyName("gt")]
    [Display(Name = "大于")]
    GreaterThan,
    /// <summary>
    /// >= 大于等于 greater than and equal
    /// </summary>
    [JsonProperty("ge")]
    [JsonPropertyName("ge")]
    [Display(Name = "大于等于")]
    GreaterThanAndEqual,
    /// <summary>
    /// < 小于 less than
    /// </summary>
    [JsonProperty("lt")]
    [JsonPropertyName("lt")]
    [Display(Name = "小于")]
    LessThan,
    /// <summary>
    /// <= 小于等于 less than and equal
    /// </summary>
    [JsonProperty("le")]
    [JsonPropertyName("le")]
    [Display(Name = "小于等于")]
    LessThanAndEqual,
    /// <summary>
    /// contains 包含
    /// </summary>
    [JsonProperty("contains")]
    [JsonPropertyName("contains")]
    [Display(Name = "包含")]
    Contains,
    /// <summary>
    /// sw 匹配开头 starts with, startswith
    /// </summary>
    [JsonProperty("startswith")]
    [JsonPropertyName("startswith")]
    [Display(Name = "匹配开头")]
    StartsWith,
    /// <summary>
    /// ew 匹配结尾 ends with, endswith
    /// </summary>
    [JsonProperty("endswith")]
    [JsonPropertyName("endswith")]
    [Display(Name = "匹配结尾")]
    EndsWith,
    /// <summary>
    /// in 包含于
    /// </summary>
    [JsonProperty("in")]
    [JsonPropertyName("in")]
    [Display(Name = "包含于")]
    In,
    /// <summary>
    /// notin 未包含于
    /// </summary>
    [JsonProperty("notin")]
    [JsonPropertyName("notin")]
    [Display(Name = "未包含于")]
    NotIn
}
