﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 终结点类型
/// </summary>
[Flags]
[Description("终结点类型")]
public enum EndpointType
{
    /// <summary>
    /// 主节点
    /// </summary>
    [Display(Name = "主节点")]
    Main = 0x01,
    /// <summary>
    /// 从节点
    /// </summary>
    [Display(Name = "从属节点")]
    Subordinate = 0x02,
    /// <summary>
    /// 备份节点
    /// </summary>
    [Display(Name = "备份节点")]
    Backup = 0x04,
    /// <summary>
    /// 预留节点
    /// </summary>
    [Display(Name = "预留节点")]
    Reserve = 0x08,

    /// <summary>
    /// 本地节点
    /// </summary>
    [Display(Name = "本地节点")]
    Local = 0x10,
    /// <summary>
    /// 远程节点
    /// </summary>
    [Display(Name = "远程节点")]
    Remote = 0x20,

    /// <summary>
    /// 源节点
    /// </summary>
    [Display(Name = "源节点")]
    Source = 0x40,
    /// <summary>
    /// 目标节点
    /// </summary>
    [Display(Name = "目标节点")]
    Target = 0x80
}