﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// Specifies a asymmetric encryption algorithm to use for providing confidentiality to protected payloads.
/// </summary>
[Description("非对称加密算法")]
public enum AsymmetricAlgorithm
{
    /// <summary>
    /// RSA 算法, 推荐使用, 推荐首选密钥长度 2048 位
    /// </summary>
    [Display(Name = "RSA 算法")]
    RSA,
    /// <summary>
    /// ECDsa 椭圆曲线数字签名算法
    /// </summary>
    [Display(Name = "ECDsa 算法")]
    ECDSA,
    /// <summary>
    /// ECDiffieHellman 椭圆曲线 Diffie-Hellman (ECDH) 算法
    /// </summary>
    [Display(Name = "ECDiffieHellman 算法")]
    ECDH,
    /// <summary>
    /// DSA 算法, 仅可用于签名, 不如 RSA 安全
    /// </summary>
    [Display(Name = "DSA 算法")]
    DSA,
    /// <summary>
    /// 国密 SM2 算法
    /// </summary>
    [Display(Name = "国密 SM2 算法")]
    SM2
}