﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 认证方式
/// </summary>
[Description("认证方式")]
public enum AuthType
{
    /// <summary>
    /// 不认证
    /// </summary>
    [Display(Name = "不认证")]
    None,

    /// <summary>
    /// 用户名密码认证
    /// </summary>
    [Display(Name = "用户名密码认证")]
    Password,
    /// <summary>
    /// AccessKey 认证, 使用 AccessKeyId 和 AccessKeySecret 的方式 
    /// </summary>
    [Display(Name = "AccessKey 认证")]
    AccessKey,
    /// <summary>
    /// AppCode 认证
    /// </summary>
    [Display(Name = "AppCode 认证")]
    AppCode,
    /// <summary>
    /// 基本身份认证, 使用用户名密码的形式
    /// </summary>
    [Display(Name = "Basic 认证")]
    Basic,
    /// <summary>
    /// 使用 access token 的认证方式
    /// </summary>
    [Display(Name = "Token 认证")]
    Token,
    /// <summary>
    /// OpenAuth2 认证
    /// </summary>
    [Display(Name = "OpenAuth2 认证")]
    OAuth2,
    /// <summary>
    /// Oidc 认证
    /// </summary>
    [Display(Name = "Oidc 认证")]
    Oidc,
    /// <summary>
    /// Bearer Token 认证
    /// </summary>
    [Display(Name = "Bearer Token 认证")]
    BearerToken,

    /// <summary>
    /// 微信认证
    /// </summary>
    [Display(Name = "微信认证")]
    Weixin,
    /// <summary>
    /// 支付宝认证
    /// </summary>
    [Display(Name = "支付宝认证")]
    Alipay,

    /// <summary>
    /// 验证码认证方式, 仅仅适用于一次性登录认证
    /// </summary>
    [Display(Name = "验证码认证方式")]
    Captcha,

    /// <summary>
    /// 其他认证方式
    /// </summary>
    [Display(Name = "其他")]
    Other
}