using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// Specifies a message authentication algorithm to use for providing tamper-proofing
/// to protected payloads.
/// </summary>
[Description("ɢ���㷨")]
public enum HashAlgorithm
{
    /// <summary>
    /// The HMAC algorithm (RFC 2104) using the SHA-512 hash function (FIPS 180-4).
    /// </summary>
    [Display(Name = "MD5")]
    MD5,
    /// <summary>
    /// The HMAC algorithm (RFC 2104) using the SHA-1 hash function (FIPS 180-4).
    /// </summary>      
    [Display(Name = "SHA-1")]
    SHA1,
    /// <summary>
    /// The HMAC algorithm (RFC 2104) using the SHA-256 hash function (FIPS 180-4).
    /// </summary>        
    [Display(Name = "SHA-256")]
    SHA256,
    /// <summary>
    /// The HMAC algorithm (RFC 2104) using the SHA-384 hash function (FIPS 180-4).
    /// </summary>
    [Display(Name = "SHA-384")]
    SHA384,
    /// <summary>
    /// The HMAC algorithm (RFC 2104) using the SHA-512 hash function (FIPS 180-4).
    /// </summary>
    [Display(Name = "SHA-512")]
    SHA512
}