﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 授权类型
/// 在用户, 角色, 组织中的身份类型高于 Authorizer 时, 将不在作为内置角色, 不可编辑
/// </summary>
[Description("授权类型")]
public enum AuthorizeType
{
    /// <summary>
    /// 匿名的
    /// </summary>
    [Display(Name = "匿名")]
    Anonymous,
    /// <summary>
    /// 公众的
    /// </summary>
    [Display(Name = "公众")]
    Public,
    /// <summary>
    /// 认证用户
    /// 指已经登录, 但是尚未分配权限的人/身份, 此时仅有基本的系统访问权限
    /// </summary>
    [Display(Name = "认证用户")]
    Authenticator,
    /// <summary>
    /// 授权访问者
    /// 此身份类型需要直属部门/级别等更高级的成员授权后可访问的
    /// </summary>
    [Display(Name = "授权访问者")]
    AuthorizedAccessor,
    /// <summary>
    /// 第三方的
    /// </summary>
    [Display(Name = "第三方")]
    ThirdParty,
    /// <summary>
    /// 合作方的
    /// </summary>
    [Display(Name = "合作方")]
    Partner,
    /// <summary>
    /// 受保护的
    /// </summary>
    [Display(Name = "受保护的")]
    Protected,
    /// <summary>
    /// 外部管理员
    /// 外部身份类型, 非系统管理员, 可以对此身份以下的身份类型进行管理的
    /// </summary>
    [Display(Name = "外部管理员")]
    ExternalAdministrator,
    /// <summary>
    /// 内部的
    /// </summary>
    [Display(Name = "内部")]
    Internal,
    /// <summary>
    /// 官方的
    /// </summary>
    [Display(Name = "官方")]
    Official,
    /// <summary>
    /// 业务管理员
    /// </summary>
    [Display(Name = "业务管理员")]
    BusinessAdministrator,
    /// <summary>
    /// 授权者
    /// 可以给别人进行授权的高级别身份, 仅次于超管和开发者的身份类型
    /// </summary>
    [Display(Name = "授权者")]
    Authorizer,
    /// <summary>
    /// 管理员的
    /// </summary>
    [Display(Name = "管理员")]
    Administrator,
    /// <summary>
    /// 开发使用的
    /// </summary>
    [Display(Name = "开发者")]
    Developer,
    /// <summary>
    /// 系统的
    /// </summary>
    [Display(Name = "系统")]
    System
}