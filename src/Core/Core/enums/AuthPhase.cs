﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 认证阶段
/// </summary>
[Description("认证阶段")]
public enum AuthPhase
{
    /// <summary>
    /// 未验证
    /// </summary>
    [Display(Name = "未验证")]
    None,
    /// <summary>
    /// 已认证手机号码
    /// </summary>
    [Display(Name = "手机号码")]
    PhoneNumber,
    /// <summary>
    /// 已认证身份证号码
    /// </summary>
    [Display(Name = "身份证")]
    IdCard,
    /// <summary>
    /// 已完成人脸比对
    /// </summary>
    [Display(Name = "人脸比对")]
    FaceCompare,
    /// <summary>
    /// 已完成人脸活体检测
    /// </summary>
    [Display(Name = "人脸活体检测")]
    FaceDetection,
    /// <summary>
    /// 已完成所有检测项
    /// </summary>
    [Display(Name = "已完成")]
    Finish
}