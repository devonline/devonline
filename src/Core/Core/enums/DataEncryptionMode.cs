﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 数据加密方式, 指加密后的数据结构形式
/// </summary>
[Description("数据加密方式")]
public enum DataEncryptionMode
{
    /// <summary>
    /// 仅包含数据密文部分
    /// </summary>
    [Display(Name = "仅数据")]
    DataOnly,
    /// <summary>
    /// 使用密钥编号 + 数据密文的方式
    /// </summary>
    [Display(Name = "使用密钥编号")]
    KeyId,
    /// <summary>
    /// 使用完整 KeyIV + 数据密文的方式
    /// </summary>
    [Display(Name = "使用 KeyIV")]
    KeyIV,
}