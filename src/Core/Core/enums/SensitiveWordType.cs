﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 敏感词类型
/// </summary>
[Description("敏感词类型")]
public enum SensitiveWordType
{
    /// <summary>
    /// 政治
    /// </summary>
    [Display(Name = "政治类")]
    Political,
    /// <summary>
    /// 暴力恐怖
    /// </summary>
    [Display(Name = "暴恐类")]
    Violence,
    /// <summary>
    /// 诈骗
    /// </summary>
    [Display(Name = "诈骗类")]
    Defraud,
    /// <summary>
    /// 色情
    /// </summary>
    [Display(Name = "色情类")]
    Pornographic,
    /// <summary>
    /// 低俗
    /// </summary>
    [Display(Name = "低俗类")]
    Vulgar,
    /// <summary>
    /// 赌博
    /// </summary>
    [Display(Name = "赌博类")]
    Gamble,
    /// <summary>
    /// 侵权
    /// </summary>
    [Display(Name = "侵权类")]
    Infringement,
    /// <summary>
    /// 谣言类
    /// </summary>
    [Display(Name = "谣言类")]
    Rumor,
    /// <summary>
    /// 毒品
    /// </summary>
    [Display(Name = "毒品类")]
    Drug,
    /// <summary>
    /// 未成年人
    /// </summary>
    [Display(Name = "未成年人类")]
    Minor,
    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "其他类")]
    Other = 99
}