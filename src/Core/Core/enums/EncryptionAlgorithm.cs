﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 加密算法
/// </summary>
[Description("加密算法")]
public enum EncryptionAlgorithm
{
    #region 常规非加密编码, 字符编码算法
    /// <summary>
    /// Base64 编码, 严格意义上不算加密算法, 不推荐在加密场景使用
    /// </summary>
    [Display(Name = "Base64 编码")]
    Base64,
    /// <summary>
    /// BDC 编码, 严格意义上不算加密算法, 不推荐在加密场景使用
    /// </summary>
    [Display(Name = "BDC 编码")]
    BCD,
    /// <summary>
    /// 16进制编码
    /// </summary>
    [Display(Name = "16 进制编码")]
    Hex,
    #endregion

    #region 哈希算法, 散列算法
    /// <summary>
    /// MD5, 弱哈希算法, 不推荐使用
    /// </summary>
    [Display(Name = "MD5 算法")]
    MD5 = 11,
    /// <summary>
    /// SHA1 弱哈希算法, 不推荐使用
    /// </summary>
    [Display(Name = "SHA1 算法")]
    SHA1,
    /// <summary>
    /// SHA256 强哈希算法, 推荐使用, 推荐首选
    /// </summary>
    [Display(Name = "SHA256 算法")]
    SHA256,
    /// <summary>
    /// SHA384 强哈希算法, 推荐使用
    /// </summary>
    [Display(Name = "SHA384 算法")]
    SHA384,
    /// <summary>
    /// SHA512 强哈希算法, 推荐使用
    /// </summary>
    [Display(Name = "SHA512 算法")]
    SHA512,
    #endregion

    #region 对称加密算法
    /// <summary>
    /// AES 算法, 推荐算法, 推荐首选 256 位
    /// </summary>
    [Display(Name = "AES 算法")]
    Aes = 21,
    /// <summary>
    /// 三重数据加密标准算法, 推荐算法
    /// </summary>
    [Display(Name = "3DES 算法")]
    TripleDES,
    /// <summary>
    /// DES 弱对称加密算法, 不在推荐使用
    /// </summary>
    [Display(Name = "DES 算法")]
    DES,
    /// <summary>
    /// RC2 弱对称加密算法, 不在推荐使用
    /// </summary>
    [Display(Name = "RC2 算法")]
    RC2,
    /// <summary>
    /// Rijndael 弱对称加密算法, 不在推荐使用
    /// </summary>
    [Display(Name = "Rijndael 算法")]
    Rijndael,
    #endregion

    #region 非对称加密算法, 签名算法
    /// <summary>
    /// RSA 算法, 推荐使用, 推荐首选密钥长度 2048 位
    /// </summary>
    [Display(Name = "RSA 算法")]
    RSA = 31,
    /// <summary>
    /// ECDsa 椭圆曲线数字签名算法
    /// </summary>
    [Display(Name = "ECDsa 算法")]
    ECDSA,
    /// <summary>
    /// ECDiffieHellman 椭圆曲线 Diffie-Hellman (ECDH) 算法
    /// </summary>
    [Display(Name = "ECDiffieHellman 算法")]
    ECDH,
    /// <summary>
    /// DSA 算法, 仅可用于签名, 不如 RSA 安全
    /// </summary>
    [Display(Name = "DSA 算法")]
    DSA,
    /// <summary>
    /// 国密 SM2 算法
    /// </summary>
    [Display(Name = "国密 SM2 算法")]
    SM2
    #endregion
}