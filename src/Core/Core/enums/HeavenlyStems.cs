﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 天干 tiān ɡān
/// Heavenly Stems
/// </summary>
[Description("天干")]
public enum HeavenlyStems
{
    /// <summary>
    /// 甲 jiǎ
    /// </summary>
    [Display(Name = "甲")]
    Jia = 1,
    /// <summary>
    /// 乙 yǐ
    /// </summary>
    [Display(Name = "乙")]
    Yi,
    /// <summary>
    /// 丙 bǐnɡ
    /// </summary>
    [Display(Name = "丙")]
    Binɡ,
    /// <summary>
    /// 丁 dīnɡ
    /// </summary>
    [Display(Name = "丁")]
    Ding,
    /// <summary>
    /// 戊 wù
    /// </summary>
    [Display(Name = "戊")]
    Wu,
    /// <summary>
    /// 己 jǐ
    /// </summary>
    [Display(Name = "己")]
    Ji,
    /// <summary>
    /// 庚 ɡēnɡ
    /// </summary>
    [Display(Name = "庚")]
    Geng,
    /// <summary>
    /// 辛 xīn
    /// </summary>
    [Display(Name = "辛")]
    Xin,
    /// <summary>
    /// 壬 rén
    /// </summary>
    [Display(Name = "壬")]
    Ren,
    /// <summary>
    /// 癸 ɡuǐ
    /// </summary>
    [Display(Name = "癸")]
    Gui
}