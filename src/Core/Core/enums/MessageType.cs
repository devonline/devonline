﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 消息类型
/// </summary>
[Description("消息类型")]
public enum MessageType
{
    #region 数据传输类消息
    /// <summary>
    /// 常规的文本内容消息
    /// </summary>
    [Display(Name = "文本")]
    Text,
    /// <summary>
    /// 数据消息, 消息内容为经过编码的数据
    /// </summary>
    [Display(Name = "数据")]
    Data,
    /// <summary>
    /// 缓存消息, 消息内容为数据缓存或缓存名称
    /// </summary>
    [Display(Name = "缓存")]
    Cache,
    /// <summary>
    /// 事件消息, 消息内容为事件
    /// </summary>
    [Display(Name = "事件")]
    Event,
    /// <summary>
    /// 图像消息, 消息内容为一个图像文件远程地址或者经过编码的图像内容
    /// </summary>
    [Display(Name = "图像")]
    Image,
    /// <summary>
    /// 音频消息, 消息内容为一个音频文件远程地址或者经过编码的音频内容
    /// </summary>
    [Display(Name = "音频")]
    Audio,
    /// <summary>
    /// 视频消息, 消息内容为一个视频文件远程地址或者经过编码的视频内容
    /// </summary>
    [Display(Name = "视频")]
    Video,
    /// <summary>
    /// 文件消息, 消息内容为一个文件远程地址或者经过编码的文件内容
    /// </summary>
    [Display(Name = "文件")]
    File,
    /// <summary>
    /// 表情消息, 消息内容为一个表情描述
    /// </summary>
    [Display(Name = "表情")]
    Emoji,
    /// <summary>
    /// 新闻消息, 消息内容为一个新闻内容或新闻页面的超链接
    /// </summary>
    [Display(Name = "新闻")]
    News,
    /// <summary>
    /// 公告消息, 消息内容为一个公告内容或公告页面的超链接
    /// </summary>
    [Display(Name = "公告")]
    Announcement,
    /// <summary>
    /// 位置消息的消息内容为一组坐标值
    /// </summary>
    [Display(Name = "位置")]
    Location,
    /// <summary>
    /// 回复消息, 代表回复某个历史消息, 消息内容为回复内容
    /// 回复的消息编号由 Relay 字段指定, 内容为 Relay 消息要求内容类型, 由接收者自行处理
    /// </summary>
    [Display(Name = "回复")]
    Reply,
    /// <summary>
    /// 指令/命令消息, 代表接收者需要执行消息内容指定的指令/命令
    /// </summary>
    [Display(Name = "指令")]
    Command,
    /// <summary>
    /// 心跳消息
    /// </summary>
    [Display(Name = "心跳")]
    Heartbeat,
    /// <summary>
    /// 位置消息的消息内容为一条记录
    /// </summary>
    [Display(Name = "记录")]
    Record,
    /// <summary>
    /// 位置消息的消息内容为一条日志
    /// </summary>
    [Display(Name = "日志")]
    Log,
    #endregion

    #region 状态通知类消息
    [Display(Name = "跟踪")]
    Trace,
    [Display(Name = "调试")]
    Debug,
    [Display(Name = "细节")]
    Detail,
    [Display(Name = "简短")]
    Brief,
    [Display(Name = "信息")]
    Info,
    [Display(Name = "通知")]
    Notice,
    [Display(Name = "警告")]
    Warning,
    [Display(Name = "错误")]
    Error,
    [Display(Name = "危急")]
    Critical,
    [Display(Name = "致命")]
    Fatal,
    #endregion

    #region 指令/命令执行类消息
    [Display(Name = "执行")]
    Execute,
    [Display(Name = "安装")]
    Setup,
    [Display(Name = "调用")]
    Call,
    [Display(Name = "打印")]
    Print,
    [Display(Name = "格式化")]
    Format,
    [Display(Name = "读")]
    Read,
    [Display(Name = "写")]
    Write,
    [Display(Name = "回执")]
    Ack,

    [Display(Name = "启动")]
    Startup,
    [Display(Name = "初始化")]
    Initial,
    [Display(Name = "回收")]
    Recycle,
    [Display(Name = "释放")]
    Dispose,

    [Display(Name = "开始")]
    Start,
    [Display(Name = "停止")]
    Stop,
    [Display(Name = "暂停")]
    Pause,
    [Display(Name = "继续")]
    Continue,

    [Display(Name = "报警")]
    Alarm,
    [Display(Name = "急停")]
    Scram,
    [Display(Name = "终止")]
    Abort,
    [Display(Name = "完成")]
    Complete,
    [Display(Name = "完毕")]
    Finish,

    [Display(Name = "刷新")]
    Refresh,
    [Display(Name = "重置")]
    Reset,
    [Display(Name = "清除")]
    Clear,
    [Display(Name = "移除")]
    Remove,
    [Display(Name = "删除")]
    Delete,
    [Display(Name = "销毁")]
    Destroy,

    [Display(Name = "进入")]
    Enter,
    [Display(Name = "退出")]
    Exit,
    [Display(Name = "离开")]
    Quit,

    [Display(Name = "开始")]
    Begin,
    [Display(Name = "结束")]
    End,

    [Display(Name = "启用")]
    Enable,
    [Display(Name = "禁用")]
    Disable,

    [Display(Name = "开启")]
    On,
    [Display(Name = "关闭")]
    Off,

    [Display(Name = "打开")]
    Open,
    [Display(Name = "关闭")]
    Close,

    [Display(Name = "同步")]
    Sync,
    [Display(Name = "异步")]
    Async,

    [Display(Name = "锁定")]
    Lock,
    [Display(Name = "解锁")]
    Unlock,

    [Display(Name = "绑定")]
    Bind,
    [Display(Name = "解绑")]
    Unbind,

    [Display(Name = "安装")]
    Install,
    [Display(Name = "卸载")]
    Uninstall,

    /// <summary>
    /// 主动的, 活跃的, 激活状态的
    /// </summary>
    [Display(Name = "活跃")]
    Active,
    /// <summary>
    /// 不主动的, 不活跃的, 非激活状态的
    /// </summary>
    [Display(Name = "不活跃")]
    Inactive,
    [Display(Name = "被动")]
    Passive,

    [Display(Name = "加载")]
    Load,
    [Display(Name = "重新加载")]
    Reload,
    [Display(Name = "卸载")]
    Unload,

    [Display(Name = "连接")]
    Connect,
    [Display(Name = "重新连接")]
    Reconnect,
    [Display(Name = "断开连接")]
    Disconnect,

    [Display(Name = "在线")]
    Online,
    [Display(Name = "离线")]
    Offline,

    [Display(Name = "登录")]
    Login,
    [Display(Name = "注销")]
    Logout,

    [Display(Name = "登录")]
    SignIn,
    [Display(Name = "注销")]
    SignOut,

    [Display(Name = "签入")]
    CheckIn,
    [Display(Name = "签出")]
    CheckOut,

    [Display(Name = "获取")]
    Get,
    [Display(Name = "设置")]
    Set,

    [Display(Name = "发送")]
    Send,
    [Display(Name = "接收")]
    Receive,

    [Display(Name = "拿起")]
    Take,
    [Display(Name = "放下")]
    Put,

    [Display(Name = "推")]
    Push,
    [Display(Name = "拉")]
    Pull,

    [Display(Name = "发布")]
    Publish,
    [Display(Name = "订阅")]
    Subscribe,

    [Display(Name = "有效")]
    Valid,
    [Display(Name = "无效")]
    Invalid,

    [Display(Name = "序列化")]
    Serialize,
    [Display(Name = "反序列化")]
    Deserialize,

    [Display(Name = "进")]
    In,
    [Display(Name = "出")]
    Out,

    [Display(Name = "输入")]
    Input,
    [Display(Name = "输出")]
    Output,

    [Display(Name = "上传")]
    Upload,
    [Display(Name = "下载")]
    Download,

    [Display(Name = "上")]
    Up,
    [Display(Name = "下")]
    Down,
    [Display(Name = "前")]
    Before,
    [Display(Name = "后")]
    After,
    [Display(Name = "左")]
    Left,
    [Display(Name = "右")]
    Right,

    [Display(Name = "回归")]
    Home,
    [Display(Name = "返回")]
    Back,
    [Display(Name = "返回")]
    Return,
    [Display(Name = "第一个")]
    First,
    [Display(Name = "最后一个")]
    Last,
    [Display(Name = "上一个")]
    Prev,
    [Display(Name = "下一个")]
    Next,
    #endregion

    /// <summary>
    /// 其他消息类型
    /// </summary>
    [Display(Name = "其他")]
    Other = 999
}