﻿using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 易经 64 卦
/// 卦象 |¦ 阴阳顺序从右向左
/// </summary>
public enum Hexagram
{
    /// <summary>
    /// ||||||
    /// 乾 qián
    /// The Creative
    /// </summary>
    [Display(Name = "乾 qián")]
    Force = 1,
    /// <summary>
    /// ¦¦¦¦¦¦
    /// 坤 kūn
    /// The Receptive
    /// </summary>
    [Display(Name = "坤 kūn")]
    Field = 2,
    /// <summary>
    /// |¦¦¦|¦
    /// 屯 chún
    /// Difficulty at the Beginning
    /// </summary>
    [Display(Name = "屯 chún")]
    Sprouting = 3,
    /// <summary>
    /// ¦|¦¦¦|
    /// 蒙 méng
    /// Youthful Folly
    /// </summary>
    [Display(Name = "蒙 méng")]
    Enveloping = 4,
    /// <summary>
    /// |||¦|¦
    /// 需 xū
    /// Waiting
    /// </summary>
    [Display(Name = "需 xū")]
    Attending = 5,
    /// <summary>
    /// ¦|¦|||
    /// 訟 sòng
    /// Conflict
    /// </summary>
    [Display(Name = "訟 sòng")]
    Arguing = 6,
    /// <summary>
    /// ¦|¦¦¦¦
    /// 師 shī
    /// The Army
    /// </summary>
    [Display(Name = "師 shī")]
    Leading = 7,
    /// <summary>
    /// ¦¦¦¦|¦
    /// 比 bǐ
    /// Holding Together
    /// </summary>
    [Display(Name = "比 bǐ")]
    Grouping = 8,
    /// <summary>
    /// |||¦||
    /// 小畜 xiǎo chù
    /// Small Taming
    /// </summary>
    [Display(Name = "小畜 xiǎo chù")]
    SmallAccumulating = 9,
    /// <summary>
    /// ||¦|||
    /// 履 lǚ
    /// Treading (Conduct)
    /// </summary>
    [Display(Name = "履 lǚ")]
    Treading = 10,
    /// <summary>
    /// |||¦¦¦
    /// 泰 tài
    /// Peace
    /// </summary>
    [Display(Name = "泰 tài")]
    Prevading = 11,
    /// <summary>
    /// ¦¦¦|||
    /// 否 pǐ
    /// Standstill
    /// </summary>
    [Display(Name = "否 pǐ")]
    Obstruction = 12,
    /// <summary>
    /// |¦||||
    /// 同人 tóng rén
    /// Fellowship
    /// </summary>
    [Display(Name = "同人 tóng rén")]
    ConcordingPeople = 13,
    /// <summary>
    /// ||||¦|
    /// 大有 dà yǒu
    /// Great Possession
    /// </summary>
    [Display(Name = "大有 dà yǒu")]
    GreatPossessing = 14,
    /// <summary>
    /// ¦¦|¦¦¦
    /// 謙 qiān
    /// Modesty
    /// </summary>
    [Display(Name = "謙 qiān")]
    Humbling = 15,
    /// <summary>
    /// ¦¦¦|¦¦
    /// 豫 yù
    /// Enthusiasm
    /// </summary>
    [Display(Name = "豫 yù")]
    ProvidingFor = 16,
    /// <summary>
    /// |¦¦||¦
    /// 隨 suí
    /// Following
    /// </summary>
    [Display(Name = "隨 suí")]
    Following = 17,
    /// <summary>
    /// ¦||¦¦|
    /// 蠱 gǔ
    /// Work on the Decayed
    /// </summary>
    [Display(Name = "蠱 gǔ")]
    Corrupting = 18,
    /// <summary>
    /// ||¦¦¦¦
    /// 臨 lín
    /// Approach
    /// </summary>
    [Display(Name = "臨 lín")]
    Nearing = 19,
    /// <summary>
    /// ¦¦¦¦||
    /// 觀 guān
    /// Contemplation
    /// </summary>
    [Display(Name = "觀 guān")]
    Viewing = 20,
    /// <summary>
    /// |¦¦|¦|
    /// 噬嗑 shì kè
    /// Biting Through
    /// </summary>
    [Display(Name = "噬嗑 shì kè")]
    GnawingBite = 21,
    /// <summary>
    /// |¦|¦¦|
    /// 賁 bì
    /// Grace
    /// </summary>
    [Display(Name = "賁 bì")]
    Adorning = 22,
    /// <summary>
    /// ¦¦¦¦|
    /// 剝 bō
    /// Splitting Apart
    /// </summary>
    [Display(Name = "剝 bō")]
    Stripping = 23,
    /// <summary>
    /// |¦¦¦¦¦
    /// 復 fù
    /// Return
    /// </summary>
    [Display(Name = "復 fù")]
    Returning = 24,
    /// <summary>
    /// |¦¦|||
    /// 無妄 wú wàng
    /// Innocence
    /// </summary>
    [Display(Name = "無妄 wú wàng")]
    WithoutEmbroiling = 25,
    /// <summary>
    /// |||¦¦|
    /// 大畜 dà chù
    /// Great Taming
    /// </summary>
    [Display(Name = "大畜 dà chù")]
    GreatAccumulating = 26,
    /// <summary>
    /// |¦¦¦¦|
    /// 頤 yí
    /// Mouth Corners
    /// </summary>
    [Display(Name = "頤 yí")]
    Swallowing = 27,
    /// <summary>
    /// ¦||||¦
    /// 大過 dà guò
    /// Great Preponderance
    /// </summary>
    [Display(Name = "大過 dà guò")]
    GreatExceeding = 28,
    /// <summary>
    /// ¦|¦¦|¦
    /// 坎 kǎn
    /// The Abysmal Water
    /// </summary>
    [Display(Name = "坎 kǎn")]
    Gorge = 29,
    /// <summary>
    /// |¦||¦|
    /// 離 lí
    /// The Clinging
    /// </summary>
    [Display(Name = "離 lí")]
    Radiance = 30,
    /// <summary>
    /// ¦¦|||¦
    /// 咸 xián
    /// Influence
    /// </summary>
    [Display(Name = "咸 xián")]
    Conjoining = 31,
    /// <summary>
    /// ¦|||¦¦
    /// 恆 héng
    /// Duration
    /// </summary>
    [Display(Name = "恆 héng")]
    Persevering = 32,
    /// <summary>
    /// ¦¦||||
    /// 遯 dùn
    /// Retreat
    /// </summary>
    [Display(Name = "遯 dùn")]
    Retiring = 33,
    /// <summary>
    /// ||||¦¦
    /// 大壯 dà zhuàng
    /// Great Power
    /// </summary>
    [Display(Name = "大壯 dà zhuàng")]
    GreatInvigorating = 34,
    /// <summary>
    /// ¦¦¦|¦|
    /// 晉 jìn
    /// Progress
    /// </summary>
    [Display(Name = "晉 jìn")]
    Prospering = 35,
    /// <summary>
    /// |¦|¦¦¦
    /// 明夷 míng yí
    /// Darkening of the Light
    /// </summary>
    [Display(Name = "明夷 míng yí")]
    BrightnessHiding = 36,
    /// <summary>
    /// |¦|¦||
    /// 家人 jiā rén
    /// The Family
    /// </summary>
    [Display(Name = "家人 jiā rén")]
    DwellingPeople = 37,
    /// <summary>
    /// ||¦|¦|
    /// 睽 kuí
    /// Opposition
    /// </summary>
    [Display(Name = "睽 kuí")]
    Polarising = 38,
    /// <summary>
    /// ¦¦|¦|¦
    /// 蹇 jiǎn
    /// Obstruction
    /// </summary>
    [Display(Name = "蹇 jiǎn")]
    Limping = 39,
    /// <summary>
    /// ¦|¦|¦¦
    /// 解 xiè
    /// Deliverance
    /// </summary>
    [Display(Name = "解 xiè")]
    TakingApart = 40,
    /// <summary>
    /// ||¦¦¦|
    /// 損 sǔn
    /// Decrease
    /// </summary>
    [Display(Name = "損 sǔn")]
    Diminishing = 41,
    /// <summary>
    /// |¦¦¦||
    /// 益 yì
    /// Increase
    /// </summary>
    [Display(Name = "益 yì")]
    Augmenting = 42,
    /// <summary>
    /// |||||¦
    /// 夬 guài
    /// Breakthrough
    /// </summary>
    [Display(Name = "夬 guài")]
    Parting = 43,
    /// <summary>
    /// ¦|||||
    /// 姤 gòu
    /// Coming to Meet
    /// </summary>
    [Display(Name = "姤 gòu")]
    Coupling = 44,
    /// <summary>
    /// ¦¦¦||¦
    /// 萃 cuì
    /// Gathering Together
    /// </summary>
    [Display(Name = "萃 cuì")]
    Clustering = 45,
    /// <summary>
    /// ¦||¦¦¦
    /// 升 shēng
    /// Pushing Upward
    /// </summary>
    [Display(Name = "升 shēng")]
    Ascending = 46,
    /// <summary>
    /// ¦|¦||¦
    /// 困 kùn
    /// Oppression
    /// </summary>
    [Display(Name = "困 kùn")]
    Confining = 47,
    /// <summary>
    /// ¦||¦|¦
    /// 井 jǐng
    /// The Well
    /// </summary>
    [Display(Name = "井 jǐng")]
    Welling = 48,
    /// <summary>
    /// |¦|||¦
    /// 革 gé
    /// Revolution
    /// </summary>
    [Display(Name = "革 gé")]
    Skinning = 49,
    /// <summary>
    /// ¦|||¦|
    /// 鼎 dǐng
    /// The Cauldron
    /// </summary>
    [Display(Name = "鼎 dǐng")]
    Holding = 50,
    /// <summary>
    /// |¦¦|¦¦
    /// 震 zhèn
    /// Arousing
    /// </summary>
    [Display(Name = "震 zhèn")]
    Shake = 51,
    /// <summary>
    /// ¦¦|¦¦|
    /// 艮 gèn
    /// The Keeping Still
    /// </summary>
    [Display(Name = "艮 gèn")]
    Bound = 52,
    /// <summary>
    /// ¦¦|¦||
    /// 漸 jiàn
    /// Development
    /// </summary>
    [Display(Name = "漸 jiàn")]
    Infiltrating = 53,
    /// <summary>
    /// ||¦|¦¦
    /// 歸妹 guī mèi
    /// The Marrying Maiden
    /// </summary>
    [Display(Name = "歸妹 guī mèi")]
    ConvertingTheMaiden = 54,
    /// <summary>
    /// |¦||¦¦
    /// 豐 fēng
    /// Abundance
    /// </summary>
    [Display(Name = "豐 fēng")]
    Abounding = 55,
    /// <summary>
    /// ¦¦||¦|
    /// 旅 lǚ
    /// The Wanderer
    /// </summary>
    [Display(Name = "旅 lǚ")]
    Sojourning = 56,
    /// <summary>
    /// ¦||¦||
    /// 巽 xùn
    /// The Gentle
    /// </summary>
    [Display(Name = "巽 xùn")]
    Ground = 57,
    /// <summary>
    /// ||¦||¦
    /// 兌 duì
    /// The Joyous
    /// </summary>
    [Display(Name = "兌 duì")]
    Open = 58,
    /// <summary>
    /// ¦|¦¦||
    /// 渙 huàn
    /// Dispersion
    /// </summary>
    [Display(Name = "渙 huàn")]
    Dispersing = 59,
    /// <summary>
    /// ||¦¦|¦
    /// 節 jié
    /// Limitation
    /// </summary>
    [Display(Name = "節 jié")]
    Articulating = 60,
    /// <summary>
    /// ||¦¦||
    /// 中孚 zhōng fú
    /// Inner Truth
    /// </summary>
    [Display(Name = "中孚 zhōng fú")]
    CentreConfirming = 61,
    /// <summary>
    /// ¦¦||¦¦
    /// 小過 xiǎo guò
    /// Small Preponderance
    /// </summary>
    [Display(Name = "小過 xiǎo guò")]
    SmallExceeding = 62,
    /// <summary>
    /// |¦|¦|¦
    /// 既濟 jì jì
    /// After Completion
    /// </summary>
    [Display(Name = "既濟 jì jì")]
    AlreadyFording = 63,
    /// <summary>
    /// ¦|¦|¦|
    /// 未濟 wèi jì
    /// Before Completion
    /// </summary>
    [Display(Name = "未濟 wèi jì")]
    NotYetFording = 64
}