﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 登录类型
/// </summary>
[Description("登录类型")]
public enum LoginType
{
    /// <summary>
    /// 匿名的, 不登录的
    /// </summary>
    [Display(Name = "匿名")]
    Anonymous,

    /// <summary>
    /// 用户名密码登录方式
    /// </summary>
    [Display(Name = "用户名密码登录")]
    Password,
    /// <summary>
    /// 手机号码登录方式
    /// </summary>
    [Display(Name = "验证码登录方式")]
    PhoneNumber,
    /// <summary>
    /// 邮件地址登录方式
    /// </summary>
    [Display(Name = "邮件地址登录方式")]
    Email,

    /// <summary>
    /// 外部账户授权登录方式, 微信, 支付宝等
    /// </summary>
    [Display(Name = "外部账户授权登录")]
    External,

    /// <summary>
    /// 验证码登录方式, 仅仅适用于一次性登录
    /// </summary>
    [Display(Name = "验证码登录方式")]
    Captcha,

    /// <summary>
    /// 其他认证方式
    /// </summary>
    [Display(Name = "其他")]
    Other
}