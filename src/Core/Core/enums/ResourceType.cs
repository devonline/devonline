﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 资源类型
/// </summary>
[Description("资源类型")]
public enum ResourceType
{
    /// <summary>
    /// 文件
    /// </summary>
    [Display(Name = "文件")]
    File,
    /// <summary>
    /// 数据
    /// </summary>
    [Display(Name = "数据")]
    Data,
    /// <summary>
    /// 连接地址
    /// </summary>
    [Display(Name = "地址")]
    Url,
    /// <summary>
    /// 元素
    /// </summary>
    [Display(Name = "元素")]
    Element,
    /// <summary>
    /// api 接口
    /// </summary>
    [Display(Name = "接口")]
    Api,
    /// <summary>
    /// 页面
    /// </summary>
    [Display(Name = "页面")]
    Page,
    /// <summary>
    /// 服务
    /// </summary>
    [Display(Name = "服务")]
    Service,
    /// <summary>
    /// 应用模块
    /// </summary>
    [Display(Name = "模块")]
    Module,
    /// <summary>
    /// 应用系统
    /// </summary>
    [Display(Name = "系统")]
    System,
    /// <summary>
    /// 所有的
    /// </summary>
    [Display(Name = "全部")]
    All = 99
}