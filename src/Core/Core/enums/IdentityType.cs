﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 身份类型
/// </summary>
[Description("身份类型")]
public enum IdentityType
{
    /// <summary>
    /// 匿名
    /// 任何人都可以访问的
    /// </summary>
    [Display(Name = "匿名")]
    Anonymous,
    /// <summary>
    /// 用户
    /// 独立个人
    /// </summary>
    [Display(Name = "用户")]
    User,
    /// <summary>
    /// 角色
    /// 角色属无嵌套和等级的用户列表
    /// </summary>
    [Display(Name = "角色")]
    Role,
    /// <summary>
    /// 组织
    /// 组织是有嵌套(上下级)关系的的用户列表
    /// </summary>
    [Display(Name = "组织")]
    Group,
    /// <summary>
    /// 级别
    /// 抽象分配类型, 级别是有层次(等级概念)的用户列表
    /// </summary>
    [Display(Name = "级别")]
    Level,
    /// <summary>
    /// 系统
    /// 系统是终端用户无法访问类型
    /// </summary>
    [Display(Name = "系统")]
    System,
    /// <summary>
    /// 全部, 所有类型
    /// </summary>
    [Display(Name = "全部")]
    All = 99
}