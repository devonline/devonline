﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 交易结果
/// </summary>
[Description("交易结果")]
public enum TradeResult
{
    /// <summary>
    /// 成功
    /// </summary>
    [Display(Name = "成功")]
    Succee,
    /// <summary>
    /// 失败
    /// </summary>
    [Display(Name = "失败")]
    Failed,
    /// <summary>
    /// 取消
    /// </summary>
    [Display(Name = "取消")]
    Cancel,

    /// <summary>
    /// 其他结果
    /// </summary>
    [Display(Name = "其他")]
    Other = 99
}