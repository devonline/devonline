﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 终端类型
/// </summary>
[Description("终端类型")]
public enum TerminalType
{
    /// <summary>
    /// 服务器端
    /// </summary>
    [Display(Name = "服务器端")]
    Server,
    /// <summary>
    /// Web 浏览器/客户端
    /// </summary>
    [Display(Name = "Web 客户端")]
    WebClient,
    /// <summary>
    /// Windows 桌面客户端
    /// </summary>
    [Display(Name = "Windows 客户端")]
    Windows,
    /// <summary>
    /// Linux 桌面客户端
    /// </summary>
    [Display(Name = "Linux 客户端")]
    Linux,
    /// <summary>
    /// 苹果 MacOS 桌面客户端
    /// </summary>
    [Display(Name = "MacOS 客户端")]
    MacOS,
    /// <summary>
    /// Android 客户端
    /// </summary>
    [Display(Name = "Android 客户端")]
    Android,
    /// <summary>
    /// iOS 客户端
    /// </summary>
    [Display(Name = "iOS 客户端")]
    IOS,
    /// <summary>
    /// 电子邮件
    /// </summary>
    [Display(Name = "电子邮件")]
    Email,
    /// <summary>
    /// 短信
    /// </summary>
    [Display(Name = "短信")]
    SMS,
    /// <summary>
    /// 微信
    /// </summary>
    [Display(Name = "微信")]
    WeChat,
    /// <summary>
    /// 微信小程序
    /// </summary>
    [Display(Name = "微信小程序")]
    WeChatApp,

    /// <summary>
    /// 其他类型
    /// </summary>
    [Display(Name = "其他")]
    Other = 99
}