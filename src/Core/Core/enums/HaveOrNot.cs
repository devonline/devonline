﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 有无
/// </summary>
[Description("有无")]
public enum HaveOrNot
{
    /// <summary>
    /// 无
    /// </summary>
    [Display(Name = "无")]
    Not,
    /// <summary>
    /// 有
    /// </summary>
    [Display(Name = "有")]
    Have
}