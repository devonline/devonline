﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.Core;

/// <summary>
/// 逻辑操作类型
/// </summary>
[Description("逻辑操作类型")]
public enum LogicType
{
    /// <summary>
    /// and 逻辑与
    /// </summary>
    [JsonProperty("and")]
    [JsonPropertyName("and")]
    [Display(Name = "逻辑与")]
    And,
    /// <summary>
    /// or 逻辑或
    /// </summary>
    [JsonProperty("or")]
    [JsonPropertyName("or")]
    [Display(Name = "逻辑或")]
    Or,
    /// <summary>
    /// not 逻辑非
    /// </summary>
    [JsonProperty("not")]
    [JsonPropertyName("not")]
    [Display(Name = "逻辑非")]
    Not
}