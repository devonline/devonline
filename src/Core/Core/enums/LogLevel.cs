﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 日志级别
/// </summary>
[Description("日志级别")]
public enum LogLevel
{
    [Display(Name = "跟踪")]
    Trace,
    [Display(Name = "调试")]
    Debug,
    [Display(Name = "细节")]
    Detail,
    [Display(Name = "简短")]
    Brief,
    [Display(Name = "信息")]
    Info,
    [Display(Name = "通知")]
    Notice,
    [Display(Name = "警告")]
    Warning,
    [Display(Name = "错误")]
    Error,
    [Display(Name = "危急")]
    Critical,
    [Display(Name = "致命")]
    Fatal,
    [Display(Name = "关闭")]
    Close
}