﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 时间刻度
/// </summary>
[Description("时间刻度")]
public enum TimeKind
{
    /// <summary>
    /// 无限的
    /// </summary>
    [Display(Name = "无限")]
    Infinite,
    /// <summary>
    /// 世纪
    /// </summary>
    [Display(Name = "世纪")]
    Century,
    /// <summary>
    /// 年
    /// </summary>
    [Display(Name = "年")]
    Year,
    /// <summary>
    /// 半年
    /// </summary>
    [Display(Name = "半年")]
    HalfAYear,
    /// <summary>
    /// 季度
    /// </summary>
    [Display(Name = "季度")]
    Quarter,
    /// <summary>
    /// 月
    /// </summary>
    [Display(Name = "月")]
    Month,
    /// <summary>
    /// 星期
    /// </summary>
    [Display(Name = "星期")]
    Week,
    /// <summary>
    /// 天
    /// </summary>
    [Display(Name = "天")]
    Day,
    /// <summary>
    /// 小时
    /// </summary>
    [Display(Name = "小时")]
    Hour,
    /// <summary>
    /// 分钟
    /// </summary>
    [Display(Name = "分钟")]
    Minute,
    /// <summary>
    /// 秒
    /// </summary>
    [Display(Name = "秒")]
    Second,
    /// <summary>
    /// 毫秒
    /// </summary>
    [Display(Name = "毫秒")]
    MilliSecond,
    /// <summary>
    /// 微秒
    /// </summary>
    [Display(Name = "微秒")]
    MicroSecond,
    /// <summary>
    /// 纳秒
    /// </summary>
    [Display(Name = "纳秒")]
    NanoSecond
}