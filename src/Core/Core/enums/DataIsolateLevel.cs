﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 数据隔离级别
/// </summary>
[Description("数据隔离级别")]
public enum DataIsolateLevel
{
    /// <summary>
    /// 不使用数据隔离功能
    /// </summary>
    [Display(Name = "不隔离")]
    None,
    /// <summary>
    /// 使用用户编号进行数据隔离
    /// </summary>
    [Display(Name = "个人级别")]
    Individual,
    /// <summary>
    /// 使用当前用户所直属组织单位编号标记当前数据
    /// </summary>
    [Display(Name = "直属级别")]
    Subordinate,
    /// <summary>
    /// 最高级别, 指使用最顶级组织单位编号标记当前数据
    /// </summary>
    [Display(Name = "最高级别")]
    TopGroup
}