﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 月份
/// </summary>
[Description("月份")]
public enum Month
{
    /// <summary>
    /// 一月
    /// </summary>
    [Display(Name = "一月"), Abbreviation("Jan"), LunisolarCalendar("正月")]
    January = 1,
    /// <summary>
    /// 二月
    /// </summary>
    [Display(Name = "二月"), Abbreviation("Feb"), LunisolarCalendar("二月")]
    February = 2,
    /// <summary>
    /// 三月
    /// </summary>
    [Display(Name = "三月"), Abbreviation("Mar"), LunisolarCalendar("三月")]
    March,
    /// <summary>
    /// 四月
    /// </summary>
    [Display(Name = "四月"), Abbreviation("Apr"), LunisolarCalendar("四月")]
    April,
    /// <summary>
    /// 五月
    /// </summary>
    [Display(Name = "五月"), Abbreviation("May"), LunisolarCalendar("五月")]
    May,
    /// <summary>
    /// 六月
    /// </summary>
    [Display(Name = "六月"), Abbreviation("Jun"), LunisolarCalendar("六月")]
    June,
    /// <summary>
    /// 七月
    /// </summary>
    [Display(Name = "七月"), Abbreviation("Jul"), LunisolarCalendar("七月")]
    July,
    /// <summary>
    /// 八月
    /// </summary>
    [Display(Name = "八月"), Abbreviation("Aug"), LunisolarCalendar("八月")]
    August,
    /// <summary>
    /// 九月
    /// </summary>
    [Display(Name = "九月"), Abbreviation("Sep"), LunisolarCalendar("九月")]
    September,
    /// <summary>
    /// 十月
    /// </summary>
    [Display(Name = "十月"), Abbreviation("Oct"), LunisolarCalendar("十月")]
    October,
    /// <summary>
    /// 十一月
    /// </summary>
    [Display(Name = "十一月"), Abbreviation("Nov"), LunisolarCalendar("冬月")]
    November,
    /// <summary>
    /// 十二月
    /// </summary>
    [Display(Name = "十二月"), Abbreviation("Dec"), LunisolarCalendar("腊月")]
    December
}