﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 安全级别
/// 安全级别是要求系统对用户校验的安全性程度要求级别
/// </summary>
[Description("安全级别")]
public enum SecurityLevel
{
    /// <summary>
    /// 不要求
    /// </summary>
    [Display(Name = "不要求")]
    None = 0,
    /// <summary>
    /// 基本要求
    /// </summary>
    [Display(Name = "基本要求")]
    Basic = 0,
    /// <summary>
    /// 安全要求
    /// </summary>
    [Display(Name = "安全要求")]
    Security = 2,
    /// <summary>
    /// 高级安全要求
    /// </summary>
    [Display(Name = "高级安全要求")]
    Advance = 3
}