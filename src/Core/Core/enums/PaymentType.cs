﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 支付方式
/// </summary>
[Description("支付方式")]
public enum PaymentType
{
    /// <summary>
    /// 微信支付
    /// </summary>
    [Display(Name = "微信支付")]
    WechatPay,
    /// <summary>
    /// 支付宝支付
    /// </summary>
    [Display(Name = "支付宝支付")]
    Alipay,
    /// <summary>
    /// 银联支付
    /// </summary>
    [Display(Name = "银联支付")]
    UnionPay,
    /// <summary>
    /// 抖音支付
    /// </summary>
    [Display(Name = "抖音支付")]
    Ulpay,
    /// <summary>
    /// 美团支付
    /// </summary>
    [Display(Name = "美团支付")]
    MeituanPay,
    /// <summary>
    /// 银行自主平台支付
    /// </summary>
    [Display(Name = "银行自主平台支付")]
    BankPlatformPay,
    /// <summary>
    /// 现金支付
    /// </summary>
    [Display(Name = "现金支付")]
    Cash,
    /// <summary>
    /// POS机刷卡支付
    /// </summary>
    [Display(Name = "POS机刷卡支付")]
    POS,

    /// <summary>
    /// 其它类型
    /// </summary>
    [Display(Name = "其他")]
    Other
}