﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 是否同意
/// </summary>
[Description("是否同意")]
public enum AgreeOrNot
{
    /// <summary>
    /// 无
    /// </summary>
    [Display(Name = "不同意")]
    Not,
    /// <summary>
    /// 有
    /// </summary>
    [Display(Name = "同意")]
    Agree
}