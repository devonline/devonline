﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 允许的类型
/// </summary>
[Description("允许的类型")]
public enum AllowType
{
    /// <summary>
    /// 允许
    /// </summary>
    [Display(Name = "允许")]
    Allow,
    /// <summary>
    /// 禁止
    /// </summary>
    [Display(Name = "禁止")]
    Forbid,
    /// <summary>
    /// 需授权
    /// </summary>
    [Display(Name = "授权")]
    Authorize
}