﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 民族
/// </summary>
[Description("民族")]
public enum Nation
{
    /// <summary>
    /// 未知
    /// </summary>
    [Display(Name = "未知")]
    Unknown = 0,
    /// <summary>
    /// 汉族
    /// </summary>
    [Display(Name = "汉族")]
    Han = 1,
    /// <summary>
    /// 蒙古族
    /// </summary>
    [Display(Name = "蒙古族")]
    Mongol = 2,
    /// <summary>
    /// 回族
    /// </summary>
    [Display(Name = "回族")]
    Hui = 3,
    /// <summary>
    /// 藏族
    /// </summary>
    [Display(Name = "藏族")]
    Tibetan = 4,
    /// <summary>
    /// 维吾尔族
    /// </summary>
    [Display(Name = "维吾尔族")]
    Uighur = 5,
    /// <summary>
    /// 苗族
    /// </summary>
    [Display(Name = "苗族")]
    Hmong = 6,
    /// <summary>
    /// 彝族
    /// </summary>
    [Display(Name = "彝族")]
    Yi = 7,
    /// <summary>
    /// 壮族
    /// </summary>
    [Display(Name = "壮族")]
    Zhuang = 8,
    /// <summary>
    /// 布依族
    /// </summary>
    [Display(Name = "布依族")]
    Buyi = 9,
    /// <summary>
    /// 朝鲜族
    /// </summary>
    [Display(Name = "朝鲜族")]
    Korean = 10,
    /// <summary>
    /// 满族
    /// </summary>
    [Display(Name = "满族")]
    Manchu = 11,
    /// <summary>
    /// 侗族
    /// </summary>
    [Display(Name = "侗族")]
    Dong = 12,
    /// <summary>
    /// 瑶族
    /// </summary>
    [Display(Name = "瑶族")]
    Yao = 13,
    /// <summary>
    /// 白族
    /// </summary>
    [Display(Name = "白族")]
    Bai = 14,
    /// <summary>
    /// 土家族
    /// </summary>
    [Display(Name = "土家族")]
    Tujia = 15,
    /// <summary>
    /// 哈尼族
    /// </summary>
    [Display(Name = "哈尼族")]
    Hani = 16,
    /// <summary>
    /// 哈萨克族
    /// </summary>
    [Display(Name = "哈萨克族")]
    Kazakh = 17,
    /// <summary>
    /// 傣族
    /// </summary>
    [Display(Name = "傣族")]
    Dai = 18,
    /// <summary>
    /// 黎族
    /// </summary>
    [Display(Name = "黎族")]
    Li = 19,
    /// <summary>
    /// 傈僳族
    /// </summary>
    [Display(Name = "傈僳族")]
    Lisu = 20,
    /// <summary>
    /// 佤族
    /// </summary>
    [Display(Name = "佤族")]
    Wa = 21,
    /// <summary>
    /// 畲族
    /// </summary>
    [Display(Name = "畲族")]
    She = 22,
    /// <summary>
    /// 高山族
    /// </summary>
    [Display(Name = "高山族")]
    Gaoshan = 23,
    /// <summary>
    /// 拉祜族
    /// </summary>
    [Display(Name = "拉祜族")]
    Lahu = 24,
    /// <summary>
    /// 水族
    /// </summary>
    [Display(Name = "水族")]
    Shui = 25,
    /// <summary>
    /// 东乡族
    /// </summary>
    [Display(Name = "东乡族")]
    Dongxiang = 26,
    /// <summary>
    /// 纳西族
    /// </summary>
    [Display(Name = "纳西族")]
    Naxi = 27,
    /// <summary>
    /// 景颇族
    /// </summary>
    [Display(Name = "景颇族")]
    Jingpo = 28,
    /// <summary>
    /// 柯尔克孜族
    /// </summary>
    [Display(Name = "柯尔克孜族")]
    Kirghiz = 29,
    /// <summary>
    /// 土族
    /// </summary>
    [Display(Name = "土族")]
    Du = 30,
    /// <summary>
    /// 达斡尔族
    /// </summary>
    [Display(Name = "达斡尔族")]
    Daur = 31,
    /// <summary>
    /// 仫佬族
    /// </summary>
    [Display(Name = "仫佬族")]
    Mulam = 32,
    /// <summary>
    /// 羌族
    /// </summary>
    [Display(Name = "羌族")]
    Qiang = 33,
    /// <summary>
    /// 布朗族
    /// </summary>
    [Display(Name = "布朗族")]
    Blang = 34,
    /// <summary>
    /// 撒拉族
    /// </summary>
    [Display(Name = "撒拉族")]
    Salar = 35,
    /// <summary>
    /// 毛南族
    /// </summary>
    [Display(Name = "毛南族")]
    Maonan = 36,
    /// <summary>
    /// 仡佬族
    /// </summary>
    [Display(Name = "仡佬族")]
    Gelao = 37,
    /// <summary>
    /// 锡伯族
    /// </summary>
    [Display(Name = "锡伯族")]
    Xibe = 38,
    /// <summary>
    /// 阿昌族
    /// </summary>
    [Display(Name = "阿昌族")]
    Achang = 39,
    /// <summary>
    /// 普米族
    /// </summary>
    [Display(Name = "普米族")]
    Pumi = 40,
    /// <summary>
    /// 塔吉克族
    /// </summary>
    [Display(Name = "塔吉克族")]
    Tajik = 41,
    /// <summary>
    /// 怒族
    /// </summary>
    [Display(Name = "怒族")]
    Nu = 42,
    /// <summary>
    /// 乌孜别克族
    /// </summary>
    [Display(Name = "乌孜别克族")]
    Uzbek = 43,
    /// <summary>
    /// 俄罗斯族
    /// </summary>
    [Display(Name = "俄罗斯族")]
    Russian = 44,
    /// <summary>
    /// 鄂温克族
    /// </summary>
    [Display(Name = "鄂温克族")]
    Evenki = 45,
    /// <summary>
    /// 德昂族
    /// </summary>
    [Display(Name = "德昂族")]
    Deang = 46,
    /// <summary>
    /// 保安族
    /// </summary>
    [Display(Name = "保安族")]
    Bonan = 47,
    /// <summary>
    /// 裕固族
    /// </summary>
    [Display(Name = "裕固族")]
    Yugur = 48,
    /// <summary>
    /// 京族
    /// </summary>
    [Display(Name = "京族")]
    Gin = 49,
    /// <summary>
    /// 塔塔尔族
    /// </summary>
    [Display(Name = "塔塔尔族")]
    Tatar = 50,
    /// <summary>
    /// 独龙族
    /// </summary>
    [Display(Name = "独龙族")]
    Drung = 51,
    /// <summary>
    /// 鄂伦春族
    /// </summary>
    [Display(Name = "鄂伦春族")]
    Oroqin = 52,
    /// <summary>
    /// 赫哲族
    /// </summary>
    [Display(Name = "赫哲族")]
    Hezhen = 53,
    /// <summary>
    /// 门巴族
    /// </summary>
    [Display(Name = "门巴族")]
    Menba = 54,
    /// <summary>
    /// 珞巴族
    /// </summary>
    [Display(Name = "珞巴族")]
    Lhoba = 55,
    /// <summary>
    /// 基诺族
    /// </summary>
    [Display(Name = "基诺族")]
    Jino = 56
}