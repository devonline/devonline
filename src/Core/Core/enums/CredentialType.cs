﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 证件类型
/// </summary>
[Description("证件类型")]
public enum CredentialType
{
    /// <summary>
    /// 身份证
    /// </summary>
    [Display(Name = "身份证")]
    IdCard,
    /// <summary>
    /// 营业执照
    /// </summary>
    [Display(Name = "营业执照")]
    BusinessLicense,
    /// <summary>
    /// 护照
    /// </summary>
    [Display(Name = "护照")]
    Passport,
    /// <summary>
    /// 驾驶证
    /// </summary>
    [Display(Name = "驾驶证")]
    DrivingLicense,
    /// <summary>
    /// 资质证书
    /// </summary>
    [Display(Name = "资质证书")]
    QualificationCertificate,
    /// <summary>
    /// 认证证书
    /// </summary>
    [Display(Name = "认证证书")]
    CertificationCertificate,
    /// <summary>
    /// 房产证
    /// </summary>
    [Display(Name = "房产证")]
    PremisesPermit,

    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "其他")]
    Other
}