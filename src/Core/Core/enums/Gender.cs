﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 性别
/// </summary>
[Description("性别")]
public enum Gender
{
    /// <summary>
    /// 未知
    /// </summary>
    [Display(Name = "未知")]
    Unknown = 0,
    /// <summary>
    /// 男
    /// </summary>
    [Display(Name = "男")]
    Male = 1,
    /// <summary>
    /// 女
    /// </summary>
    [Display(Name = "女")]
    Female = 2,
    /// <summary>
    /// 未说明
    /// </summary>
    [Display(Name = "未说明")]
    Unspecified = 9
}