﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 货币
/// </summary>
[Description("货币")]
public enum Currency
{
    /// <summary>
    /// 人民币 ¥
    /// </summary>
    [Display(Name = "人民币")]
    CNY = 0,
    /// <summary>
    /// 美元 $
    /// </summary>
    [Display(Name = "美元")]
    USD = 1,
    /// <summary>
    /// 欧元 €
    /// </summary>
    [Display(Name = "欧元")]
    EUR = 2,
    /// <summary>
    /// 港币 $
    /// </summary>
    [Display(Name = "港币")]
    HKD = 3,
    /// <summary>
    /// 英镑 £
    /// </summary>
    [Display(Name = "英镑")]
    GBP = 4,

    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "其他")]
    Other = 999
}