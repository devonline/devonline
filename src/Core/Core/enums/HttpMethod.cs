﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// http method type
/// </summary>
[Description("Http 请求类型")]
public enum HttpMethod
{
    /// <summary>
    /// http get
    /// </summary>
    [Display(Name = "GET")]
    Get,
    /// <summary>
    /// http post
    /// </summary>
    [Display(Name = "POST")]
    Post,
    /// <summary>
    /// http put
    /// </summary>
    [Display(Name = "PUT")]
    Put,
    /// <summary>
    /// http delete
    /// </summary>
    [Display(Name = "DELETE")]
    Delete,
    /// <summary>
    /// http head
    /// </summary>
    [Display(Name = "HEAD")]
    Head,
    /// <summary>
    /// http options
    /// </summary>
    [Display(Name = "OPTIONS")]
    Options,
    /// <summary>
    /// http patch
    /// </summary>
    [Display(Name = "PATCH")]
    Patch,
    /// <summary>
    /// http trace
    /// </summary>
    [Display(Name = "TRACE")]
    Trace
}