﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 数据库类型
/// </summary>
[Description("数据库类型")]
public enum DatabaseType
{
    /// <summary>
    /// PostgreSQL
    /// </summary>
    [Display(Name = "PostgreSQL")]
    PostgreSQL,
    /// <summary>
    /// MS Sql Server 数据库
    /// </summary>
    [Display(Name = "SqlServer")]
    SqlServer,
    /// <summary>
    /// Oracle 数据库
    /// </summary>
    [Display(Name = "Oracle")]
    Oracle,
    /// <summary>
    /// MySQL 数据库
    /// </summary>
    [Display(Name = "MySQL")]
    MySQL,
    /// <summary>
    /// SQLite 数据库
    /// </summary>
    [Display(Name = "SQLite")]
    SQLite,
    /// <summary>
    /// redis 数据库
    /// </summary>
    [Display(Name = "Redis")]
    Redis,
    /// <summary>
    /// MongoDB
    /// </summary>
    [Display(Name = "MongoDB")]
    MongoDB,
    /// <summary>
    /// InfluxDB 数据库
    /// </summary>
    [Display(Name = "InfluxDB")]
    InfluxDB,
    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "Other")]
    Other = 99
}