﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 审核状态
/// </summary>
[Description("审核状态")]
public enum AuditState
{
    /// <summary>
    /// 待审核
    /// </summary>
    [Display(Name = "待审核")]
    Pending,
    /// <summary>
    /// 已通过
    /// </summary>
    [Display(Name = "已通过")]
    Passed,
    /// <summary>
    /// 已拒绝
    /// </summary>
    [Display(Name = "已拒绝")]
    Rejected,
    /// <summary>
    /// 已终止
    /// </summary>
    [Display(Name = "已终止")]
    Terminated,
}