﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 数据状态
/// </summary>
[Description("数据状态")]
public enum DataState
{
    /// <summary>
    /// 可用
    /// 表示数据状态正常
    /// </summary>
    [Display(Name = "可用")]
    Available,
    /// <summary>
    /// 不可用的, 已禁用的
    /// 表示数据状态处于被禁用的状态
    /// </summary>
    [Display(Name = "不可用")]
    Unavailable,
    /// <summary>
    /// 已冻结
    /// 表示数据处于暂时被禁用的状态
    /// </summary>
    [Display(Name = "已冻结")]
    Frozen,

    /// <summary>
    /// 已更新
    /// 适用于逻辑更新
    /// </summary>
    [Display(Name = "已更新")]
    Updated = 11,
    /// <summary>
    /// 已删除
    /// 适用于逻辑删除
    /// </summary>
    [Display(Name = "已删除")]
    Deleted = 12,

    /// <summary>
    /// 草稿
    /// </summary>
    [Display(Name = "草稿")]
    Draft = 21,
    /// <summary>
    /// 已作废
    /// </summary>
    [Display(Name = "已作废")]
    Canceled = 22,
    /// <summary>
    /// 已关闭
    /// </summary>
    [Display(Name = "已关闭")]
    Closed = 23,

    /// <summary>
    /// 已丢失
    /// </summary>
    [Display(Name = "已丢失")]
    Lost = 31,
    /// <summary>
    /// 已死亡
    /// </summary>
    [Display(Name = "已死亡")]
    Died = 32,

    /// <summary>
    /// 已废弃, 过时不用的
    /// 适用于标记数据项已被废除, 但存在历史数据依赖故而留存的情况
    /// </summary>
    [Display(Name = "已废弃")]
    Obsoleted = 91,
    /// <summary>
    /// 已销毁
    /// 适用于标记数据已经被注销, 销毁, 处于不可被使用的状态
    /// </summary>
    [Display(Name = "已销毁")]
    Destroyed = 92
}