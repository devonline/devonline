﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 交易类型
/// </summary>
[Description("交易类型")]
public enum TradeType
{
    /// <summary>
    /// 支付, 充值
    /// </summary>
    [Display(Name = "支付")]
    Payment,
    /// <summary>
    /// 消费, 花费, 使用
    /// </summary>
    [Display(Name = "消费")]
    Consume,
    /// <summary>
    /// 提现, 取现
    /// </summary>
    [Display(Name = "提现")]
    WithdrawCash,
    /// <summary>
    /// 兑换
    /// </summary>
    [Display(Name = "兑换")]
    Exchange,
    /// <summary>
    /// 退款, 退费
    /// </summary>
    [Display(Name = "退款")]
    Refund,

    /// <summary>
    /// 系统
    /// </summary>
    [Display(Name = "系统")]
    System = 99
}