﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 终结点模式
/// </summary>
[Description("终结点模式")]
public enum EndpointMode
{
    /// <summary>
    /// 独立节点
    /// </summary>
    [Display(Name = "独立的")]
    Standalone,
    /// <summary>
    /// 分布式的
    /// </summary>
    [Display(Name = "分布式的")]
    Distributed,
    /// <summary>
    /// 主备式的
    /// </summary>
    [Display(Name = "主备式的")]
    Backup
}