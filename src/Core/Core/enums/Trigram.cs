﻿using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 八卦类型
/// 卦象 |¦ 阴阳顺序从右向左
/// 此枚举顺序为: 先天八卦顺序
/// </summary>
public enum Trigram
{
    /// <summary>
    /// |||
    /// 乾卦
    /// The Creative
    /// </summary>
    [Display(Name = "乾")]
    Force = 1,
    /// <summary>
    /// 兑卦
    /// ||¦
    /// The Joyous
    /// </summary>
    [Display(Name = "兑")]
    Open = 2,
    /// <summary>
    /// 离卦
    /// |¦|
    /// The Clinging
    /// </summary>
    [Display(Name = "离")]
    Radiance = 3,
    /// <summary>
    /// 震卦
    /// |¦¦
    /// Arousing
    /// </summary>
    [Display(Name = "震")]
    Shake = 4,
    /// <summary>
    /// 巽卦
    /// ¦||
    /// The Gentle
    /// </summary>
    [Display(Name = "巽")]
    Ground = 5,
    /// <summary>
    /// 坎卦
    /// ¦|¦
    /// The Abysmal Water
    /// </summary>
    [Display(Name = "坎")]
    Gorge = 6,
    /// <summary>
    /// 艮卦
    /// ¦¦|
    /// The Keeping Still
    /// </summary>
    [Display(Name = "艮")]
    Bound = 7,
    /// <summary>
    /// 坤卦
    /// ¦¦¦
    /// The Receptive
    /// </summary>
    [Display(Name = "坤")]
    Field = 8
}