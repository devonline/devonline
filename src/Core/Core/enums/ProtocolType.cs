﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 协议类型
/// </summary>
[Description("协议类型")]
public enum ProtocolType
{
    /// <summary>
    /// 以 http api 传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "http 协议")]
    Http,
    /// <summary>
    /// 以 https api 传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "https 协议")]
    Https,
    /// <summary>
    /// 以 WebSocket 传输方式定义的数据结构的协议
    /// </summary>
    [Display(Name = "WebSocket 协议")]
    WS,
    /// <summary>
    /// 以 WebSocketSecure 传输方式定义的数据结构的协议
    /// </summary>
    [Display(Name = "WebSocketSecure 协议")]
    WSS,
    /// <summary>
    /// 以 gRPC 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "gRPC 协议")]
    GRPC,
    /// <summary>
    /// 以 tcp 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "tcp 协议")]
    TCP,
    /// <summary>
    /// 以 udp 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "udp 协议")]
    UDP,
    /// <summary>
    /// 以 OPCUA 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "OPC UA 协议")]
    OPCUA,
    /// <summary>
    /// 以 Modbus 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "Modbus 协议")]
    Modbus,
    /// <summary>
    /// 以 Message Queuing Telemetry Transport 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "MQTT 协议")]
    MQTT,
    /// <summary>
    /// 以 ftp 协议传输方式定义数据结构的协议
    /// </summary>
    [Display(Name = "ftp 协议")]
    FTP,
    /// <summary>
    /// 以文本文件协议形式定义数据结构的协议
    /// </summary>
    [Display(Name = "文本文件")]
    File
}