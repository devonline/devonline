﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 数据访问方式
/// </summary>
[Flags]
[Description("数据访问方式")]
public enum DataAccess
{
    /// <summary>
    /// 未变化
    /// </summary>
    [Display(Name = "未变化")]
    NoChange = 0,
    /// <summary>
    /// 读
    /// </summary>
    [Display(Name = "读")]
    Read = 0x01,
    /// <summary>
    /// 写
    /// </summary>
    [Display(Name = "写")]
    Create = 0x02,
    /// <summary>
    /// 更新
    /// </summary>
    [Display(Name = "更新")]
    Update = 0x04,
    /// <summary>
    /// 删除
    /// </summary>
    [Display(Name = "删除")]
    Delete = 0x08,
    /// <summary>
    /// 不可删除, 即增改查
    /// </summary>
    [Display(Name = "查增改")]
    CanNotDelete = Read | Create | Update,
    /// <summary>
    /// 不限
    /// </summary>
    [Display(Name = "增删改查")]
    All = Read | Create | Update | Delete
}