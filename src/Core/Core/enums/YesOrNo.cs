﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 是否
/// </summary>
[Description("是否")]
public enum YesOrNo
{
    /// <summary>
    /// 否
    /// </summary>
    [Display(Name = "否")]
    No,
    /// <summary>
    /// 是
    /// </summary>
    [Display(Name = "是")]
    Yes
}