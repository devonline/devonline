﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 数据保护级别
/// </summary>
[Description("数据保护级别")]
public enum DataProtectionLevel
{
    /// <summary>
    /// 不使用数据保护功能
    /// </summary>
    [Display(Name = "不保护")]
    None,
    /// <summary>
    /// 使用 asp.net core DataProtection 进行数据保护
    /// 系统级别加解密
    /// </summary>
    [Display(Name = "基本数据保护")]
    Protection,
    /// <summary>
    /// 使用 Identity.Security 进行数据保护
    /// 用户级别加解密
    /// </summary>
    [Display(Name = "安全的数据保护")]
    Security
}