﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 访问级别
/// 默认访问级别为所有者, 他人无法访问
/// 除所有者, 个人和禁止的之外, 
/// 前三种满足条件即可访问, 且组织, 指的是访问规则中配置的访问对象组织, 范围更大, 且可配置
/// 后三种需要授权才能访问, 且组织, 指的是资源所有者所在组织, 范围更小, 不可配置
/// 最后一种不可访问
/// 由于组织存在嵌套, 可访问权限向上开放, 向下闭合
/// 以下 "授权" 默认指的是由管理者授权
/// </summary>
[Description("访问级别")]
public enum AccessLevel
{
    /// <summary>
    /// 所有者(人, 组织, 角色或级别)
    /// 只有所有者可以访问
    /// </summary>
    [Display(Name = "所有者")]
    Owner,
    /// <summary>
    /// 个人的
    /// 属于独立个人的, 只有个人访问, 其他人无法访问的
    /// </summary>
    [Display(Name = "个人的")]
    Personal,

    /// <summary>
    /// 公开的
    /// 任何人都可以访问
    /// </summary>
    [Display(Name = "公开的")]
    Public = 11,
    /// <summary>
    /// 受保护的
    /// 资源所有者(人, 组织, 角色或级别)授权后可公开访问
    /// </summary>
    [Display(Name = "受保护的")]
    Protected = 12,
    /// <summary>
    /// 内部的
    /// 和所有者同一组织, 角色或级别的可以公开访问的, 包括上级组织和更高的级别
    /// 即: 组织/角色/级别 多选一模式
    /// 如果授权到组织, 则所有者组织及上级组织可以访问
    /// 如果授权到角色, 则权限范围和 Owner 相同, 只有同一角色可以访问
    /// 如果授权到级别, 则级别等于或高于所有者级别可以访问
    /// 授权到用户, 系统等其他情况无意义
    /// </summary>
    [Display(Name = "内部公开的")]
    Internal = 13,

    /// <summary>
    /// 保密级别
    /// 需要组织内部到达可访问级别才可以访问的
    /// 即: 组织+级别 模式
    /// </summary>
    [Display(Name = "保密的")]
    Confidential = 21,
    /// <summary>
    /// 机密级别
    /// 需要组织内部到达可访问级别, 且有资源访问权限级别高一级的人授权才可以访问的
    /// 如资源可访问级别已经是最高, 则只有级别最高的人有访问权限
    /// 即: 组织+级别+授权 模式
    /// </summary>
    [Display(Name = "机密的")]
    Classified = 22,
    /// <summary>
    /// 绝密级别, 最高机密
    /// 需要组织内部满足可访问级别, 且有资源访问权限级别高一级的所有人联合授权才可以访问的
    /// 如资源可访问级别已经是最高, 则只有级别最高的所有人联合授权才有访问权限
    /// 即: 组织+级别+联合授权 模式
    /// </summary>
    [Display(Name = "绝密的")]
    TopSecret = 23,

    /// <summary>
    /// 禁止访问的
    /// 任何人不能访问
    /// </summary>
    [Display(Name = "禁止访问的")]
    Forbidden = 99
}