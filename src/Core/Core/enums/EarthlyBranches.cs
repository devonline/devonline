﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// 地支 dì zhǐ
/// Earthly Branches
/// 壬子凶 癸丑凶 甲寅吉 乙卯凶 丙辰吉 丁巳吉 戊午凶 己未凶 庚申吉 辛酉吉 壬戌凶 癸亥吉
/// </summary>
[Description("地支")]
public enum EarthlyBranches
{
    /// <summary>
    /// 子 zǐ
    /// 子时	23：00—01：00（三更）子时，十二时辰之一，对应现代时间的23时至1时，古时尚有午夜、子夜、夜半、夜分、宵分、未旦、未央等别称。
    /// </summary>
    [Display(Name = "子")]
    Zi = 1,
    /// <summary>
    /// 丑 chǒu
    /// 丑时	01：00—03：00（四更）丑时，十二时辰之一，又称鸡鸣，荒鸡。对应现代时间的凌晨1时至凌晨3时。如《诗经·汝日鸡鸣》：“汝日鸡鸣，士日昧旦。”
    /// </summary>
    [Display(Name = "丑")]
    Chou,
    /// <summary>
    /// 寅 yín
    /// 寅时	03：00—05：00（五更）寅时，十二时辰之一，对应现代时间的凌晨三点钟到五点钟，又称平旦、黎明、早晨、日旦等，是夜与日的交替之际。
    /// </summary>
    [Display(Name = "寅")]
    Yin,
    /// <summary>
    /// 卯 mǎo
    /// 卯时	05：00—07：00 卯时，十二时辰之一，对应现代时间的5时至7时，又名日始、破晓、旭日、日出等，指太阳刚刚露脸，冉冉初升的那段时间，为古时官署开始办公的时间，故又称点卯。
    /// </summary>
    [Display(Name = "卯")]
    Mao,
    /// <summary>
    /// 辰 chén
    /// 辰时	07：00—09：00 辰时，十二时辰之一，对应现代时间的七时至九时，别称食时。中国古时把一天划分为十二个时辰，每个时辰相等於两小时。
    /// </summary>
    [Display(Name = "辰")]
    Chen,
    /// <summary>
    /// 巳 sì
    /// 巳时	09：00—11：00 巳时，十二时辰之一，指上午9时至中午11时。隅中，又名日禺等：临近中午的时候称为隅中。
    /// </summary>
    [Display(Name = "巳")]
    Si,
    /// <summary>
    /// 午 wǔ
    /// 午时	11：00—13：00 午时，十二时辰之一，对应现代时间的11时整至13时整，即日中、日正、中午等。而正午十二时又有平午、平昼、亭午等别称。
    /// </summary>
    [Display(Name = "午")]
    Wu,
    /// <summary>
    /// 未 wèi
    /// 未时	13：00—15：00 未时，十二时辰之一，对应现代时间的13时至15时。此时太阳蹉跌而下，开始偏西，故又谓之日侧、日映。
    /// </summary>
    [Display(Name = "未")]
    Wei,
    /// <summary>
    /// 申 shēn
    /// 申时	15：00—17：00 申时，十二时辰之一，对应现代时间的15时正至17时正。别名哺时、日哺。
    /// </summary>
    [Display(Name = "申")]
    Shen,
    /// <summary>
    /// 酉 yǒu
    /// 酉时	17：00—19：00 酉时，十二时辰之一，酉时对应的时间是十七点至十九点。日入，又名日落、日沉、傍晚：意为太阳落山的时候。
    /// </summary>
    [Display(Name = "酉")]
    You,
    /// <summary>
    /// 戌 xū
    /// 戌时	19：00—21：00（一更）戌时，十二时辰之一，对应现代时间的19时至21时。别称黄昏。十二时辰是古人根据一日间太阳出没的自然规律、天色的变化以及自己日常的生产活动、生活习惯而归纳总结、独创于世的。
    /// </summary>
    [Display(Name = "戌")]
    Xu,
    /// <summary>
    /// 亥 hài
    /// 亥时	21：00—23：00（二更）亥时，十二时辰之一，对应现代时间的21时至23时。此时正是夜阑人静之夕，故又称人定；亥时又称彦夜王实甫《西厢记》：“谁著你彦夜入人家，非奸做贼拿。”
    /// </summary>
    [Display(Name = "亥")]
    Hai
}