﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Core;

/// <summary>
/// Specifies a symmetric encryption algorithm to use for providing confidentiality to protected payloads.
/// </summary>
[Description("对称加密算法")]
public enum SymmetricAlgorithm
{
    /// <summary>
    /// The AES algorithm (FIPS 197) with a 128-bit key running in Cipher Block Chaining mode.
    /// </summary>
    [Display(Name = "AES 128 CBC")]
    AES_128_CBC,
    /// <summary>
    /// The AES algorithm (FIPS 197) with a 192-bit key running in Cipher Block Chaining mode.
    /// </summary>
    [Display(Name = "AES 192 CBC")]
    AES_192_CBC,
    /// <summary>
    /// The AES algorithm (FIPS 197) with a 256-bit key running in Cipher Block Chaining mode.
    /// </summary>
    [Display(Name = "AES 256 CBC")]
    AES_256_CBC,
    /// <summary>
    /// The AES algorithm (FIPS 197) with a 128-bit key running in Galois/Counter Mode (FIPS SP 800-38D).
    /// </summary>
    /// <remarks>
    /// This cipher mode produces a 128-bit authentication tag. This algorithm is currently only
    /// supported on Windows.
    /// </remarks>
    [Display(Name = "AES 128 GCM")]
    AES_128_GCM,
    /// <summary>
    /// The AES algorithm (FIPS 197) with a 192-bit key running in Galois/Counter Mode (FIPS SP 800-38D).
    /// </summary>
    /// <remarks>
    /// This cipher mode produces a 128-bit authentication tag.
    /// </remarks>
    [Display(Name = "AES 192 GCM")]
    AES_192_GCM,
    /// <summary>
    /// The AES algorithm (FIPS 197) with a 256-bit key running in Galois/Counter Mode (FIPS SP 800-38D).
    /// </summary>
    /// <remarks>
    /// This cipher mode produces a 128-bit authentication tag.
    /// </remarks>
    [Display(Name = "AES 256 GCM")]
    AES_256_GCM
}