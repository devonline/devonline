﻿namespace Devonline.Core;

/// <summary>
/// 缩写, 定义类型/字段/属性的名称缩写
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, Inherited = true)]
public sealed class AbbreviationAttribute(string value) : Attribute
{
    /// <summary>
    /// 缩写的具体值
    /// </summary>
    public string Value { get; } = value;
}