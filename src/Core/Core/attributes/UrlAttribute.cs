﻿namespace Devonline.Core;

/// <summary>
/// Url 地址描述
/// </summary>
[AttributeUsage(AttributeTargets.All, Inherited = true)]
public sealed class UrlAttribute(string url) : Attribute
{
    /// <summary>
    /// url address
    /// </summary>
    public string Url { get; } = url;
}