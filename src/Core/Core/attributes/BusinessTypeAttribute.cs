﻿namespace Devonline.Core;

/// <summary>
/// 此特性用于设置实体对象字段对应的业务类型, 即不可重复, 可设置多个字段
/// </summary>
[AttributeUsage(AttributeTargets.All, Inherited = true)]
public sealed class BusinessTypeAttribute : Attribute
{
    /// <summary>
    /// 业务类型, 当一个集合指定了业务类型名称的值, 则作为附件即只接受唯一一种业务类型的附件
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// 是否是附件字段, true, 则为一个附件文件地址, 或者一组指定业务类型的附件集合
    /// </summary>
    public bool IsAttachment { get; set; }
}