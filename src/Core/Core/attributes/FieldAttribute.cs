﻿using System.Reflection;

namespace Devonline.Core;

/// <summary>
/// 对于将数据使用某种协议形式传输时的字段值格式转换描述
/// </summary>
[AttributeUsage(AttributeTargets.Property, Inherited = true)]
public sealed class FieldAttribute : Attribute
{
    /// <summary>
    /// 字段名
    /// </summary>
    public string Name { get; set; } = null!;
    /// <summary>
    /// field 指向的属性
    /// </summary>
    public PropertyInfo Property { get; set; } = null!;
    /// <summary>
    /// 描述
    /// </summary>
    public string? Doc { get; set; }
    /// <summary>
    /// 序号
    /// </summary>
    public int Index { get; set; }
    /// <summary>
    /// 数据类型
    /// </summary>
    public TypeCode Type { get; set; }
    /// <summary>
    /// 长度
    /// </summary>
    public int Size { get; set; }
    /// <summary>
    /// 主键列, 关键列
    /// </summary>
    public bool IsKey { get; set; }
    /// <summary>
    /// 是否文件, true, 则为一个文件名
    /// </summary>
    public bool IsFile { get; set; }
    /// <summary>
    /// 是否可为空, 如果可为空, 使用默认值占位
    /// </summary>
    public bool Nullable { get; set; }
    /// <summary>
    /// excel cell value format
    /// </summary>
    public string? Format { get; set; }
}