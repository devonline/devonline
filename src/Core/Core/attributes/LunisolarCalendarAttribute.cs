﻿namespace Devonline.Core;

/// <summary>
/// 农历, 阴阳历
/// </summary>
public class LunisolarCalendarAttribute(string value) : Attribute
{
    /// <summary>
    /// 农历的具体值
    /// </summary>
    public string Value { get; } = value;
}