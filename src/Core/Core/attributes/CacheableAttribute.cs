﻿namespace Devonline.Core;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
public sealed class CacheableAttribute : Attribute
{
    /// <summary>
    /// 是否需要缓存到客户端
    /// </summary>
    public bool LocalCache { set; get; }
    /// <summary>
    /// 当前缓存的依赖关系
    /// </summary>
    public IEnumerable<string>? Relationships { set; get; }
}