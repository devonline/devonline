﻿namespace Devonline.Core;

/// <summary>
/// 启用了数据过滤功能的
/// </summary>
[AttributeUsage(AttributeTargets.All, Inherited = true)]
public sealed class FilterableAttribute : Attribute;