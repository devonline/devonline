﻿namespace Devonline.Core;

/// <summary>
/// 此特性用于设置实体对象字段值是否唯一, 即不可重复, 可设置多个字段
/// </summary>
[AttributeUsage(AttributeTargets.Property, Inherited = true)]
public sealed class UniqueAttribute : Attribute;