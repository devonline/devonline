﻿using System.Reflection;

namespace Devonline.Core;

/// <summary>
/// excel 导入导出通用标签
/// excel Name, Size, FieldName, Format 等大多数参数可由扩展方法 GetExcelAttribute 计算得到默认值, 因此可以不做设置, 除非需要改变默认设置
/// epplus 目前仅支持导出的列头使用 DisplayName 和 Description 特性申明
/// </summary>
[AttributeUsage(AttributeTargets.Property, Inherited = true)]
public sealed class ExcelAttribute : Attribute
{
    public ExcelAttribute()
    {
        Index = -1;
        Required = false;
    }

    /// <summary>
    /// excel column Index
    /// </summary>
    public int Index { get; set; }
    /// <summary>
    /// excel column name
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// excel column size
    /// </summary>
    public float Size { get; set; }

    /// <summary>
    /// excel field is required
    /// </summary>
    public bool Required { get; set; }
    /// <summary>
    /// excel field value max input length
    /// </summary>
    public int? MaxLength { get; set; }
    /// <summary>
    /// excel cell value format
    /// </summary>
    public string? Format { get; set; }
    /// <summary>
    /// excel field info
    /// </summary>
    public PropertyInfo? Field { get; set; }
    /// <summary>
    /// excel field export type
    /// </summary>
    public Type? ExportType { get; set; }
}