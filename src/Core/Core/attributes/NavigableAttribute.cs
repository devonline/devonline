﻿namespace Devonline.Core;

/// <summary>
/// 此特性用于描述实体对象之间的主从依赖关系
/// 例如: A 有子表 B, B 总是伴随 A 一起编辑, 则在 A 的 Bs 属性设置此特性
/// 例如: A 有引用表 B, B 总是伴随 A 一起编辑, 则在 A 的属性 B 设置此特性
/// 例如: A 有引用表 B, B 总是伴随 A 一起查询, 则在 B 的被关联属性 C 设置此特性
/// 注意: 禁止循环依赖, 如 C 伴随 A, A 又伴随 C 这种情况, 只能同时在其中一个属性上设置此特性
/// 对于导航引用的数据增删改查四种数据操作方式, 使用 Mode 属性限制使用方式
/// PropertyName 用于按引用取值时使用的引用对象的字段名
/// </summary>
[AttributeUsage(AttributeTargets.Property, Inherited = true)]
public sealed class NavigableAttribute(DataAccess mode = DataAccess.CanNotDelete) : Attribute
{
    /// <summary>
    /// 数据导航访问方式
    /// </summary>
    public DataAccess Mode { get; } = mode;
    /// <summary>
    /// 设置导航指定的列名
    /// </summary>
    public string? PropertyName { get; set; }
}