﻿namespace Devonline.Core;

/// <summary>
/// 接口资源设定特性
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public sealed class AccessAuthorizeAttribute : Attribute
{
    /// <summary>
    /// 资源编号, 全局唯一的资源编号, 用于标记和查找资源的唯一编号, 不可重复
    /// 如果没有资源编号, 将从上层资源编号往下计算得到
    /// </summary>
    public string Code { get; set; } = null!;
    /// <summary>
    /// 资源类型
    /// </summary>
    public ResourceType ResourceType { get; set; } = ResourceType.Api;
    /// <summary>
    /// 资源访问级别
    /// </summary>
    public AuthorizeType AuthorizeType { get; set; } = AuthorizeType.Internal;
    /// <summary>
    /// 资源名称
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// 资源标题
    /// </summary>
    public string? Title { get; set; }
    /// <summary>
    /// 资源内容, 访问地址
    /// </summary>
    public string? Content { get; set; }
    /// <summary>
    /// 上级资源编号
    /// </summary>
    public string? Parent { get; set; }

    /// <summary>
    /// 授权访问的用户
    /// </summary>
    public string? Users { get; set; }
    /// <summary>
    /// 授权访问的角色
    /// 如果接口存在 Authorize 特性, 且不配置授权角色, 则默认分配给认证用户(Authenticator)的角色
    /// </summary>
    public string? Roles { get; set; }
    /// <summary>
    /// 授权访问的组
    /// </summary>
    public string? Groups { get; set; }
    /// <summary>
    /// 授权访问的等级
    /// </summary>
    public string? Levels { get; set; }
    /// <summary>
    /// 访问条件, 这是针对授权对象存在的情况下
    /// </summary>
    public string? Condition { get; set; }
}