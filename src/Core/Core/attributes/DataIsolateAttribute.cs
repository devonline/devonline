﻿namespace Devonline.Core;

/// <summary>
/// 此特性用于设置实体对象字段对应的业务类型, 即不可重复, 可设置多个字段
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, Inherited = true)]
public sealed class DataIsolateAttribute : Attribute
{
    /// <summary>
    /// 数据隔离级别, 默认不隔离
    /// </summary>
    public DataIsolateLevel DataIsolate { get; set; } = DataIsolateLevel.None;
    /// <summary>
    /// 数据隔离使用的字段名
    /// </summary>
    public string? Field { get; set; }
}