﻿using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Devonline.Core;

/// <summary>
/// 全局基础设置
/// </summary>
public static class AppSettings
{
    static AppSettings()
    {
        JsonSerializerSettings = new JsonSerializerSettings
        {
            //使用驼峰样式的key
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            //忽略循环引用
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            TypeNameHandling = TypeNameHandling.None,
            NullValueHandling = NullValueHandling.Ignore,
            //设置时间格式
            DateTimeZoneHandling = DateTimeZoneHandling.Local,
            DateParseHandling = DateParseHandling.DateTime,
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            //DateFormatString = DEFAULT_DATETIME_FORMAT
        };

        //枚举类型转字符串输出
        JsonSerializerSettings.Converters.Add(new StringEnumConverter());

        // 默认 json 格式化设置
        JsonSerializerOptions = new JsonSerializerOptions
        {
            AllowTrailingCommas = true,
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
            DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
            //IgnoreReadOnlyFields = false,
            //IgnoreReadOnlyProperties = false,
            //IncludeFields = false,
            MaxDepth = UNIT_EIGHT,
            NumberHandling = JsonNumberHandling.AllowReadingFromString | JsonNumberHandling.WriteAsString | JsonNumberHandling.AllowNamedFloatingPointLiterals,
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            ReadCommentHandling = JsonCommentHandling.Skip,
            WriteIndented = true
        };

        // 枚举使用字符串表示形式
        JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));

        // 时间格式使用自定义
        //JsonSerializerOptions.Converters.Add(new DateTimeConverter(DEFAULT_DATETIME_FORMAT));
        //JsonSerializerOptions.Converters.Add(new DateTimeOffsetConverter(DEFAULT_DATETIME_FORMAT));
        //JsonSerializerOptions.Converters.Add(new DateOnlyConverter(DEFAULT_DATE_FORMAT));
    }

    #region 配置文件环境变量
    /// <summary>
    /// 配置文件名
    /// </summary>
    public const string CONFIG_FILE_NAME = "appsettings";
    /// <summary>
    /// 配置文件短名
    /// </summary>
    public const string CONFIG_FILE_SHORT_NAME = "app";
    /// <summary>
    /// 开发环境变量短名
    /// </summary>
    public const string ENVIRONMENT_DEVELOPMENT_SHORT_NAME = "dev";
    /// <summary>
    /// 生产环境变量短名
    /// </summary>
    public const string ENVIRONMENT_PRODUCTION_SHORT_NAME = "pro";
    /// <summary>
    /// 稳定版环境变量短名
    /// </summary>
    public const string ENVIRONMENT_STAGING_SHORT_NAME = "stag";
    /// <summary>
    /// dotnet core 环境变量名称
    /// </summary>
    public const string DOTNETCORE_ENVIRONMENT = "DOTNETCORE_ENVIRONMENT";
    /// <summary>
    /// 日志记录的应用程序名
    /// </summary>
    public const string LOGGING_APPLICATION_NAME = "Application";
    /// <summary>
    /// 日志记录的环境变量名
    /// </summary>
    public const string LOGGING_ENVIRONMENT_NAME = "Environment";
    #endregion

    #region 常用字符定义
    /// <summary>
    /// 符号点, 默认连接符
    /// </summary>
    public const char CHAR_DOT = '.';
    /// <summary>
    /// 逗号
    /// </summary>
    public const char CHAR_COMMA = ',';
    /// <summary>
    /// 空字符
    /// </summary>
    public const char CHAR_EMPTY = '\0';
    /// <summary>
    /// 空格字符
    /// </summary>
    public const char CHAR_SPACE = ' ';
    /// <summary>
    /// 冒号
    /// </summary>
    public const char CHAR_COLON = ':';
    /// <summary>
    /// 分号
    /// </summary>
    public const char CHAR_SEMICOLON = ';';
    /// <summary>
    /// 等于号
    /// </summary>
    public const char CHAR_EQUAL = '=';
    /// <summary>
    /// 横线, 减号, 破折号
    /// </summary>
    public const char CHAR_HLINE = '-';
    /// <summary>
    /// 竖线
    /// </summary>
    public const char CHAR_VLINE = '|';
    /// <summary>
    /// * 号, 通配符
    /// </summary>
    public const char CHAR_STAR = '*';
    /// <summary>
    /// # 符号, 默认注释符号
    /// </summary>
    public const char CHAR_SHARP = '#';
    /// <summary>
    /// 下划线
    /// </summary>
    public const char CHAR_UNDERLINE = '_';
    /// <summary>
    /// 斜杠, 路径分隔符
    /// </summary>
    public const char CHAR_SLASH = '/';
    /// <summary>
    /// 反斜杠, 转义符
    /// </summary>
    public const char CHAR_BACK_SLASH = '\\';
    /// <summary>
    /// 字符 0
    /// </summary>
    public const char CHAR_ZERO = '0';
    /// <summary>
    /// 引号, 默认 sql 语句连接符等
    /// </summary>
    public const char CHAR_QUOTE = '\'';
    /// <summary>
    /// 双引号, 默认字符串表示符等
    /// </summary>
    public const char CHAR_DOUBLE_QUOTE = '\"';
    /// <summary>
    /// 撇号, 特殊字符
    /// </summary>
    public const char CHAR_APOSTROPHE = '`';
    /// <summary>
    /// 回车
    /// </summary>
    public const char CHAR_RETURN = '\r';
    /// <summary>
    /// 换行
    /// </summary>
    public const char CHAR_NEW_LINE = '\n';
    /// <summary>
    /// 字符 X, 十六进制格式化符号
    /// </summary>
    public const char CHAR_HEX = 'X';
    /// <summary>
    /// 字符 ?, 问号
    /// </summary>
    public const char CHAR_QUESTION = '?';
    /// <summary>
    /// 字符 %, 百分号
    /// </summary>
    public const char CHAR_PERCENT = '%';
    /// <summary>
    /// 字符 !, 叹号
    /// </summary>
    public const char CHAR_EXCLAMATION = '!';
    /// <summary>
    /// 字符 &, & 符号
    /// </summary>
    public const char CHAR_ADD = '&';
    /// <summary>
    /// 字符 @, @ 符号
    /// </summary>
    public const char CHAR_AT = '@';
    #endregion

    #region 常用数字单位定义
    /// <summary>
    /// 数字单位 0
    /// </summary>
    public const int UNIT_ZERO = 0;
    /// <summary>
    /// 数字单位 1
    /// </summary>
    public const int UNIT_ONE = 1;
    /// <summary>
    /// 数字单位 2
    /// </summary>
    public const int UNIT_TWO = 2;
    /// <summary>
    /// 数字单位 3
    /// </summary>
    public const int UNIT_THREE = 3;
    /// <summary>
    /// 数字单位 4, 2 的 2 次方
    /// </summary>
    public const int UNIT_FOUR = 4;
    /// <summary>
    /// 数字单位 8, 2 的 3 次方
    /// </summary>
    public const int UNIT_EIGHT = 8;
    /// <summary>
    /// 数字单位 16, 2 的 4 次方
    /// </summary>
    public const int UNIT_SIXTEEN = 16;
    /// <summary>
    /// 数字单位 K, 2 的 10 次方
    /// </summary>
    public const int UNIT_KILO = 1024;
    /// <summary>
    /// 数字单位 M, 2 的 20 次方 = 1024 * 1024
    /// </summary>
    public const int UNIT_MEGA = 1_048_576;
    /// <summary>
    /// 数字单位 G, 2 的 30 次方 = 1024 * 1024 * 1024
    /// </summary>
    public const int UNIT_GIGA = 1_073_741_824;
    /// <summary>
    /// 数字单位 T, 2 的 40 次方 = 1024 * 1024 * 1024 * 1024
    /// </summary>
    public const long UNIT_TERA = 1_099_511_627_776;
    /// <summary>
    /// 数字单位 10
    /// </summary>
    public const int UNIT_TEN = 10;
    /// <summary>
    /// 数字单位 100
    /// </summary>
    public const int UNIT_HUNDRED = 100;
    /// <summary>
    /// 数字单位 千
    /// </summary>
    public const int UNIT_THOUSAND = 1_000;
    /// <summary>
    /// 数字单位 百万
    /// </summary>
    public const int UNIT_MILLION = 1_000_000;
    /// <summary>
    /// 数字单位 十亿
    /// </summary>
    public const int UNIT_BILLION = 1_000_000_000;
    /// <summary>
    /// 数字单位 万亿
    /// </summary>
    public const long UNIT_TRILLION = 1_000_000_000_000;
    /// <summary>
    /// 数字单位: 每分钟 60 秒, 同每小时 60 分钟
    /// </summary>
    public const int UNIT_SECONDS_A_MINUTE = 60;
    /// <summary>
    /// 数字单位: 半分钟 30 秒, 同半小时 30 分钟
    /// </summary>
    public const int UNIT_SECONDS_HALF_A_MINUTE = 30;
    /// <summary>
    /// 数字单位: 每天 24 小时
    /// </summary>
    public const int UNIT_HOURS_A_DAY = 24;
    /// <summary>
    /// 数字单位: 半天 12 小时
    /// </summary>
    public const int UNIT_HOURS_HALF_A_DAY = 12;
    /// <summary>
    /// 数字单位: 每年 12 个月
    /// </summary>
    public const int UNIT_MONTHS_A_YEAR = 12;
    /// <summary>
    /// 数字单位: 每周 7 天, 精确值
    /// </summary>
    public const int UNIT_DAYS_A_WEEK = 7;
    /// <summary>
    /// 数字单位: 每月平均 30 天, 不精确值
    /// </summary>
    public const int UNIT_DAYS_A_MONTH = 30;
    /// <summary>
    /// 数字单位: 每年平均 365 天, 不精确值
    /// </summary>
    public const int UNIT_DAYS_A_YEAR = 365;
    /// <summary>
    /// 数字单位: 每小时 60 分钟, 同每分钟 60 秒
    /// </summary>
    public const int UNIT_MINUTES_AN_HOUR = UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 数字单位: 半小时 30 分钟, 同半分钟 30 秒
    /// </summary>
    public const int UNIT_MINUTES_HALF_AN_HOUR = UNIT_SECONDS_HALF_A_MINUTE;
    /// <summary>
    /// 数字单位: 一小时有多少秒, 精确值
    /// </summary>
    public const int UNIT_SECONDS_AN_HOUR = UNIT_SECONDS_A_MINUTE * UNIT_MINUTES_AN_HOUR;
    /// <summary>
    /// 数字单位: 一天有多少秒, 精确值
    /// </summary>
    public const int UNIT_SECONDS_A_DAY = UNIT_HOURS_A_DAY * UNIT_SECONDS_AN_HOUR;
    /// <summary>
    /// 数字单位: 一天能有多少分钟, 精确值
    /// </summary>
    public const int UNIT_MINUTES_A_DAY = UNIT_MINUTES_AN_HOUR * UNIT_HOURS_A_DAY;
    /// <summary>
    /// 数字单位: 中国时区, 东八区, UTC+8=480 分钟
    /// </summary>
    public const int UNIT_TIME_ZONE_CHINA = UNIT_MINUTES_AN_HOUR * UNIT_EIGHT;
    #endregion

    #region 默认端口号定义
    /// <summary>
    /// 默认的 http 协议端口号
    /// </summary>
    public const int DEFAULT_PORT_Http = 80;
    /// <summary>
    /// 默认的 https 协议端口号
    /// </summary>
    public const int DEFAULT_PORT_HTTPS = 443;
    /// <summary>
    /// 默认的 ftp 协议端口号
    /// </summary>
    public const int DEFAULT_PORT_FTP = 21;
    /// <summary>
    /// 默认的 ssh 协议端口号
    /// </summary>
    public const int DEFAULT_PORT_SSH = 22;
    /// <summary>
    /// 默认的 Redis 端口号
    /// </summary>
    public const int DEFAULT_PORT_REDIS = 6379;
    /// <summary>
    /// 默认的 PostgreSQL 数据库端口号
    /// </summary>
    public const int DEFAULT_PORT_POSTGRESQL = 5432;
    /// <summary>
    /// 默认的 SqlServer 数据库端口号
    /// </summary>
    public const int DEFAULT_PORT_SQLSERVER = 1433;
    /// <summary>
    /// 默认的 MySql 数据库端口号
    /// </summary>
    public const int DEFAULT_PORT_MYSQL = 3306;
    /// <summary>
    /// 默认的 Oracle 数据库端口号
    /// </summary>
    public const int DEFAULT_PORT_ORACLE = 1521;
    /// <summary>
    /// 默认的 InfluxDB 数据库端口号
    /// </summary>
    public const int DEFAULT_PORT_INFLUXDB = 8086;
    /// <summary>
    /// 默认的 MongoDB 数据库端口号
    /// </summary>
    public const int DEFAULT_PORT_MONGODB = 27017;
    /// <summary>
    /// 默认的 通讯中心 协议端口号
    /// </summary>
    public const int DEFAULT_PORT_COMMUNICATION = 9527;
    #endregion

    #region 公共常量定义       
    /// <summary>
    /// 默认分页大小
    /// </summary>
    public const int DEFAULT_PAGE_SIZE = 10;
    /// <summary>
    /// 默认页码
    /// </summary>
    public const int DEFAULT_PAGE_INDEX = 1;
    /// <summary>
    /// 二进制默认字符串占位符
    /// </summary>
    public const byte PADDING_CHAR = 0x20;
    /// <summary>
    /// 二进制默认数字占位符
    /// </summary>
    public const byte PADDING_NUMBER = 0x00;
    /// <summary>
    /// 默认错误路径名
    /// </summary>
    public const string DEFAULT_ERROR_PATH = "errors/";
    /// <summary>
    /// 默认 api 路由前缀
    /// </summary>
    public const string DEFAULT_API_ROUTE_PREFIX = "api";
    /// <summary>
    /// 默认 odata 路由前缀
    /// </summary>
    public const string DEFAULT_ODATA_ROUTE_PREFIX = "odata";
    /// <summary>
    /// 默认 lambda 表达式变量名
    /// </summary>
    public const string DEFAULT_LAMBDA_PARAMETER = "arg";
    /// <summary>
    /// 默认连接符 -
    /// </summary>
    public const string DEFAULT_CONNECTOR_STRING = "-";
    /// <summary>
    /// 默认分隔符, 通用风格
    /// </summary>
    public const string DEFAULT_SPLITER_STRING = ", ";
    /// <summary>
    /// 默认内部分隔符, json 风格
    /// </summary>
    public const string DEFAULT_INNER_SPLITER = ": ";
    /// <summary>
    /// 默认外部分隔符, json 风格
    /// </summary>
    public const string DEFAULT_OUTER_SPLITER = ", ";
    /// <summary>
    /// 默认分段符
    /// </summary>
    public const string DEFAULT_SECTION_STRING = "; ";
    /// <summary>
    /// 默认协议分隔符 ://
    /// </summary>
    public const string DEFAULT_PROTOCOL_SPLITER = "://";
    /// <summary>
    /// URL 内部分隔符
    /// </summary>
    public const string DEFAULT_URL_INNER_SPLITER = "=";
    /// <summary>
    /// URL 外部分隔符
    /// </summary>
    public const string DEFAULT_URL_OUTER_SPLITER = "&";
    /// <summary>
    /// 默认格式化字符串
    /// </summary>
    public const string DEFAULT_FORMAT_STRING = "{0}";
    /// <summary>
    /// 默认格式化二进制字符串
    /// </summary>
    public const string DEFAULT_FORMAT_HEX_STRING = "{0:X2}";
    /// <summary>
    /// 插件和流程代码中的默认执行方法
    /// </summary>
    public const string DEFAULT_EXECUTE_METHOD = "Execute";
    /// <summary>
    /// 插件和流程代码中的默认异步执行方法
    /// </summary>
    public const string DEFAULT_EXECUTE_ASYNC_METHOD = "ExecuteAsync";
    /// <summary>
    /// 文本文件扩展名
    /// </summary>
    public const string DEFAULT_TEXT_FILE_EXTENSION = ".txt";
    /// <summary>
    /// 配置文件扩展名
    /// </summary>
    public const string DEFAULT_CONFIG_FILE_EXTENSION = ".json";
    /// <summary>
    /// 日志文件扩展名
    /// </summary>
    public const string DEFAULT_LOG_FILE_EXTENSION = ".log";
    /// <summary>
    /// 程序集文件扩展名
    /// </summary>
    public const string DEFAULT_DLL_FILE_EXTENSION = ".dll";
    /// <summary>
    /// 压缩文件扩展名
    /// </summary>
    public const string DEFAULT_ZIP_FILE_EXTENSION = ".zip";
    /// <summary>
    /// 默认图片文件扩展名
    /// </summary>
    public const string DEFAULT_IMAGE_FILE_EXTENSION = ".jpg";
    /// <summary>
    /// 默认临时文件扩展名
    /// </summary>
    public const string DEFAULT_TEMPORARY_FILE_EXTENSION = ".temp";
    /// <summary>
    /// 默认 sql 文件扩展名
    /// </summary>
    public const string DEFAULT_SQL_FILE_EXTENSION = ".sql";
    /// <summary>
    /// 默认 pdf 文件扩展名
    /// </summary>
    public const string DEFAULT_PDF_FILE_EXTENSION = ".pdf";
    /// <summary>
    /// 默认 Excel 文件扩展名, Excel 07 以上格式
    /// </summary>
    public const string DEFAULT_EXCEL_FILE_EXTENSION = ".xlsx";
    /// <summary>
    /// 默认 Excel xls 文件扩展名
    /// </summary>
    public const string DEFAULT_EXCEL_FILE_EXTENSION_XLS = ".xls";
    /// <summary>
    /// 默认 Excel csv 文件扩展名
    /// </summary>
    public const string DEFAULT_EXCEL_FILE_EXTENSION_CSV = ".csv";
    /// <summary>
    /// 默认 x.509 证书文件扩展名
    /// </summary>
    public const string DEFAULT_EXCEL_FILE_EXTENSION_CERTIFICATE = ".pfx";
    /// <summary>
    /// 日期格式
    /// </summary>
    public const string DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    /// <summary>
    /// 时间格式
    /// </summary>
    public const string DEFAULT_TIME_FORMAT = "HH:mm:ss";
    /// <summary>
    /// 中文时间格式
    /// </summary>
    public const string DEFAULT_DATE_FORMAT_CN = "yyyy年MM月dd日";
    /// <summary>
    /// 全时间格式
    /// </summary>
    public const string DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /// <summary>
    /// 全时间格式
    /// </summary>
    public const string DEFAULT_DATETIMEOFFSET_FORMAT = "yyyy-MM-dd HH:mm:ss.fffffff zzzz";
    /// <summary>
    /// 时间文字格式
    /// </summary>
    public const string DEFAULT_DATETIME_FORMAT_CN = "yyyy年MM月dd日 HH时mm分ss秒";
    /// <summary>
    /// 时间格式的年月部分
    /// </summary>
    public const string DEFAULT_DATE_MONTH_FORMAT = "yyyy-MM";
    /// <summary>
    /// 文件中日期格式
    /// </summary>
    public const string DEFAULT_FILE_DATE_FORMAT = "yyyyMMdd";
    /// <summary>
    /// 文件中时间格式
    /// </summary>
    public const string DEFAULT_FILE_DATETIME_FORMAT = "yyyyMMddHHmmss";
    /// <summary>
    /// 默认 ISO 全时间格式
    /// </summary>
    public const string DEFAULT_ISO_DATETIME_FORMAT = "yyyy-MM-ddTHH:mm:ssZ";
    /// <summary>
    /// 默认 ISO 全时间格式, 包含时区
    /// </summary>
    public const string DEFAULT_ISO_DATETIME_WITH_TIMEZONE_FORMAT = "yyyy-MM-ddTHH:mm:ssK";
    /// <summary>
    /// 默认内容类型
    /// </summary>
    public const string DEFAULT_CONTENT_TYPE = "application/json;charset=utf-8";
    /// <summary>
    /// 默认文本编码
    /// </summary>
    public const string DEFAULT_ENCODING = "utf-8";
    /// <summary>
    /// 默认跨域规则名称
    /// </summary>
    public const string DEFAULT_CORS_POLICY = "AllowSpecificOrigins";
    /// <summary>
    /// 基于配置的默认授权策略规则名称
    /// </summary>
    public const string DEFAULT_AUTHORIZATION_POLICY = "ConfigurationBasedUserPermission";
    /// <summary>
    /// 默认的认证 scheme 
    /// </summary>
    public const string DEFAULT_AUTHENTICATION_SCHEME = "Bearer";
    /// <summary>
    /// 不存在
    /// </summary>
    public const string DOES_NOT_EXIST = "doesn't exist";
    /// <summary>
    /// 缓存级别 应用程序缓存
    /// </summary>
    public const string CACHE_APPLICATION = "CACHE_APP_";
    /// <summary>
    /// 缓存级别 数据缓存
    /// </summary>
    public const string CACHE_DATA = "CACHE_DATA_";
    /// <summary>
    /// 缓存级别 用户缓存
    /// </summary>
    public const string CACHE_USER = "CACHE_USER_";
    /// <summary>
    /// 缓存级别 会话缓存
    /// </summary>
    public const string CACHE_SESSION = "CACHE_SESSION_";
    /// <summary>
    /// 缓存级别 用户身份选择
    /// </summary>
    public const string CACHE_USER_IDENTITY_TYPE = "CACHE_USER_IDENTITY_TYPE_";
    /// <summary>
    /// 缓存级别 用户是否已取得授权
    /// </summary>
    public const string CACHE_USER_HAS_AUTHORIZED_ACCESS = "CACHE_USER_HAS_AUTHORIZED_ACCESS_";
    /// <summary>
    /// 系统组, 内置成员账户组
    /// </summary>
    public const string GROUP_SYSTEM = "systems";
    /// <summary>
    /// 超级管理员组
    /// </summary>
    public const string GROUP_ADMINISTRATOR = "administrators";
    /// <summary>
    /// 开发人员用户组
    /// </summary>
    public const string GROUP_DEVELOPER = "developers";
    /// <summary>
    /// 维护者用户组, 包含了超级管理员和开发者
    /// </summary>
    public const string GROUP_MAINTAINERS = "administrators, developers";
    /// <summary>
    /// 授权者
    /// </summary>
    public const string GROUP_AUTHORIZER = "authorizers";
    /// <summary>
    /// 认证用户
    /// </summary>
    public const string GROUP_AUTHENTICATOR = "authenticators";
    /// <summary>
    /// 业务管理员
    /// </summary>
    public const string GROUP_BUSINESS_ADMINISTRATOR = "businessAdministrators";
    /// <summary>
    /// 外部管理员
    /// </summary>
    public const string GROUP_EXTERNAL_ADMINISTRATOR = "externalAdministrators";
    /// <summary>
    /// 匿名用户组
    /// </summary>
    public const string GROUP_ANONYMOUS = "anonymous";
    /// <summary>
    /// 内置角色组, 系统初始化时必须存在的角色, 不可编辑和删除
    /// </summary>
    public const string BUILDIN_ROLES = "systems, administrators, developers, authorizers, businessAdministrators, externalAdministrators, authenticators, anonymous";
    /// <summary>
    /// 系统用户
    /// </summary>
    public const string USER_SYSTEM = "system";
    /// <summary>
    /// 匿名用户
    /// </summary>
    public const string USER_ANONYMOUS = "anonymous";
    /// <summary>
    /// 认证用户
    /// </summary>
    public const string USER_AUTHENTICATOR = "authenticator";
    /// <summary>
    /// 默认超级管理员
    /// </summary>
    public const string USER_ADMINISTRATOR = "administrator";
    /// <summary>
    /// 默认开发者用户
    /// </summary>
    public const string USER_DEVELOPER = "developer";
    /// <summary>
    /// 维护者用户, 包含了超级管理员和开发者
    /// </summary>
    public const string USER_MAINTAINERS = "administrator, developer";
    /// <summary>
    /// 内置用户组, 系统初始化时必须存在的用户, 不可编辑和删除
    /// </summary>
    public const string BUILDIN_USERS = "system, administrator, developer, anonymous";
    /// <summary>
    /// Claim ResourceType of id
    /// </summary>
    public const string CLAIM_TYPE_ID = "id";
    /// <summary>
    /// Claim ResourceType of user id
    /// </summary>
    public const string CLAIM_TYPE_USER_ID = "userId";
    /// <summary>
    /// Claim ResourceType of group id
    /// </summary>
    public const string CLAIM_TYPE_GROUP_ID = "groupId";
    /// <summary>
    /// Claim ResourceType of data isolate id
    /// </summary>
    public const string CLAIM_TYPE_DATA_ISOLATE_ID = "dataIsolateId";
    /// <summary>
    /// Claim ResourceType of jwt subject
    /// </summary>
    public const string CLAIM_TYPE_JWT_SUBJECT = "sub";
    /// <summary>
    /// Claim ResourceType of user's name
    /// </summary>
    public const string CLAIM_TYPE_NAME = "name";
    /// <summary>
    /// Claim ResourceType of user's userName
    /// </summary>
    public const string CLAIM_TYPE_USER_NAME = "userName";
    /// <summary>
    /// Claim ResourceType of user identifier
    /// </summary>
    public const string CLAIM_TYPE_USER_IDENTIFIER = "userIdentifier";
    /// <summary>
    /// Claim ResourceType of user identifier name, 用户姓名本来存储于 name 字段, 但是有些系统需要将 name 字段用于用户标识, 即作用与 userName 相同, 所以增加了 userIdentifierName 字段, 用于标识用户姓名
    /// </summary>
    public const string CLAIM_TYPE_USER_NAME_IDENTIFIER = "nameIdentifier";
    /// <summary>
    /// Claim ResourceType of user's state
    /// </summary>
    public const string CLAIM_TYPE_USER_STATE = "state";
    /// <summary>
    /// Claim ResourceType of user's phoneNumber
    /// </summary>
    public const string CLAIM_TYPE_PHONE_NUMBER = "phoneNumber";
    /// <summary>
    /// Claim ResourceType of user's opemId
    /// </summary>
    public const string CLAIM_TYPE_OPEN_ID = "openId";
    /// <summary>
    /// Claim ResourceType of user's image
    /// </summary>
    public const string CLAIM_TYPE_IMAGE = "image";
    /// <summary>
    /// Claim ResourceType of user's headimgurl
    /// </summary>
    public const string CLAIM_TYPE_HEAD_IMG_URL = "headimgurl";
    /// <summary>
    /// Claim ResourceType of user's head image
    /// </summary>
    public const string CLAIM_TYPE_HEAD_IMAGE = "headImage";
    /// <summary>
    /// Claim ResourceType of user's Alias
    /// </summary>
    public const string CLAIM_TYPE_ALIAS = "alias";
    /// <summary>
    /// Claim ResourceType of user's RedirectUri
    /// </summary>
    public const string CLAIM_TYPE_REDIRECT_URI = "redirect_uri";
    /// <summary>
    /// Claim ResourceType of user's RedirectUrl
    /// </summary>
    public const string CLAIM_TYPE_REDIRECT_URL = "redirect_url";
    /// <summary>
    /// Claim ResourceType of user's HasRealNameAuthentication
    /// </summary>
    public const string CLAIM_TYPE_HAS_REAL_NAME_AUTH = "hasRealNameAuthentication";
    /// <summary>
    /// 保存真实 IP 地址的 http header 名称
    /// </summary>
    public const string HTTP_HEADER_REAL_IP = "X-Real-IP";
    /// <summary>
    /// 保存真实 IP 地址的 http header 名称
    /// </summary>
    public const string HTTP_HEADER_FORWARDED_FOR = "X-Forwarded-For";
    /// <summary>
    /// bool 字符串的默认字符串表示形式: true
    /// </summary>
    public const string DEFAULT_VALUE_TRUE = "true";
    /// <summary>
    /// bool 字符串的默认字符串表示形式: false
    /// </summary>
    public const string DEFAULT_VALUE_FALSE = "false";
    /// <summary>
    /// ContentType of image
    /// </summary>
    public const string CONTENT_TYPE_IMAGE = "image";
    /// <summary>
    /// ContentType of video
    /// </summary>
    public const string CONTENT_TYPE_VIDEO = "video";
    #endregion

    #region about file operation
    /// <summary>
    /// 图片文件缩略图默认前缀
    /// </summary>
    public const string DEFAULT_IMAGE_THUMBNAIL_PREFIX = "thumb_";
    /// <summary>
    /// 图片文件缩略图默认尺寸, 宽高像素数, 默认 128
    /// </summary>
    public const int DEFAULT_IMAGE_THUMBNAIL_SIZE = 128;
    /// <summary>
    /// 图片文件裁剪文件默认前缀
    /// </summary>
    public const string DEFAULT_IMAGE_CROP_PREFIX = "crop_";
    /// <summary>
    /// 视频文件抽帧文件默认前缀
    /// </summary>
    public const string DEFAULT_VIDEO_CAPTURE_PREFIX = "capture_";
    /// <summary>
    /// 图片文件裁剪文件默认尺寸, 宽高像素数, 默认 1280, 用于进行人脸识别等用途时先对图片进行裁剪在使用裁剪后的图片文件, 否则原文件会因为尺寸过大失败或处理速度过慢
    /// </summary>
    public const int DEFAULT_IMAGE_CROP_SIZE = 1280;
    /// <summary>
    /// 默认附件集合名称
    /// </summary>
    public const string DEFAULT_ATTACHMENT_NAME = "Attachments";
    /// <summary>
    /// 默认附件路径
    /// </summary>
    public const string DEFAULT_ATTACHMENT_PATH = "attachments/";
    /// <summary>
    /// 临时文件变量名, 即不记录到数据库的文件设置开关
    /// 这是一个标记, 需要从 http request 中获取该字段的值, 以判断附件是否需要保存
    /// </summary>
    public const string TEMPORARY_ATTACHMENT = nameof(TEMPORARY_ATTACHMENT);
    /// <summary>
    /// 默认支持的可上传文件扩展名
    /// </summary>
    public const string DEFAULT_ALLOW_FILE_EXTENSIONS = ".PDF,.TXT,.LOG,.MD,.JPG,.JPEG,.PNG,.BMP,.GIF,.ICO,.XLSX,.XLS,.DOCX,.DOC,.PPTX,.PPT,.CSV,.MP3,.WMA,.WAV,.M4A,.MP4,.MKV,.AVI,.MOV,.FLV,.ZIP,.RAR,.7Z,.GZ";
    /// <summary>
    /// 默认支持的图片可上传文件扩展名
    /// </summary>
    public const string DEFAULT_IMAGE_FILE_EXTENSIONS = ".JPG,.JPEG,.PNG,.BMP";
    /// <summary>
    /// 视频文件支持的上传文件扩展名
    /// </summary>
    public const string DEFAULT_VIDEO_FILE_EXTENSIONS = ".MP4,.MKV,.AVI,.MOV,.FLV";
    /// <summary>
    /// 默认单附件文件大小, 为 10MB
    /// </summary>
    public const int DEFAULT_MAX_FILE_SIZE = UNIT_TEN * UNIT_MEGA;
    /// <summary>
    /// 默认总附件文件大小, 为 100MB
    /// </summary>
    public const int DEFAULT_TOTAL_FILE_SIZE = UNIT_HUNDRED * UNIT_MEGA;
    /// <summary>
    /// 默认每个表格多少行, 用于导入导出时的单页最大数量
    /// </summary>
    public const int DEFAULT_PAGE_SIZE_OF_ONE_SHEET = UNIT_TEN * UNIT_THOUSAND;
    /// <summary>
    /// 默认下载的 Excel 文件内容类型
    /// </summary>
    public const string CONTENT_TYPE_OF_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    /// <summary>
    /// 默认下载的 Excel xls 格式文件内容类型
    /// </summary>
    public const string CONTENT_TYPE_OF_EXCEL_XLS = "application/vnd.ms-excel";
    /// <summary>
    /// 默认 excel 列宽
    /// </summary>
    public const int DEFAULT_EXCEL_COLUMN_WIDTH = UNIT_TEN;
    /// <summary>
    /// 默认 excel 序号列列名
    /// </summary>
    public const string DEFAULT_EXCEL_INDEX_FIELD_NAME = "RowIndex";
    /// <summary>
    /// 默认 excel 序号列显示名
    /// </summary>
    public const string DEFAULT_EXCEL_INDEX_COLUMN_NAME = "序号";
    /// <summary>
    /// 默认 excel 字符串类型值格式
    /// </summary>
    public const string DEFAULT_EXCEL_STRING_FORMAT = "@";
    /// <summary>
    /// 默认 excel 整数类型值格式
    /// </summary>
    public const string DEFAULT_EXCEL_NUMBER_FORMAT = "#,##0";
    /// <summary>
    /// 默认 excel 浮点数类型值格式
    /// </summary>
    public const string DEFAULT_EXCEL_DECIMAL_FORMAT = "#,##0.00";
    /// <summary>
    /// 默认 excel 字符串类型值长度
    /// </summary>
    public const int DEFAULT_EXCEL_STRING_SIZE = 30;
    /// <summary>
    /// 默认 excel 整数类型值长度
    /// </summary>
    public const int DEFAULT_EXCEL_NUMBER_SIZE = 10;
    /// <summary>
    /// 默认 excel 浮点数类型值长度
    /// </summary>
    public const int DEFAULT_EXCEL_DECIMAL_SIZE = 15;
    /// <summary>
    /// 默认 excel 时间类型值长度
    /// </summary>
    public const int DEFAULT_EXCEL_DATETIME_SIZE = 20;
    #endregion

    #region about query options
    /// <summary>
    /// count 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_COUNT = "$count";
    /// <summary>
    /// top 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_TOP = "$top";
    /// <summary>
    /// skip 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_SKIP = "$skip";
    /// <summary>
    /// skip 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_FILTER = "$filter";
    /// <summary>
    /// orderby 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_ORDERBY = "$orderby";
    /// <summary>
    /// select 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_SELECT = "$select";
    /// <summary>
    /// expand 查询选项参数名
    /// </summary>
    public const string QUERY_OPTION_EXPAND = "$expand";
    /// <summary>
    /// 是否执行底层模糊查询, 以及执行那些字段的查询, 形如: id; id, name; 等...
    /// </summary>
    public const string QUERY_REQUEST_FILTER = "filter";
    #endregion

    #region 全局设定
    /// <summary>
    /// application startup path
    /// Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
    /// </summary>
    public static readonly string StartupPath = AppContext.BaseDirectory;
    /// <summary>
    /// json 全局设置 system.text.json
    /// </summary>
    public static JsonSerializerOptions JsonSerializerOptions { get; private set; }
    /// <summary>
    /// json 全局设置 newtonsoft.json
    /// </summary>
    public static JsonSerializerSettings JsonSerializerSettings { get; private set; }
    #endregion
}