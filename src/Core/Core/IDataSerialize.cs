﻿namespace Devonline.Core;

/// <summary>
/// 数据与特定类型的序列化和反序列化
/// </summary>
/// <typeparam name="T">数据类型</typeparam>
public interface IDataSerialize<T>
{
    /// <summary>
    /// 将当前对象序列化为特定类型
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    /// <returns></returns>
    T Serialize();
    /// <summary>
    /// 从一个特定类型反序列化到当前对象
    /// </summary>
    /// <typeparam name="T">数据类型</typeparam>
    /// <param name="data">特定类型的实例对象</param>
    /// <returns></returns>
    void Deserialize(T data);
}

/// <summary>
/// 数据与二进制流的序列化和反序列化
/// </summary>
public interface IDataSerialize : IDataSerialize<byte[]>;
/// <summary>
/// 数据与字符串的序列化和反序列化
/// </summary>
public interface IStringSerialize : IDataSerialize<string>;