﻿using Microsoft.Extensions.DependencyInjection;

namespace Devonline.Core;

/// <summary>
/// 默认的基础配置
/// </summary>
public class AppSetting
{
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// 描述
    /// </summary>
    public string? Doc { get; set; }
    /// <summary>
    /// 启动站点配置
    /// </summary>
    public string? Hosts { get; set; }
    /// <summary>
    /// aspnetcore 启动站点配置
    /// </summary>
    public string? Urls { get; set; }
    /// <summary>
    /// 允许访问的地址
    /// </summary>
    public string? AllowedHosts { get; set; }
    /// <summary>
    /// 跨域配置
    /// </summary>
    public string? CorsOrigins { get; set; }
    /// <summary>
    /// 数据迁移作用的程序集
    /// </summary>
    public string? MigrationsAssembly { get; set; }
    /// <summary>
    /// 默认应用数据库连接字符串
    /// </summary>
    public string? ApplicationDbContext { get; set; }
    /// <summary>
    /// 系统使用的时间类型, 默认: UTC 时间
    /// </summary>
    public DateTimeKind DateTimeKind { get; set; } = DateTimeKind.Utc;
    /// <summary>
    /// 发布协议, 此处仅支持 Http 和 Https, 默认 Https
    /// </summary>
    public ProtocolType ProtocolType { get; set; } = ProtocolType.Https;
    /// <summary>
    /// 数据库类型
    /// </summary>
    public DatabaseType DatabaseType { get; set; } = DatabaseType.PostgreSQL;
    /// <summary>
    /// 数据隔离级别, 默认不隔离
    /// </summary>
    public DataIsolateLevel DataIsolate { get; set; } = DataIsolateLevel.Subordinate;
    /// <summary>
    /// 数据保护级别
    /// </summary>
    public DataProtectionLevel ProtectionLevel { get; set; } = DataProtectionLevel.Protection;
    /// <summary>
    /// 默认的服务生命周期
    /// </summary>
    public ServiceLifetime ServiceLifetime { get; set; } = ServiceLifetime.Scoped;
    /// <summary>
    /// 安全要求级别, 基本安全要求, 在登录后确认用户身份时, 需要使用手机号码短信验证码验证
    /// </summary>
    public SecurityLevel SecurityLevel { get; set; } = SecurityLevel.Basic;

    /// <summary>
    /// 缓存设置
    /// </summary>
    public CacheSetting Cache { get; set; } = new();
    /// <summary>
    /// 任务执行相关配置选型
    /// </summary>
    public ExecuteSetting Execute { get; set; } = new();
}

/// <summary>
/// 缓存相关设置
/// </summary>
public class CacheSetting
{
    /// <summary>
    /// 默认缓存数据库连接字符串
    /// </summary>
    public string? Configuration { get; set; }
    /// <summary>
    /// 消息中心在 redis 中记录的消息前缀
    /// </summary>
    public string? Prefix { get; set; } = "dev_";
    /// <summary>
    /// 缓存过期时间, 单位: 天, 默认一周
    /// </summary>
    public int ExpireTime { get; set; } = AppSettings.UNIT_DAYS_A_WEEK;
    /// <summary>
    /// 缓存保留时间, 单位: 天, 默认为十天
    /// </summary>
    public int ReserveTime { get; set; } = AppSettings.UNIT_TEN;
}

/// <summary>
/// 任务执行相关配置选项
/// </summary>
public class ExecuteSetting
{
    /// <summary>
    /// 启用当前操作功能
    /// </summary>
    public bool Enable { get; set; }
    /// <summary>
    /// 开始执行时间, 不设置则立即执行
    /// </summary>
    public DateTime? StartTime { get; set; }
    /// <summary>
    /// 结束时间, 不设置则一直执行
    /// </summary>
    public DateTime? EndTime { get; set; }
    /// <summary>
    /// 超时时间, 单位: 秒, 默认: 30秒
    /// </summary>
    public int Timeout { get; set; } = AppSettings.UNIT_SECONDS_HALF_A_MINUTE;
    /// <summary>
    /// 执行间隔, 单位秒
    /// </summary>
    public int ExecuteInterval { get; set; } = AppSettings.UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 查询间隔, 单位毫秒
    /// </summary>
    public int QueryInterval { get; set; } = AppSettings.UNIT_HUNDRED;
    /// <summary>
    /// 读数据的最大间隔时间, 即单次读取数据在最长等待多长时间后就要进行下一次读取, 单位: 秒
    /// </summary>
    public int ReadInterval { get; set; } = AppSettings.UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 写数据的最大等待时间, 即单次写入数据时最长等待多长时间就要写入一次数据库, 无论是否有足够的数据, 单位: 秒
    /// </summary>
    public int WriteInterval { get; set; } = AppSettings.UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 监控间隔, 单位: 秒
    /// </summary>
    public int MonitorInterval { get; set; } = AppSettings.UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 最大尝试次数, 操作失败时, 最大尝试次数
    /// </summary>
    public int MaxRetryCount { get; set; } = AppSettings.UNIT_THREE;
    /// <summary>
    /// 读数据的最大条数, 即单次读取数据的最大上限, 以限制每次读取的数据量
    /// </summary>
    public int MaxReadCount { get; set; } = AppSettings.UNIT_HUNDRED;
    /// <summary>
    /// 写数据的最大条数, 即单次写入数据库的最大上限, 以限制每次写入的数据量
    /// </summary>
    public int MaxWriteCount { get; set; } = AppSettings.UNIT_HUNDRED;
}