﻿namespace Devonline.Core;

/// <summary>
/// 静态使用的帮助类方法
/// </summary>
public static class Utility
{
    #region 全局功能方法
    /// <summary>
    /// 延迟方法, 时间间隔单位: 秒, 默认 1 秒
    /// </summary>
    /// <param name="interval"></param>
    public static Task DelayAsync(int interval = AppSettings.UNIT_ONE)
    {
        return Task.Delay(interval * AppSettings.UNIT_THOUSAND);
    }
    /// <summary>
    /// 等待任务完成
    /// </summary>
    /// <param name="condition">等待条件</param>
    /// <param name="interval">判断间隔, 单位: 秒</param>
    /// <returns></returns>
    public static async Task WaitingAsync(Func<bool> condition, int interval = AppSettings.UNIT_SECONDS_A_MINUTE)
    {
        while (condition())
        {
            await DelayAsync(interval);
        }
    }
    #endregion

    #region 连续尝试任务直到成功
    /// <summary>
    /// 连续尝试任务直到成功或到达执行次数上限
    /// </summary>
    /// <param name="execute">连续尝试执行的主方法</param>
    /// <param name="maxCount">连续执行的最大次数</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="OnFail">执行结束后执行的方法</param>
    /// <returns>执行结果</returns>
    public static bool Execute(Action execute, int maxCount = AppSettings.UNIT_TEN, Action<Exception>? whenException = default, Action? OnFail = default)
    {
        var index = 1;
        var result = false;

        do
        {
            try
            {
                execute();
                result = true;
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (!result && OnFail is not null)
                {
                    OnFail();
                }
            }
        } while (!result && index++ < maxCount);

        return result;
    }
    /// <summary>
    /// 连续尝试任务直到成功或到达执行次数上限
    /// </summary>
    /// <param name="execute">连续尝试执行的主方法</param>
    /// <param name="maxCount">连续执行的最大次数</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="OnFail">执行结束后执行的方法</param>
    /// <returns>执行结果</returns>
    public static async Task<bool> ExecuteAsync(Func<Task> execute, int maxCount = AppSettings.UNIT_TEN, Action<Exception>? whenException = default, Action? OnFail = default)
    {
        var index = 1;
        var result = false;

        do
        {
            try
            {
                await execute();
                result = true;
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (!result && OnFail is not null)
                {
                    OnFail();
                }
            }
        } while (!result && index++ < maxCount);

        return result;
    }

    /// <summary>
    /// 连续尝试任务直到成功或到达执行次数上限
    /// </summary>
    /// <param name="execute">连续尝试执行的主方法</param>
    /// <param name="maxCount">连续执行的最大次数</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="OnFail">执行结束后执行的方法</param>
    /// <returns>执行结果</returns>
    public static T? Execute<T>(Func<T> execute, int maxCount = AppSettings.UNIT_TEN, Action<Exception>? whenException = default, Action? OnFail = default)
    {
        var index = 1;
        var result = false;
        T? t = default;

        do
        {
            try
            {
                t = execute();
                result = t is not null;
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (!result && OnFail is not null)
                {
                    OnFail();
                }
            }
        } while (!result && index++ < maxCount);

        return t;
    }
    /// <summary>
    /// 连续尝试任务直到成功或到达执行次数上限
    /// </summary>
    /// <param name="execute">连续尝试执行的主方法</param>
    /// <param name="maxCount">连续执行的最大次数</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="OnFail">执行结束后执行的方法</param>
    /// <returns>执行结果</returns>
    public static async Task<T?> ExecuteAsync<T>(Func<Task<T>> execute, int maxCount = AppSettings.UNIT_TEN, Action<Exception>? whenException = default, Action? OnFail = default)
    {
        var index = 1;
        var result = false;
        T? t = default;

        do
        {
            try
            {
                t = await execute();
                result = t is not null;
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (!result && OnFail is not null)
                {
                    OnFail();
                }
            }
        } while (!result && index++ < maxCount);

        return t;
    }
    #endregion

    #region 任务执行相关方法
    /// <summary>
    /// 间隔执行任务直到退出, new task need start
    /// </summary>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="isParallel">是否并行执行</param>
    /// <returns></returns>
    public static Task ExecuteIntervalAsync(Func<bool> exitCondition, Func<Task> execute, Func<Task>? whenFinally = null, Action<Exception>? whenException = null, bool isParallel = false) => new Task(async () =>
    {
        do
        {
            try
            {
                if (isParallel)
                {
                    execute().Start();
                }
                else
                {
                    await execute().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (whenFinally == null)
                {
                    await DelayAsync().ConfigureAwait(false);
                }
                else
                {
                    await whenFinally().ConfigureAwait(false);
                }
            }
        } while (!exitCondition());
    }, TaskCreationOptions.LongRunning);
    /// <summary>
    /// 间隔执行任务直到退出的泛型版本, 使用当前对象作为参数执行长期间隔循环执行任务, new task need start
    /// </summary>
    /// <typeparam name="T">当前执行参数类型</typeparam>
    /// <param name="t">当前执行参数</param>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="isParallel">是否并行执行</param>
    /// <returns></returns>
    public static Task ExecuteIntervalAsync<T>(T t, Func<bool> exitCondition, Func<T, Task> execute, Func<Task>? whenFinally = null, Action<Exception>? whenException = null, bool isParallel = false) => new Task(async () =>
    {
        do
        {
            try
            {
                if (isParallel)
                {
                    execute(t).Start();
                }
                else
                {
                    await execute(t).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (whenFinally == null)
                {
                    await DelayAsync().ConfigureAwait(false);
                }
                else
                {
                    await whenFinally().ConfigureAwait(false);
                }
            }
        } while (!exitCondition());
    }, TaskCreationOptions.LongRunning);
    /// <summary>
    /// 间隔执行任务直到退出的泛型版本, 使用当前对象作为参数执行长期间隔循环执行任务, new task need start
    /// </summary>
    /// <typeparam name="T">当前执行参数类型</typeparam>
    /// <param name="t">当前执行参数</param>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <param name="isParallel">是否并行执行</param>
    /// <returns></returns>
    public static Task ExecuteIntervalAsync<T>(T t, Func<T, bool> exitCondition, Func<T, Task> execute, Func<T, Task>? whenFinally = null, Action<Exception>? whenException = null, bool isParallel = false) => new Task(async () =>
    {
        do
        {
            try
            {
                if (isParallel)
                {
                    execute(t).Start();
                }
                else
                {
                    await execute(t).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                whenException?.Invoke(ex);
            }
            finally
            {
                if (whenFinally == null)
                {
                    await DelayAsync().ConfigureAwait(false);
                }
                else
                {
                    await whenFinally(t).ConfigureAwait(false);
                }
            }
        } while (!exitCondition(t));
    }, TaskCreationOptions.LongRunning);

    /// <summary>
    /// 并行批量执行长期执行的间隔循环执行任务
    /// </summary>
    /// <param name="parallelCount">并发数</param>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <returns></returns>
    public static ParallelLoopResult ExecuteIntervalParallel(int parallelCount, Func<bool> exitCondition, Func<Task> execute, Func<Task>? whenFinally = null, Action<Exception>? whenException = null) => Parallel.For(AppSettings.UNIT_ZERO, parallelCount, index => ExecuteIntervalAsync(exitCondition, execute, whenFinally, whenException).Start());
    /// <summary>
    /// 并行批量执行长期执行的间隔循环执行任务的泛型方法, 提供使用 int 类型作为任务数量和编号的参数来启动并行任务
    /// </summary>
    /// <param name="parallelCount">并发数</param>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <returns></returns>
    public static ParallelLoopResult ExecuteIntervalParallel(int parallelCount, Func<int, bool> exitCondition, Func<int, Task> execute, Func<int, Task>? whenFinally = null, Action<Exception>? whenException = null) => Parallel.For(AppSettings.UNIT_ZERO, parallelCount, index => ExecuteIntervalAsync(index, exitCondition, execute, whenFinally, whenException).Start());
    /// <summary>
    /// 并行批量执行长期执行的间隔循环执行任务的泛型方法
    /// </summary>
    /// <typeparam name="T">执行对象类型</typeparam>
    /// <param name="args">执行对象</param>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <returns></returns>
    public static ParallelLoopResult ExecuteIntervalParallel<T>(IEnumerable<T> args, Func<bool> exitCondition, Func<T, Task> execute, Func<Task>? whenFinally = null, Action<Exception>? whenException = null) => Parallel.ForEach(args, arg => ExecuteIntervalAsync(arg, exitCondition, execute, whenFinally, whenException).Start());
    /// <summary>
    /// 并行批量执行长期执行的间隔循环执行任务的泛型方法
    /// </summary>
    /// <typeparam name="T">执行对象类型</typeparam>
    /// <param name="args">执行对象</param>
    /// <param name="exitCondition">执行退出条件</param>
    /// <param name="execute">每一轮执行的主方法</param>
    /// <param name="whenFinally">每一轮执行结束后执行的方法</param>
    /// <param name="whenException">发生异常时执行的方法</param>
    /// <returns></returns>
    public static ParallelLoopResult ExecuteIntervalParallel<T>(IEnumerable<T> args, Func<T, bool> exitCondition, Func<T, Task> execute, Func<T, Task>? whenFinally = null, Action<Exception>? whenException = null) => Parallel.ForEach(args, arg => ExecuteIntervalAsync(arg, exitCondition, execute, whenFinally, whenException).Start());
    #endregion
}