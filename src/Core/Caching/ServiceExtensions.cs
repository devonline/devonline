﻿using Devonline.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Devonline.Caching;

public static class ServiceExtensions
{
    /// <summary>
    /// 注入 缓存
    /// </summary>
    /// <param name="services">依赖注入服务</param>
    /// <param name="cacheSetting">缓存配置</param>
    public static IServiceCollection AddDistributedCache(this IServiceCollection services, CacheSetting? cacheSetting = default)
    {
        if (cacheSetting is null || string.IsNullOrWhiteSpace(cacheSetting.Configuration))
        {
            services.AddDistributedMemoryCache();
        }
        else
        {
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = cacheSetting.Configuration;
                options.InstanceName = cacheSetting.Prefix;
            });
        }

        return services;
    }
}