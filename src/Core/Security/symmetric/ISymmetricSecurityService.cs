﻿namespace Devonline.Security;

/// <summary>
/// 对称加密算法实现的数据安全服务
/// </summary>
public interface ISymmetricSecurityService : ISecurityService
{
    /// <summary>
    /// 构造 Aes 的 KeyIV
    /// </summary>
    /// <returns>Aes 的 Key IV</returns>
    KeyIV GetKeyIV();
    /// <summary>
    /// 根据密文数据携带的 Aes 密钥编号获取 Aes 密钥
    /// </summary>
    /// <param name="keyId">密钥编号</param>
    /// <returns>返回匹配的 Aes 密钥</returns>
    /// <exception cref="KeyNotFoundException"></exception>
    KeyIV GetKeyIV(long keyId);
    /// <summary>
    /// 添加一个新的 KeyIV
    /// </summary>
    /// <param name="keyId">密钥编号</param>
    /// <param name="keyIV">KeyIV 对象</param>
    void SetKeyIV(long keyId, KeyIV keyIV);

    /// <summary>
    /// 使用 Key IV 构造的 Aes 算法加密数据 data
    /// </summary>
    /// <param name="key">Aes Key</param>
    /// <param name="iv">Aes IV</param>
    /// <param name="data">待加密的数据</param>
    /// <returns>加密后的数据</returns>
    Task<byte[]> EncryptAsync(byte[] key, byte[] iv, byte[] data);
    /// <summary>
    /// 使用 Key IV 构造的 Aes 算法解密密文数据 data
    /// </summary>
    /// <param name="key">Aes Key</param>
    /// <param name="iv">Aes IV</param>
    /// <param name="data">待解密的数据</param>
    /// <returns>解密后的数据</returns>
    Task<byte[]> DecryptAsync(byte[] key, byte[] iv, byte[] data);

    /// <summary>
    /// 使用 Key IV 构造的 Aes 算法加密数据 plainText
    /// </summary>
    /// <param name="key">Aes Key</param>
    /// <param name="iv">Aes IV</param>
    /// <param name="plainText">待加密的字符串</param>
    /// <returns>Base64 编码的密文字符串</returns>
    Task<string> EncryptAsync(byte[] key, byte[] iv, string plainText);
    /// <summary>
    /// 使用 Key IV 构造的 Aes 算法解密密文数据 cipherText
    /// </summary>
    /// <param name="key">Aes Key</param>
    /// <param name="iv">Aes IV</param>
    /// <param name="cipherText">Base64 编码的密文字符串</param>
    /// <returns>解密后的数据</returns>
    Task<string> DecryptAsync(byte[] key, byte[] iv, string cipherText);
}