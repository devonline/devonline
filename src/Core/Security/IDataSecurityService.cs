﻿using System.Security.Cryptography.X509Certificates;

namespace Devonline.Security;

/// <summary>
/// 数据安全服务
/// 提供对数据的加密解密
/// </summary>
public interface IDataSecurityService : ISecurityService
{
    /// <summary>
    /// 使用 RSA 证书公钥和自动 Aes 密码加密数据 data
    /// </summary>
    /// <param name="certificate">加密用的证书</param>
    /// <param name="data">待加密的数据明文</param>
    /// <returns>携带 Aes 密钥信息的加密后的数据密文</returns>
    Task<byte[]> EncryptAsync(X509Certificate2 certificate, byte[] data);
    /// <summary>
    /// 使用 RSA 证书私钥解密数据 data
    /// </summary>
    /// <param name="certificate">解密用的证书</param>
    /// <param name="data">携带 Aes 密钥信息的待解密的数据密文</param>
    /// <returns>解密后的数据明文</returns>
    Task<byte[]> DecryptAsync(X509Certificate2 certificate, byte[] data);
}