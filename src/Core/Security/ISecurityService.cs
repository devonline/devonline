﻿using System.Text;

namespace Devonline.Security;

/// <summary>
/// 数据安全服务
/// 提供对数据的加密解密
/// </summary>
public interface ISecurityService
{
    /// <summary>
    /// 加密明文数据
    /// </summary>
    /// <param name="data">明文数据</param>
    /// <returns>加密后的数据</returns>
    Task<byte[]> EncryptAsync(byte[] data);
    /// <summary>
    /// 解密密文数据
    /// </summary>
    /// <param name="data">密文数据</param>
    /// <returns>解密后的数据</returns>
    Task<byte[]> DecryptAsync(byte[] data);

    /// <summary>
    /// 加密明文文本
    /// 先使用特定字符编码对明文字符串进行编码, 然后使用已实现的加密算法进行加密, 在将结果转换为 Base64 编码的字符串格式输出
    /// </summary>
    /// <param name="plainText">明文文本</param>
    /// <param name="encoding">字符编码, 默认将使用 UTF8</param>
    /// <returns>加密后的密文文本</returns>
    Task<string> EncryptAsync(string plainText, Encoding? encoding = default);
    /// <summary>
    /// 解密密文文本
    /// 先将原始密文以 Base64 编码的字符串格式进行解码, 然后使用已实现的加密算法进行解密, 在将结果使用特定字符编码进行编码输出
    /// </summary>
    /// <param name="cipherText">密文文本</param>
    /// <param name="encoding">字符编码, 默认将使用 UTF8</param>
    /// <returns>解密后的明文文本</returns>
    Task<string> DecryptAsync(string cipherText, Encoding? encoding = default);
}