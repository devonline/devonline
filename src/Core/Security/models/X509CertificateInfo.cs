﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Devonline.Security;

/// <summary>
/// x.509 证书签名信息
/// </summary>
/// <param name="signatureName">证书签名</param>
public class X509CertificateInfo(string signatureName)
{
    /// <summary>
    /// RSA 密钥大小
    /// </summary>
    public int KeySize { get; set; } = 2048;
    /// <summary>
    /// 签名名称
    /// </summary>
    public string SignatureName { get; set; } = signatureName;
    /// <summary>
    /// 签名算法名称
    /// </summary>
    public HashAlgorithmName HashAlgorithmName { get; set; } = HashAlgorithmName.SHA256;
    /// <summary>
    /// 对齐方式
    /// </summary>
    public RSASignaturePadding SignaturePadding { get; set; } = RSASignaturePadding.Pkcs1;
    /// <summary>
    /// X509 Key Usage Flags
    /// </summary>
    public X509KeyUsageFlags KeyUsageFlags { get; set; } = X509KeyUsageFlags.DigitalSignature;
    /// <summary>
    /// true if the extension is critical; otherwise, false.
    /// </summary>
    public bool Critical { get; set; } = true;
    /// <summary>
    /// 过期时间, 单位: 月
    /// </summary>
    public int ExpireTime { get; set; } = AppSettings.UNIT_MONTHS_A_YEAR;
    /// <summary>
    /// 证书内容保存类型
    /// </summary>
    public X509ContentType ContentType { get; set; } = X509ContentType.Pfx;
    /// <summary>
    /// 证书密码
    /// </summary>
    public string? Password { get; set; }
    /// <summary>
    /// 证书导出的文件路径
    /// </summary>
    public string? FilePath { get; set; }
}