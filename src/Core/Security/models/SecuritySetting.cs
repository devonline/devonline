﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using AsymmetricAlgorithm = Devonline.Core.AsymmetricAlgorithm;
using HashAlgorithm = Devonline.Core.HashAlgorithm;
using SymmetricAlgorithm = Devonline.Core.SymmetricAlgorithm;

namespace Devonline.Security;

/// <summary>
/// 安全服务相关设置
/// </summary>
public class SecuritySetting
{
    /// <summary>
    /// 数据加密时使用的证书
    /// </summary>
    public X509Certificate2? Certificate { get; set; }
    /// <summary>
    /// 字符编码
    /// </summary>
    public Encoding Encoding { get; set; } = Encoding.UTF8;

    /// <summary>
    /// 对称加密算法
    /// </summary>
    public SymmetricAlgorithm SymmetricAlgorithm { get; set; } = SymmetricAlgorithm.AES_256_CBC;
    /// <summary>
    /// 非对称加密算法
    /// </summary>
    public AsymmetricAlgorithm AsymmetricAlgorithm { get; set; } = AsymmetricAlgorithm.RSA;
    /// <summary>
    /// 散列算法/消息摘要算法
    /// </summary>
    public HashAlgorithm HashAlgorithm { get; set; } = HashAlgorithm.SHA256;

    /// <summary>
    /// 数据加密模式
    /// </summary>
    public DataEncryptionMode EncryptionMode { get; set; } = DataEncryptionMode.KeyId;
    /// <summary>
    /// 密钥长度
    /// </summary>
    public int KeySize { get; set; } = 256;
    /// <summary>
    /// 加密模式
    /// </summary>
    public CipherMode CipherMode = CipherMode.CBC;
    /// <summary>
    /// 密码最大使用次数
    /// </summary>
    public int UsageCount { get; set; } = AppSettings.UNIT_TEN;
    /// <summary>
    /// 密码过期时间, 单位分钟
    /// </summary>
    public int ExpireTime { get; set; } = AppSettings.UNIT_ONE;
}