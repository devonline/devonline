﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Security;

/// <summary>
/// 密钥生成方式, 密钥的生成和保存形式
/// </summary>
[Description("密钥生成方式")]
public enum KeyGenerateMode
{
    /// <summary>
    /// 同时生成公钥和私钥
    /// </summary>
    [Display(Name = "公钥和私钥")]
    Both,
    /// <summary>
    /// 仅公钥
    /// </summary>
    [Display(Name = "仅公钥")]
    PublicKey,
    /// <summary>
    /// 仅私钥
    /// </summary>
    [Display(Name = "仅私钥")]
    PrivateKey,
}