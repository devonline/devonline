﻿namespace Devonline.Security;

/// <summary>
/// 非对称加密算法提供的数据安全服务
/// </summary>
/// <param name="securitySetting"></param>
public abstract class AsymmetricSecurityService(SecuritySetting securitySetting) : SecurityService(securitySetting), IAsymmetricSecurityService, ISecurityService
{
    /// <summary>
    /// 使用公钥加密数据
    /// </summary>
    /// <param name="publicKey">加密用的公钥</param>
    /// <param name="data">待加密的数据</param>
    /// <returns>加密后的数据</returns>
    public abstract byte[] Encrypt(byte[] publicKey, byte[] data);
    /// <summary>
    /// 使用私钥解密数据
    /// </summary>
    /// <param name="privateKey">解密用的私钥</param>
    /// <param name="data">待解密的数据</param>
    /// <returns>解密后的数据</returns>
    public abstract byte[] Decrypt(byte[] privateKey, byte[] data);

    /// <summary>
    /// 生成密钥对
    /// </summary>
    /// <param name="privateKey">输出私钥</param>
    /// <param name="publicKey">输出公钥</param>
    /// <returns></returns>
    public abstract void Generate(out byte[] privateKey, out byte[] publicKey);
    /// <summary>
    /// 生成密钥对并保存到文件
    /// 如密钥文件名 www.xxx.com(或/cert/www.xxx.com.crt), generateMode == Both, 则生成
    /// www.xxx.com.crt(公私钥)
    /// www.xxx.com.pem(公钥)
    /// www.xxx.com.key(私钥)
    /// </summary>
    /// <param name="fileName">密钥文件名</param>
    /// <param name="generateMode">密钥生成方式</param>
    /// <returns></returns>
    public abstract Task GenerateAsync(string keyName, KeyGenerateMode generateMode = KeyGenerateMode.Both);

    /// <summary>
    /// 从公钥和私钥数据导入密钥
    /// </summary>
    /// <param name="publicKey">公钥</param>
    /// <param name="privateKey">私钥</param>
    /// <returns></returns>
    public abstract void Import(byte[] publicKey, byte[]? privateKey = default);
    /// <summary>
    /// 从文件导入密钥
    /// </summary>
    /// <param name="fileName">密钥文件名</param>
    /// <returns></returns>
    public abstract Task ImportAsync(string fileName);
}