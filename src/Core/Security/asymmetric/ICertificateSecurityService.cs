﻿using System.Security.Cryptography.X509Certificates;

namespace Devonline.Security;

/// <summary>
/// 数据安全服务
/// 提供对数据的加密解密
/// </summary>
public interface ICertificateSecurityService : ISecurityService
{
    /// <summary>
    /// 使用证书加密数据
    /// </summary>
    /// <param name="certificate">加密用的证书</param>
    /// <param name="data">待加密的数据</param>
    /// <returns>加密后的数据</returns>
    Task<byte[]> EncryptAsync(X509Certificate2 certificate, byte[] data);
    /// <summary>
    /// 使用证书解密数据
    /// </summary>
    /// <param name="certificate">解密用的证书</param>
    /// <param name="data">待解密的数据</param>
    /// <returns>解密后的数据</returns>
    Task<byte[]> DecryptAsync(X509Certificate2 certificate, byte[] data);

    /// <summary>
    /// 创建 x.509 证书
    /// </summary>
    /// <param name="info">证书信息</param>
    /// <returns></returns>
    Task<X509Certificate2> GenerateAsync(X509CertificateInfo info);
}