﻿global using Devonline.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Devonline.Security;

/// <summary>
/// 服务注册扩展类型
/// </summary>
public static class ServiceExtensions
{
    /// <summary>
    /// 添加默认的数据安全服务
    /// </summary>
    /// <param name="services">依赖注入服务</param>
    /// <param name="securitySetting">安全设置</param>
    /// <param name="serviceLifetime">安全服务生命周期</param>
    /// <returns></returns>
    public static IServiceCollection AddSecurityService(this IServiceCollection services, SecuritySetting securitySetting, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
    {
        services.AddSingleton<SecuritySetting>(securitySetting);
        switch (serviceLifetime)
        {
            case ServiceLifetime.Transient:
                services.AddTransient<RSASecurityService>();
                services.AddTransient<SM2SecurityService>();
                services.AddTransient<AesSecurityService>();

                if (securitySetting.AsymmetricAlgorithm == AsymmetricAlgorithm.RSA)
                {
                    services.AddTransient<IAsymmetricSecurityService, RSASecurityService>();
                }
                else
                {
                    services.AddTransient<IAsymmetricSecurityService, SM2SecurityService>();
                }

                services.AddTransient<ISymmetricSecurityService, AesSecurityService>();
                services.AddTransient<ICertificateSecurityService, CertificateSecurityService>();
                services.AddTransient<ISecurityService, DataSecurityService>();
                services.AddTransient<IDataSecurityService, DataSecurityService>();
                break;
            case ServiceLifetime.Singleton:
                services.AddSingleton<RSASecurityService>();
                services.AddSingleton<SM2SecurityService>();
                services.AddSingleton<AesSecurityService>();

                if (securitySetting.AsymmetricAlgorithm == AsymmetricAlgorithm.RSA)
                {
                    services.AddSingleton<IAsymmetricSecurityService, RSASecurityService>();
                }
                else
                {
                    services.AddSingleton<IAsymmetricSecurityService, SM2SecurityService>();
                }

                services.AddSingleton<ISymmetricSecurityService, AesSecurityService>();
                services.AddSingleton<ICertificateSecurityService, CertificateSecurityService>();
                services.AddSingleton<ISecurityService, DataSecurityService>();
                services.AddSingleton<IDataSecurityService, DataSecurityService>();
                break;
            case ServiceLifetime.Scoped:
            default:
                services.AddScoped<RSASecurityService>();
                services.AddScoped<SM2SecurityService>();
                services.AddScoped<AesSecurityService>();

                if (securitySetting.AsymmetricAlgorithm == AsymmetricAlgorithm.RSA)
                {
                    services.AddScoped<IAsymmetricSecurityService, RSASecurityService>();
                }
                else
                {
                    services.AddScoped<IAsymmetricSecurityService, SM2SecurityService>();
                }

                services.AddScoped<ISymmetricSecurityService, AesSecurityService>();
                services.AddScoped<ICertificateSecurityService, CertificateSecurityService>();
                services.AddScoped<ISecurityService, DataSecurityService>();
                services.AddScoped<IDataSecurityService, DataSecurityService>();
                break;
        }

        return services;
    }
}