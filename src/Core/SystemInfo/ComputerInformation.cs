﻿using Devonline.Core;

namespace Devonline.SystemInfo;

/// <summary>
/// 主要的计算机信息
/// </summary>
public class ComputerInformation
{
    /// <summary>
    /// 操作系统
    /// </summary>
    public string OperatingSystem { get; set; } = null!;
    /// <summary>
    /// 用户账户
    /// </summary>
    public string User { get; set; } = null!;
    /// <summary>
    /// 计算机机器名
    /// </summary>
    public string MachineName { get; set; } = null!;
    /// <summary>
    /// 计算机产品名称
    /// </summary>
    public string ComputerName { get; set; } = null!;
    /// <summary>
    /// 计算机产品型号
    /// </summary>
    public string IdentifyingNumber { get; set; } = null!;
    /// <summary>
    /// 处理器序列号
    /// </summary>
    public string BIOS { get; set; } = null!;
    /// <summary>
    /// 主板
    /// </summary>
    public string Mainboard { get; set; } = null!;
    /// <summary>
    /// 处理器序列号
    /// </summary>
    public string CPU { get; set; } = null!;
    /// <summary>
    /// 显卡
    /// </summary>
    public string GPU { get; set; } = null!;
    /// <summary>
    /// 硬盘驱动器
    /// </summary>
    public string HardDrive { get; set; } = null!;
    /// <summary>
    /// 网卡
    /// </summary>
    public string NetworkAdapter { get; set; } = null!;

    /// <summary>
    /// 获取时间
    /// </summary>
    public DateTime DateTime { get; set; }

    /// <summary>
    /// 系统信息描述
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return nameof(OperatingSystem) + Environment.NewLine + OperatingSystem + Environment.NewLine
            + nameof(User) + Environment.NewLine + User + Environment.NewLine
            + nameof(MachineName) + Environment.NewLine + MachineName + Environment.NewLine
            + nameof(ComputerName) + Environment.NewLine + ComputerName + Environment.NewLine
            + nameof(IdentifyingNumber) + Environment.NewLine + IdentifyingNumber + Environment.NewLine
            + nameof(BIOS) + Environment.NewLine + BIOS + Environment.NewLine
            + nameof(Mainboard) + Environment.NewLine + Mainboard + Environment.NewLine
            + nameof(CPU) + Environment.NewLine + CPU + Environment.NewLine
            + nameof(GPU) + Environment.NewLine + GPU + Environment.NewLine
            + nameof(HardDrive) + Environment.NewLine + HardDrive + Environment.NewLine
            + nameof(NetworkAdapter) + Environment.NewLine + NetworkAdapter + Environment.NewLine
            + nameof(DateTime) + Environment.NewLine + DateTime.ToString(AppSettings.DEFAULT_DATETIME_FORMAT);
    }

    /// <summary>
    /// 将计算机信息序列化为字符串
    /// </summary>
    /// <returns></returns>
    public string Serialize()
    {
        return nameof(OperatingSystem) + AppSettings.CHAR_EQUAL + OperatingSystem + AppSettings.CHAR_ADD
            + nameof(User) + AppSettings.CHAR_EQUAL + User + AppSettings.CHAR_ADD
            + nameof(MachineName) + AppSettings.CHAR_EQUAL + MachineName + AppSettings.CHAR_ADD
            + nameof(ComputerName) + AppSettings.CHAR_EQUAL + ComputerName + AppSettings.CHAR_ADD
            + nameof(IdentifyingNumber) + AppSettings.CHAR_EQUAL + IdentifyingNumber + AppSettings.CHAR_ADD
            + nameof(BIOS) + AppSettings.CHAR_EQUAL + BIOS + AppSettings.CHAR_ADD
            + nameof(Mainboard) + AppSettings.CHAR_EQUAL + Mainboard + AppSettings.CHAR_ADD
            + nameof(CPU) + AppSettings.CHAR_EQUAL + CPU + AppSettings.CHAR_ADD
            + nameof(GPU) + AppSettings.CHAR_EQUAL + GPU + AppSettings.CHAR_ADD
            + nameof(HardDrive) + AppSettings.CHAR_EQUAL + HardDrive + AppSettings.CHAR_ADD
            + nameof(NetworkAdapter) + AppSettings.CHAR_EQUAL + NetworkAdapter + AppSettings.CHAR_ADD
            + nameof(DateTime) + AppSettings.CHAR_EQUAL + DateTime.ToString(AppSettings.DEFAULT_DATETIME_FORMAT);
    }
}