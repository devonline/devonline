﻿namespace Devonline.Entity;

/// <summary>
/// 具有数据隔离的对象模型基础接口
/// 字符串主键的默认接口
/// </summary>
public interface IDataIsolate : IDataIsolate<string>, IEntitySet;

/// <summary>
/// 具有数据隔离的对象模型基础接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IDataIsolate<TKey> : IEntitySet<TKey>
{
    /// <summary>
    /// 数据隔离编号, 用于标记多租户或基于组织单位的数据隔离
    /// </summary>
    TKey IsolateId { get; set; }
}