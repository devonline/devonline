﻿namespace Devonline.Entity;

/// <summary>
/// 具有数据隔离的对象模型抽象基类
/// 字符串类型的默认基类
/// </summary>
public abstract class DataIsolateWithCreateAndUpdate : DataIsolateWithCreateAndUpdate<string>, IDataIsolate, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public DataIsolateWithCreateAndUpdate() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有数据隔离的对象模型抽象基类
/// </summary>
public abstract class DataIsolateWithCreateAndUpdate<TKey> : EntitySetWithCreateAndUpdate<TKey>, IDataIsolate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 数据隔离编号, 用于标记多租户或基于组织单位的数据隔离
    /// </summary>
    [Column("isolate_id"), DisplayName("数据隔离编号"), Required, MaxLength(36), Excel]
    public virtual TKey IsolateId { get; set; } = default!;
}