﻿namespace Devonline.Entity;

/// <summary>
/// 班次, 字符串类型的默认实现
/// </summary>
[Table("work_shift"), DisplayName("班次")]
public class WorkShift : WorkShift<string>, IIdentity, ITimeRange, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 工作班次
/// 设置和记录某种工作每日的工作时间
/// </summary>
[Table("work_shift"), DisplayName("班次")]
public class WorkShift<TKey> : Identity<TKey>, IIdentity<TKey>, ITimeRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 班次名称
    /// </summary>
    [Column("name"), Required, Unique, MaxLength(255), DisplayName("名称"), Excel]
    public override string? Name { get; set; } = null!;
    /// <summary>
    /// 起始时间
    /// </summary>
    [Column("start"), DisplayName("起始时间"), Excel]
    public virtual TimeOnly? Start { get; set; }
    /// <summary>
    /// 结束时间
    /// </summary>
    [Column("end"), DisplayName("结束时间"), Excel]
    public virtual TimeOnly? End { get; set; }
}