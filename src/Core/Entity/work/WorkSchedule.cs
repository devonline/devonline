﻿namespace Devonline.Entity;

/// <summary>
/// 排班/工作计划
/// 设置和记录某个员工, 在某个时间段, 在某个岗位, 按某个班次工作
/// </summary>
[Table("work_schedule"), DisplayName("工作计划")]
public class WorkSchedule : WorkSchedule<string>, IDateTimeRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 岗位
    /// </summary>
    public virtual WorkStation? WorkStation { get; set; }
    /// <summary>
    /// 班次
    /// </summary>
    public virtual WorkShift? WorkShift { get; set; }
    /// <summary>
    /// 用户
    /// </summary>
    public virtual Personal? Personal { get; set; }
}

/// <summary>
/// 排班/工作计划
/// 设置和记录某个员工, 在某个时间段, 在某个岗位, 按某个班次工作
/// </summary>
[Table("work_schedule"), DisplayName("工作计划")]
public class WorkSchedule<TKey> : EntitySetWithDateTimeRange<TKey>, IDateTimeRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 岗位编号
    /// </summary>
    [Column("work_station_id"), DisplayName("岗位编号"), Required, MaxLength(36), Excel]
    public virtual TKey WorkStationId { get; set; } = default!;
    /// <summary>
    /// 班次编号
    /// </summary>
    [Column("work_shift_id"), DisplayName("班次编号"), Required, MaxLength(36), Excel]
    public virtual TKey WorkShiftId { get; set; } = default!;
    /// <summary>
    /// 个人信息编号
    /// </summary>
    [Column("personal_id"), DisplayName("个人信息编号"), Required, MaxLength(36), Excel]
    public virtual TKey PersonalId { get; set; } = default!;
    /// <summary>
    /// 循环周期
    /// </summary>
    [Column("cycle_period", TypeName = "varchar(16)"), DisplayName("循环周期"), Required, DefaultValue(TimeKind.Month), Excel]
    public virtual TimeKind CyclePeriod { get; set; }
    /// <summary>
    /// 优先级, 当前排班的优先级
    /// </summary>
    [Column("priority"), DisplayName("优先级"), Excel]
    public virtual int Priority { get; set; }
}