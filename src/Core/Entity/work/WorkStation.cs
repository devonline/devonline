﻿namespace Devonline.Entity;

/// <summary>
/// 岗位, 字符串类型的默认实现
/// </summary>
[Table("work_station"), DisplayName("岗位")]
public class WorkStation : WorkStation<string>, IIdentity, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 岗位
/// </summary>
[Table("work_station"), DisplayName("岗位")]
public class WorkStation<TKey> : Identity<TKey>, IIdentity<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 岗位名称
    /// </summary>
    [Column("name"), Required, Unique, MaxLength(255), DisplayName("名称"), Excel]
    public override string? Name { get; set; } = null!;
    /// <summary>
    /// 所属部门编号
    /// </summary>
    [Column("organization_id"), DisplayName("所属部门编号"), Required, MaxLength(36), Excel]
    public virtual TKey OrganizationId { get; set; } = default!;
}