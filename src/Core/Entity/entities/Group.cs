﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;

namespace Devonline.Entity;

/// <summary>
/// 组织机构, 字符串类型的默认实现
/// </summary>
[Table("group"), DisplayName("组织机构"), Index(nameof(Name), IsUnique = true)]
public class Group : Group<string>, IIdentity, IParent, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 上级组织
    /// </summary>
    public virtual Group? Parent { get; set; }
    /// <summary>
    /// 组织级别
    /// </summary>
    public virtual Level? Level { get; set; }
    /// <summary>
    /// 所在区域
    /// </summary>
    public virtual Region? Region { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 组织机构
/// </summary>
[Table("group"), DisplayName("组织机构"), Index(nameof(Name), IsUnique = true)]
public class Group<TKey> : Identity<TKey>, IIdentity<TKey>, IParent<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [Column("name"), Required, Unique, MaxLength(255), DisplayName("名称"), Excel]
    public override string? Name { get; set; } = null!;
    /// <summary>
    /// 上级组织
    /// </summary>
    [Column("parent_id"), DisplayName("上级组织"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
    /// <summary>
    /// 组织级别
    /// </summary>
    [Column("level_id"), DisplayName("组织级别"), MaxLength(36), Excel]
    public virtual TKey? LevelId { get; set; }
    /// <summary>
    /// 地区, 所在区域
    /// </summary>
    [Column("region_id"), DisplayName("所在区域"), MaxLength(36), Excel]
    public virtual TKey? RegionId { get; set; }
}