﻿namespace Devonline.Entity;

/// <summary>
/// 日志, 字符串主键的默认实现
/// 日志将不再记录日志类型, 因为此处日志模型为存储于数据库中的少量的访问日志, 并非记录完整操作过程的大量日志
/// 大量过程日志应该使用专用日志组件
/// </summary>
[Table("log"), DisplayName("日志")]
public class Log : Log<string>, IBusiness, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Log() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 日志
/// 日志将不再记录日志类型, 因为此处日志模型为存储于数据库中的少量的访问日志, 并非记录完整操作过程的大量日志
/// 大量过程日志应该使用专用日志组件
/// </summary>
[Table("log"), DisplayName("日志")]
public abstract class Log<TKey> : EntitySetWithBusiness<TKey>, IBusiness<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 日志级别
    /// </summary>
    [Column("level", TypeName = "varchar(16)"), DisplayName("日志级别"), Required, DefaultValue(LogLevel.Info), Excel]
    public virtual LogLevel Level { get; set; } = LogLevel.Info;
    /// <summary>
    /// 客户端 IP 地址
    /// IP v6 地址较长, 内嵌 v4 的地址则更长
    /// </summary>
    [Column("ip_address"), DisplayName("客户端 IP 地址"), MaxLength(64), Excel]
    public virtual string? IpAddress { get; set; }
    /// <summary>
    /// 操作者
    /// </summary>
    [Column("operator"), DisplayName("操作者"), MaxLength(36), Excel]
    public virtual string? Operator { get; set; }
    /// <summary>
    /// 内容
    /// </summary>
    [Column("content"), DisplayName("日志内容"), Excel]
    public virtual string? Content { get; set; }
}