﻿namespace Devonline.Entity;

/// <summary>
/// 附件, 只有对应实体附件才会有值, 字符串类型的默认实现
/// 附件的工作模式分两种情况, 但所有附件, 都在当前类型的由 NotMapped 特性标记的返回值为 IEnumerable<IAttachment<>> 字段中
/// 1. 类型级附件: 属于多附件, 此时可以上传多个附件, 附件的 BusinessType 将记录类型名, BusinessKey 记录业务对象的编号
/// 2. 字段级附件: 单体附件, 此时只能有一个附件, 附件的 BusinessType 将记录类型名+.+字段名, BusinessKey 记录业务对象的编号
/// 例如: 用户 (User) 的附件, 同时存在两种情况:
/// 1. 属于用户编号为: 12345 的 User 的附件, 保存形式为: BusinessType = "User", BusinessKey = "12345"
/// 2. 属于用户编号为: 12345 的 User 的 Image 属性的附件, 保存形式为: BusinessType = "User.Image", BusinessKey = "12345", 此时, 附件变化后除了保存附件以外, 需要更新 User.Image 字段的值, 此时需要 Attachment 主动提供 BusinessType 的, 否则无法自动匹配到
/// </summary>
[Table("attachment"), DisplayName("附件")]
public class Attachment : Attachment<string>, IAttachment, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Attachment() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 附件, 只有对应实体附件才会有值
/// 附件的工作模式分两种情况, 但所有附件, 都在当前类型的由 NotMapped 特性标记的返回值为 IEnumerable<IAttachment<>> 字段中
/// 1. 类型级附件: 属于多附件, 此时可以上传多个附件, 附件的 BusinessType 将记录类型名, BusinessKey 记录业务对象的编号
/// 2. 字段级附件: 单体附件, 此时只能有一个附件, 附件的 BusinessType 将记录类型名+.+字段名, BusinessKey 记录业务对象的编号
/// 例如: 用户 (User) 的附件, 同时存在两种情况:
/// 1. 属于用户编号为: 12345 的 User 的附件, 保存形式为: BusinessType = "User", BusinessKey = "12345"
/// 2. 属于用户编号为: 12345 的 User 的 Image 属性的附件, 保存形式为: BusinessType = "User.Image", BusinessKey = "12345", 此时, 附件变化后除了保存附件以外, 需要更新 User.Image 字段的值, 此时需要 Attachment 主动提供 BusinessType 的, 否则无法自动匹配到
/// </summary>
[Table("attachment"), DisplayName("附件")]
public abstract class Attachment<TKey> : EntitySetWithBusiness<TKey>, IAttachment<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 附件名
    /// </summary>
    [Column("name"), DisplayName("附件名"), Required, MaxLength(128), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 文件大小
    /// </summary>
    [Column("length"), DisplayName("文件大小"), Excel]
    public virtual long Length { get; set; }
    /// <summary>
    /// 文件扩展名
    /// </summary>
    [Column("extension"), DisplayName("文件扩展名"), Required, MaxLength(16), Excel]
    public virtual string Extension { get; set; } = null!;
    /// <summary>
    /// 文件的 content type
    /// </summary>
    [Column("content_type"), DisplayName("内容类型"), Required, MaxLength(128), Excel]
    public virtual string ContentType { get; set; } = null!;
    /// <summary>
    /// 附件地址, 相对地址加上文件名
    /// </summary>
    [Column("path"), DisplayName("附件地址"), Required, MaxLength(255), Excel]
    public virtual string Path { get; set; } = null!;
}