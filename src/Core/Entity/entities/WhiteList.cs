﻿namespace Devonline.Entity;

/// <summary>
/// 白名单, 字符串主键的默认实现
/// 限制名单: 限制名单中的用户/单位等访问某种业务
/// 白 名 单: 限制别人访问白名单中的内容, 比如禁止查询白名单中的内容等  
/// </summary>
//[Cacheable]
[Table("white_list"), DisplayName("白名单")]
public class WhiteList : WhiteList<string>, IDateTimeRange, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public WhiteList() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 白名单
/// 限制名单: 限制名单中的用户/单位等访问某种业务
/// 白 名 单: 限制别人访问白名单中的内容, 比如禁止查询白名单中的内容等  
/// </summary>
[Table("white_list"), DisplayName("白名单")]
public abstract class WhiteList<TKey> : LimitList<TKey>, IDateTimeRange, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible { }