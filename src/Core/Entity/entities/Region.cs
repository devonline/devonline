﻿namespace Devonline.Entity;

/// <summary>
/// 行政区域, 字符串主键的默认实现
/// </summary>
//[Cacheable(LocalCache = true)]
[Table("region"), DisplayName("行政区域")]
public class Region : Region<string>, IParent, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Region() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 上级行政区域
    /// </summary>
    public virtual Region? Parent { get; set; }
}

/// <summary>
/// 行政区域
/// </summary>
[Table("region"), DisplayName("行政区域")]
public abstract class Region<TKey> : EntitySetWithCreate<TKey>, IParent<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 序号
    /// </summary>
    [Column("index"), DisplayName("序号"), Excel]
    public virtual int Index { get; set; }
    /// <summary>
    /// 区域名称
    /// </summary>
    [Column("name"), DisplayName("区域名称"), Required, MaxLength(64), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 区域编码, 作为行政区域编码(区号), 在省级单位并没有, 因此不是必填
    /// </summary>
    [Column("code"), DisplayName("区域编码"), MaxLength(16), Excel]
    public virtual string? Code { get; set; }
    /// <summary>
    /// 上级行政区域编号
    /// </summary>
    [Column("parent_id"), DisplayName("上级行政区域编号"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
}