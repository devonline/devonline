﻿namespace Devonline.Entity;

/// <summary>
/// 评论, 字符串主键的默认实现
/// </summary>
[Table("comment"), DisplayName("评论")]
public class Comment : Comment<string>, IPersonalBusiness, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Comment() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 用户个人信息
    /// </summary>
    public virtual Personal? Personal { get; set; }
    /// <summary>
    /// 回复引用的评论, 或者说评论引用的相关评论
    /// </summary>
    public virtual Comment? Reply { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 评论
/// </summary>
[Table("comment"), DisplayName("评论")]
public abstract class Comment<TKey> : PersonalBusiness<TKey>, IPersonalBusiness<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 回复编号
    /// </summary>
    [Column("reply_id"), MaxLength(36), DisplayName("回复编号"), Excel]
    public virtual TKey? ReplyId { get; set; }
}