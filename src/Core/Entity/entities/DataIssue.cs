﻿namespace Devonline.Entity;

/// <summary>
/// 数据问题, 字符主键的默认实现
/// 用于记录每日对前一日数据问题的分析结果, 以便进行人为干预修复
/// </summary>
[Table("data_issue"), DisplayName("数据问题")]
public class DataIssue : DataIssue<string>, IBusiness, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public DataIssue() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 数据问题
/// 用于记录每日对前一日数据问题的分析结果, 以便进行人为干预修复
/// </summary>
[Table("data_issue"), DisplayName("数据问题")]
public abstract class DataIssue<TKey> : EntitySetWithBusiness<TKey>, IBusiness<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 问题描述
    /// </summary>
    [Column("description"), DisplayName("问题描述"), MaxLength(1024), Excel]
    public virtual string? Description { get; set; }
}