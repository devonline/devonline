﻿namespace Devonline.Entity;

/// <summary>
/// 反馈, 字符串主键的默认实现
/// </summary>
[Table("feedback"), DisplayName("反馈")]
public class Feedback : Feedback<string>, IPersonalBusiness, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Feedback() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 用户个人信息
    /// </summary>
    public virtual Personal? Personal { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 反馈
/// </summary>
[Table("feedback"), DisplayName("反馈")]
public abstract class Feedback<TKey> : PersonalBusiness<TKey>, IPersonalBusiness<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible { }