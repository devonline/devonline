﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 客户端, 字符主键的默认实现
/// </summary>
[Table("client"), DisplayName("客户端")]
public class Client : Client<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Client() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 客户端所有者
    /// </summary>
    public virtual Personal? Personal { get; set; }
}

/// <summary>
/// 客户端
/// </summary>
/// <typeparam name="TKey"></typeparam>
[Table("client"), DisplayName("客户端"), Index(nameof(Code), IsUnique = true)]
public abstract class Client<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 客户端所有者编号
    /// </summary>
    [Column("personal_id"), DisplayName("客户端所有者编号"), MaxLength(36), Excel]
    public virtual TKey? PersonalId { get; set; }
    /// <summary>
    /// 客户端类型
    /// </summary>
    [Column("type", TypeName = "varchar(16)"), DisplayName("客户端类型"), Required, DefaultValue(TerminalType.Server), Excel]
    public virtual TerminalType Type { get; set; } = TerminalType.Server;
    /// <summary>
    /// 客户端上名字
    /// </summary>
    [Column("name"), DisplayName("客户端姓名"), Required, MaxLength(128), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 客户端号码
    /// </summary>
    [Column("code"), DisplayName("客户端编号"), Required, MaxLength(36), Unique, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 版本号
    /// </summary>
    [Column("version"), MaxLength(128), DisplayName("版本号"), Excel]
    public virtual string? Version { get; set; }
}