﻿namespace Devonline.Entity;

/// <summary>
/// 二进制数据内容的默认实现, 即数据消息
/// </summary>
[Table("data"), DisplayName("数据")]
public class Data : Message<byte[]>, IMessage<byte[]>, IViewModel, IEntitySet { }

/// <summary>
/// 字符串内容的默认实现, 即文本消息
/// </summary>
[Table("message"), DisplayName("消息")]
public class Message : Message<string>, IMessage, IViewModel, IEntitySet { }

/// <summary>
/// 字符串类型作为键的默认实现
/// </summary>
/// <typeparam name="TContent"></typeparam>
[Table("message"), DisplayName("消息")]
public class Message<TContent> : Message<string, TContent>, IMessage<TContent>, IViewModel, IEntitySet
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Message() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 消息是不存储在关系型数据库中的数据, 因此不需要实现
/// </summary>
/// <typeparam name="TKey"></typeparam>
/// <typeparam name="TContent"></typeparam>
[Table("message"), DisplayName("消息")]
public abstract class Message<TKey, TContent> : EntitySet<TKey>, IMessage<TKey, TContent>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 消息类型, MessageType 枚举值
    /// </summary>
    [Column("type", TypeName = "varchar(16)"), DisplayName("消息类型"), Required, DefaultValue(MessageType.Text), Excel]
    public virtual MessageType Type { get; set; } = MessageType.Text;
    /// <summary>
    /// 发送者, 记录谁发出来的消息
    /// </summary>
    [Column("sender"), DisplayName("发送者"), Required, MaxLength(36), Excel]
    public virtual TKey Sender { get; set; } = default!;
    /// <summary>
    /// 接收者, 记录消息的接收对象, 如果接收对象是一个群组中的成员, 则当前消息为 @ 该成员的消息
    /// 接收者可以有很多个, 逗号隔开, 但存储的时候, 每个接收者消息需要单独存储
    /// 当接收者有多个时, 服务器需要对消息进行分流
    /// </summary>
    [Column("receiver"), DisplayName("接收者"), MaxLength(36), Excel]
    public virtual TKey? Receiver { get; set; }

    /// <summary>
    /// 消息群组, 如果消息是发到群里的, 需要记录群组编号, 消息群组的用法如下:
    /// 1. Group 没有值, Receiver 有值, 则消息是发给独立个人的
    /// 2. Group 有值, Receiver 没有值, 则消息是发到群组里
    /// 3. Group 和 Receiver 都有值, 则消息是发到群组里 @ 个人的
    /// 4. Group 和 Receiver 都没有值, 则是发给所有人的消息
    /// </summary>
    [Column("group"), DisplayName("消息群组"), MaxLength(36), Excel]
    public virtual TKey? Group { get; set; }
    /// <summary>
    /// 引用的消息, 关联的消息, 回复的消息
    /// </summary>
    [Column("reply"), DisplayName("引用的消息"), MaxLength(36), Excel]
    public virtual TKey? Reply { get; set; }
    /// <summary>
    /// 消息发送者客户端编号
    /// </summary>
    [Column("from"), DisplayName("发送者客户端"), MaxLength(36), Excel]
    public virtual TKey? From { get; set; }
    /// <summary>
    /// 消息接收者客户端编号
    /// </summary>
    [Column("to"), DisplayName("接收者客户端"), MaxLength(36), Excel]
    public virtual TKey? To { get; set; }

    /// <summary>
    /// 创建时间, 消息的创建时间
    /// </summary>
    [Column("create_time"), DisplayName("创建时间"), Excel]
    public virtual DateTime CreateTime { get; set; } = DateTime.Now;
    /// <summary>
    /// 过期时间, 消息的有效期内, 如果客户端重新上线, 则需要重新发送尚未过期的消息
    /// </summary>
    [Column("expire_time"), DisplayName("过期时间"), Excel]
    public virtual DateTime? ExpireTime { get; set; }
    /// <summary>
    /// 发送时间, 消息一经发出即获得发送时间
    /// </summary>
    [Column("send_time"), DisplayName("发送时间"), Excel]
    public virtual DateTime? SendTime { get; set; }
    /// <summary>
    /// 接收时间, 消息一经接收, 服务器收到回执, 即获得接收时间
    /// </summary>
    [Column("receive_time"), DisplayName("接收时间"), Excel]
    public virtual DateTime? ReceiveTime { get; set; }
    /// <summary>
    /// 读取时间, 消息被已读时的时间, 服务器收到接收者发送的已读信号设置消息状态为已读
    /// </summary>
    [Column("read_time"), DisplayName("读取时间"), Excel]
    public virtual DateTime? ReadTime { get; set; }

    /// <summary>
    /// 消息内容
    /// </summary>
    [Column("content"), DisplayName("消息内容"), MaxLength(1024), Excel]
    public virtual TContent? Content { get; set; }
}