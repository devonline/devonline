﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 身份证, 字符串主键的默认实现
/// </summary>
[Table("id_card"), DisplayName("身份证"), Index(nameof(IdCode), IsUnique = true)]
public class IdCard : IdCard<string>, IEntitySet, IEntitySetWithCreate, IDateRange
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public IdCard() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 身份证
/// </summary>
[Table("id_card"), DisplayName("身份证")]
public abstract class IdCard<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IDateRange where TKey : IConvertible
{
    /// <summary>
    /// 姓名
    /// </summary>
    [Column("name"), DisplayName("姓名"), Required, MaxLength(255), Field(Index = 1, Size = 30), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 性别 Gender 枚举值
    /// </summary>
    [Column("gender", TypeName = "varchar(8)"), DisplayName("性别"), Required, DefaultValue(Gender.Male), Field(Index = 2, Size = 2), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Gender Gender { get; set; } = Gender.Male;
    /// <summary>
    /// 名族
    /// </summary>
    [Column("nation", TypeName = "varchar(16)"), DisplayName("民族"), Required, DefaultValue(Nation.Han), Field(Index = 3, Size = 4), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Nation Nation { get; set; } = Nation.Han;
    /// <summary>
    /// 出身日期
    /// </summary>
    [Column("birthday"), DisplayName("出生"), Field(Index = 4, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateOnly? Birthday { get; set; }
    /// <summary>
    /// 住址
    /// </summary>
    [Column("address"), DisplayName("住址"), MaxLength(255), Field(Index = 5, Size = 70), Excel]
    public virtual string? Address { get; set; }
    /// <summary>
    /// 身份证号码
    /// </summary>
    [Column("id_code"), DisplayName("公民身份号码"), MaxLength(255), Required, Unique, Field(Index = 6, Size = 36), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string IdCode { get; set; } = null!;
    /// <summary>
    /// 签发机关
    /// </summary>
    [Column("issued_by"), DisplayName("签发机关"), MaxLength(36), Field(Index = 7, Size = 30), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? IssuedBy { get; set; }
    /// <summary>
    /// 有效期始
    /// </summary>
    [Column("start"), DisplayName("有效期始"), Field(Index = 8, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateOnly? Start { get; set; }
    /// <summary>
    /// 有效期至
    /// </summary>
    [Column("end"), DisplayName("有效期至"), Field(Index = 9, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateOnly? End { get; set; }
    /// <summary>
    /// 身份证头像照片
    /// </summary>
    [Column("head_image"), DisplayName("头像照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? HeadImage { get; set; }
    /// <summary>
    /// 身份证正面照片
    /// </summary>
    [Column("front_image"), DisplayName("身份证正面照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? FrontImage { get; set; }
    /// <summary>
    /// 身份证反面照片
    /// </summary>
    [Column("back_image"), DisplayName("身份证反面照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? BackImage { get; set; }
}
