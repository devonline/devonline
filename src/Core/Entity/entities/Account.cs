﻿namespace Devonline.Entity;

/// <summary>
/// 用户账户, 字符主键的默认实现
/// </summary>
[Table("account"), DisplayName("用户账户")]
public class Account : Account<string>, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Account() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 账户交易记录
/// </summary>
[Table("account"), DisplayName("用户账户")]
public abstract class Account<TKey> : EntitySetWithCreateAndUpdate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 金额, 余额
    /// </summary>
    [Column("amount", TypeName = "decimal(18,4)"), DisplayName("金额"), Excel]
    public virtual decimal Amount { get; set; }
    /// <summary>
    /// 支付金额, 历史支付总额
    /// </summary>
    [Column("payment_amount", TypeName = "decimal(18,4)"), DisplayName("支付金额"), Excel]
    public virtual decimal PaymentAmount { get; set; }
    /// <summary>
    /// 使用金额/消费金额, 历史使用总额
    /// </summary>
    [Column("used_amount", TypeName = "decimal(18,4)"), DisplayName("使用金额"), Excel]
    public virtual decimal UsedAmount { get; set; }
}