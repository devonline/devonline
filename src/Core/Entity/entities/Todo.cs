﻿namespace Devonline.Entity;

/// <summary>
/// 待办事项, 字符主键的默认实现
/// </summary>
[Table("todo"), DisplayName("待办事项")]
public class Todo : Todo<string>, IPersonalBusiness, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Todo() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 用户个人信息
    /// </summary>
    public virtual Personal? Personal { get; set; }
}

/// <summary>
/// 待办事项
/// 待办事项基类字段提供待办事项名称, 待办对象类型和待办对象编号
/// TODO TBD 待办事项具体条目尚未确定
/// </summary>
[Table("todo"), DisplayName("待办事项")]
public abstract class Todo<TKey> : PersonalBusiness<TKey>, IPersonalBusiness<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible { }
