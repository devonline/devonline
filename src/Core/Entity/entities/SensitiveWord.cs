﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 敏感词, 字符串主键的默认实现
/// 可用于禁止输入, 查询
/// </summary>
//[Cacheable]
[Table("sensitive_word"), DisplayName("敏感词")]
public class SensitiveWord : SensitiveWord<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public SensitiveWord() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 敏感词
/// 可用于禁止输入, 查询
/// </summary>
[Table("sensitive_word"), DisplayName("敏感词"), Index(nameof(Text), IsUnique = true)]
public abstract class SensitiveWord<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 敏感词类型, SensitiveWordType 枚举值
    /// </summary>
    [Column("type", TypeName = "varchar(16)"), DisplayName("敏感词类型"), Required, DefaultValue(SensitiveWordType.Other), Excel]
    public virtual SensitiveWordType Type { get; set; } = SensitiveWordType.Other;
    /// <summary>
    /// 敏感词
    /// </summary>
    [Column("text"), Required, Unique, MaxLength(128), DisplayName("敏感词")]
    public virtual string Text { get; set; } = null!;
    /// <summary>
    /// 备注说明
    /// </summary>
    [Column("description"), DisplayName("备注说明"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }
}