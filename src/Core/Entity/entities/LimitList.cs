﻿namespace Devonline.Entity;

/// <summary>
/// 限制名单, 字符串主键的默认实现
/// 限制名单: 限制名单中的用户/单位等访问某种业务
/// 白 名 单: 限制别人访问白名单中的内容, 比如禁止查询白名单中的内容等  
/// </summary>
//[Cacheable]
[Table("limit_list"), DisplayName("限制名单")]
public class LimitList : LimitList<string>, IDateTimeRange, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public LimitList() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 限制名单
/// 限制名单: 限制名单中的用户/单位等访问某种业务
/// 白 名 单: 限制别人访问白名单中的内容, 比如禁止查询白名单中的内容等  
/// </summary>
[Table("limit_list"), DisplayName("限制名单")]
public abstract class LimitList<TKey> : EntitySetWithBusiness<TKey>, IDateTimeRange, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [Column("name"), DisplayName("名称"), Required, MaxLength(128), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 内容
    /// </summary>
    [Column("content"), DisplayName("内容"), Required, MaxLength(255), Excel]
    public virtual string Content { get; set; } = null!;
    /// <summary>
    /// 限制时间
    /// </summary>
    [Column("start"), DisplayName("限制时间"), Excel]
    public virtual DateTime? Start { get; set; }
    /// <summary>
    /// 解除时间
    /// </summary>
    [Column("end"), DisplayName("解除时间"), Excel]
    public virtual DateTime? End { get; set; }
}