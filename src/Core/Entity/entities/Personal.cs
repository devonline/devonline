﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;


/// <summary>
/// 个人信息, 字符串主键的默认实现
/// 用户信息, 个人信息, 实名认证信息每个用户仅有一个, 第三方认证授权登录信息每个用户多个
/// 业务关联的用户信息, 用于和用户业务数据隔离
/// 用户和认证信息是一对一关系, 没有字段相关指向, 也没有外键直接关联, 他们的主键是同一个值
/// 用户和个人信息是一对一关系, 没有字段相关指向, 也没有外键直接关联, 通过认证信息和个人信息中身份证号匹配
/// </summary>
//[Cacheable]
[Table("personal"), DisplayName("个人信息"), Index(nameof(PhoneNumber), IsUnique = true)]
public class Personal : Personal<string>, IIdentity, IEntitySet, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Personal() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 所在区域
    /// </summary>
    public virtual Region? Region { get; set; }
    /// <summary>
    /// 身份证
    /// </summary>
    public virtual IdCard? IdCard { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 个人信息
/// 用户信息, 个人信息, 实名认证信息每个用户仅有一个, 第三方认证授权登录信息每个用户多个
/// 业务关联的用户信息, 用于和用户业务数据隔离
/// 用户和认证信息是一对一关系, 没有字段相关指向, 也没有外键直接关联, 他们的主键是同一个值
/// 用户和个人信息是一对一关系, 没有字段相关指向, 也没有外键直接关联, 通过认证信息和个人信息中身份证号匹配
/// </summary>
[Table("personal"), DisplayName("个人信息")]
public abstract class Personal<TKey> : Identity<TKey>, IIdentity<TKey>, IEntitySet<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 性别
    /// </summary>
    [Column("gender", TypeName = "varchar(8)"), DisplayName("性别"), Required, DefaultValue(Gender.Male), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Gender Gender { get; set; } = Gender.Male;
    /// <summary>
    /// 年龄
    /// </summary>
    [Column("age"), DisplayName("年龄"), Excel]
    public virtual int? Age { get; set; }
    /// <summary>
    /// 电话
    /// </summary>
    [Column("phone_number"), MaxLength(255), DisplayName("电话"), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? PhoneNumber { get; set; }
    /// <summary>
    /// 邮箱地址
    /// </summary>
    [Column("email"), MaxLength(255), DisplayName("邮箱"), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? Email { get; set; }
    /// <summary>
    /// 地址
    /// </summary>
    [Column("address"), DisplayName("地址"), MaxLength(255), Excel]
    public virtual string? Address { get; set; }
    /// <summary>
    /// 地区, 所在区域
    /// </summary>
    [Column("region_id"), DisplayName("所在区域编号"), MaxLength(36), Excel]
    public virtual TKey? RegionId { get; set; }
    /// <summary>
    /// 身份证编号
    /// </summary>
    [Column("id_card_id"), DisplayName("身份证编号"), MaxLength(36), Excel]
    public virtual TKey? IdCardId { get; set; }
}