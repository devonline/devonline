﻿namespace Devonline.Entity;

/// <summary>
/// 电子邮件, 字符主键的默认实现
/// </summary>
[Table("email"), DisplayName("电子邮件")]
public class Email : Email<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Email() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// email
/// </summary>
[Table("email"), DisplayName("电子邮件")]
public abstract class Email<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 发件人/发件地址
    /// </summary>
    [Column("from"), DisplayName("发件人"), MaxLength(36), Excel]
    public virtual string? From { get; set; }
    /// <summary>
    /// 接收人/接受地址
    /// </summary>
    [Column("to"), DisplayName("接收人"), MaxLength(1024), Excel]
    public virtual string? To { get; set; }
    /// <summary>
    /// 抄送/抄送地址
    /// </summary>
    [Column("cc"), DisplayName("抄送"), MaxLength(1024), Excel]
    public virtual string? CC { get; set; }
    /// <summary>
    /// 密送/密送地址
    /// </summary>
    [Column("bcc"), DisplayName("密送"), MaxLength(1024), Excel]
    public virtual string? Bcc { get; set; }
    /// <summary>
    /// 邮件主题
    /// </summary>
    [Column("subject"), DisplayName("邮件主题"), MaxLength(255), Excel]
    public virtual string? Subject { get; set; }
    /// <summary>
    /// 邮件正文
    /// </summary>
    [Column("body"), DisplayName("邮件正文"), Excel]
    public virtual string? Body { get; set; }
    /// <summary>
    /// 邮件内容是否 HTML 格式
    /// </summary>
    [Column("is_html"), DisplayName("邮件内容是否 HTML 格式"), Excel]
    public virtual bool IsHtml { get; set; }
    /// <summary>
    /// 附件列表
    /// </summary>
    [Column("attachments"), DisplayName("附件列表"), MaxLength(2048), Excel]
    public virtual string? Attachments { get; set; }

    /// <summary>
    /// 发送时间, 消息一经发出即获得发送时间
    /// </summary>
    [Column("send_time"), DisplayName("发送时间"), Excel]
    public virtual DateTime? SendTime { get; set; }
}