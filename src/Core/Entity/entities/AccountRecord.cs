﻿namespace Devonline.Entity;

/// <summary>
/// 账户交易记录, 字符主键的默认实现
/// </summary>
[Table("account_record"), DisplayName("账户交易记录")]
public class AccountRecord : AccountRecord<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public AccountRecord() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 账户交易记录
/// </summary>
[Table("account_record"), DisplayName("账户交易记录")]
public abstract class AccountRecord<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 交易账户编号
    /// </summary>
    [Column("account_id"), DisplayName("账户编号"), Required, MaxLength(36), Excel]
    public virtual TKey AccountId { get; set; } = default!;
    /// <summary>
    /// 具体的交易订单编号
    /// </summary>
    [Column("order_id"), DisplayName("订单编号"), Required, MaxLength(36), Excel]
    public virtual TKey OrderId { get; set; } = default!;
    /// <summary>
    /// 操作人
    /// </summary>
    [Column("operator"), DisplayName("操作人"), Required, MaxLength(36), Excel]
    public virtual string Operator { get; set; } = null!;
    /// <summary>
    /// 操作时间, 精确到秒
    /// </summary>
    [Column("operate_time"), DisplayName("操作时间"), Excel]
    public virtual DateTime OperateTime { get; set; }
    /// <summary>
    /// 交易金额
    /// </summary>
    [Column("amount", TypeName = "decimal(18,4)"), DisplayName("交易金额"), Excel]
    public virtual decimal Amount { get; set; }
    /// <summary>
    /// 订单汇率, 支付时的当时汇率, 是指结算货币相对于人民币的汇率
    /// 如果订单结算时为人民币支付, 则汇率默认为: 1
    /// </summary>
    [Column("exchange_rate", TypeName = "decimal(12,6)"), DisplayName("汇率"), Excel]
    public virtual decimal ExchangeRate { get; set; } = 1;
    /// <summary>
    /// 结算货币
    /// </summary>
    [Column("currency", TypeName = "varchar(8)"), DisplayName("结算货币"), DefaultValue(Currency.CNY), Excel]
    public virtual Currency Currency { get; set; }
    /// <summary>
    /// 支付方式
    /// </summary>
    [Column("payment_type", TypeName = "varchar(16)"), DisplayName("支付方式"), DefaultValue(PaymentType.Cash), Excel]
    public virtual PaymentType PaymentType { get; set; } = PaymentType.Cash;
    /// <summary>
    /// 交易类型 TradeType 枚举值
    /// </summary>
    [Column("trade_type", TypeName = "varchar(16)"), DisplayName("交易类型"), Required, DefaultValue(TradeType.Payment), Excel]
    public virtual TradeType TradeType { get; set; } = TradeType.Payment;
    /// <summary>
    /// 交易结果
    /// </summary>
    [Column("trade_result", TypeName = "varchar(16)"), DisplayName("交易结果"), DefaultValue(TradeResult.Succee), Excel]
    public virtual TradeResult TradeResult { get; set; } = TradeResult.Succee;
    /// <summary>
    /// 交易编号, 来自第三方记录订单编号
    /// </summary>
    [Column("trade_number"), DisplayName("交易编号"), MaxLength(36), Excel]
    public virtual string? TradeNumber { get; set; }
}