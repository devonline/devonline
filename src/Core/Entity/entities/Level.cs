﻿namespace Devonline.Entity;

/// <summary>
/// 级别, 字符串类型的默认实现
/// </summary>
[Table("level"), DisplayName("级别")]
public class Level : Level<string>, IIdentity, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 级别
/// </summary>
[Table("level"), DisplayName("级别")]
public class Level<TKey> : Identity<TKey>, IIdentity<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [Column("name"), Required, Unique, MaxLength(255), DisplayName("名称"), Excel]
    public override string? Name { get; set; } = null!;
    /// <summary>
    /// 级别的值
    /// </summary>
    [Column("value"), DisplayName("级别"), Excel]
    public virtual int Value { get; set; }
}