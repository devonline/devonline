﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 证件, 字符串表达式的默认实现
/// </summary>
[Table("credential"), DisplayName("证件")]
public class Credential : Credential<string>, IEntitySet, IEntitySetWithCreate, IDateRange
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Credential() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 证件
/// </summary>
[Table("credential"), DisplayName("证件"), Index(nameof(Code), IsUnique = true)]
public abstract class Credential<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IDateRange where TKey : IConvertible
{
    /// <summary>
    /// 证件名字
    /// </summary>
    [Column("name"), DisplayName("证件名字"), Required, MaxLength(255), Field(Index = 1, Size = 30), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 证件类型
    /// </summary>
    [Column("type", TypeName = "varchar(36)"), DisplayName("证件类型"), Required, DefaultValue(CredentialType.IdCard), Excel]
    public virtual CredentialType Type { get; set; } = CredentialType.IdCard;
    /// <summary>
    /// 证件号码
    /// </summary>
    [Column("code"), DisplayName("证件号码"), Required, MaxLength(255), Unique, Field(Index = 6, Size = 36), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 签发机关
    /// </summary>
    [Column("issued_by"), DisplayName("签发机关"), MaxLength(36), Field(Index = 7, Size = 30), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? IssuedBy { get; set; }
    /// <summary>
    /// 有效期始
    /// </summary>
    [Column("start"), DisplayName("有效期始"), Field(Index = 8, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateOnly? Start { get; set; }
    /// <summary>
    /// 有效期至
    /// </summary>
    [Column("end"), DisplayName("有效期至"), Field(Index = 9, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateOnly? End { get; set; }
    /// <summary>
    /// 证件头像照片
    /// </summary>
    [Column("head_image"), DisplayName("头像照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? HeadImage { get; set; }
    /// <summary>
    /// 正面照片
    /// </summary>
    [Column("front_image"), DisplayName("正面照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? FrontImage { get; set; }
    /// <summary>
    /// 反面照片
    /// </summary>
    [Column("back_image"), DisplayName("反面照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? BackImage { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    [Column("description"), DisplayName("备注说明"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }
}