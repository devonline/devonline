﻿namespace Devonline.Entity;

/// <summary>
/// MongoDB的密文带数据隔离的数据对象模型
/// </summary>
public interface IDataSecurityIsolateMongoModel : IDataSecurityMongoModel, IIsolateMongoModel, IDataSecurity, IIsolate, IMongoModel, IViewModel, IEntitySet;