﻿namespace Devonline.Entity;

/// <summary>
/// MongoDB 带数据隔离的顶级数据对象模型接口
/// </summary>
public interface IIsolateMongoModel : IIsolate, IMongoModel, IViewModel, IEntitySet;