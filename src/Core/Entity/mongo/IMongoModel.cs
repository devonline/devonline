﻿namespace Devonline.Entity;

/// <summary>
/// MongoDB 顶级数据对象模型接口
/// </summary>
public interface IMongoModel : IViewModel, IEntitySet;