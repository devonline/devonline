﻿namespace Devonline.Entity;

/// <summary>
/// MongoDB的密文数据对象模型
/// </summary>
public interface IDataSecurityMongoModel : IDataSecurity, IMongoModel, IViewModel, IEntitySet;