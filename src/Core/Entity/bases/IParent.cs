﻿namespace Devonline.Entity;

/// <summary>
/// 具有上下级关系的数据对象模型基础接口
/// 字符串主键的默认接口
/// </summary>
public interface IParent : IParent<string>;

/// <summary>
/// 具有上下级关系的数据对象模型基础接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IParent<TKey>
{
    /// <summary>
    /// 外键, 父级编号
    /// </summary>
    TKey? ParentId { get; set; }
}