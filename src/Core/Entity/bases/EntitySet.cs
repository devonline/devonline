﻿global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.ComponentModel.DataAnnotations.Schema;

namespace Devonline.Entity;

/// <summary>
/// 字符串类型的默认基类
/// </summary>
public abstract class EntitySet : EntitySet<string>, IEntitySet
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySet() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 基础数据对象模型抽象基类
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySet<TKey> : IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySet() => Id = KeyGenerator.GetKey<TKey>();

    /// <summary>
    /// 数据主键
    /// </summary>
    [Column("id"), DisplayName("编号"), DatabaseGenerated(DatabaseGeneratedOption.None), Key, MaxLength(36), Excel]
    public virtual TKey Id { get; set; }

    /// <summary>
    /// 重写的 ToString 方法输出对象的字符串表达式
    /// </summary>
    /// <returns></returns>
    public override string ToString() => this.ToKeyValuePairs().ToString(AppSettings.DEFAULT_OUTER_SPLITER, AppSettings.DEFAULT_INNER_SPLITER);
}