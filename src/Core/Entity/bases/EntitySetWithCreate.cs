﻿namespace Devonline.Entity;

/// <summary>
/// 字符串类型的默认基类
/// </summary>
public abstract class EntitySetWithCreate : EntitySetWithCreate<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithCreate() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 带新增的基础数据模型抽象基类
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySetWithCreate<TKey> : EntitySet<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 行版本号
    /// </summary>
    [Column("row_version"), DisplayName("行版本号"), ConcurrencyCheck, MaxLength(36)]
    public virtual TKey? RowVersion { get; set; }
    /// <summary>
    /// 数据状态
    /// </summary>
    [Column("state", TypeName = "varchar(16)"), DisplayName("数据状态"), DefaultValue(DataState.Available), Excel]
    public virtual DataState State { get; set; } = DataState.Available;
    /// <summary>
    /// 创建时间
    /// </summary>
    [Column("created_on"), DisplayName("创建时间"), Excel]
    public virtual DateTime? CreatedOn { get; set; }
    /// <summary>
    /// 创建人
    /// </summary>
    [Column("created_by"), DisplayName("创建人"), MaxLength(36), Excel]
    public virtual string? CreatedBy { get; set; }

    /// <summary>
    /// 创建方法
    /// </summary>
    public virtual void Create(string? createdBy = default, DateTimeKind kind = DateTimeKind.Utc)
    {
        Id ??= KeyGenerator.GetKey<TKey>();
        RowVersion = Id;
        CreatedOn = kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;
        CreatedBy = createdBy ?? CreatedBy ?? AppSettings.USER_SYSTEM;
    }
}