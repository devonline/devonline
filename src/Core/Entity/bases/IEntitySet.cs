﻿global using Devonline.Core;

namespace Devonline.Entity;

/// <summary>
/// 数据对象模型基础接口
/// 字符串主键的默认接口
/// </summary>
public interface IEntitySet : IEntitySet<string>;

/// <summary>
/// 数据对象模型基础接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IEntitySet<TKey>
{
    /// <summary>
    /// 主键
    /// </summary>
    TKey Id { get; set; }
}
