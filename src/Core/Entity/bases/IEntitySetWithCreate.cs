﻿namespace Devonline.Entity;

/// <summary>
/// 带创建类基础字段的实体对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IEntitySetWithCreate : IEntitySetWithCreate<string>, IEntitySet;

/// <summary>
/// 带创建类基础字段的实体对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IEntitySetWithCreate<TKey> : IEntitySet<TKey>
{
    /// <summary>
    /// 行版本号
    /// </summary>
    TKey? RowVersion { get; set; }
    /// <summary>
    /// 数据状态
    /// 取值为 DataState 枚举值
    /// </summary>
    DataState State { get; set; }
    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime? CreatedOn { get; set; }
    /// <summary>
    /// 创建人
    /// </summary>
    string? CreatedBy { get; set; }

    /// <summary>
    /// 基础类型的新增方法
    /// </summary>
    void Create(string? createdBy = null, DateTimeKind kind = DateTimeKind.Utc);
}
