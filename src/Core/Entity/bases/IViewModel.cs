﻿namespace Devonline.Entity;

/// <summary>
/// 数据对象模型的 View Model 基础抽象接口字符串形式的默认接口
/// </summary>
public interface IViewModel : IViewModel<string>, IEntitySet;

/// <summary>
/// 数据对象模型的 View Model 基础抽象接口
/// </summary>
/// <typeparam name="TKey">业务数据主键类型</typeparam>
public interface IViewModel<TKey> : IEntitySet<TKey>;