﻿namespace Devonline.Entity;

/// <summary>
/// 带更新类基础字段的实体对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IEntitySetWithCreateAndUpdate : IEntitySetWithCreateAndUpdate<string>, IEntitySetWithCreate, IEntitySet;

/// <summary>
/// 带更新类基础字段的实体对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IEntitySetWithCreateAndUpdate<TKey> : IEntitySetWithCreate<TKey>
{
    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime? UpdatedOn { get; set; }
    /// <summary>
    /// 创建人
    /// </summary>
    string? UpdatedBy { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    string? Description { get; set; }

    /// <summary>
    /// 基础类型的修改方法
    /// </summary>
    void Update(string? updatedBy = null, DateTimeKind kind = DateTimeKind.Utc);
}
