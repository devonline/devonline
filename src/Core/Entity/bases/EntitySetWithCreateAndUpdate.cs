﻿namespace Devonline.Entity;

/// <summary>
/// 字符串类型的默认基类
/// </summary>
public abstract class EntitySetWithCreateAndUpdate : EntitySetWithCreateAndUpdate<string>, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithCreateAndUpdate() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 带更新的基础数据对象模型抽象基类
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySetWithCreateAndUpdate<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 更新时间
    /// </summary>
    [Column("updated_on"), DisplayName("更新时间"), Excel]
    public virtual DateTime? UpdatedOn { get; set; }
    /// <summary>
    /// 更新人
    /// </summary>
    [Column("updated_by"), DisplayName("更新人"), MaxLength(36), Excel]
    public virtual string? UpdatedBy { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    [Column("description"), DisplayName("备注说明"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }

    /// <summary>
    /// 更新方法
    /// </summary>
    public virtual void Update(string? updatedBy = default, DateTimeKind kind = DateTimeKind.Utc)
    {
        RowVersion = KeyGenerator.GetKey<TKey>();
        //UpdatedOn = DateTime.SpecifyKind(DateTime.Now, kind);
        UpdatedOn = kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;
        UpdatedBy = updatedBy ?? UpdatedBy ?? AppSettings.USER_SYSTEM;
    }
}