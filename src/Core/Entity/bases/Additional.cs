﻿namespace Devonline.Entity;

/// <summary>
/// 键值对, 子类中和对应的实体数据建立主子表关系以保存额外的信息, 字符串类型的抽象实现
/// 新版本 efcore 可以使用 json 类型字段, 因此不在需要建立独立表了
/// </summary>
public class Additional : IKeyValuePair
{
    /// <summary>
    /// 序号
    /// </summary>
    [Column("index"), DisplayName("序号"), Excel]
    public virtual int Index { get; set; }
    /// <summary>
    /// 键
    /// </summary>
    [Column("key"), DisplayName("键"), Required, MaxLength(128), Excel]
    public virtual string Key { get; set; } = null!;
    /// <summary>
    /// 值
    /// </summary>
    [Column("value"), DisplayName("值"), Required, MaxLength(255), Excel]
    public virtual string Value { get; set; } = null!;
}