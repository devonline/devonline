﻿namespace Devonline.Entity;

/// <summary>
/// 数据对象模型的 View Model 字符串作为主键的基础抽象类型
/// </summary>
public abstract class ViewModel : ViewModel<string>, IViewModel, IEntitySet { }

/// <summary>
/// 数据对象模型的 View Model 基础抽象类型
/// </summary>
/// <typeparam name="TKey">业务数据主键类型</typeparam>
public abstract class ViewModel<TKey> : IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public ViewModel() => Id = KeyGenerator.GetKey<TKey>();

    /// <summary>
    /// 数据主键
    /// </summary>
    [DisplayName("编号"), MaxLength(36)]
    public virtual TKey Id { get; set; }
    /// <summary>
    /// 数据状态
    /// </summary>
    [DisplayName("数据状态"), Excel]
    public virtual DataState State { get; set; } = DataState.Available;
}