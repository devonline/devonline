﻿namespace Devonline.Entity;

/// <summary>
/// 带键值对的基础数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IKeyValuePair : IKeyValuePair<string, string>;

/// <summary>
/// 带键值对的基础数据对象模型接口
/// </summary>
/// <typeparam name="TKey">键类型</typeparam>
/// <typeparam name="TValue">值类型</typeparam>
public interface IKeyValuePair<TKey, TValue> where TKey : IConvertible, IEquatable<TKey>
{
    /// <summary>
    /// 序号
    /// </summary>
    int Index { get; set; }
    /// <summary>
    /// 键
    /// </summary>
    TKey Key { get; set; }
    /// <summary>
    /// 值
    /// </summary>
    TValue Value { get; set; }
}