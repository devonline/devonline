﻿namespace Devonline.Entity;

/// <summary>
/// 具有上下级关系的数据对象模型基础接口字符串类型的默认基类
/// </summary>
public abstract class EntitySetWithParent : EntitySetWithParent<string>, IParent, IEntitySet
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithParent() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有上下级关系的数据对象模型基础接口
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySetWithParent<TKey> : EntitySet<TKey>, IParent<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 父项编号
    /// </summary>
    [Column("parent_id"), DisplayName("父级编号"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
}