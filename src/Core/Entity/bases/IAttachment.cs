﻿namespace Devonline.Entity;

/// <summary>
/// 附加信息基础数据抽象接口
/// 字符串主键的默认接口
/// </summary>
public interface IAttachment : IAttachment<string>, IBusiness, IEntitySet;

/// <summary>
/// 附件基础数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IAttachment<TKey> : IBusiness<TKey>, IEntitySet<TKey>
{
    /// <summary>
    /// 附件名
    /// </summary>
    string Name { get; set; }
    /// <summary>
    /// 文件大小
    /// </summary>
    long Length { get; set; }
    /// <summary>
    /// 文件扩展名
    /// </summary>
    string Extension { get; set; }
    /// <summary>
    /// 文件的 content type
    /// </summary>
    string ContentType { get; set; }
    /// <summary>
    /// 附件地址, 相对地址加上文件名
    /// </summary>
    string Path { get; set; }
}