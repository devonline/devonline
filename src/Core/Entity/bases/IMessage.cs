﻿namespace Devonline.Entity;

/// <summary>
/// 消息接口, 消息是不存储在关系型数据库中的数据, 因此不需要实现
/// </summary>
public interface IMessage : IMessage<string>, IViewModel, IEntitySet;

/// <summary>
/// 消息接口, 消息是不存储在关系型数据库中的数据, 因此不需要实现
/// </summary>
/// <typeparam name="TContent"></typeparam>
public interface IMessage<TContent> : IMessage<string, TContent>, IViewModel, IEntitySet;

/// <summary>
/// 消息接口, 消息是不存储在关系型数据库中的数据, 因此不需要实现
/// </summary>
/// <typeparam name="TKey"></typeparam>
/// <typeparam name="TContent"></typeparam>
public interface IMessage<TKey, TContent> : IViewModel<TKey>, IEntitySet<TKey>
{
    /// <summary>
    /// 消息类型, MessageType 枚举值
    /// </summary>
    MessageType Type { get; set; }
    /// <summary>
    /// 发送者, 记录谁发出来的消息
    /// </summary>
    TKey Sender { get; set; }
    /// <summary>
    /// 接收者, 记录消息的接收对象, 如果接收对象是一个群组中的成员, 则当前消息为 @ 该成员的消息
    /// </summary>
    TKey? Receiver { get; set; }
    /// <summary>
    /// 消息群组, 如果消息是发到群里的, 需要记录群组编号, 消息群组的用法如下:
    /// 1. Group 没有值, Receiver 有值, 则消息是发给独立个人的
    /// 2. Group 有值, Receiver 没有值, 则消息是发到群组里
    /// 3. Group 和 Receiver 都有值, 则消息是发到群组里 @ 个人的
    /// 4. Group 和 Receiver 都没有值, 则是发给所有人的消息
    /// </summary>
    TKey? Group { get; set; }
    /// <summary>
    /// 引用的消息, 关联的消息, 回复的消息
    /// </summary>
    TKey? Reply { get; set; }
    /// <summary>
    /// 消息发送者客户端编号
    /// </summary>
    TKey? From { get; set; }
    /// <summary>
    /// 消息接收者客户端编号
    /// </summary>
    TKey? To { get; set; }

    /// <summary>
    /// 创建时间, 消息的创建时间
    /// </summary>
    DateTime CreateTime { get; set; }
    /// <summary>
    /// 过期时间, 消息的有效期内, 如果客户端重新上线, 则需要重新发送尚未过期的消息
    /// </summary>
    DateTime? ExpireTime { get; set; }
    /// <summary>
    /// 发送时间, 消息一经发出即获得发送时间
    /// </summary>
    DateTime? SendTime { get; set; }
    /// <summary>
    /// 接收时间, 消息一经接收, 服务器收到回执, 即获得接收时间
    /// </summary>
    DateTime? ReceiveTime { get; set; }
    /// <summary>
    /// 读取时间, 消息被已读时的时间, 服务器收到接收者发送的已读信号设置消息状态为已读
    /// </summary>
    DateTime? ReadTime { get; set; }

    /// <summary>
    /// 消息内容
    /// </summary>
    TContent? Content { get; set; }
}