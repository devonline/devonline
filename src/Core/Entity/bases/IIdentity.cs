﻿namespace Devonline.Entity;

/// <summary>
/// 授权控制方面的顶级接口
/// 字符串主键的默认接口
/// </summary>
public interface IIdentity : IIdentity<string>, IEntitySet;

/// <summary>
/// 授权控制方面的顶级接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IIdentity<TKey> : IEntitySet<TKey>
{
    /// <summary>
    /// 名称
    /// </summary>
    string? Name { get; set; }
    /// <summary>
    /// 称呼, 昵称, 别名
    /// </summary>
    string? Alias { get; set; }
    /// <summary>
    /// 头像, 图标, 封面等, 可以保存路径或者图标名称
    /// </summary>
    string? Image { get; set; }
    /// <summary>
    /// 身份授权类型
    /// </summary>
    AuthorizeType Type { get; set; }
}