﻿namespace Devonline.Entity;

/// <summary>
/// 通用身份抽象基类
/// 字符串形式默认实现
/// </summary>
public abstract class Identity : Identity<string>, IIdentity, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Identity() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 通用身份抽象基类
/// </summary>
public abstract class Identity<TKey> : EntitySetWithCreateAndUpdate<TKey>, IIdentity<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [Column("name"), Required, MaxLength(255), DisplayName("名称"), Excel]
    public virtual string? Name { get; set; }
    /// <summary>
    /// 昵称
    /// </summary>
    [Column("alias"), MaxLength(255), DisplayName("昵称"), Excel]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 头像
    /// </summary>
    [Column("image"), MaxLength(255), DisplayName("头像"), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 身份授权类型, 现在是有 Flags 后, 允许两个值合成
    /// </summary>
    [Column("type", TypeName = "varchar(36)"), DisplayName("授权类型"), Required, DefaultValue(AuthorizeType.Internal), Excel]
    public virtual AuthorizeType Type { get; set; } = AuthorizeType.Internal;
}