﻿namespace Devonline.Entity;

/// <summary>
/// 价格历史/供应商报价历史
/// </summary>
[Table("price_history"), DisplayName("价格历史")]
public class PriceHistory : PriceHistory<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 供应商
    /// </summary>
    public virtual Supplier? Supplier { get; set; }
    /// <summary>
    /// 货品
    /// </summary>
    public virtual Product? Product { get; set; }
}

/// <summary>
/// 价格历史/供应商报价历史
/// </summary>
[Table("price_history"), DisplayName("价格历史")]
public abstract class PriceHistory<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 供应商
    /// </summary>
    [Column("supplier_id"), MaxLength(36), DisplayName("供应商"), Required, Excel]
    public virtual TKey SupplierId { get; set; } = default!;
    /// <summary>
    /// 货品编号
    /// </summary>
    [Column("product_id"), MaxLength(36), DisplayName("货品编号"), Required, Excel]
    public virtual TKey ProductId { get; set; } = default!;
    /// <summary>
    /// 报价
    /// </summary>
    [Column("price", TypeName = "decimal(18,4)"), DisplayName("报价"), Excel]
    public virtual decimal Price { get; set; }
    /// <summary>
    /// 报价汇率, 报价时的当时汇率, 是指结算货币相对于人民币的汇率
    /// 如果报价时货币为人民币, 则汇率默认为: 1
    /// </summary>
    [Column("exchange_rate", TypeName = "decimal(12,6)"), DisplayName("汇率"), Excel]
    public virtual decimal ExchangeRate { get; set; } = 1;
    /// <summary>
    /// 报价货币
    /// </summary>
    [Column("currency", TypeName = "varchar(8)"), DisplayName("货币"), DefaultValue(Currency.CNY), Excel]
    public virtual Currency Currency { get; set; }
}