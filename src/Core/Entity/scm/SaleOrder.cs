﻿namespace Devonline.Entity;

/// <summary>
/// 销售订单
/// 数据规模: 万
/// </summary>
[Table("sale_order"), DisplayName("销售订单")]
public class SaleOrder : SaleOrder<string>, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 下单客户
    /// </summary>
    public virtual Customer? Customer { get; set; }
    /// <summary>
    /// 销售订单明细
    /// </summary>
    public virtual ICollection<SaleOrderDetail>? Details { get; set; }
    /// <summary>
    /// 销售订单支付记录
    /// </summary>
    public virtual ICollection<SaleOrderPaymentRecord>? PaymentRecords { get; set; }
}

/// <summary>
/// 销售订单
/// 数据规模: 万
/// </summary>
[Table("sale_order"), DisplayName("销售订单")]
public abstract class SaleOrder<TKey> : Order<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 客户编号
    /// </summary>
    [Column("customer_id"), DisplayName("客户编号"), Required, MaxLength(36), Excel]
    public virtual TKey CustomerId { get; set; } = default!;
}

/// <summary>
/// 订单明细
/// </summary>
[Table("sale_order_detail"), DisplayName("订单明细")]
public class SaleOrderDetail : OrderDetail<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 订单
    /// </summary>
    public virtual SaleOrder? Order { get; set; }
    /// <summary>
    /// 货品
    /// </summary>
    public virtual Product? Product { get; set; }
}

/// <summary>
/// 销售订单支付记录
/// </summary>
[Table("sale_order_payment_record"), DisplayName("销售订单支付记录")]
public class SaleOrderPaymentRecord : AccountRecord<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 交易账户
    /// </summary>
    public virtual Account? Account { get; set; }
    /// <summary>
    /// 销售订单
    /// </summary>
    public virtual SaleOrder? Order { get; set; }
}