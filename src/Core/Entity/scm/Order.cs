﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 销售订单
/// 数据规模: 万
/// </summary>
[Table("order"), DisplayName("订单"), Index(nameof(OrderNumber), IsUnique = true)]
public abstract class Order<TKey> : EntitySetWithCreateAndUpdate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 订单号
    /// </summary>
    [Column("order_number"), DisplayName("订单号"), Required, MaxLength(36)]
    public virtual string OrderNumber { get; set; } = null!;
    /// <summary>
    /// 下单时间
    /// </summary>
    [Column("order_time"), DisplayName("下单时间"), Excel]
    public virtual DateTime OrderTime { get; set; }
    /// <summary>
    /// 订单金额
    /// </summary>
    [Column("amount", TypeName = "decimal(18,4)"), DisplayName("订单金额"), Excel]
    public virtual decimal Amount { get; set; }
    /// <summary>
    /// 订单汇率, 下单时的当时汇率, 是指结算货币相对于人民币的汇率
    /// 如果订单结算时为人民币支付, 则汇率默认为: 1
    /// </summary>
    [Column("exchange_rate", TypeName = "decimal(12,6)"), DisplayName("汇率"), Excel]
    public virtual decimal ExchangeRate { get; set; } = 1;
    /// <summary>
    /// 结算货币
    /// </summary>
    [Column("currency", TypeName = "varchar(8)"), DisplayName("结算货币"), DefaultValue(Currency.CNY), Excel]
    public virtual Currency Currency { get; set; }
    /// <summary>
    /// 总重量
    /// </summary>
    [Column("weight", TypeName = "decimal(18,2)"), DisplayName("总重量"), Excel]
    public virtual decimal? Weight { get; set; }
    /// <summary>
    /// 总体积
    /// </summary>
    [Column("volume", TypeName = "decimal(18,2)"), DisplayName("总体积"), Excel]
    public virtual decimal? Volume { get; set; }
    /// <summary>
    /// 运费
    /// </summary>
    [Column("freight", TypeName = "decimal(18,2)"), DisplayName("运费"), Excel]
    public virtual decimal? Freight { get; set; }
    /// <summary>
    /// 运单号
    /// </summary>
    [Column("tracking_number"), DisplayName("运单号"), MaxLength(36)]
    public virtual string? TrackingNumber { get; set; }
    /// <summary>
    /// 发货时间
    /// </summary>
    [Column("shipping_time"), DisplayName("发货时间"), Excel]
    public virtual DateTime? ShippingTime { get; set; }
    /// <summary>
    /// 到货时间
    /// </summary>
    [Column("deliver_time"), DisplayName("到货时间"), Excel]
    public virtual DateTime? DeliverTime { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    [Column("description"), DisplayName("备注说明"), MaxLength(1024), Excel]
    public override string? Description { get; set; }
}