﻿namespace Devonline.Entity;

/// <summary>
/// 采购订单和订单的货品对应关系
/// </summary>
[Table("order_correlation"), DisplayName("订单关联")]
public class OrderCorrelation : OrderCorrelation<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 销售订单
    /// </summary>
    public virtual SaleOrder? SaleOrder { get; set; }
    /// <summary>
    /// 采购订单
    /// </summary>
    public virtual PurchaseOrder? PurchaseOrder { get; set; }
}

/// <summary>
/// 采购订单和订单的货品对应关系
/// </summary>
[Table("order_correlation"), DisplayName("订单关联")]
public abstract class OrderCorrelation<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 销售订单编号
    /// </summary>
    [Column("sale_order_id"), DisplayName("销售订单编号"), Required, MaxLength(36), Excel]
    public virtual TKey SaleOrderId { get; set; } = default!;
    /// <summary>
    /// 采购订单编号
    /// </summary>
    [Column("purchase_order_id"), DisplayName("采购订单编号"), Required, MaxLength(36), Excel]
    public virtual TKey PurchaseOrderId { get; set; } = default!;
}