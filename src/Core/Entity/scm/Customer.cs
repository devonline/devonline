﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 客户
/// </summary>
[Table("customer"), DisplayName("客户")]
public class Customer : Customer<string>, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 客户所在区域
    /// </summary>
    public virtual Region? Region { get; set; }
    /// <summary>
    /// 客户账户
    /// </summary>
    public virtual Account? Account { get; set; }
    /// <summary>
    /// 客户联系人
    /// </summary>
    public virtual ICollection<CustomerContact>? Contacts { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 客户
/// </summary>
[Table("customer"), DisplayName("客户"), Index(nameof(Code), IsUnique = true)]
public abstract class Customer<TKey> : EntitySetWithCreateAndUpdate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 区域编号
    /// </summary>
    [Column("region_id"), DisplayName("区域编号"), Required, MaxLength(36), Excel]
    public virtual TKey RegionId { get; set; } = default!;
    /// <summary>
    /// 客户账户编号
    /// </summary>
    [Column("account_id"), DisplayName("客户账户编号"), Required, MaxLength(36), Excel]
    public virtual TKey AccountId { get; set; } = default!;
    /// <summary>
    /// 客户名称
    /// </summary>
    [Column("name"), Required, MaxLength(128), DisplayName("客户名称"), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 昵称, 别名
    /// </summary>
    [Column("alias"), MaxLength(255), DisplayName("别名"), Excel]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 客户照片
    /// </summary>
    [Column("image"), MaxLength(128), DisplayName("客户照片"), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 客户编码
    /// </summary>
    [Column("code"), MaxLength(16), DisplayName("客户编码"), Required, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 客户类型
    /// </summary>
    [Column("type"), MaxLength(128), DisplayName("客户类型"), Excel]
    public virtual string? Type { get; set; }
    /// <summary>
    /// 客户品质
    /// </summary>
    [Column("quality"), MaxLength(128), DisplayName("客户品质"), Excel]
    public virtual string? Quality { get; set; }
}

/// <summary>
/// 客户联系人
/// </summary>
[Table("customer_contact"), DisplayName("客户联系人")]
public class CustomerContact : CustomerContact<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 客户
    /// </summary>
    public virtual Customer? Customer { get; set; }
    /// <summary>
    /// 联系人
    /// </summary>
    public virtual Personal? Contact { get; set; }
}

/// <summary>
/// 客户联系人
/// </summary>
[Table("customer_contact"), DisplayName("客户联系人")]
public abstract class CustomerContact<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 客户编号
    /// </summary>
    [Column("customer_id"), DisplayName("客户编号"), Required, MaxLength(36), Excel]
    public virtual TKey CustomerId { get; set; } = default!;
    /// <summary>
    /// 联系人编号
    /// </summary>
    [Column("contact_id"), DisplayName("联系人编号"), Required, MaxLength(36), Excel]
    public virtual TKey ContactId { get; set; } = default!;
}