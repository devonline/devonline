﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 供应商
/// </summary>
[Table("supplier"), DisplayName("供应商")]
public class Supplier : Supplier<string>, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 供应商所在区域
    /// </summary>
    public virtual Region? Region { get; set; }
    /// <summary>
    /// 供应商联系人
    /// </summary>
    public virtual ICollection<SupplierContact>? Contacts { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 供应商
/// </summary>
[Table("supplier"), DisplayName("供应商"), Index(nameof(Code), IsUnique = true)]
public abstract class Supplier<TKey> : Customer<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 供应商编码
    /// </summary>
    [Column("code"), MaxLength(16), DisplayName("供应商编码"), Excel]
    public override string Code { get; set; } = null!;
    /// <summary>
    /// 供应商类型
    /// </summary>
    [Column("type"), MaxLength(128), DisplayName("供应商类型"), Excel]
    public override string? Type { get; set; }
    /// <summary>
    /// 供应商品质
    /// </summary>
    [Column("quality"), MaxLength(128), DisplayName("供应商品质"), Excel]
    public override string? Quality { get; set; }
}

/// <summary>
/// 供应商联系人
/// </summary>
[Table("supplier_contact"), DisplayName("供应商联系人")]
public class SupplierContact : SupplierContact<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 供应商
    /// </summary>
    public virtual Supplier? Supplier { get; set; }
    /// <summary>
    /// 联系人
    /// </summary>
    public virtual Personal? Contact { get; set; }
}

/// <summary>
/// 供应商联系人
/// </summary>
[Table("supplier_contact"), DisplayName("供应商联系人")]
public abstract class SupplierContact<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 供应商编号
    /// </summary>
    [Column("supplier_id"), DisplayName("供应商编号"), Required, MaxLength(36), Excel]
    public virtual TKey SupplierId { get; set; } = default!;
    /// <summary>
    /// 联系人编号
    /// </summary>
    [Column("contact_id"), DisplayName("联系人编号"), Required, MaxLength(36), Excel]
    public virtual TKey ContactId { get; set; } = default!;
}