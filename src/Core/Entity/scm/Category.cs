﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Entity;

/// <summary>
/// 货品类别
/// </summary>
[Table("category"), DisplayName("货品类别")]
public class Category : Category<string>, IParent, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 父项品类
    /// </summary>
    public virtual Category? Parent { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 货品类别
/// </summary>
[Table("category"), DisplayName("货品类别"), Index(nameof(Code), IsUnique = true)]
public abstract class Category<TKey> : EntitySetWithCreate<TKey>, IParent<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [Column("name"), Required, MaxLength(128), DisplayName("名称"), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 昵称, 别名
    /// </summary>
    [Column("alias"), MaxLength(255), DisplayName("别名"), Excel]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 类别编码
    /// </summary>
    [Column("code"), MaxLength(16), DisplayName("类别编码"), Required, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 封面图
    /// </summary>
    [Column("image"), MaxLength(128), DisplayName("封面图"), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 上一级类别
    /// </summary>
    [Column("parent_id"), DisplayName("上一级类别编号"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
}