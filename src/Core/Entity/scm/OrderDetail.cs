﻿namespace Devonline.Entity;

/// <summary>
/// 订单明细
/// 订单明细中的结算货币和汇率跟着订单走, 一个订单使用同一个结算货币和汇率
/// </summary>
[Table("order_detail"), DisplayName("订单明细")]
public abstract class OrderDetail<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 订单编号
    /// </summary>
    [Column("order_id"), DisplayName("订单编号"), Required, MaxLength(36), Excel]
    public virtual string OrderId { get; set; } = null!;
    /// <summary>
    /// 货品编号
    /// </summary>
    [Column("product_id"), DisplayName("货品编号"), Required, MaxLength(36), Excel]
    public virtual string ProductId { get; set; } = null!;
    /// <summary>
    /// 单价
    /// </summary>
    [Column("price", TypeName = "decimal(18,4)"), DisplayName("单价"), Excel]
    public virtual decimal Price { get; set; }
    /// <summary>
    /// 数量
    /// </summary>
    [Column("count", TypeName = "decimal(12,2)"), DisplayName("数量"), Excel]
    public virtual decimal Count { get; set; }
}