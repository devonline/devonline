﻿namespace Devonline.Entity;

/// <summary>
/// 采购订单
/// </summary>
[Table("purchase_order"), DisplayName("采购订单")]
public class PurchaseOrder : PurchaseOrder<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 供应商
    /// </summary>
    public virtual Supplier? Supplier { get; set; }
    /// <summary>
    /// 采购订单明细
    /// </summary>
    public virtual ICollection<PurchaseOrderDetail>? Details { get; set; }
    /// <summary>
    /// 采购订单支付记录
    /// </summary>
    public virtual ICollection<PurchaseOrderPaymentRecord>? PaymentRecords { get; set; }
}

/// <summary>
/// 采购订单
/// </summary>
[Table("purchase_order"), DisplayName("采购订单")]
public abstract class PurchaseOrder<TKey> : Order<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 供应商
    /// </summary>
    [Column("supplier_id"), MaxLength(36), DisplayName("供应商"), Required, Excel]
    public virtual string SupplierId { get; set; } = null!;
}

/// <summary>
/// 采购订单明细
/// </summary>
[Table("purchase_order_detail"), DisplayName("采购订单明细")]
public class PurchaseOrderDetail : OrderDetail<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 采购订单
    /// </summary>
    public virtual PurchaseOrder? Order { get; set; }
    /// <summary>
    /// 货品
    /// </summary>
    public virtual Product? Product { get; set; }
}

/// <summary>
/// 采购订单支付记录
/// </summary>
[Table("purchase_order_payment_record"), DisplayName("采购订单支付记录")]
public class PurchaseOrderPaymentRecord : AccountRecord<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 交易账户
    /// </summary>
    public virtual Account? Account { get; set; }
    /// <summary>
    /// 采购订单
    /// </summary>
    public virtual PurchaseOrder? Order { get; set; }
}