﻿namespace Devonline.Entity;

/// <summary>
/// database endpoint
/// </summary>
public interface IDatabaseEndpoint : IEndpoint, IAuthEndpoint
{
    /// <summary>
    /// database connection string
    /// </summary>
    string? ConnectionString { get; set; }
    /// <summary>
    /// database type
    /// </summary>
    DatabaseType DatabaseType { get; set; }
    /// <summary>
    /// end point default database
    /// </summary>
    string? Database { get; set; }
    /// <summary>
    /// end point default database table name
    /// </summary>
    string? DataTable { get; set; }
    /// <summary>
    /// 限制查询或写入的行数, 默认 0, 即不限制
    /// </summary>
    int? Limit { get; set; }
    /// <summary>
    /// 分页的偏移量, 配合 Limit 设置
    /// </summary>
    int? Offset { get; set; }
    /// <summary>
    /// 分页的页码
    /// </summary>
    int? PageIndex { get; set; }
    /// <summary>
    /// 分页的页大小, 配合 PageIndex 设置
    /// </summary>
    int? PageSize { get; set; }
    /// <summary>
    /// 数据访问的总数目, 不需要配置, 数据处理过程中记录用的
    /// </summary>
    long? Total { get; set; }
    /// <summary>
    /// 重试次数, 写入失败时的再次尝试次数
    /// </summary>
    int? Retry { get; set; }
    /// <summary>
    /// 时间精度
    /// </summary>
    TimeKind Precision { get; set; }
}