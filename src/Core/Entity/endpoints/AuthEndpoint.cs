﻿namespace Devonline.Entity;

/// <summary>
/// 认证相关终结点配置
/// </summary>
[Table("endpoint"), DisplayName("终结点")]
public class AuthEndpoint : Endpoint, IAuthEndpoint, IEndpoint, IEntitySet
{
    /// <summary>
    /// authorization type
    /// </summary>
    [Column("auth_type", TypeName = "varchar(16)"), DisplayName("认证类型"), DefaultValue(AuthType.None), Excel]
    public virtual AuthType AuthType { get; set; } = AuthType.None;
    /// <summary>
    /// user name
    /// </summary>
    [Column("user_name"), DisplayName("用户名"), MaxLength(32), Excel]
    public virtual string? UserName { get; set; }
    /// <summary>
    /// password
    /// </summary>
    [Column("password"), DisplayName("密码"), MaxLength(255), Excel]
    public virtual string? Password { get; set; }
    /// <summary>
    /// app id
    /// </summary>
    [Column("app_id"), DisplayName("应用编号"), MaxLength(36), Excel]
    public virtual string? AppId { get; set; }
    /// <summary>
    /// app secret
    /// </summary>
    [Column("secret"), DisplayName("应用密码"), MaxLength(128), Excel]
    public virtual string? Secret { get; set; }
    /// <summary>
    /// 授权类型
    /// </summary>
    [Column("grant_type"), DisplayName("授权类型"), MaxLength(36), Excel]
    public virtual string? GrantType { get; set; }
    /// <summary>
    /// app scope
    /// </summary>
    [Column("scope"), DisplayName("应用授权作用域"), MaxLength(128), Excel]
    public virtual string? Scope { get; set; }
    /// <summary>
    /// 访问令牌
    /// </summary>
    [Column("token"), DisplayName("访问令牌"), MaxLength(4096), Excel]
    public virtual string? Token { get; set; }
    /// <summary>
    /// 所在域
    /// </summary>
    [Column("domain"), DisplayName("所在域"), MaxLength(128), Excel]
    public virtual string? Domain { get; set; }
}