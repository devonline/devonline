﻿namespace Devonline.Entity;

/// <summary>
/// 认证相关终结点配置
/// </summary>
public interface IAuthEndpoint : IEndpoint
{
    /// <summary>
    /// authorization type
    /// </summary>
    AuthType AuthType { get; set; }
    /// <summary>
    /// user name
    /// </summary>
    string? UserName { get; set; }
    /// <summary>
    /// password
    /// </summary>
    string? Password { get; set; }
    /// <summary>
    /// app id
    /// </summary>
    string? AppId { get; set; }
    /// <summary>
    /// app secret
    /// </summary>
    string? Secret { get; set; }
    /// <summary>
    /// grant type
    /// </summary>
    string? GrantType { get; set; }
    /// <summary>
    /// 应用授权作用域
    /// </summary>
    string? Scope { get; set; }
    /// <summary>
    /// 访问令牌
    /// </summary>
    string? Token { get; set; }
    /// <summary>
    /// 所在域
    /// </summary>
    string? Domain { get; set; }
}