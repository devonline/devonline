﻿namespace Devonline.Entity;

/// <summary>
/// database endpoint configuration
/// </summary>
[Table("endpoint"), DisplayName("终结点")]
public class DistributedEndpoint : AuthEndpoint, IDistributedEndpoint, IAuthEndpoint, IEndpoint, IEntitySet
{
    /// <summary>
    /// endpoint mode
    /// </summary>
    [Column("mode", TypeName = "varchar(16)"), DisplayName("终结点模式"), DefaultValue(EndpointMode.Standalone), Excel]
    public virtual EndpointMode Mode { get; set; } = EndpointMode.Standalone;
    /// <summary>
    /// endpoint type
    /// </summary>
    [Column("type", TypeName = "varchar(16)"), DisplayName("终结点类型"), DefaultValue(EndpointType.Main), Excel]
    public virtual EndpointType Type { get; set; } = EndpointType.Main;
    /// <summary>
    /// 基准码, 即为最大节点数量, 用于决定最多可以有多少个分布式节点, 取值 2^N-1, N 为二进制字长;
    /// 如 0xFF, 255, 8 位二进制;
    /// 默认值为 1, 即为只有一台
    /// </summary>
    [Column("benchmark"), DisplayName("基准码"), Excel]
    public virtual int Benchmark { get; set; } = AppSettings.UNIT_ONE;
    /// <summary>
    /// 机器码, 用于产生固定机器码后缀的 key 值, 不能超过基准后缀码的范围;
    /// 默认值 0x00; 即第 0 号机器节点;
    /// 
    /// 当前机器码, 最大 5 位二进制;
    /// 默认值 0x00; 即第 0 号机器节点;
    /// </summary>
    [Column("machine_code"), DisplayName("机器码"), Excel]
    public virtual int MachineCode { get; set; } = 0x00;
    /// <summary>
    /// 当前数据中心码, 最大 5 位二进制
    /// 默认值 0x00; 即第 0 号数据中心节点;
    /// </summary>    
    [Column("data_center_code"), DisplayName("当前数据中心码"), Excel]
    public virtual int DataCenterCode { get; set; } = 0x00;
}