﻿namespace Devonline.Entity;

/// <summary>
/// endpoint configuration
/// </summary>
public interface IEndpoint
{
    /// <summary>
    /// endpoint id
    /// </summary>
    string Id { get; set; }
    /// <summary>
    /// end point name, db name eg
    /// </summary>
    string Name { get; set; }
    /// <summary>
    /// 描述
    /// </summary>
    string? Doc { get; set; }
    /// <summary>
    /// host name or ip address
    /// </summary>
    string? Host { get; set; }
    /// <summary>
    /// host port
    /// </summary>
    int? Port { get; set; }
    /// <summary>
    /// endpoint path
    /// </summary>
    string? Path { get; set; }
    /// <summary>
    /// endpoint protocol
    /// </summary>
    ProtocolType Protocol { get; set; }
}