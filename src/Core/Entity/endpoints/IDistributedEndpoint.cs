﻿namespace Devonline.Entity;

/// <summary>
/// distributed endpoint
/// </summary>
public interface IDistributedEndpoint : IEndpoint, IAuthEndpoint
{
    /// <summary>
    /// endpoint mode
    /// </summary>
    public EndpointMode Mode { get; set; }
    /// <summary>
    /// endpoint type
    /// </summary>
    public EndpointType Type { get; set; }
    /// <summary>
    /// 基准机器码, 即为最大节点数量, 用于决定最多可以有多少个分布式节点, 取值 2^N-1, N 为二进制字长;
    /// 如 0xFF, 255, 8 位二进制;
    /// 默认值为 1, 即为只有一台
    /// </summary>
    public int Benchmark { get; set; }
    /// <summary>
    /// 机器码, 用于产生固定机器码后缀的 key 值, 不能超过基准后缀码的范围;
    /// 默认值 0x00; 即第 0 号机器节点;
    /// 
    /// 当前机器码, 最大 5 位二进制;
    /// 默认值 0x00; 即第 0 号机器节点;
    /// </summary>
    public int MachineCode { get; set; }
    /// <summary>
    /// 当前数据中心码, 最大 5 位二进制
    /// 默认值 0x00; 即第 0 号数据中心节点;
    /// </summary>
    public int DataCenterCode { get; set; }
}