﻿namespace Devonline.Entity;

/// <summary>
/// endpoint configuration
/// </summary>
[Table("endpoint"), DisplayName("终结点")]
public class Endpoint : EntitySet, IEndpoint, IEntitySet
{
    /// <summary>
    /// end point name, db name eg
    /// </summary>
    [Column("name"), DisplayName("名称"), Required, MaxLength(128), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 描述
    /// </summary>
    [Column("doc"), DisplayName("描述"), MaxLength(255), Excel]
    public virtual string? Doc { get; set; }
    /// <summary>
    /// 图标, 图标文件路径
    /// </summary>
    [Column("icon"), DisplayName("图标"), MaxLength(255), Excel]
    public virtual string? Icon { get; set; }
    /// <summary>
    /// host name or ip address
    /// </summary>
    [Column("host"), DisplayName("主机名"), MaxLength(255), Excel]
    public virtual string? Host { get; set; }
    /// <summary>
    /// host port
    /// </summary>
    [Column("port"), DisplayName("端口号"), Excel]
    public virtual int? Port { get; set; }
    /// <summary>
    /// endpoint path
    /// </summary>
    [Column("path"), DisplayName("地址"), MaxLength(1024), Excel]
    public virtual string? Path { get; set; }
    /// <summary>
    /// endpoint protocol
    /// </summary>
    [Column("protocol", TypeName = "varchar(16)"), DisplayName("协议"), DefaultValue(ProtocolType.Http), Excel]
    public virtual ProtocolType Protocol { get; set; } = ProtocolType.Http;

    /// <summary>
    /// TODO 试验属性
    /// </summary>
    public virtual ICollection<KeyValuePair<string, string>>? Specs { get; set; }
}