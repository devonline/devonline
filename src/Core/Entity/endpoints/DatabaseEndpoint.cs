﻿namespace Devonline.Entity;

/// <summary>
/// database endpoint configuration
/// </summary>
[Table("endpoint"), DisplayName("终结点")]
public class DatabaseEndpoint : AuthEndpoint, IDatabaseEndpoint, IAuthEndpoint, IEndpoint, IEntitySet
{
    /// <summary>
    /// database type
    /// </summary>
    [Column("database_type", TypeName = "varchar(16)"), DisplayName("数据库类型"), DefaultValue(DatabaseType.PostgreSQL), Excel]
    public virtual DatabaseType DatabaseType { get; set; } = DatabaseType.PostgreSQL;
    /// <summary>
    /// database connection string
    /// </summary>
    [Column("connection_string"), DisplayName("连接字符串"), MaxLength(128), Excel]
    public virtual string? ConnectionString { get; set; }
    /// <summary>
    /// end point default database
    /// </summary>
    [Column("database"), DisplayName("数据库"), MaxLength(128), Excel]
    public virtual string? Database { get; set; }
    /// <summary>
    /// end point default database table name
    /// </summary>
    [Column("data_table"), DisplayName("数据表"), MaxLength(128), Excel]
    public virtual string? DataTable { get; set; }
    /// <summary>
    /// 限制查询或写入的行数, 默认 0, 即不限制
    /// </summary>
    [Column("limit"), DisplayName("获取上限"), Excel]
    public virtual int? Limit { get; set; }
    /// <summary>
    /// 分页的偏移量, 配合 Limit 设置
    /// </summary>
    [Column("offset"), DisplayName("偏移量"), Excel]
    public virtual int? Offset { get; set; }
    /// <summary>
    /// 分页的页码
    /// </summary>
    [Column("page_index"), DisplayName("页码"), Excel]
    public virtual int? PageIndex { get; set; }
    /// <summary>
    /// 分页的页大小, 配合 PageIndex 设置
    /// </summary>
    [Column("page_size"), DisplayName("页大小"), Excel]
    public virtual int? PageSize { get; set; }
    /// <summary>
    /// 数据访问的总数目, 不需要配置, 数据处理过程中记录用的
    /// </summary>
    [Column("total"), DisplayName("总数目"), Excel]
    public virtual long? Total { get; set; }
    /// <summary>
    /// 重试次数, 写入失败时的再次尝试次数
    /// </summary>
    [Column("retry"), DisplayName("重试次数"), Excel]
    public virtual int? Retry { get; set; }
    /// <summary>
    /// 时间精度
    /// </summary>
    [Column("precision", TypeName = "varchar(16)"), DisplayName("时间精度"), DefaultValue(TimeKind.MilliSecond), Excel]
    public virtual TimeKind Precision { get; set; } = TimeKind.MilliSecond;
}