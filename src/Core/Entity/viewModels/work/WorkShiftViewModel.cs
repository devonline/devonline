﻿namespace Devonline.Entity;

/// <summary>
/// 班次, 字符串类型的默认实现
/// </summary>
[DisplayName("班次")]
public class WorkShiftViewModel : WorkShiftViewModel<string>, IViewModel, ITimeRange, IEntitySet, IIdentity
{
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 工作班次
/// 设置和记录某种工作每日的工作时间
/// </summary>
[DisplayName("班次")]
public class WorkShiftViewModel<TKey> : IdentityViewModel<TKey>, IViewModel<TKey>, ITimeRange, IEntitySet<TKey>, IIdentity<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 起始时间
    /// </summary>
    [DisplayName("起始时间"), Excel]
    public TimeOnly? Start { get; set; }
    /// <summary>
    /// 结束时间
    /// </summary>
    [DisplayName("结束时间"), Excel]
    public TimeOnly? End { get; set; }
}