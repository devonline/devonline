﻿namespace Devonline.Entity;

/// <summary>
/// 岗位, 字符串类型的默认实现
/// </summary>
[DisplayName("岗位")]
public class WorkStationViewModel : WorkStationViewModel<string>, IViewModel, IEntitySet, IIdentity
{
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 岗位
/// </summary>
[DisplayName("岗位")]
public class WorkStationViewModel<TKey> : IdentityViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IIdentity<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 所属部门编号
    /// </summary>
    [DisplayName("所属部门编号"), Required, MaxLength(36), Excel]
    public TKey GroupId { get; set; } = default!;
}