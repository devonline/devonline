﻿namespace Devonline.Entity;

/// <summary>
/// 排班/工作计划
/// </summary>
[DisplayName("工作计划")]
public class WorkScheduleViewModel : WorkScheduleViewModel<string>, IViewModel, IDateTimeRange, IEntitySet;

/// <summary>
/// 排班/工作计划
/// 设置和记录某个员工, 在某个时间段, 在某个岗位, 按某个班次工作
/// </summary>
[DisplayName("工作计划")]
public class WorkScheduleViewModel<TKey> : DateTimeRangeViewModel<TKey>, IViewModel<TKey>, IDateTimeRange, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 岗位编号
    /// </summary>
    [DisplayName("岗位编号"), Required, MaxLength(36), Excel]
    public TKey WorkStationId { get; set; } = default!;
    /// <summary>
    /// 班次编号
    /// </summary>
    [DisplayName("班次编号"), Required, MaxLength(36), Excel]
    public TKey WorkShiftId { get; set; } = default!;
    /// <summary>
    /// 个人信息编号
    /// </summary>
    [DisplayName("个人信息编号"), Required, MaxLength(36), Excel]
    public TKey PersonalId { get; set; } = default!;
    /// <summary>
    /// 循环周期
    /// </summary>
    [DisplayName("循环周期"), Required, Excel]
    public TimeKind CyclePeriod { get; set; }
    /// <summary>
    /// 优先级, 当前排班的优先级
    /// </summary>
    [DisplayName("优先级"), Excel]
    public int Priority { get; set; }
}