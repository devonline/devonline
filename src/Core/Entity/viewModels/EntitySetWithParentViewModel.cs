﻿namespace Devonline.Entity;

/// <summary>
/// 具有上下级关系的数据对象模型基础接口字符串类型的默认基类
/// </summary>
public abstract class EntitySetWithParentViewModel : EntitySetWithParentViewModel<string>, IViewModel, IEntitySet, IParent { }

/// <summary>
/// 具有上下级关系的数据对象模型基础接口
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySetWithParentViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IParent<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 父项编号
    /// </summary>
    [DisplayName("父级编号"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
}