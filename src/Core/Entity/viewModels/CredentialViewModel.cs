﻿namespace Devonline.Entity;

/// <summary>
/// 证件, 字符串表达式的默认实现
/// </summary>
[DisplayName("证件")]
public class CredentialViewModel : CredentialViewModel<string>, IViewModel, IEntitySet, IDateTimeRange
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public virtual ICollection<Attachment>? Attachments { get; set; }

    /// <summary>
    /// 定义与证件信息 Credential 的隐式类型转换
    /// </summary>
    /// <param name="model">证件视图对象模型</param>
    public static implicit operator Credential(CredentialViewModel model) => new Credential
    {
        Id = model.Id,
        State = model.State,
        Name = model.Name,
        Code = model.Code,
        Type = model.Type,
        BackImage = model.BackImage,
        Start = model.Start.HasValue ? DateOnly.FromDateTime(model.Start.Value) : null,
        End = model.End.HasValue ? DateOnly.FromDateTime(model.End.Value) : null,
        FrontImage = model.FrontImage,
        HeadImage = model.HeadImage,
        IssuedBy = model.IssuedBy,
        Description = model.Description,

        Attachments = model.Attachments
    };
    /// <summary>
    /// 定义与证件视图对象模型 CredentialViewModel 的隐式类型转换
    /// </summary>
    /// <param name="credential">证件信息</param>
    public static implicit operator CredentialViewModel(Credential credential) => new CredentialViewModel
    {
        Id = credential.Id,
        State = credential.State,
        Name = credential.Name,
        Code = credential.Code,
        Type = credential.Type,
        BackImage = credential.BackImage,
        Start = credential.Start.HasValue ? credential.Start.Value.ToDateTime() : null,
        End = credential.End.HasValue ? credential.End.Value.ToDateTime() : null,
        FrontImage = credential.FrontImage,
        HeadImage = credential.HeadImage,
        IssuedBy = credential.IssuedBy,
        Description = credential.Description,

        Attachments = credential.Attachments
    };
}

/// <summary>
/// 证件
/// </summary>
[DisplayName("证件")]
public abstract class CredentialViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IDateTimeRange where TKey : IConvertible
{
    /// <summary>
    /// 证件名字
    /// </summary>
    [DisplayName("证件名字"), Required, MaxLength(255), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 证件类型
    /// </summary>
    [DisplayName("证件类型"), Required, Excel]
    public virtual CredentialType Type { get; set; } = CredentialType.IdCard;
    /// <summary>
    /// 证件号码
    /// </summary>
    [DisplayName("证件号码"), MaxLength(255), Unique, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 签发机关
    /// </summary>
    [DisplayName("签发机关"), MaxLength(36), Excel]
    public virtual string? IssuedBy { get; set; }
    /// <summary>
    /// 有效期始
    /// </summary>
    [DisplayName("有效期始"), Excel]
    public virtual DateTime? Start { get; set; }
    /// <summary>
    /// 有效期至
    /// </summary>
    [DisplayName("有效期至"), Excel]
    public virtual DateTime? End { get; set; }
    /// <summary>
    /// 证件头像照片
    /// </summary>
    [DisplayName("头像照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? HeadImage { get; set; }
    /// <summary>
    /// 正面照片
    /// </summary>
    [DisplayName("正面照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? FrontImage { get; set; }
    /// <summary>
    /// 反面照片
    /// </summary>
    [DisplayName("反面照片"), MaxLength(128), BusinessType(IsAttachment = true), Excel]
    public virtual string? BackImage { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    [DisplayName("备注说明"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }
}