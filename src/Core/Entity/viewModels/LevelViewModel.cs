﻿namespace Devonline.Entity;

/// <summary>
/// 级别
/// </summary>
[DisplayName("级别")]
public class LevelViewModel : LevelViewModel<string>, IIdentity, IViewModel, IEntitySet
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 级别
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("级别")]
public class LevelViewModel<TKey> : IdentityViewModel<TKey>, IIdentity<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 级别
    /// </summary>
    [DisplayName("级别"), MaxLength(255), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public string? Level { get; set; }
}