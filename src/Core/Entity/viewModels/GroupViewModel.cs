﻿namespace Devonline.Entity;

/// <summary>
/// 组织机构
/// </summary>
[DisplayName("组织机构")]
public class GroupViewModel : GroupViewModel<string>, IIdentity, IParent, IViewModel, IEntitySet
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 组织机构
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("组织机构")]
public class GroupViewModel<TKey> : IdentityViewModel<TKey>, IIdentity<TKey>, IParent<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 上级组织
    /// </summary>
    [DisplayName("上级组织"), MaxLength(36), Excel]
    public TKey? ParentId { get; set; }
    /// <summary>
    /// 组织级别
    /// </summary>
    [DisplayName("组织级别"), MaxLength(36), Excel]
    public TKey? LevelId { get; set; }
    /// <summary>
    /// 地区, 所在区域
    /// </summary>
    [DisplayName("所在区域"), MaxLength(36), Excel]
    public TKey? RegionId { get; set; }
}