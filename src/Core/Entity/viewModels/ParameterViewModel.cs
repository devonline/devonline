﻿namespace Devonline.Entity;

/// <summary>
/// 基础数据, 字符串类型的默认实现
/// </summary>
[DisplayName("基础数据")]
public class ParameterViewModel : ParameterViewModel<string>, IViewModel, IEntitySet, IKeyValuePair, IParent
{
    /// <summary>
    /// 父项编号
    /// </summary>
    [DisplayName("父项"), Excel]
    public virtual string? ParentText => Parent?.Text;

    /// <summary>
    /// 父级基础数据项
    /// </summary>
    public virtual ParameterViewModel? Parent { get; set; }
}

/// <summary>
/// 通用身份抽象基类模型
/// </summary>
[DisplayName("基础数据")]
public abstract class ParameterViewModel<TKey> : EntitySetWithParentViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IKeyValuePair, IParent<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 序号
    /// </summary>
    [DisplayName("序号"), Unique, Excel]
    public virtual int Index { get; set; }
    /// <summary>
    /// 键
    /// </summary>
    [DisplayName("键"), Required, Unique, MaxLength(128), Excel]
    public virtual string Key { get; set; } = null!;
    /// <summary>
    /// 值
    /// </summary>
    [DisplayName("值"), Required, MaxLength(255), Excel]
    public virtual string Value { get; set; } = null!;
    /// <summary>
    /// 基础数据
    /// </summary>
    [DisplayName("文本"), Required, MaxLength(128), Excel]
    public virtual string Text { get; set; } = null!;
}