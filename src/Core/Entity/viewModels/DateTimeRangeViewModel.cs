﻿namespace Devonline.Entity;

/// <summary>
/// 具有日期和时间范围的视图数据对象模型基类
/// 字符串主键的默认实现
/// </summary>
public abstract class DateTimeRangeViewModel : DateTimeRangeViewModel<string>, IViewModel, IEntitySet, IDateTimeRange { }

/// <summary>
/// 具有日期和时间范围的视图数据对象模型基类
/// </summary>
public abstract class DateTimeRangeViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IDateTimeRange where TKey : IConvertible
{
    /// <summary>
    /// 起始时间
    /// </summary>
    [DisplayName("起始时间"), Excel]
    public virtual DateTime? Start { get; set; }
    /// <summary>
    /// 结束时间
    /// </summary>
    [DisplayName("结束时间"), Excel]
    public virtual DateTime? End { get; set; }
}

/// <summary>
/// 具有日期范围的视图数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public abstract class DateRangeViewModel : DateRangeViewModel<string>, IViewModel, IEntitySet, IDateRange { }

/// <summary>
/// 具有日期范围的视图数据对象模型接口
/// </summary>
public abstract class DateRangeViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IDateRange where TKey : IConvertible
{
    /// <summary>
    /// 起始日期
    /// </summary>
    [DisplayName("起始日期"), Excel]
    public virtual DateOnly? Start { get; set; }
    /// <summary>
    /// 结束日期
    /// </summary>
    [DisplayName("结束日期"), Excel]
    public virtual DateOnly? End { get; set; }
}

/// <summary>
/// 具有时间范围的视图数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public abstract class TimeRangeViewModel : TimeRangeViewModel<string>, IViewModel, IEntitySet, ITimeRange { }

/// <summary>
/// 具有时间范围的视图数据对象模型接口
/// </summary>
public abstract class TimeRangeViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, ITimeRange where TKey : IConvertible
{
    /// <summary>
    /// 起始时间
    /// </summary>
    [DisplayName("起始时间"), Excel]
    public virtual TimeOnly? Start { get; set; }
    /// <summary>
    /// 结束时间
    /// </summary>
    [DisplayName("结束时间"), Excel]
    public virtual TimeOnly? End { get; set; }
}