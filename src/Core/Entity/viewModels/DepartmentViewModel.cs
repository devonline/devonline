﻿namespace Devonline.Entity;

/// <summary>
/// 部门
/// </summary>
[DisplayName("部门")]
public class DepartmentViewModel : DepartmentViewModel<string>, IIdentity, IViewModel, IEntitySet
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 部门
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("部门")]
public class DepartmentViewModel<TKey> : GroupViewModel<TKey>, IIdentity<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible;