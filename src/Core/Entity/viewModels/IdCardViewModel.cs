﻿namespace Devonline.Entity;

/// <summary>
/// 身份证
/// </summary>
[DisplayName("身份证")]
public class IdCardViewModel : IdCardViewModel<string>, IViewModel, IEntitySet, IDateTimeRange
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public virtual ICollection<Attachment>? Attachments { get; set; }

    /// <summary>
    /// 定义与身份证信息 IdCard 的隐式类型转换
    /// </summary>
    /// <param name="model">身份证视图对象模型</param>
    public static implicit operator IdCard(IdCardViewModel model) => new IdCard
    {
        Id = model.Id,
        State = model.State,
        Address = model.Address,
        BackImage = model.BackImage,
        Birthday = model.Birthday.HasValue ? DateOnly.FromDateTime(model.Birthday.Value) : null,
        Start = model.Start.HasValue ? DateOnly.FromDateTime(model.Start.Value) : null,
        End = model.End.HasValue ? DateOnly.FromDateTime(model.End.Value) : null,
        FrontImage = model.FrontImage,
        Gender = model.Gender,
        HeadImage = model.HeadImage,
        IdCode = model.IdCode,
        IssuedBy = model.IssuedBy,
        Name = model.Name,
        Nation = model.Nation,

        Attachments = model.Attachments
    };
    /// <summary>
    /// 定义与身份证视图对象模型 IdCardViewModel 的隐式类型转换
    /// </summary>
    /// <param name="idCard">身份证信息</param>
    public static implicit operator IdCardViewModel(IdCard idCard) => new IdCardViewModel
    {
        Id = idCard.Id,
        State = idCard.State,
        Address = idCard.Address,
        BackImage = idCard.BackImage,
        Birthday = idCard.Birthday.HasValue ? idCard.Birthday.Value.ToDateTime() : null,
        Start = idCard.Start.HasValue ? idCard.Start.Value.ToDateTime() : null,
        End = idCard.End.HasValue ? idCard.End.Value.ToDateTime() : null,
        FrontImage = idCard.FrontImage,
        Gender = idCard.Gender,
        HeadImage = idCard.HeadImage,
        IdCode = idCard.IdCode,
        IssuedBy = idCard.IssuedBy,
        Name = idCard.Name,
        Nation = idCard.Nation,

        Attachments = idCard.Attachments
    };
}

/// <summary>
/// 身份证
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("身份证")]
public abstract class IdCardViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IDateTimeRange where TKey : IConvertible
{
    /// <summary>
    /// 姓名
    /// </summary>
    [DisplayName("姓名"), Required, MaxLength(255), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 性别 Gender 枚举值
    /// </summary>
    [DisplayName("性别"), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Gender Gender { get; set; } = Gender.Male;
    /// <summary>
    /// 名族
    /// </summary>
    [DisplayName("民族"), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Nation Nation { get; set; } = Nation.Han;
    /// <summary>
    /// 出身日期
    /// </summary>
    [DisplayName("出生"), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateTime? Birthday { get; set; }
    /// <summary>
    /// 住址
    /// </summary>
    [DisplayName("住址"), MaxLength(255), Excel]
    public virtual string? Address { get; set; }
    /// <summary>
    /// 身份证号码
    /// </summary>
    [DisplayName("公民身份号码"), MaxLength(255), Unique, Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string IdCode { get; set; } = null!;
    /// <summary>
    /// 签发机关
    /// </summary>
    [DisplayName("签发机关"), MaxLength(36), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? IssuedBy { get; set; }
    /// <summary>
    /// 有效期始
    /// </summary>
    [DisplayName("有效期始"), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateTime? Start { get; set; }
    /// <summary>
    /// 有效期至
    /// </summary>
    [DisplayName("有效期至"), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateTime? End { get; set; }
    /// <summary>
    /// 身份证头像照片
    /// </summary>
    [DisplayName("头像照片"), MaxLength(128), BusinessType(IsAttachment = true, Name = "IdCard.HeadImage"), Excel]
    public virtual string? HeadImage { get; set; }
    /// <summary>
    /// 身份证正面照片
    /// </summary>
    [DisplayName("身份证正面照片"), MaxLength(128), BusinessType(IsAttachment = true, Name = "IdCard.FrontImage"), Excel]
    public virtual string? FrontImage { get; set; }
    /// <summary>
    /// 身份证反面照片
    /// </summary>
    [DisplayName("身份证反面照片"), MaxLength(128), BusinessType(IsAttachment = true, Name = "IdCard.BackImage"), Excel]
    public virtual string? BackImage { get; set; }
}