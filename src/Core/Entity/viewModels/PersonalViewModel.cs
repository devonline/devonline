﻿namespace Devonline.Entity;

/// <summary>
/// 个人信息模型
/// </summary>
[DisplayName("个人信息")]
public class PersonalViewModel : PersonalViewModel<string>, IViewModel, IEntitySet, IIdentity
{
    /// <summary>
    /// 所在区域
    /// </summary>
    [DisplayName("所在区域"), MaxLength(36), Excel]
    public virtual string? RegionName => Region?.Name;
    [DisplayName("身份证号"), MaxLength(36), Excel]
    public virtual string? IdCode => IdCard?.IdCode;

    /// <summary>
    /// 所在区域
    /// </summary>
    public virtual RegionViewModel? Region { get; set; }
    /// <summary>
    /// 身份证
    /// </summary>
    public virtual IdCardViewModel? IdCard { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 个人信息模型
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("个人信息")]
public abstract class PersonalViewModel<TKey> : IdentityViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IIdentity<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 性别
    /// </summary>
    [DisplayName("性别"), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Gender Gender { get; set; } = Gender.Male;
    /// <summary>
    /// 年龄
    /// </summary>
    [DisplayName("年龄"), Excel]
    public virtual int? Age { get; set; }
    /// <summary>
    /// 电话
    /// </summary>
    [DisplayName("电话"), MaxLength(255), Unique, Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? PhoneNumber { get; set; }
    /// <summary>
    /// 邮箱地址
    /// </summary>
    [DisplayName("邮箱"), MaxLength(255), Excel(Size = AppSettings.UNIT_TWO * AppSettings.UNIT_TEN)]
    public virtual string? Email { get; set; }
    /// <summary>
    /// 地址
    /// </summary>
    [DisplayName("地址"), MaxLength(255), Excel]
    public virtual string? Address { get; set; }
    /// <summary>
    /// 地区, 所在区域
    /// </summary>
    [DisplayName("所在区域编号"), MaxLength(36), Excel]
    public virtual TKey? RegionId { get; set; }
    /// <summary>
    /// 身份证编号
    /// </summary>
    [DisplayName("身份证编号"), MaxLength(36), Excel]
    public virtual TKey? IdCardId { get; set; }
}