﻿namespace Devonline.Entity;

/// <summary>
/// 附加信息试图模型, 字符串作为主键的基础类型
/// </summary>
public class AdditionalViewModel : AdditionalViewModel<string>, IViewModel, IKeyValuePair { }

/// <summary>
/// 附加信息试图模型
/// </summary>
/// <typeparam name="TKey">业务数据主键类型</typeparam>
public class AdditionalViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IKeyValuePair where TKey : IConvertible
{
    /// <summary>
    /// 标题
    /// </summary>
    [DisplayName("标题"), Required, MaxLength(128), Excel]
    public virtual string? Title { get; set; }
    /// <summary>
    /// 附加信息业务类型
    /// </summary>
    [DisplayName("附加信息业务类型"), Required, MaxLength(36), Excel]
    public virtual string? AdditionalType { get; set; }
    /// <summary>
    /// 序号
    /// </summary>
    [DisplayName("序号"), Unique, Excel]
    public virtual int Index { get; set; }
    /// <summary>
    /// 键
    /// </summary>
    [DisplayName("键"), Required, Unique, MaxLength(128), Excel]
    public virtual string Key { get; set; } = null!;
    /// <summary>
    /// 值
    /// </summary>
    [DisplayName("值"), Required, MaxLength(255), Excel]
    public virtual string Value { get; set; } = null!;
}