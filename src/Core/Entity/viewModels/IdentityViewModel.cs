﻿namespace Devonline.Entity;

/// <summary>
/// 通用身份抽象基类模型
/// </summary>
public class IdentityViewModel : IdentityViewModel<string>, IViewModel, IEntitySet, IIdentity
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 通用身份抽象基类模型
/// </summary>
public abstract class IdentityViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IIdentity<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称"), MaxLength(255), Required, Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual string? Name { get; set; }
    /// <summary>
    /// 昵称
    /// </summary>
    [DisplayName("昵称"), MaxLength(255), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 头像
    /// </summary>
    [DisplayName("头像"), MaxLength(255), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 身份授权类型
    /// </summary>
    [DisplayName("授权类型"), Excel(Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual AuthorizeType Type { get; set; } = AuthorizeType.Internal;
    /// <summary>
    /// 备注说明
    /// </summary>
    [DisplayName("备注说明"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }
}