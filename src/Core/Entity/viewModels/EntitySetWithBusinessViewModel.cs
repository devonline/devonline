﻿namespace Devonline.Entity;

/// <summary>
/// 字符串类型的默认基类
/// </summary>
public abstract class EntitySetWithBusinessViewModel : EntitySetWithBusinessViewModel<string>, IViewModel, IEntitySet { }

/// <summary>
/// 业务数据对象模型的抽象基类
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySetWithBusinessViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 业务类型
    /// </summary> 
    [DisplayName("业务类型"), MaxLength(128), Excel]
    public virtual string BusinessType { get; set; } = null!;
    /// <summary>
    /// 业务主键
    /// </summary>
    [DisplayName("业务主键"), MaxLength(36), Excel]
    public virtual TKey BusinessKey { get; set; } = default!;
}