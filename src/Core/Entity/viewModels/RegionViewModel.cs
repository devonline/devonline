﻿namespace Devonline.Entity;

/// <summary>
/// 行政区域
/// </summary>
[DisplayName("行政区域")]
public class RegionViewModel : RegionViewModel<string>, IViewModel, IEntitySet, IParent
{
    /// <summary>
    /// 上级行政区域名称
    /// </summary>
    [DisplayName("上级行政区域名称"), Excel]
    public virtual string? ParentName => Parent?.Name;

    /// <summary>
    /// 父项区域
    /// </summary>
    public virtual RegionViewModel? Parent { get; set; }
}

/// <summary>
/// 行政区域
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("行政区域")]
public abstract class RegionViewModel<TKey> : EntitySetWithParentViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IParent<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 序号
    /// </summary>
    [DisplayName("序号"), Excel]
    public virtual int Index { get; set; }
    /// <summary>
    /// 区域名称
    /// </summary>
    [DisplayName("区域名称"), Required, MaxLength(64), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 区域编码, 作为行政区域编码(区号), 在省级单位并没有, 因此不是必填
    /// </summary>
    [DisplayName("区域编码"), MaxLength(16), Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 上级行政区域编号
    /// </summary>
    [DisplayName("上级行政区域编号"), MaxLength(36), Excel]
    public override TKey? ParentId { get; set; }
}