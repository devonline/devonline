﻿namespace Devonline.Entity;

/// <summary>
/// 树形视图模型, 键和值都为字符串类型的基础抽象节点描述
/// </summary>
public class TreeViewModel : TreeViewModel<string>
{
    /// <summary>
    /// 父节点
    /// </summary>
    public virtual TreeViewModel? Parent { get; set; }
    /// <summary>
    /// 子节点集合
    /// </summary>
    public virtual List<TreeViewModel>? Children { set; get; }
}

/// <summary>
/// 树形视图模型, 值都为泛型的基础抽象节点描述
/// </summary>
/// <typeparam name="TValue"></typeparam>
public abstract class TreeViewModel<TValue> : TreeViewModel<string, TValue> where TValue : IConvertible, IComparable<TValue> { }

/// <summary>
/// 树形视图模型, 键和值都为泛型的基础抽象节点描述
/// </summary>
public abstract class TreeViewModel<TKey, TValue> : EntitySetWithParentViewModel<TKey> where TKey : IConvertible where TValue : IConvertible, IComparable<TValue>
{
    /// <summary>
    /// 节点名称
    /// </summary>
    public virtual string Name { set; get; } = null!;
    /// <summary>
    /// 节点值
    /// </summary>
    public virtual TValue Value { set; get; } = default!;
    /// <summary>
    /// 节点图标
    /// </summary>
    public virtual string? Icon { set; get; }
}