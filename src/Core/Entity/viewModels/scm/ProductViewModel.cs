﻿namespace Devonline.Entity;

/// <summary>
/// 货品, 字符串表达式的默认实现
/// </summary>
[DisplayName("货品")]
public class ProductViewModel : ProductViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 货品大类
    /// </summary>
    [DisplayName("货品大类"), Excel]
    public virtual string? MainCategory => Category?.Parent?.Name;
    /// <summary>
    /// 货品品类
    /// </summary>
    [DisplayName("货品品类"), Excel]
    public virtual string? CategoryName => Category?.Name;

    /// <summary>
    /// 货品品类
    /// </summary>
    public virtual Category? Category { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 货品
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("货品")]
public class ProductViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 品类编号
    /// </summary>
    [MaxLength(36), DisplayName("品类编号"), Excel]
    public virtual TKey? CategoryId { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    [Required, MaxLength(128), DisplayName("名称"), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 英文名
    /// </summary>
    [MaxLength(128), DisplayName("英文名"), Excel]
    public virtual string? EnglishName { get; set; }
    /// <summary>
    /// 昵称, 别名
    /// </summary>
    [MaxLength(255), DisplayName("别名"), Excel]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 封面图
    /// </summary>
    [MaxLength(128), DisplayName("封面图"), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 编码
    /// </summary>
    [MaxLength(128), DisplayName("货品编码"), Required, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 货品类型
    /// </summary>
    [MaxLength(128), DisplayName("货品类型"), Excel]
    public virtual string? Type { get; set; }
    /// <summary>
    /// 货品品质
    /// </summary>
    [MaxLength(128), DisplayName("货品品质"), Excel]
    public virtual string? Quality { get; set; }
    /// <summary>
    /// 包装单位, 包装单位
    /// </summary>
    [MaxLength(128), DisplayName("包装单位"), Excel]
    public virtual string? Unit { get; set; }
    /// <summary>
    /// 外观尺寸, 装箱规格, 装箱尺寸, 长宽高[+公差]+单位
    /// 表述形式: 长*宽*高[±公差]单位
    /// 如: 25mm*35mm*10mm±1mm
    /// 或: 25*35*10±1 mm
    /// </summary>
    [MaxLength(128), DisplayName("尺寸"), Excel]
    public virtual string? Size { get; set; }
    /// <summary>
    /// 重量
    /// </summary>
    [DisplayName("重量"), Excel]
    public virtual decimal? Weight { get; set; }

    /// <summary>
    /// 附加信息
    /// </summary>
    public virtual ICollection<Additional>? Additionals { get; set; }
}
