﻿namespace Devonline.Entity;

/// <summary>
/// 订单
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("订单")]
public abstract class OrderViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 订单号
    /// </summary>
    [DisplayName("订单号"), Required, MaxLength(36)]
    public virtual string OrderNumber { get; set; } = null!;
    /// <summary>
    /// 下单时间
    /// </summary>
    [DisplayName("下单时间"), Excel]
    public virtual DateTime OrderTime { get; set; }
    /// <summary>
    /// 订单金额
    /// </summary>
    [DisplayName("订单金额"), Excel]
    public virtual decimal Amount { get; set; }
    /// <summary>
    /// 订单汇率, 下单时的当时汇率, 是指结算货币相对于人民币的汇率
    /// 如果订单结算时为人民币支付, 则汇率默认为: 1
    /// </summary>
    [DisplayName("汇率"), Excel]
    public virtual decimal ExchangeRate { get; set; } = 1;
    /// <summary>
    /// 结算货币
    /// </summary>
    [DisplayName("结算货币"), DefaultValue(Currency.CNY), Excel]
    public virtual Currency Currency { get; set; }
    /// <summary>
    /// 总重量
    /// </summary>
    [DisplayName("总重量"), Excel]
    public virtual decimal? Weight { get; set; }
    /// <summary>
    /// 总体积
    /// </summary>
    [DisplayName("总体积"), Excel]
    public virtual decimal? Volume { get; set; }
    /// <summary>
    /// 运费
    /// </summary>
    [DisplayName("运费"), Excel]
    public virtual decimal? Freight { get; set; }
    /// <summary>
    /// 运单号
    /// </summary>
    [DisplayName("运单号"), MaxLength(36)]
    public virtual string? TrackingNumber { get; set; }
    /// <summary>
    /// 发货时间
    /// </summary>
    [DisplayName("发货时间"), Excel]
    public virtual DateTime? ShippingTime { get; set; }
    /// <summary>
    /// 到货时间
    /// </summary>
    [DisplayName("到货时间"), Excel]
    public virtual DateTime? DeliverTime { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    [DisplayName("备注说明"), MaxLength(1024)]
    public virtual string? Description { get; set; }
}