﻿namespace Devonline.Entity;

/// <summary>
/// 货品类别
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("采购订单")]
public class PurchaseOrderViewModel : PurchaseOrderViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 供应商
    /// </summary>
    public virtual SupplierViewModel? Supplier { get; set; }
    /// <summary>
    /// 采购订单明细
    /// </summary>
    public virtual ICollection<PurchaseOrderDetailViewModel>? Details { get; set; }
}

/// <summary>
/// 货品类别
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("采购订单")]
public abstract class PurchaseOrderViewModel<TKey> : OrderViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 供应商
    /// </summary>
    [MaxLength(36), DisplayName("供应商"), Required, Excel]
    public virtual string SupplierId { get; set; } = null!;
}

/// <summary>
/// 采购订单明细
/// </summary>
[DisplayName("采购订单明细")]
public class PurchaseOrderDetailViewModel : OrderDetailViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 采购订单
    /// </summary>
    public virtual PurchaseOrderViewModel? Order { get; set; }
    /// <summary>
    /// 货品
    /// </summary>
    public virtual ProductViewModel? Product { get; set; }
}