﻿namespace Devonline.Entity;

/// <summary>
/// 供应商
/// </summary>
[DisplayName("供应商")]
public class SupplierViewModel : SupplierViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 供应商区域
    /// </summary>
    [NotMapped, DisplayName("供应商区域"), Excel]
    public virtual string? RegionName => Region?.Name;
    /// <summary>
    /// 供应商所在区域
    /// </summary>
    public virtual RegionViewModel? Region { get; set; }
    /// <summary>
    /// 供应商联系人
    /// </summary>
    public virtual ICollection<SupplierContact>? Contacts { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 供应商
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("供应商")]
public class SupplierViewModel<TKey> : CustomerViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 供应商编码
    /// </summary>
    [MaxLength(16), DisplayName("供应商编码"), Excel]
    public override string Code { get; set; } = null!;
    /// <summary>
    /// 供应商类型
    /// </summary>
    [MaxLength(128), DisplayName("供应商类型"), Excel]
    public override string? Type { get; set; }
    /// <summary>
    /// 供应商品质
    /// </summary>
    [MaxLength(128), DisplayName("供应商品质"), Excel]
    public override string? Quality { get; set; }
}