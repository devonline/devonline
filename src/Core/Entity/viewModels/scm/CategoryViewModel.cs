﻿namespace Devonline.Entity;

/// <summary>
/// 货品类别, 字符串表达式的默认实现
/// </summary>
[DisplayName("货品类别")]
public class CategoryViewModel : CategoryViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 上一级类别
    /// </summary>
    [DisplayName("上一级类别"), Excel]
    public virtual string? ParentName => Parent?.Name;

    /// <summary>
    /// 父项品类
    /// </summary>
    public virtual Category? Parent { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 货品类别
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("货品类别")]
public class CategoryViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 名称
    /// </summary>
    [Required, MaxLength(128), DisplayName("名称"), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 昵称, 别名
    /// </summary>
    [MaxLength(255), DisplayName("别名"), Excel]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 类别编码
    /// </summary>
    [MaxLength(32), DisplayName("类别编码"), Required, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 封面图
    /// </summary>
    [MaxLength(128), DisplayName("封面图"), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 上一级类别
    /// </summary>
    [DisplayName("上一级类别编号"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
}
