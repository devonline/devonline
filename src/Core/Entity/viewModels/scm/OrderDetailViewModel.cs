﻿namespace Devonline.Entity;

/// <summary>
/// 订单明细
/// </summary>
[DisplayName("订单明细")]
public abstract class OrderDetailViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 订单编号
    /// </summary>
    [DisplayName("订单编号"), Required, MaxLength(36)]
    public virtual TKey OrderId { get; set; } = default!;
    /// <summary>
    /// 货品编号
    /// </summary>
    [DisplayName("货品编号"), Required, MaxLength(36), Excel]
    public virtual TKey ProductId { get; set; } = default!;
    /// <summary>
    /// 单价
    /// </summary>
    [DisplayName("单价"), Excel]
    public virtual decimal Price { get; set; }
    /// <summary>
    /// 数量
    /// </summary>
    [DisplayName("数量"), Excel]
    public virtual decimal Count { get; set; }
}