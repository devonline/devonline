﻿namespace Devonline.Entity;

/// <summary>
/// 客户
/// </summary>
[DisplayName("客户")]
public class CustomerViewModel : CustomerViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 客户区域
    /// </summary>
    [NotMapped, DisplayName("客户区域"), Excel]
    public virtual string? RegionName => Region?.Name;
    /// <summary>
    /// 客户所在区域
    /// </summary>
    public virtual RegionViewModel? Region { get; set; }
    /// <summary>
    /// 客户账户
    /// </summary>
    public virtual Account? Account { get; set; }
    /// <summary>
    /// 客户联系人
    /// </summary>
    public virtual ICollection<CustomerContact>? Contacts { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 客户
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("客户")]
public class CustomerViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 区域编号
    /// </summary>
    [DisplayName("区域编号"), Required, MaxLength(36), Excel]
    public virtual TKey RegionId { get; set; } = default!;
    /// <summary>
    /// 客户账户编号
    /// </summary>
    [DisplayName("客户账户编号"), Required, MaxLength(36), Excel]
    public virtual TKey AccountId { get; set; } = default!;
    /// <summary>
    /// 客户名称
    /// </summary>
    [Required, MaxLength(128), DisplayName("客户名称"), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 昵称, 别名
    /// </summary>
    [MaxLength(255), DisplayName("别名"), Excel]
    public virtual string? Alias { get; set; }
    /// <summary>
    /// 客户照片
    /// </summary>
    [MaxLength(128), DisplayName("客户照片"), BusinessType(IsAttachment = true), Excel]
    public virtual string? Image { get; set; }
    /// <summary>
    /// 客户编码
    /// </summary>
    [MaxLength(16), DisplayName("客户编码"), Required, Excel]
    public virtual string Code { get; set; } = null!;
    /// <summary>
    /// 客户类型
    /// </summary>
    [MaxLength(128), DisplayName("客户类型"), Excel]
    public virtual string? Type { get; set; }
    /// <summary>
    /// 客户品质
    /// </summary>
    [MaxLength(128), DisplayName("客户品质"), Excel]
    public virtual string? Quality { get; set; }
}
