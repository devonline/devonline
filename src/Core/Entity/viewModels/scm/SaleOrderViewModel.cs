﻿namespace Devonline.Entity;

/// <summary>
/// 销售订单
/// </summary>
[DisplayName("销售订单")]
public class SaleOrderViewModel : SaleOrderViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 下单客户
    /// </summary>
    public virtual CustomerViewModel? Customer { get; set; }
    /// <summary>
    /// 销售订单明细
    /// </summary>
    public virtual ICollection<SaleOrderDetailViewModel>? Details { get; set; }
}

/// <summary>
/// 销售订单
/// </summary>
/// <typeparam name="TKey"></typeparam>
[DisplayName("销售订单")]
public abstract class SaleOrderViewModel<TKey> : OrderViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 客户编号
    /// </summary>
    [DisplayName("客户编号"), Required, MaxLength(36), Excel]
    public virtual TKey CustomerId { get; set; } = default!;
}

/// <summary>
/// 销售订单明细
/// </summary>
[DisplayName("销售订单明细")]
public class SaleOrderDetailViewModel : OrderDetailViewModel<string>, IViewModel, IEntitySet
{
    /// <summary>
    /// 订单
    /// </summary>
    public virtual SaleOrderViewModel? Order { get; set; }
    /// <summary>
    /// 货品
    /// </summary>
    public virtual ProductViewModel? Product { get; set; }
}