﻿namespace Devonline.Entity;

/// <summary>
/// 资源密钥容器, 字符串类型的默认实现
/// </summary>
[Table("resource_secret"), DisplayName("资源密钥")]
public class ResourceSecret : ResourceSecret<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 资源
    /// </summary>
    public virtual Resource? Resource { get; set; }
    /// <summary>
    /// 机密
    /// </summary>
    public virtual Secret? Secret { get; set; }
}

/// <summary>
/// 资源密钥
/// 加密过程: 
/// 1. 对当前资源产生随机对称密钥, 以下简称: 资源密钥
/// 2. 用资源密钥作为加密资源本身
/// 3. 使用资源所有者非对称公钥加密资源密钥, 并保存到 Ciphertext 字段
/// 4. 用户非对称私钥由用户密钥加密保存
/// 解密过程:
/// 1. 取得用户授权, 使用用户密码解密用户私钥
/// 2. 使用用户私钥解密资源密钥
/// 3. 使用资源密钥解密资源本身
/// 特权解密不同之处: 使用特权用户密码解密用户密钥
/// </summary>
/// <typeparam name="TKey"></typeparam>
[Table("resource_secret"), DisplayName("资源密钥")]
public class ResourceSecret<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 资源编号
    /// </summary>
    [Column("resource_id"), DisplayName("资源编号"), Required, MaxLength(36)]
    public virtual TKey ResourceId { get; set; } = default!;
    /// <summary>
    /// 密钥容器编号
    /// </summary>
    [Column("secret_id"), DisplayName("密钥容器编号"), Required, MaxLength(36)]
    public virtual TKey SecretId { get; set; } = default!;
}