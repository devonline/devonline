﻿namespace Devonline.Entity;

/// <summary>
/// 数据密钥
/// 字符串类型的默认实现
/// </summary>
[Table("data_secret"), DisplayName("数据密钥")]
public class DataSecret : DataSecret<string>, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 机密
    /// </summary>
    public virtual Secret? Secret { get; set; }
}

/// <summary>
/// 数据密钥
/// BusinessKey 不填, 则同一种类型的数据使用同一个密钥加解密
/// 
/// 加密过程: 
/// 1. 对当前数据产生随机对称密钥, 以下简称: 数据密钥
/// 2. 用数据密钥作为加密数据本身
/// 3. 使用数据所有者非对称公钥加密数据密钥, 并保存到 Ciphertext 字段
/// 4. 用户非对称私钥由用户密钥加密保存
/// 解密过程:
/// 1. 取得用户授权, 使用用户密码解密用户私钥
/// 2. 使用用户私钥解密数据密钥
/// 3. 使用数据密钥解密数据本身
/// 特权解密不同之处: 使用特权用户密码解密用户密钥
/// </summary>
/// <typeparam name="TKey"></typeparam>
[Table("data_secret"), DisplayName("数据密钥")]
public class DataSecret<TKey> : EntitySetWithBusiness<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 密钥容器编号
    /// </summary>
    [Column("secret_id"), DisplayName("密钥容器编号"), Required, MaxLength(36)]
    public virtual TKey SecretId { get; set; } = default!;
    /// <summary>
    /// 数据定义的可访问级别 AccessLevel 枚举类型的值
    /// </summary>
    [Column("access_level", TypeName = "varchar(16)"), DisplayName("访问级别"), Excel]
    public virtual AccessLevel AccessLevel { get; set; }
}