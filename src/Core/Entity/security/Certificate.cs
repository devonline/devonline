﻿namespace Devonline.Entity;

/// <summary>
/// 证书, 字符串表达式的默认实现
/// 服务器颁发给客户端的证书, 用于校验客户端身份
/// 也可以只保存服务器公钥
/// </summary>
[Table("certificate"), DisplayName("证书")]
public class Certificate : Certificate<string>, IDateTimeRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Certificate() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 证书
/// 服务器颁发给客户端的证书, 用于校验客户端身份
/// 也可以只保存服务器公钥
/// </summary>
[Table("certificate"), DisplayName("证书")]
public abstract class Certificate<TKey> : EntitySetWithCreate<TKey>, IDateTimeRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 证书所有者编号
    /// </summary>
    [Column("owner_id"), DisplayName("证书所有者编号"), Required, MaxLength(36)]
    public virtual TKey OwnerId { get; set; } = default!;
    /// <summary>
    /// 证书所有者身份类型 IdentityType 枚举值
    /// </summary>
    [Column("identity_type", TypeName = "varchar(16)"), DisplayName("证书所有者身份类型"), DefaultValue(IdentityType.User)]
    public virtual IdentityType IdentityType { get; set; }
    /// <summary>
    /// 有效期始
    /// </summary>
    [Column("start"), DisplayName("有效期始"), Field(Index = 8, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateTime? Start { get; set; }
    /// <summary>
    /// 有效期至
    /// </summary>
    [Column("end"), DisplayName("有效期至"), Field(Index = 9, Size = 16, Format = AppSettings.DEFAULT_FILE_DATE_FORMAT), Excel(Format = AppSettings.DEFAULT_DATE_FORMAT, Size = AppSettings.DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual DateTime? End { get; set; }
    /// <summary>
    /// 散列算法(证书散列算法名称)
    /// </summary>
    [Column("hash_algorithm", TypeName = "varchar(16)"), DisplayName("散列算法"), Required, DefaultValue(HashAlgorithm.SHA256), Excel]
    public virtual HashAlgorithm HashAlgorithm { get; set; } = HashAlgorithm.SHA256;
    /// <summary>
    /// 对称加密算法
    /// </summary>
    [Column("encryption_algorithm", TypeName = "varchar(16)"), DisplayName("对称加密算法"), DefaultValue(SymmetricAlgorithm.AES_256_CBC)]
    public virtual SymmetricAlgorithm EncryptionAlgorithm { get; set; }
    /// <summary>
    /// 签名算法(证书非对称算法名称)
    /// </summary>
    [Column("signature_algorithm", TypeName = "varchar(16)"), DisplayName("签名算法"), Required, DefaultValue(Devonline.Core.EncryptionAlgorithm.RSA), Excel]
    public virtual EncryptionAlgorithm SignatureAlgorithm { get; set; } = Devonline.Core.EncryptionAlgorithm.RSA;
    /// <summary>
    /// 非对称密钥长度, 默认 2048 位
    /// </summary>
    [Column("key_size"), DisplayName("非对称密钥长度"), DefaultValue(2048), Excel]
    public virtual int KeySize { get; set; } = 2048;
    /// <summary>
    /// 数据 Hash 值, 用于确认数据是否被篡改
    /// </summary>
    [Column("hash_code"), DisplayName("数据哈希值"), Required, MaxLength(128), Excel]
    public virtual string HashCode { get; set; } = null!;
    /// <summary>
    /// 证书公钥, 非对称算法公钥, 用于加密用户密钥, 保存为明文, 密钥长度 2048 位, 256 字节, BASE64 编码字符串, 长度 392 字符
    /// </summary>
    [Column("public_key"), DisplayName("证书公钥"), Required, MaxLength(512), Excel]
    public virtual string PublicKey { get; set; } = null!;
    /// <summary>
    /// 证书内容, 完整证书内容, 保存为明文, BASE64 编码字符串, 长度不确定, 但不超过 8K
    /// </summary>
    [Column("content"), DisplayName("证书内容"), MaxLength(8192), Excel]
    public virtual string? Content { get; set; }
    /// <summary>
    /// 证书密码
    /// </summary>
    [Column("password"), DisplayName("证书密码"), MaxLength(32)]
    public virtual string? Password { get; set; }
    /// <summary>
    /// 文件地址, 相对地址加上文件名, 证书若是已文件形式保存, 这是保存地址, 此处仅支持单文件证书
    /// </summary>
    [Column("path"), DisplayName("文件地址"), MaxLength(255)]
    public virtual string? Path { get; set; }
}