﻿namespace Devonline.Entity;

/// <summary>
/// 数据安全模型接口, 密文数据对象模型顶级接口
/// </summary>
public interface IDataSecurity
{
    /// <summary>
    /// 数据加密模式
    /// </summary>
    DataEncryptionMode EncryptionMode { get; set; }
    /// <summary>
    /// 数据版本号, 用于给数据增加一个变量, 在计算 Hash 值时增加不确定因素
    /// </summary>
    string Version { get; set; }
    /// <summary>
    /// 数据 Hash 值, 用于确认数据是否被篡改
    /// </summary>
    string HashCode { get; set; }
    /// <summary>
    /// 数据密文, 数据被用户数据密钥加密后的结果
    /// </summary>
    string Ciphertext { get; set; }

    /// <summary>
    /// 计算并设置数据的安全数据: 计算 Hash 值和数据密文, 并返回安全数据
    /// </summary>
    string Security();
}