﻿namespace Devonline.Entity;

/// <summary>
/// 个人业务, 字符串主键的默认实现
/// </summary>
[Table("personal_business"), DisplayName("个人业务")]
public class PersonalBusiness : PersonalBusiness<string>, IPersonalBusiness, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public PersonalBusiness() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 用户个人信息
    /// </summary>
    public virtual Personal? Personal { get; set; }
}

/// <summary>
/// 个人业务
/// </summary>
[Table("personal_business"), DisplayName("个人业务")]
public abstract class PersonalBusiness<TKey> : EntitySetWithBusiness<TKey>, IPersonalBusiness<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 用户个人信息
    /// </summary>
    [Column("personal_id"), DisplayName("个人信息编号"), Required, MaxLength(36), Excel]
    public virtual TKey PersonalId { get; set; } = default!;
    /// <summary>
    /// 业务内容
    /// </summary>
    [Column("content"), DisplayName("内容"), Required, MaxLength(1024), Excel]
    public virtual string Content { get; set; } = null!;
}