﻿namespace Devonline.Entity;

/// <summary>
/// 申请审核类业务数据对象模型抽象基类
/// 字符串主键的默认抽象基类
/// </summary>
public abstract class BusinessChange : BusinessChange<string>, IBusinessChange, IBusinessAuditApply, IBusinessAudit, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    protected BusinessChange() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 申请审核类业务数据对象模型抽象基类
/// </summary>
/// <typeparam name="TKey"></typeparam>
public abstract class BusinessChange<TKey> : BusinessAuditApply<TKey>, IBusinessChange<TKey>, IBusinessAuditApply<TKey>, IBusinessAudit<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 原业务数据编号, 始终对应修改前的数据
    /// </summary>
    [Column("original_id"), DisplayName("原业务数据编号"), Required, MaxLength(36), Excel]
    public virtual TKey OriginalId { get; set; } = default!;
    /// <summary>
    /// 当前业务数据编号, 始终对应修改后的数据
    /// </summary>
    [Column("current_id"), DisplayName("当前业务数据编号"), Required, MaxLength(36), Excel]
    public virtual TKey CurrentId { get; set; } = default!;
    /// <summary>
    /// 变更操作人
    /// </summary>
    [Column("operator"), DisplayName("变更操作人"), Required, MaxLength(36), Excel]
    public virtual string? Operator { get; set; }
}