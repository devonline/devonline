﻿namespace Devonline.Entity;

/// <summary>
/// 审核类业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public abstract class BusinessAudit : BusinessAudit<string>, IBusinessAudit, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public BusinessAudit() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 审核类业务数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public abstract class BusinessAudit<TKey> : EntitySetWithCreateAndUpdate<TKey>, IBusinessAudit<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey>, IEntitySetWithCreateAndUpdate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 当前业务处于的审核记录状态
    /// 这是当前业务数据处于的审核记录的当前状态
    /// </summary>
    [Column("audit_record_id"), DisplayName("审核记录编号"), MaxLength(36), Excel]
    public virtual TKey? AuditRecordId { get; set; }
}