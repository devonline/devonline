﻿namespace Devonline.Entity;

/// <summary>
/// 申请审核类业务数据对象模型抽象基类
/// 字符串主键的默认抽象基类
/// </summary>
public abstract class BusinessApply : BusinessApply<string>, IBusinessApply, IBusinessAudit
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public BusinessApply() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 申请审核类业务数据对象模型抽象基类
/// </summary>
/// <typeparam name="TKey"></typeparam>
public abstract class BusinessApply<TKey> : BusinessAudit<TKey>, IBusinessApply<TKey>, IBusinessAudit<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 当前业务的审核申请记录编号
    /// </summary>
    [Column("apply_record_id"), DisplayName("申请记录编号"), MaxLength(36), Excel]
    public virtual TKey? ApplyRecordId { get; set; }
}