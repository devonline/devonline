﻿namespace Devonline.Entity;

/// <summary>
/// 针对原始业务数据的变更申请过程记录接口
/// 字符串主键的默认接口
/// </summary>
public interface IBusinessChange : IBusinessChange<string>, IBusinessAuditApply, IBusinessAudit, IBusiness;

/// <summary>
/// 针对原始业务数据的变更申请过程记录接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IBusinessChange<TKey> : IBusinessAuditApply<TKey>, IBusinessAudit<TKey>, IBusiness<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 原业务数据编号, 始终对应修改前的数据
    /// </summary>
    TKey OriginalId { get; set; }
    /// <summary>
    /// 当前业务数据编号, 始终对应修改后的数据
    /// </summary>
    TKey CurrentId { get; set; }
    /// <summary>
    /// 操作人
    /// </summary>
    string? Operator { get; set; }
}