﻿namespace Devonline.Entity;

/// <summary>
/// 申请审核类业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IBusinessAuditApply : IBusinessAuditApply<string>, IBusinessAudit, IBusiness;

/// <summary>
/// 申请审核类业务数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IBusinessAuditApply<TKey> : IBusinessAudit<TKey>, IBusiness<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 申请时间
    /// </summary>
    DateTime ApplyDate { get; set; }
    /// <summary>
    /// 申请人
    /// </summary>
    TKey? Applicant { get; set; }
    /// <summary>
    /// 申请原因
    /// </summary>
    string? Reason { get; set; }
}