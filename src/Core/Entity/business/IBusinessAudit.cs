﻿namespace Devonline.Entity;

/// <summary>
/// 审核类业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IBusinessAudit : IBusinessAudit<string>;

/// <summary>
/// 审核类业务数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IBusinessAudit<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 当前业务处于的审核记录状态
    /// 这是当前业务数据处于的审核记录的当前状态
    /// </summary>
    TKey? AuditRecordId { get; set; }
}