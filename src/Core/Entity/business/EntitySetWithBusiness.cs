﻿namespace Devonline.Entity;

/// <summary>
/// 字符串类型的默认基类
/// </summary>
public abstract class EntitySetWithBusiness : EntitySetWithBusiness<string>, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithBusiness() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 业务数据对象模型的抽象基类
/// epplus 自动导出的列头仅支持 DisplayName 和 Description 特性
/// </summary>
public abstract class EntitySetWithBusiness<TKey> : EntitySetWithCreate<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 业务类型
    /// </summary> 
    [Column("business_type"), DisplayName("业务类型"), Required, MaxLength(128), Excel]
    public virtual string BusinessType { get; set; } = null!;
    /// <summary>
    /// 业务主键
    /// </summary>
    [Column("business_key"), DisplayName("业务主键"), MaxLength(36), Excel]
    public virtual TKey? BusinessKey { get; set; }
}