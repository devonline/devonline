﻿namespace Devonline.Entity;

/// <summary>
/// 业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IBusiness : IBusiness<string>;

/// <summary>
/// 业务数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IBusiness<TKey>
{
    /// <summary>
    /// 业务类型
    /// </summary>
    string BusinessType { get; set; }
    /// <summary>
    /// 业务主键
    /// </summary>
    TKey? BusinessKey { get; set; }
}