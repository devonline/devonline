﻿namespace Devonline.Entity;

/// <summary>
/// 申请审核类业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IBusinessApply : IBusinessApply<string>;

/// <summary>
/// 申请审核类业务数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IBusinessApply<TKey> : IBusinessAudit<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 当前业务的审核申请记录编号
    /// </summary>
    TKey? ApplyRecordId { get; set; }
}