﻿namespace Devonline.Entity;

/// <summary>
/// 审核类业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public abstract class BusinessAuditApply : BusinessAuditApply<string>, IBusinessAuditApply, IBusinessAudit, IBusiness, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public BusinessAuditApply() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 审核类业务申请数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public abstract class BusinessAuditApply<TKey> : EntitySetWithBusiness<TKey>, IBusinessAuditApply<TKey>, IBusinessAudit<TKey>, IBusiness<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 业务主键
    /// </summary>
    [Column("business_key"), DisplayName("业务主键"), Required, MaxLength(36), Excel]
    public override TKey? BusinessKey { get; set; }

    /// <summary>
    /// 当前业务处于的审核记录状态
    /// 这是当前业务数据处于的审核记录的当前状态
    /// </summary>
    [Column("audit_record_id"), DisplayName("审核记录编号"), MaxLength(36), Excel]
    public virtual TKey? AuditRecordId { get; set; }
    /// <summary>
    /// 申请时间
    /// </summary>
    [Column("apply_date"), DisplayName("申请时间"), Excel]
    public virtual DateTime ApplyDate { get; set; }
    /// <summary>
    /// 申请人
    /// </summary>
    [Column("applicant"), DisplayName("申请人"), Required, MaxLength(36), Excel]
    public virtual TKey? Applicant { get; set; }
    /// <summary>
    /// 申请原因
    /// </summary>
    [Column("reason"), DisplayName("申请原因"), MaxLength(255), Excel]
    public virtual string? Reason { get; set; }
}