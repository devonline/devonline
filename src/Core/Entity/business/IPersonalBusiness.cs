﻿namespace Devonline.Entity;

/// <summary>
/// 个人业务数据对象模型接口
/// 字符串主键的默认接口
/// </summary>
public interface IPersonalBusiness : IPersonalBusiness<string>;

/// <summary>
/// 个人业务数据对象模型接口
/// </summary>
/// <typeparam name="TKey"></typeparam>
public interface IPersonalBusiness<TKey>
{
    /// <summary>
    /// 用户个人信息
    /// </summary>
    TKey PersonalId { get; set; }
    /// <summary>
    /// 业务内容
    /// </summary>
    string Content { get; set; }
}