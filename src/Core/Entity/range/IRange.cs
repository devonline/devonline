﻿namespace Devonline.Entity;

/// <summary>
/// 具有范围描述的数据对象模型接口
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IRange<T> where T : struct, IEquatable<T>
{
    /// <summary>
    /// 开始
    /// </summary>
    T? Start { get; set; }
    /// <summary>
    /// 结束
    /// </summary>
    T? End { get; set; }
}