﻿namespace Devonline.Entity;

/// <summary>
/// 具有数字范围的数据对象模型泛型基类
/// 字符串主键的默认实现
/// </summary>
public abstract class EntitySetWithIntegerRange : EntitySetWithIntegerRange<string>, IIntegerRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithIntegerRange() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有数字范围的数据对象模型泛型基类
/// </summary>
public abstract class EntitySetWithIntegerRange<TKey> : EntitySetWithCreate<TKey>, IIntegerRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 起始值
    /// </summary>
    [Column("start"), DisplayName("起始值"), Excel]
    public virtual int? Start { get; set; }
    /// <summary>
    /// 结束值
    /// </summary>
    [Column("end"), DisplayName("结束值"), Excel]
    public virtual int? End { get; set; }
}

/// <summary>
/// 具有数字范围的数据对象模型泛型基类
/// 字符串主键的默认实现
/// </summary>
public abstract class EntitySetWithDecimalRange : EntitySetWithDecimalRange<string>, IDecimalRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithDecimalRange() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有数字范围的数据对象模型泛型基类
/// </summary>
public abstract class EntitySetWithDecimalRange<TKey> : EntitySetWithCreate<TKey>, IDecimalRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 起始值
    /// </summary>
    [Column("start"), DisplayName("起始值"), Excel]
    public virtual decimal? Start { get; set; }
    /// <summary>
    /// 结束值
    /// </summary>
    [Column("end"), DisplayName("结束值"), Excel]
    public virtual decimal? End { get; set; }
}