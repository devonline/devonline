﻿namespace Devonline.Entity;

/// <summary>
/// 具有日期和时间范围的数据对象模型接口
/// </summary>
public interface IDateTimeRange : IRange<DateTime>;

/// <summary>
/// 具有日期范围的数据对象模型接口
/// </summary>
public interface IDateRange : IRange<DateOnly>;

/// <summary>
/// 具有时间范围的数据对象模型接口
/// </summary>
public interface ITimeRange : IRange<TimeOnly>;