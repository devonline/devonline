﻿namespace Devonline.Entity;

/// <summary>
/// 具有日期和时间范围的数据对象模型基类
/// 字符串主键的默认实现
/// </summary>
public abstract class EntitySetWithDateTimeRange : EntitySetWithDateTimeRange<string>, IDateTimeRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithDateTimeRange() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有日期和时间范围的数据对象模型基类
/// </summary>
public abstract class EntitySetWithDateTimeRange<TKey> : EntitySetWithCreate<TKey>, IDateTimeRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    public EntitySetWithDateTimeRange()
    {
        Start = DateTime.Now;
    }

    /// <summary>
    /// 起始时间
    /// </summary>
    [Column("start"), DisplayName("起始时间"), Excel]
    public virtual DateTime? Start { get; set; }
    /// <summary>
    /// 结束时间
    /// </summary>
    [Column("end"), DisplayName("结束时间"), Excel]
    public virtual DateTime? End { get; set; }
}

/// <summary>
/// 具有日期范围的数据对象模型基类
/// 字符串主键的默认实现
/// </summary>
public abstract class EntitySetWithDateRange : EntitySetWithDateRange<string>, IDateRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithDateRange() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有日期范围的数据对象模型泛型基类
/// </summary>
public abstract class EntitySetWithDateRange<TKey> : EntitySetWithCreate<TKey>, IDateRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    public EntitySetWithDateRange()
    {
        Start = DateOnly.FromDateTime(DateTime.Now);
    }

    /// <summary>
    /// 起始日期
    /// </summary>
    [Column("start"), DisplayName("起始日期"), Excel]
    public virtual DateOnly? Start { get; set; }
    /// <summary>
    /// 结束日期
    /// </summary>
    [Column("end"), DisplayName("结束日期"), Excel]
    public virtual DateOnly? End { get; set; }
}

/// <summary>
/// 具有时间范围的数据对象模型基类
/// 字符串主键的默认实现
/// </summary>
public abstract class EntitySetWithTimeRange : EntitySetWithTimeRange<string>, ITimeRange, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public EntitySetWithTimeRange() => Id = KeyGenerator.GetStringKey();
}

/// <summary>
/// 具有时间范围的数据对象模型泛型基类
/// </summary>
public abstract class EntitySetWithTimeRange<TKey> : EntitySetWithCreate<TKey>, ITimeRange, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    public EntitySetWithTimeRange()
    {
        Start = TimeOnly.MinValue;
    }

    /// <summary>
    /// 起始时间
    /// </summary>
    [Column("start"), DisplayName("起始时间"), Excel]
    public virtual TimeOnly? Start { get; set; }
    /// <summary>
    /// 结束时间
    /// </summary>
    [Column("end"), DisplayName("结束时间"), Excel]
    public virtual TimeOnly? End { get; set; }
}