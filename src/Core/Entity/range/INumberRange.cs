﻿using System.Numerics;

namespace Devonline.Entity;

/// <summary>
/// 具有数字范围的数据对象模型接口
/// </summary>
public interface INumberRange<T> : IRange<T> where T : struct, IEquatable<T>, INumber<T>;

/// <summary>
/// 具有数字范围的数据对象模型接口
/// </summary>
public interface IIntegerRange : INumberRange<int>;

/// <summary>
/// 具有数字范围的数据对象模型接口
/// </summary>
public interface IDecimalRange : INumberRange<decimal>;