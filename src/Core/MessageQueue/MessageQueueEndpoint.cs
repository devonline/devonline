﻿using Devonline.Entity;

namespace Devonline.MessageQueue;

/// <summary>
/// 消息队列终结点
/// </summary>
public interface IMessageQueueEndpoint : IEndpoint;

/// <summary>
/// 消息队列终结点设置
/// </summary>
public class MessageQueueEndpoint : Endpoint, IMessageQueueEndpoint, IEndpoint;