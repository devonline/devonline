﻿namespace Devonline.MessageQueue;

/// <summary>
/// 消息队列服务
/// </summary>
public interface IMessageQueueService
{
    /// <summary>
    /// 启动消息队列客户端
    /// </summary>
    void Start();
    /// <summary>
    /// 发布消息到主题
    /// </summary>
    /// <typeparam name="TMessage">消息类型</typeparam>
    /// <param name="topic">发布主题</param>
    /// <param name="message">消息内容</param>
    /// <returns></returns>
    Task PublishAsync<TMessage>(string topic, TMessage message);
    /// <summary>
    /// 从主题订阅消息
    /// </summary>
    /// <typeparam name="TMessage">消息类型</typeparam>
    /// <param name="topic">订阅主题</param>
    /// <param name="subscriptionName">订阅名称</param>
    /// <param name="onSubscribe">接收订阅的方法委托</param>
    /// <param name="subscriptionType">订阅类型</param>
    /// <returns></returns>
    Task SubscribeAsync<TMessage>(string topic, string subscriptionName, Func<TMessage, Task> onSubscribe, string? subscriptionType = default);

    /// <summary>
    /// 停止所有生产者和消费者以及客户端对象
    /// </summary>
    /// <returns></returns>
    Task StopAsync();
    /// <summary>
    /// 停止一个 topic 对应的生产者或者消费者
    /// </summary>
    /// <param name="topic"></param>
    /// <returns></returns>
    Task StopAsync(string topic);
}