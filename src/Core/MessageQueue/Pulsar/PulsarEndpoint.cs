﻿using Devonline.Entity;

namespace Devonline.MessageQueue.Pulsar;

/// <summary>
/// Pulsar 消息队列终结点
/// </summary>
public interface IPulsarEndpoint : IMessageQueueEndpoint, IEndpoint;

/// <summary>
/// Pulsar 消息队列终结点设置
/// </summary>
public class PulsarEndpoint : MessageQueueEndpoint, IPulsarEndpoint, IMessageQueueEndpoint, IEndpoint;