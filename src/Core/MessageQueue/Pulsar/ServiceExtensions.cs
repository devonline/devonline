﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Devonline.MessageQueue.Pulsar;

public static class ServiceExtensions
{
    /// <summary>
    /// 注册消息队列服务
    /// </summary>
    /// <param name="services">依赖注入服务</param>
    /// <param name="endpoint">消息队列设置</param>
    /// <param name="serviceLifetime">MessageQueue 生命周期</param>
    public static IServiceCollection AddKeyedMessageQueue(this IServiceCollection services, IPulsarEndpoint endpoint, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
    {
        services.AddSingleton<IMessageQueueEndpoint>(endpoint);
        switch (serviceLifetime)
        {
            case ServiceLifetime.Transient:
                services.AddKeyedTransient<IMessageQueueService, PulsarService>(endpoint.Name, (serviceProvider, _) => new PulsarService(serviceProvider.GetRequiredService<ILogger<PulsarService>>(), endpoint));
                break;
            case ServiceLifetime.Singleton:
                services.AddKeyedSingleton<IMessageQueueService, PulsarService>(endpoint.Name, (serviceProvider, _) => new PulsarService(serviceProvider.GetRequiredService<ILogger<PulsarService>>(), endpoint));
                break;
            case ServiceLifetime.Scoped:
            default:
                services.AddKeyedScoped<IMessageQueueService, PulsarService>(endpoint.Name, (serviceProvider, _) => new PulsarService(serviceProvider.GetRequiredService<ILogger<PulsarService>>(), endpoint));
                break;
        }

        return services.AddKeyedPulsarService(endpoint);
    }
    /// <summary>
    /// 注册 Apache Pulsar 消息队列服务
    /// </summary>
    /// <param name="services">依赖注入服务</param>
    /// <param name="endpoint">消息队列设置</param>
    /// <param name="serviceLifetime">PulsarService 生命周期</param>
    /// <returns></returns>
    public static IServiceCollection AddKeyedPulsarService(this IServiceCollection services, IPulsarEndpoint endpoint, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
    {
        services.AddSingleton<IPulsarEndpoint>(endpoint);
        switch (serviceLifetime)
        {
            case ServiceLifetime.Transient:
                services.AddKeyedTransient<IPulsarService, PulsarService>(endpoint.Name, (serviceProvider, _) => new PulsarService(serviceProvider.GetRequiredService<ILogger<PulsarService>>(), endpoint));
                break;
            case ServiceLifetime.Singleton:
                services.AddKeyedSingleton<IPulsarService, PulsarService>(endpoint.Name, (serviceProvider, _) => new PulsarService(serviceProvider.GetRequiredService<ILogger<PulsarService>>(), endpoint));
                break;
            case ServiceLifetime.Scoped:
            default:
                services.AddKeyedScoped<IPulsarService, PulsarService>(endpoint.Name, (serviceProvider, _) => new PulsarService(serviceProvider.GetRequiredService<ILogger<PulsarService>>(), endpoint));
                break;
        }

        return services;
    }

    /// <summary>
    /// 注册消息队列服务
    /// </summary>
    /// <param name="services">依赖注入服务</param>
    /// <param name="endpoint">消息队列设置</param>
    /// <param name="serviceLifetime">MessageQueue 生命周期</param>
    public static IServiceCollection AddMessageQueue(this IServiceCollection services, IPulsarEndpoint endpoint, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
    {
        services.AddSingleton<IMessageQueueEndpoint>(endpoint);
        switch (serviceLifetime)
        {
            case ServiceLifetime.Transient:
                services.AddTransient<IMessageQueueService, PulsarService>();
                break;
            case ServiceLifetime.Singleton:
                services.AddSingleton<IMessageQueueService, PulsarService>();
                break;
            case ServiceLifetime.Scoped:
            default:
                services.AddScoped<IMessageQueueService, PulsarService>();
                break;
        }

        return services.AddPulsarService(endpoint, serviceLifetime);
    }
    /// <summary>
    /// 注册 Apache Pulsar 消息队列服务
    /// </summary>
    /// <param name="services">依赖注入服务</param>
    /// <param name="endpoint">消息队列设置</param>
    /// <param name="serviceLifetime">PulsarService 生命周期</param>
    /// <returns></returns>
    public static IServiceCollection AddPulsarService(this IServiceCollection services, IPulsarEndpoint endpoint, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
    {
        services.AddSingleton<IPulsarEndpoint>(endpoint);
        switch (serviceLifetime)
        {
            case ServiceLifetime.Transient:
                services.AddTransient<IPulsarService, PulsarService>();
                break;
            case ServiceLifetime.Singleton:
                services.AddSingleton<IPulsarService, PulsarService>();
                break;
            case ServiceLifetime.Scoped:
            default:
                services.AddScoped<IPulsarService, PulsarService>();
                break;
        }

        return services;
    }
}