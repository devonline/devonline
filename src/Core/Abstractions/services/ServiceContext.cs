﻿using Mapster;

namespace Devonline.Abstractions;

/// <summary>
/// 数据处理事务上下文
/// </summary>
public class ServiceContext
{
    /// <summary>
    /// 是否逻辑操作, 默认不是
    /// </summary>
    public virtual bool IsLogical { get; set; }
    /// <summary>
    /// 字段名和业务类型键值对
    /// </summary>
    public virtual Dictionary<string, string>? BusinessTypes { get; set; }
    /// <summary>
    /// 字段名和外键名称键值对
    /// </summary>
    public virtual Dictionary<string, string>? ForeignKeys { get; set; }
    /// <summary>
    /// 获取 TypeAdapterConfig 默认实例
    /// </summary>
    /// <returns></returns>
    public virtual TypeAdapterConfig? AdapterConfig { get; set; }
    /// <summary>
    /// 事务提交前执行的委托方法
    /// </summary>
    public virtual Action? Before { get; set; }
    /// <summary>
    /// 事务提交后执行的委托方法
    /// </summary>
    public virtual Action? After { get; set; }
}