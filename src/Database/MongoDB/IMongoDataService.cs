﻿using System.Linq.Expressions;

namespace Devonline.Database.MongoDB;

/// <summary>
/// MongoDB 数据服务接口
/// </summary>
public interface IMongoDataService : INoSQLDataService
{
    /// <summary>
    /// 根据表达式删除指定数据
    /// </summary>
    /// <typeparam name="TModel">当前数据对象模型的类型</typeparam>
    /// <param name="predicate">需要删除的表达式</param>
    /// <returns></returns>
    Task DeleteAsync<TModel>(Expression<Func<TModel, bool>> predicate) where TModel : class, IEntitySet, new();
    /// <summary>
    /// Mongo 的修改, 需要指定列名, 只能对部分列内容进行修改, 否则性能不高
    /// </summary>
    /// <typeparam name="TModel">当前对象的模型类型</typeparam>
    /// <param name="model">当前对象</param>
    /// <param name="fields">所有需要修改的字段名</param>
    /// <returns></returns>
    Task UpdateAsync<TModel>(TModel model, params string[] fields) where TModel : class, IEntitySet, new();
    /// <summary>
    /// Mongo 的修改, 需要指定列名, 只能对部分列内容进行修改, 否则性能不高
    /// </summary>
    /// <typeparam name="TModel">当前对象的模型类型</typeparam>
    /// <param name="data">修改的对象列表</param>
    /// <param name="fields">所有需要修改的字段名</param>
    /// <returns></returns>
    Task UpdatesAsync<TModel>(IEnumerable<TModel> data, params string[] fields) where TModel : class, IEntitySet, new();

    /// <summary>
    /// 新增业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="field">业务数据</param>
    /// <param name="elements">引用数据集合</param>
    /// <returns></returns>
    Task AddCollectionAsync<TModel, TElement>(TModel model, string field, IEnumerable<TElement> elements) where TModel : class, IEntitySet, new() where TElement : class, IEntitySet;
    /// <summary>
    /// 删除业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="field">业务数据</param>
    /// <param name="elements">删除数据集合</param>
    /// <returns></returns>
    Task DeleteCollectionAsync<TModel, TElement>(TModel model, string field, IEnumerable<TElement> elements) where TModel : class, IEntitySet, new() where TElement : class, IEntitySet;
}