﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Devonline.Database.MongoDB.Models;

/// <summary>
/// MongoDB 数据库模型基础基类
/// </summary>
public abstract class MongoModelWithCreate : MongoModel, IMongoModel, IViewModel, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 数据状态
    /// </summary>
    [BsonElement("state"), BsonRepresentation(BsonType.String), DisplayName("数据状态")]
    public virtual DataState State { get; set; } = DataState.Available;
    /// <summary>
    /// 行版本号
    /// </summary>
    [BsonElement("rowVersion"), DisplayName("行版本号"), ConcurrencyCheck, MaxLength(36)]
    public virtual string? RowVersion { get; set; }
    /// <summary>
    /// 创建时间
    /// </summary>
    [BsonElement("createdOn"), DisplayName("创建时间"), Excel]
    public virtual DateTime? CreatedOn { get; set; }
    /// <summary>
    /// 创建人
    /// </summary>
    [BsonElement("createdBy"), DisplayName("创建人"), MaxLength(36), Excel]
    public virtual string? CreatedBy { get; set; }

    /// <summary>
    /// 创建方法
    /// </summary>
    public virtual void Create(string? createdBy = default, DateTimeKind kind = DateTimeKind.Utc)
    {
        Id ??= KeyGenerator.GetStringKey();
        RowVersion = Id;
        CreatedOn = kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;
        CreatedBy = createdBy ?? CreatedBy ?? AppSettings.USER_SYSTEM;
    }
}