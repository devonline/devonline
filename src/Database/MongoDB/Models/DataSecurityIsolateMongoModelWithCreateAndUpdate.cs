﻿using MongoDB.Bson.Serialization.Attributes;

namespace Devonline.Database.MongoDB.Models;

/// <summary>
/// MongoDB 数据库模型基础密文基类
/// </summary>
public class DataSecurityIsolateMongoModelWithCreateAndUpdate : DataSecurityMongoModelWithCreateAndUpdate, IDataSecurityIsolateMongoModel, IDataSecurityMongoModel, IIsolateMongoModel, IDataSecurity, IIsolate, IMongoModel, IViewModel, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 数据隔离键
    /// </summary>
    [BsonElement("isolate")]
    public virtual string Isolate { get; set; } = null!;
}