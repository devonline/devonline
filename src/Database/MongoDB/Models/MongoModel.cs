﻿using MongoDB.Bson.Serialization.Attributes;

namespace Devonline.Database.MongoDB.Models;

/// <summary>
/// MongoDB 数据库模型基础基类
/// </summary>
public abstract class MongoModel : EntitySet, IMongoModel, IEntitySet
{
    [BsonIgnore]
    public override string Id { get; set; } = KeyGenerator.GetStringKey();
}