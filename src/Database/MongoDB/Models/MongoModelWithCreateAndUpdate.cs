﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson.Serialization.Attributes;

namespace Devonline.Database.MongoDB.Models;

/// <summary>
/// MongoDB 数据库模型带新增和修改的基础基类
/// </summary>
public class MongoModelWithCreateAndUpdate : MongoModelWithCreate, IMongoModel, IViewModel, IEntitySet, IEntitySetWithCreate, IEntitySetWithCreateAndUpdate
{
    /// <summary>
    /// 更新时间
    /// </summary>
    [BsonElement("updatedOn"), DisplayName("更新时间"), Excel]
    public virtual DateTime? UpdatedOn { get; set; }
    /// <summary>
    /// 更新人
    /// </summary>
    [BsonElement("updatedBy"), DisplayName("更新人"), MaxLength(36), Excel]
    public virtual string? UpdatedBy { get; set; }
    /// <summary>
    /// 备注说明
    /// </summary>
    [BsonElement("description"), DisplayName("备注说明"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }

    /// <summary>
    /// 更新方法
    /// </summary>
    public virtual void Update(string? updatedBy = default, DateTimeKind kind = DateTimeKind.Utc)
    {
        RowVersion = KeyGenerator.GetStringKey();
        //UpdatedOn = DateTime.SpecifyKind(DateTime.Now, kind);
        UpdatedOn = kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;
        UpdatedBy = updatedBy ?? UpdatedBy ?? AppSettings.USER_SYSTEM;
    }
}