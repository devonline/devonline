﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Devonline.Database.MongoDB.Models;

/// <summary>
/// MongoDB 数据库模型基础基类
/// </summary>
public class IsolateMongoModel : MongoModel, IIsolateMongoModel, IIsolate, IMongoModel, IViewModel, IEntitySet
{
    /// <summary>
    /// 数据隔离键
    /// </summary>
    [BsonElement("isolate")]
    public virtual string Isolate { get; set; } = null!;
}