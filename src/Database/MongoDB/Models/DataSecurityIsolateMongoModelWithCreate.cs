﻿using MongoDB.Bson.Serialization.Attributes;

namespace Devonline.Database.MongoDB.Models;

/// <summary>
/// MongoDB 数据库模型基础密文基类
/// </summary>
public class DataSecurityIsolateMongoModelWithCreate : DataSecurityMongoModelWithCreate, IDataSecurityIsolateMongoModel, IDataSecurityMongoModel, IIsolateMongoModel, IDataSecurity, IIsolate, IMongoModel, IViewModel, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 数据隔离键
    /// </summary>
    [BsonElement("isolate")]
    public virtual string Isolate { get; set; } = null!;
}