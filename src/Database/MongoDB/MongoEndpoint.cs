﻿namespace Devonline.Database.MongoDB;

/// <summary>
/// MongoDB 服务器终结点
/// </summary>
public interface IMongoEndpoint : IDatabaseEndpoint;

/// <summary>
/// MongoDB 服务器终结点
/// </summary>
public class MongoEndpoint : DatabaseEndpoint, IMongoEndpoint, IDatabaseEndpoint;