﻿using System.Linq.Expressions;
using Devonline.AspNetCore;
using Devonline.Core;
using Devonline.Entity;

namespace Devonline.Database.NoSQL;

/// <summary>
/// NoSQL 数据库服务
/// </summary>
public interface INoSQLService
{
    /// <summary>
    /// 数据隔离级别
    /// </summary>
    DataIsolateLevel DataIsolate { get; }
    /// <summary>
    /// 数据隔离的数据编号
    /// </summary>
    string? DataIsolateId { get; }
    /// <summary>
    /// 设置当前数据隔离的编号
    /// </summary>
    /// <param name="dataIsolate">数据隔离级别</param>
    /// <param name="dataIsolateId">数据隔离编号</param>
    void SetDataIsolate(DataIsolateLevel dataIsolate = DataIsolateLevel.None, string? dataIsolateId = default);

    /// <summary>
    /// 写入一行记录到数据库
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="model">要写入的数据</param>
    /// <returns></returns>
    Task AddAsync<TModel>(TModel model) where TModel : class, new();
    /// <summary>
    /// 写入多行记录到数据库
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="data">要写入的数据</param>
    /// <returns></returns>
    Task AddsAsync<TModel>(IEnumerable<TModel> data) where TModel : class, new();

    /// <summary>
    /// 更新一行记录到数据库
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="model">要更新的数据</param>
    /// <returns></returns>
    Task UpdateAsync<TModel>(TModel model) where TModel : class, IEntitySet, new();
    /// <summary>
    /// 更新多行记录到数据库
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="data">要更新的数据</param>
    /// <returns></returns>
    Task UpdatesAsync<TModel>(IEnumerable<TModel> data) where TModel : class, IEntitySet, new();

    /// <summary>
    /// 删除记录
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <typeparam name="TKey">业务数据主键的类型</typeparam>
    /// <param name="id">业务数据主键</param>
    /// <param name="context">数据操作上下文</param>
    /// <returns></returns>
    Task DeleteAsync<TModel>(string id, DataServiceContext? context = default) where TModel : class, IEntitySet;
    /// <summary>
    /// 删除记录
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <typeparam name="TKey">业务数据主键的类型</typeparam>
    /// <param name="ids">业务数据主键集合</param>
    /// <param name="context">数据操作上下文</param>
    /// <returns></returns>
    Task DeletesAsync<TModel>(IEnumerable<string> ids, DataServiceContext? context = default) where TModel : class, IEntitySet;

    /// <summary>
    /// 根据过滤条件返回查询结果
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <returns></returns>
    Task<IQueryable<TModel>> GetQueryableAsync<TModel>() where TModel : class, new();
    /// <summary>
    /// 根据过滤条件返回查询结果
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="predicate">查询表达式</param>
    /// <returns></returns>
    Task<IQueryable<TModel>> GetQueryableAsync<TModel>(Expression<Func<TModel, bool>> predicate) where TModel : class, new();
    /// <summary>
    /// 根据条件查询第一条记录
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="predicate">查询表达式</param>
    /// <returns></returns>
    Task<TModel?> FirstOrDefaultAsync<TModel>(Expression<Func<TModel, bool>> predicate) where TModel : class, new();
}