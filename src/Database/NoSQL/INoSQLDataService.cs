﻿using System.Linq.Expressions;
using Devonline.AspNetCore;

namespace Devonline.Database.NoSQL;

/// <summary>
/// NoSQL 数据库服务
/// </summary>
public interface INoSQLDataService : INoSQLService
{
    /// <summary>
    /// 当前登录的用户
    /// </summary>
    string? UserId { get; }
    /// <summary>
    /// 当前登录的用户
    /// </summary>
    string UserName { get; }
    /// <summary>
    /// 从 httpContext 获取用户标识, 用户标识在 User.Claims 中的 type 为 userName
    /// </summary>
    /// <returns></returns>
    string GetUserName();
    /// <summary>
    /// 获取数据隔离的数据编号
    /// </summary>
    /// <returns></returns>
    string? GetDataIsolateId();

    /// <summary>
    /// 从数据库查询记录
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <returns>查询结果</returns>
    Task<PagedResult<TModel>> GetPagedResultAsync<TModel>() where TModel : class, new();
    /// <summary>
    /// 从数据库查询记录
    /// </summary>
    /// <typeparam name="TModel">数据对象模型的类型</typeparam>
    /// <param name="predicate">查询表达式</param>
    /// <returns>查询结果</returns>
    Task<PagedResult<TModel>> GetPagedResultAsync<TModel>(Expression<Func<TModel, bool>> predicate) where TModel : class, new();
}