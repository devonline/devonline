﻿global using Devonline.Core;
global using Devonline.Database.NoSQL;
global using static Devonline.Core.AppSettings;

namespace Devonline.Database.TimeSeries;

/// <summary>
/// 时序数据库操作的抽象接口
/// </summary>
public interface ITimeSeriesService : INoSQLService;