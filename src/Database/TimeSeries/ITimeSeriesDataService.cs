﻿namespace Devonline.Database.TimeSeries;

/// <summary>
/// 时序数据库操作的抽象接口
/// </summary>
public interface ITimeSeriesDataService : INoSQLDataService;