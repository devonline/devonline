﻿namespace Devonline.Database.TimeSeries.InfluxDB;

/// <summary>
/// 适用于时序数据库的分页查询请求
/// start 和 stop 的时间格式参考
/// https://awesome.influxdata.com/docs/part-2/querying-and-data-transformations/
/// </summary>
public class FluxQueryRequest : PagedRequest
{
    /// <summary>
    /// 起始时间表达式
    /// </summary>
    public string? Start { get; set; }
    /// <summary>
    /// 结束时间表达式
    /// </summary>
    public string? Stop { get; set; }
}