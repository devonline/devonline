﻿using Devonline.Entity;

namespace Devonline.Database.TimeSeries.InfluxDB;

/// <summary>
/// InfluxDB 服务器终结点
/// </summary>
public interface IInfluxDBEndpoint : IDatabaseEndpoint;

/// <summary>
/// InfluxDB 服务器终结点
/// </summary>
public class InfluxDBEndpoint : DatabaseEndpoint, IInfluxDBEndpoint, IDatabaseEndpoint;