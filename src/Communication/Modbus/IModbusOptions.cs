﻿using Devonline.Communication.Abstractions;

namespace Devonline.Communication.Modbus;

/// <summary>
/// Modbus 的配置选项
/// </summary>
public interface IModbusOptions : ICommunicatorOptions
{
    /// <summary>
    /// 启用当前设备功能
    /// </summary>
    bool Enable { get; set; }
    /// <summary>
    /// Modbus 从设备总线地址, 0-255
    /// </summary>
    byte SlaveAddress { get; set; }
    /// <summary>
    /// 执行间隔, 单位秒
    /// </summary>
    int ExecuteInterval { get; set; }
    /// <summary>
    /// 采集上报间隔, 单位毫秒
    /// </summary>
    int ReportInterval { get; set; }
    /// <summary>
    /// 状态查询间隔, 单位毫秒
    /// </summary>
    int QueryInterval { get; set; }
    /// <summary>
    /// 状态归位间隔, 复归型功能的间隔时间, 单位毫秒
    /// </summary>
    int StateBackInterval { get; set; }
    /// <summary>
    /// 最大尝试次数
    /// </summary>
    int MaxRetryCount { get; set; }

    /// <summary>
    /// 写数据的最大条数, 即采集数据多于多少条开始写入数据库
    /// </summary>
    int MaxWriteCount { get; set; }
    /// <summary>
    /// 写数据的最大时间, 即采集数据最长多长时间就要写入一次数据库, 单位: 秒
    /// </summary>
    int MaxWriteTime { get; set; }
    /// <summary>
    /// 写入尝试次数
    /// </summary>
    int WriteRetryCount { get; set; }
}