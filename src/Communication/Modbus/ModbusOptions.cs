﻿using Devonline.Communication.Abstractions;
using Devonline.Core;

namespace Devonline.Communication.Modbus;

/// <summary>
/// Modbus 的配置选项
/// </summary>
public class ModbusOptions : CommunicatorOptions, IModbusOptions
{
    /// <summary>
    /// 启用当前设备功能
    /// </summary>
    public bool Enable { get; set; }
    /// <summary>
    /// Modbus 从设备总线地址, 0-255
    /// </summary>
    public byte SlaveAddress { get; set; }
    /// <summary>
    /// 执行间隔, 单位秒
    /// </summary>
    public int ExecuteInterval { get; set; } = 1;
    /// <summary>
    /// 采集上报间隔, 单位毫秒
    /// </summary>
    public int ReportInterval { get; set; } = AppSettings.UNIT_THOUSAND;
    /// <summary>
    /// 状态查询间隔, 单位毫秒
    /// </summary>
    public int QueryInterval { get; set; } = 50;
    /// <summary>
    /// 状态归位间隔, 复归型功能的间隔时间, 单位毫秒
    /// </summary>
    public int StateBackInterval { get; set; } = 500;
    /// <summary>
    /// 最大尝试次数
    /// </summary>
    public int MaxRetryCount { get; set; } = 10;

    /// <summary>
    /// 写数据的最大条数, 即采集数据多于多少条开始写入数据库
    /// </summary>
    public int MaxWriteCount { get; set; } = 100;
    /// <summary>
    /// 写数据的最大时间, 即采集数据最长多长时间就要写入一次数据库, 单位: 秒
    /// </summary>
    public int MaxWriteTime { get; set; } = AppSettings.UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 写入尝试次数
    /// </summary>
    public int WriteRetryCount { get; set; } = 3;

    /// <summary>
    /// 重载的 ToString 方法来返回当前对象的字符串表达式
    /// </summary>
    /// <returns></returns>
    public override string ToString() => $"Modbus 设备: {Communicator}, 总线地址: {SlaveAddress}";
}