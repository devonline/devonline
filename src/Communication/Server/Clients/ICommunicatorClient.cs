﻿namespace Devonline.Communication.Server;

/// <summary>
/// 定义客户端通讯方法的顶级接口
/// </summary>
public interface ICommunicatorClient<T>
{
    /// <summary>
    /// 客户端接收数据的方法
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    Task Receive(T t);
    /// <summary>
    /// 客户端批量接收数据的方法
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    Task Receive(IEnumerable<T> data);
    /// <summary>
    /// 客户端退出时的方法
    /// </summary>
    /// <returns></returns>
    Task Abort();
}