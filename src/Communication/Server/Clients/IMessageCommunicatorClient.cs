﻿using Devonline.Entity;

namespace Devonline.Communication.Server;

/// <summary>
/// 接收消息类型的客户端
/// 除了继承来的传输数据和数据集合的方法外, 新增传输 TMessage 数据结构的方法
/// </summary>
/// <typeparam name="TMessage">消息</typeparam>
/// <typeparam name="TKey">消息的主键类型</typeparam>
/// <typeparam name="TContent">消息的内容类型</typeparam>
public interface IMessageCommunicatorClient<TMessage, TKey, TContent> : ICommunicatorClient<TMessage> where TMessage : class, IMessage<TKey, TContent>
{
    /// <summary>
    /// 客户端接收已读的方法
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    Task Read(TMessage message);
    /// <summary>
    /// 客户端接收回执的方法
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    Task Ack(TMessage message);
}

/// <summary>
/// 字符串作为主键的通用 message 类型的消息客户端
/// </summary>
/// <typeparam name="TContent"></typeparam>
public interface IMessageCommunicatorClient<TContent> : IMessageCommunicatorClient<Message<TContent>, string, TContent> { }

/// <summary>
/// 字符串作为主键且消息内容为字符串的通用 message 类型的消息客户端
/// </summary>
public interface IMessageCommunicatorClient : IMessageCommunicatorClient<Message, string, string> { }