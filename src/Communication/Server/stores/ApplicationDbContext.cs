﻿using System.ComponentModel.DataAnnotations;
using Devonline.Entity;
using Microsoft.EntityFrameworkCore;

namespace Devonline.Communication.Server;

[Display(Name = "数据库上下文")]
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
        ChangeTracker.AutoDetectChangesEnabled = false;
    }
    public virtual DbSet<Message>? Messages { get; set; }
}