﻿namespace Devonline.Communication.Server;

/// <summary>
/// 分布式缓存的客户端链接缓存
/// 分布式链接机制如下:
/// 1. 每创建一个服务器端, 服务器需要给自己产生一个唯一编号
/// 2. 每连接一个客户端, 在主缓存中记录当前客户端连接的服务器编号
/// 3. 服务器在转发消息时, 先查询接收者是否连接到当前服务器, 没有的话, 到缓存中查询接收者对应的服务器
/// 4. 未跨服务器的消息由当前服务器直接发出
/// 5. 跨服务器的消息由服务器之间通过消息队列的发布订阅服务推送和接收后发出
/// 
/// * 尚未处理完毕的消息, 暂时记录在缓存中, 处理完毕的消息, 超过配置时间的, 记录到持久性存储中, 并从缓存中删除
/// </summary>
internal class UserCacheModel
{
    /// <summary>
    /// 服务器编号, 当前客户端链接到的服务器端编号
    /// </summary>
    public string? Server { get; set; }
    /// <summary>
    /// 客户端标识
    /// </summary>
    public string? Client { get; set; }
    /// <summary>
    /// 客户端连接编号, 用于判断客户端是否重新链接了
    /// </summary>
    public string? ConnectionId { get; set; }
}
