﻿namespace Devonline.Communication.Server;

/// <summary>
/// 服务器同步状态
/// 在分布式系统中, 用于在中心化缓存服务器中记录当前服务器分布式配置的版本更新状态
/// </summary>
internal class ServerSyncState
{
    /// <summary>
    /// 服务器编号
    /// </summary>
    public string? Server { get; set; }
    /// <summary>
    /// 配置文件版本
    /// </summary>
    public string? Version { get; set; }
    /// <summary>
    /// 同步时间
    /// </summary>
    public DateTime SyncTime { get; set; }
}