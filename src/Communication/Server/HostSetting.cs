﻿global using Devonline.Core;
using Devonline.AspNetCore;
using Devonline.Communication.Abstractions;
using Devonline.SystemInfo;

namespace Devonline.Communication.Server;

/// <summary>
/// 通讯中心配置项
/// </summary>
public class HostSetting : HttpSetting
{
    public HostSetting()
    {
        TransportType = TransportType.NewtonsoftJson;
        Host = "/dcs";
        User = "user";
        Client = "client";
        Receiver = "receiver";
        ServiceLifetime = Microsoft.Extensions.DependencyInjection.ServiceLifetime.Singleton;
        NoticeIdentityType = IdentityType.Role;
        OfflineExpireTime = AppSettings.UNIT_DAYS_A_YEAR;
        if (Cache is not null)
        {
            Cache.Prefix = "dcs_";
        }

        // 使用当前 CPU 序列号作为通讯服务器标识
        Server = SystemInformation.GetCPUID();
    }

    /// <summary>
    /// 传输交互方式
    /// </summary>
    public TransportType TransportType { get; set; }
    /// <summary>
    /// 服务器启动的终结点地址
    /// </summary>
    public string Host { get; set; }
    /// <summary>
    /// 通讯中心使用的获取用户标识的变量名
    /// </summary>
    public string User { get; set; }
    /// <summary>
    /// 通讯中心使用的获取客户端标识的变量名
    /// </summary>
    public string Client { get; set; }
    /// <summary>
    /// 通讯中心使用的获取客户端接收者的变量名
    /// </summary>
    public string Receiver { get; set; }
    /// <summary>
    /// 服务器标识, 用于确定服务器分布式的唯一性, 自动生成
    /// </summary>
    public string Server { get; private set; }
    /// <summary>
    /// 是否保存心跳记录
    /// </summary>
    public bool SaveHeartbeat { get; set; }
    /// <summary>
    /// 上线通知开关
    /// </summary>
    public bool OnlineNotice { get; set; }
    /// <summary>
    /// 离线通知开关
    /// </summary>
    public bool OfflineNotice { get; set; }
    /// <summary>
    /// 通知对象, 上下线消息接收对象类型, IdentityType 枚举值, 默认: Role
    /// </summary>
    public IdentityType NoticeIdentityType { get; set; }
    /// <summary>
    /// 通知对象, 上下线消息接收者
    /// </summary>
    public string? NoticeReceiver { get; set; }
    /// <summary>
    /// 离线接收开关, 启用了离线接收, 则有效期内的离线消息会在用户下次上线时发送给用户, 默认开
    /// </summary>
    public bool OfflineReceive { get; set; }
    /// <summary>
    /// 默认过期时间, 用于启用了离线接收后, 用户再次登录时, 通知消息的有效期范围, 单位: 天, 默认一年
    /// </summary>
    public int OfflineExpireTime { get; set; }
    /// <summary>
    /// 是否离线接收有效期内的所有消息, 启用了离线接收后有效
    /// </summary>
    public bool OfflineReceiveAll { get; set; }
    /// <summary>
    /// Gets or sets the maximum buffer size for data read by the application before backpressure is applied.
    /// The default value is 65KB.
    /// </summary>
    public long? TransportMaxBufferSize { get; set; }
    /// <summary>
    /// Gets or sets the maximum buffer size for data written by the application before backpressure is applied.
    /// The default value is 65KB.
    /// </summary>
    public long? ApplicationMaxBufferSize { get; set; }
    /// <summary>
    /// Gets or sets the amount of time the transport will wait for a send to complete.
    /// If a single send exceeds this timeout the connection is closed.
    /// The default timeout is 10 seconds.
    /// </summary>
    public int? TransportSendTimeout { get; set; }

    /// <summary>
    /// Options used to configure hub instances.
    /// </summary>
    public HubOptions? HubOptions { get; set; }
}

/// <summary>
/// Options used to configure hub instances.
/// </summary>
public class HubOptions
{
    /// <summary>
    /// Gets or sets the interval used by the server to timeout incoming handshake requests by clients. The default timeout is 15 seconds.
    /// </summary>
    public long? HandshakeTimeout { get; set; }
    /// <summary>
    /// Gets or sets the interval used by the server to send keep alive pings to connected clients. The default interval is 15 seconds.
    /// </summary>
    public long? KeepAliveInterval { get; set; }
    /// <summary>
    /// Gets or sets the time window clients have to send a message before the server closes the connection. The default timeout is 30 seconds.
    /// </summary>
    public long? ClientTimeoutInterval { get; set; }
    /// <summary>
    /// Gets or sets a collection of supported hub protocol names.
    /// </summary>
    public List<string>? SupportedProtocols { get; set; }
    /// <summary>
    /// Gets or sets the maximum message size of a single incoming hub message. The default is 32KB.
    /// </summary>
    public long? MaximumReceiveMessageSize { get; set; }
    /// <summary>
    /// Gets or sets a value indicating whether detailed error messages are sent to the client. Detailed error messages include details from exceptions thrown on the server.
    /// </summary>
    public bool? EnableDetailedErrors { get; set; }
    /// <summary>
    /// Gets or sets the max buffer size for client upload streams. The default size is 10.
    /// </summary>
    public int? StreamBufferCapacity { get; set; }
    /// <summary>
    /// By default a client is only allowed to invoke a single Hub method at a time.
    /// Changing this property will allow clients to invoke multiple methods at the same time before queueing.
    /// </summary>
    public int? MaximumParallelInvocationsPerClient { get; set; }
}