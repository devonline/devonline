﻿global using Devonline.Communication.Abstractions;
global using Devonline.Core;
using Devonline.Entity;

namespace Devonline.Communication.Messages;

/// <summary>
/// 客户端配置
/// </summary>
public class MessageOptions : CommunicatorOptions
{
    public MessageOptions()
    {
        Host = "wss://dcs.devonline.cn/dcs";
        User = "dev";
        Communicator = nameof(Message);
        TransportType = TransportType.NewtonsoftJson;
    }

    /// <summary>
    /// 传输交互方式
    /// </summary>
    public TransportType TransportType { get; set; }
    /// <summary>
    /// 连接的服务器端地址
    /// </summary>
    public string Host { get; set; }
    /// <summary>
    /// 当前客户端用户名
    /// </summary>
    public string User { get; set; }
    /// <summary>
    /// 当前客户端编号
    /// </summary>
    public string? Client { get; set; }
    /// <summary>
    /// 默认的消息接收者
    /// </summary>
    public string? Receiver { get; set; }
    /// <summary>
    /// 是否加密
    /// 客户端启用加密后, 传输的内容将作为密文传输, 在通讯前, 双方要先交换公钥
    /// </summary>
    public bool Encrypted { get; set; }
    /// <summary>
    /// 是否压缩消息内容
    /// </summary>
    public bool Compression { get; set; }

    /// <summary>
    /// 重载的 ToString 方法来返回当前对象的字符串表达式
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        if (string.IsNullOrWhiteSpace(Client))
        {
            return $"<{Communicator}> user <{User}> from <{Host}> to <{Receiver}>";
        }

        return $"<{Communicator}> user <{User}> in <{Client}> from <{Host}> to <{Receiver}>";
    }
}