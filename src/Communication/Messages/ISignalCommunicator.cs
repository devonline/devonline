﻿namespace Devonline.Communication.Messages;

/// <summary>
/// 消息通讯器接口
/// </summary>
/// <typeparam name="T">通讯器传输的数据类型</typeparam>
public interface ISignalCommunicator<T> : ICommunicator<T>
{
    /// <summary>
    /// 可以附加的查询字符串
    /// </summary>
    Dictionary<string, string> QueryString { get; }
}

/// <summary>
/// 以字符串作为通讯内容的通讯器
/// </summary>
public interface ISignalCommunicator : ISignalCommunicator<string>, ICommunicator;