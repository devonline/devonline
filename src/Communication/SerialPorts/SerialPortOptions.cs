﻿using System.ComponentModel.DataAnnotations;
using System.IO.Ports;
using Devonline.Communication.Abstractions;
using Devonline.Core;

namespace Devonline.Communication.SerialPorts;

/// <summary>
/// 串口设置
/// </summary>
[Display(Name = "串口设置")]
public class SerialPortOptions : CommunicatorOptions
{
    /// <summary>
    /// 构造方法
    /// </summary>
    public SerialPortOptions()
    {
        Communicator = nameof(SerialPort);
        PortName = "COM1";
        BaudRate = 9600;
        DataBits = AppSettings.UNIT_EIGHT;
        StopBits = StopBits.One;
        Parity = Parity.None;
        Handshake = Handshake.None;
        BufferSize = AppSettings.UNIT_KILO;
        Encoding = nameof(System.Text.Encoding.ASCII);
        MonitorInterval = AppSettings.UNIT_TEN;
    }

    #region 可配置属性
    /// <summary>
    /// 端口名
    /// </summary>
    [Display(Name = "端口名")]
    public string PortName { get; set; }
    /// <summary>
    /// 波特率
    /// </summary>
    [Display(Name = "波特率")]
    public int BaudRate { get; set; }
    /// <summary>
    /// 数据位
    /// </summary>
    [Display(Name = "数据位")]
    public int DataBits { get; set; }
    /// <summary>
    /// 停止位
    /// </summary>
    [Display(Name = "停止位")]
    public StopBits StopBits { get; set; }
    /// <summary>
    /// 奇偶校验位
    /// </summary>
    [Display(Name = "奇偶校验位")]
    public Parity Parity { get; set; }
    /// <summary>
    /// 握手协议
    /// </summary>
    [Display(Name = "握手协议")]
    public Handshake Handshake { get; set; }
    /// <summary>
    /// 忽略 null 字节
    /// </summary>
    [Display(Name = "忽略 null 字节")]
    public bool DiscardNull { get; set; }
    /// <summary>
    /// 启用数据终端就绪信息
    /// </summary>
    [Display(Name = "启用数据终端就绪信息")]
    public bool DtrEnable { get; set; }
    /// <summary>
    /// 缓冲区大小, 默认 1KB
    /// </summary>
    [Display(Name = "缓冲区大小")]
    public int BufferSize { get; set; }
    /// <summary>
    /// 字符编码
    /// </summary>
    [Display(Name = "字符编码")]
    public string Encoding { get; set; }
    /// <summary>
    /// 是否使用字节流的实例
    /// </summary>
    public bool UseStream { get; set; }
    #endregion
}