﻿using System.ComponentModel;

namespace Devonline.Communication.Abstractions;

/// <summary>
/// Communication 传输方式
/// </summary>
[Description("传输方式")]
public enum TransportType
{
    /// <summary>
    /// 使用 System.Text.Json 方式
    /// </summary>
    Json,
    /// <summary>
    /// 使用 NewtonsoftJson 方式
    /// </summary>
    NewtonsoftJson,
    /// <summary>
    /// 使用 MessagePack 方式
    /// </summary>
    MessagePack
}