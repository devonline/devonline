﻿namespace Devonline.Communication.Abstractions;

/// <summary>
/// 通讯器使用的配置选项
/// </summary>
public interface ICommunicatorOptions
{
    /// <summary>
    /// 当前通讯器名称
    /// </summary>
    string? Communicator { get; set; }
    /// <summary>
    /// 通讯器监控执行间隔, 单位: 秒
    /// </summary>
    int MonitorInterval { get; set; }
    /// <summary>
    /// 是否启用计时器
    /// </summary>
    bool Timer { get; set; }
}