﻿namespace Devonline.Communication.Abstractions;

/// <summary>
/// 通讯器使用的配置选项
/// </summary>
public class CommunicatorOptions : ICommunicatorOptions
{
    /// <summary>
    /// 当前通讯器名称
    /// </summary>
    public string? Communicator { get; set; }
    /// <summary>
    /// 通讯器监控执行间隔, 单位: 秒
    /// </summary>
    public int MonitorInterval { get; set; } = AppSettings.UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 是否启用计时器
    /// </summary>
    public bool Timer { get; set; } = true;

    /// <summary>
    /// 重载的 ToString 方法来返回当前对象的字符串表达式
    /// </summary>
    /// <returns></returns>
    public override string ToString() => this.ToKeyValuePairs().ToString(AppSettings.DEFAULT_OUTER_SPLITER, AppSettings.DEFAULT_INNER_SPLITER);
}