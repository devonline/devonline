﻿using System.ComponentModel;
using Devonline.Core;

namespace Devonline.AuxiliaryTools.DatabaseTools;

/// <summary>
/// 数据列定义
/// </summary>
[DisplayName("数据库表字段定义")]
internal class DataField
{
    /// <summary>
    /// 序号
    /// </summary>
    [DisplayName("序号"), Excel]
    public int Index { get; set; }
    /// <summary>
    /// 字段名
    /// </summary>
    [DisplayName("字段名"), Excel]
    public string? Name { get; set; }
    /// <summary>
    /// 数据库字段名
    /// </summary>
    [DisplayName("数据库字段名"), Excel]
    public string? FieldName { get; set; }
    /// <summary>
    /// 数据类型
    /// </summary>
    [DisplayName("数据类型"), Excel]
    public string? DataType { get; set; }
    /// <summary>
    /// 长度
    /// </summary>
    [DisplayName("长度"), Excel]
    public int? Length { get; set; }
    /// <summary>
    /// 是否主键
    /// </summary>
    [DisplayName("是否主键"), Excel(Size = AppSettings.DEFAULT_EXCEL_NUMBER_SIZE)]
    public bool IsPrimaryKey { get; set; }
    /// <summary>
    /// 是否可空
    /// </summary>
    [DisplayName("是否可空"), Excel(Size = AppSettings.DEFAULT_EXCEL_NUMBER_SIZE)]
    public bool IsNullable { get; set; }
    /// <summary>
    /// 描述
    /// </summary>
    [DisplayName("描述"), Excel(Size = AppSettings.DEFAULT_EXCEL_STRING_SIZE * AppSettings.UNIT_TWO)]
    public string? Description { get; set; }
}