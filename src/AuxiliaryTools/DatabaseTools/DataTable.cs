﻿using System.ComponentModel;
using Devonline.Core;

namespace Devonline.AuxiliaryTools.DatabaseTools;

/// <summary>
/// 数据表定义
/// </summary>
[DisplayName("数据库表定义")]
internal class DataTable
{
    /// <summary>
    /// 序号
    /// </summary>
    [DisplayName("序号"), Excel]
    public int Index { get; set; }
    /// <summary>
    /// 实体数据对象名
    /// </summary>
    [DisplayName("实体数据对象名"), Excel]
    public string? Name { get; set; }
    /// <summary>
    /// 数据库表名
    /// </summary>
    [DisplayName("数据库表名"), Excel]
    public string? TableName { get; set; }
    /// <summary>
    /// 描述
    /// </summary>
    [DisplayName("描述"), Excel(Size = AppSettings.DEFAULT_EXCEL_STRING_SIZE * AppSettings.UNIT_TWO)]
    public string? Description { get; set; }
}