﻿using Devonline.AspNetCore;
using Devonline.Core;
using iText.Html2pdf;
using Microsoft.Extensions.Logging;

namespace Devonline.AuxiliaryTools.FileTools;

/// <summary>
/// Pdf 文件相关服务
/// </summary>
/// <param name="logger">日志</param>
/// <param name="httpSetting">Http配置项</param>
public class PdfFileService(ILogger<PdfFileService> logger, HttpSetting httpSetting) : FileService(logger, httpSetting), IFileService
{
    /// <summary>
    /// Convert html file to pdf file
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public virtual string ConvertToPdf(string fileName)
    {
        using var htmlSource = File.Open(fileName, FileMode.Open);
        var pdfFileName = GetSameFileName(fileName, AppSettings.DEFAULT_PDF_FILE_EXTENSION);
        using var pdfDest = File.Open(pdfFileName, FileMode.Create);
        HtmlConverter.ConvertToPdf(htmlSource, pdfDest, new ConverterProperties());
        return pdfFileName;
    }
}