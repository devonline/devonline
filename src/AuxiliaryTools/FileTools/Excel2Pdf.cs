﻿using System.Text;
using iText.Kernel.Pdf;
using OfficeOpenXml;

namespace Devonline.AuxiliaryTools.FileConversionTools;

public class Excel2Pdf
{
    ///
    /// 生成pdf文件
    ///
    /// excel文件的字节流 ///
    public static byte[] Render(byte[] excelContent)
    {
        var excelPackage = new ExcelPackage(new MemoryStream(excelContent));

        PdfDocument document = new PdfDocument(new PdfWriter(""));


    }

    public static void ConvertExcelToPdf2(string excelFilePath, string pdfFilePath)
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        // 加载Excel文件
        using (FileStream fileStream = new FileStream(excelFilePath, FileMode.Open, FileAccess.Read))
        {
            IWorkbook workbook = new XSSFWorkbook(fileStream); 
            ISheet sheet = workbook.GetSheetAt(0);
            // 创建PDF文档
            Document document = new Document();
            // 创建PDF写入器
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(pdfFilePath, FileMode.Create));
            // 打开PDF文档
            document.Open();
            // 添加Excel表格内容到PDF
            PdfPTable table = new PdfPTable(sheet.GetRow(0).LastCellNum);
            table.WidthPercentage = 100;
            foreach (IRow row in sheet)
            {
                foreach (ICell cell in row)
                {
                    string value = cell.ToString();
                    PdfPCell pdfCell = new PdfPCell(new Phrase(value, GetChineseFont()));
                    table.AddCell(pdfCell);
                }
            }

            document.Add(table);
            // 关闭PDF文档
            document.Close();
        }
    }
}