﻿using Devonline.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace Devonline.AuxiliaryTools.AITools;

public static class ServiceExtensions
{
    /// <summary>
    /// 添加视频捕获服务
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddFramesCaptureService(this IServiceCollection services)
    {
        return services.AddScoped<IFramesCaptureService, FramesCaptureService>();
    }

    ///// <summary>
    ///// 人脸比对本地库接口
    ///// </summary>
    ///// <param name="services">依赖注入服务容器</param>
    ///// <param name="endpoint">人脸比对接口接入终结点</param>
    ///// <returns></returns>
    //public static IServiceCollection AddFaceRecognitionService(this IServiceCollection services, IFaceRecognitionEndpoint? endpoint = default)
    //{
    //    if (endpoint is not null)
    //    {
    //        services.AddSingleton(endpoint);
    //        services.AddScoped<IFaceRecognitionService, FaceRecognitionService>();
    //    }

    //    return services;
    //}
}