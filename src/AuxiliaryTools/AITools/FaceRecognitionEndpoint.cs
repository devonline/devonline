﻿using Devonline.AspNetCore;
using Devonline.Core;
using Devonline.Entity;

namespace Devonline.AuxiliaryTools.AITools;

/// <summary>
/// 人脸识别终结点
/// </summary>
public interface IFaceRecognitionEndpoint : IAuthEndpoint
{
    /// <summary>
    /// 人脸比对阈值, 根据算法建议, 设定的门槛值
    /// </summary>
    float FaceCompareThresholdValue { get; set; }
    /// <summary>
    /// 图片文件限制
    /// </summary>
    FileSetting ImageLimit { get; set; }
}

/// <summary>
/// 人脸识别终结点的默认实例
/// </summary>
public class FaceRecognitionEndpoint : AuthEndpoint, IFaceRecognitionEndpoint
{
    public FaceRecognitionEndpoint()
    {
        FaceCompareThresholdValue = 0.5f;
        ImageLimit = new()
        {
            Length = AppSettings.UNIT_MEGA,
            Extensions = AppSettings.DEFAULT_IMAGE_FILE_EXTENSIONS
        };
    }

    /// <summary>
    /// 人脸比对阈值, 根据算法建议, 设定的门槛值
    /// </summary>
    public float FaceCompareThresholdValue { get; set; }
    /// <summary>
    /// 图片文件限制
    /// </summary>
    public FileSetting ImageLimit { get; set; }
}