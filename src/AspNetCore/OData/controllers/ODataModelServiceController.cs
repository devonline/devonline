﻿using System.ComponentModel;
using Devonline.Core;
using Devonline.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore.OData;

/// <summary>
/// 基于 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>(
    ILogger<ODataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>> logger,
    IDataService<TDbContext, TEntitySet, TKey> dataService) :
    DataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TViewModel : class, IViewModel<TKey>, new()
    where TKey : IConvertible
{
    /// <summary>
    /// 使用 odata 协议进行查询的 get 请求
    /// </summary>
    /// <returns></returns>
    [HttpGet, EnableQuery, AccessAuthorize(Code = "IGet"), DisplayName("查询")]
    public override async Task<IActionResult> GetAsync()
    {
        _logger.LogInformation("用户 {userName} 开始查询 {typeName}!", _dataService.UserName, _dataService.TypeName);
        await Task.CompletedTask;
        return Ok(_dataService.GetQueryable());
    }
}

/// <summary>
/// 基于字符串作为主键的 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataModelServiceController<TDbContext, TEntitySet, TViewModel>(
    ILogger<ODataModelServiceController<TDbContext, TEntitySet, TViewModel>> logger,
    IDataService<TDbContext, TEntitySet> dataService) :
    ODataModelServiceController<TDbContext, TEntitySet, TViewModel, string>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet, new()
    where TViewModel : class, IViewModel, new();