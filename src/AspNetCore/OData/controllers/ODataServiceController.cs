﻿using System.ComponentModel;
using Devonline.Core;
using Devonline.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore.OData;

/// <summary>
/// 基于 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataServiceController<TDbContext, TEntitySet, TKey>(
    ILogger<ODataServiceController<TDbContext, TEntitySet, TKey>> logger,
    IDataService<TDbContext, TEntitySet, TKey> dataService) :
    DataServiceController<TDbContext, TEntitySet, TKey>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TKey : IConvertible
{
    /// <summary>
    /// 使用 odata 协议进行查询的 get 请求
    /// </summary>
    /// <returns></returns>
    [HttpGet, EnableQuery, AccessAuthorize(Code = "IGet"), DisplayName("odata 查询")]
    public override async Task<IActionResult> GetAsync()
    {
        _logger.LogInformation("用户 {userName} 开始查询 {typeName}!", _dataService.UserName, _dataService.TypeName);
        await Task.CompletedTask;
        return Ok(_dataService.GetQueryable());
    }
}

/// <summary>
/// 基于字符串作为主键的 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataServiceController<TDbContext, TEntitySet>(
    ILogger<ODataServiceController<TDbContext, TEntitySet>> logger,
    IDataService<TDbContext, TEntitySet> dataService) :
    ODataServiceController<TDbContext, TEntitySet, string>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet, new();