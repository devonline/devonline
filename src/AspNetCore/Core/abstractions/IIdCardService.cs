﻿namespace Devonline.AspNetCore;

/// <summary>
/// 身份证识别服务
/// </summary>
public interface IIdCardService
{
    /// <summary>
    /// 从身份证照片识别并获取身份证信息
    /// </summary>
    /// <param name="attachments"></param>
    /// <returns>返回识别出的身份证数据</returns>
    Task<IdCardViewModel> GetIdCardInformationAsync(ICollection<Attachment> attachments);
    /// <summary>
    /// 使用 idCode 获取已存在的 IdCard
    /// </summary>
    /// <param name="idCode">身份证号</param>
    /// <returns></returns>
    Task<IdCard?> GetByIdCodeAsync(string idCode);
    /// <summary>
    /// 保存身份证信息, 可带附件
    /// </summary>
    /// <param name="idCard">身份证信息</param>
    /// <param name="context">数据操作上下文</param>
    /// <returns></returns>
    Task<IdCard> SaveAsync(IdCardViewModel idCard, DataServiceContext? context = null);
}