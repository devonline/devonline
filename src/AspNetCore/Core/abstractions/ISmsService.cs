﻿namespace Devonline.AspNetCore;

/// <summary>
/// sms 短信息发送服务
/// </summary>
/// <typeparam name="TSmsModel"></typeparam>
public interface ISmsService
{
    /// <summary>
    /// 发送短信息
    /// </summary>
    /// <param name="smsModel">短消息对象模型</param>
    /// <returns></returns>
    Task SendAsync(ISmsModel smsModel);
    /// <summary>
    /// 发送短信息
    /// </summary>
    /// <param name="signName">短信签名</param>
    /// <param name="templateCode">短信内容模板编号</param>
    /// <param name="templateParam">短信内容模板中的变量和值的集合</param>
    /// <param name="phoneNumbers">默认发送的手机号码, phoneNumbers 或者 TSmsModel.PhoneNumbers 必须有一个有值</param>
    /// <returns></returns>
    Task SendAsync(string signName, string templateCode, Dictionary<string, string> templateParam, params string[] phoneNumbers);
}

/// <summary>
/// sms 短信验证码发送服务
/// </summary>
public interface ISmsCaptchaService : ISmsService
{
    /// <summary>
    /// 发送短信验证码
    /// </summary>
    /// <param name="phoneNumbers">默认发送的手机号码, phoneNumbers 或者 TSmsModel.PhoneNumbers 必须有一个有值</param>
    /// <returns></returns>
    Task<string> SendCaptchaAsync(params string[] phoneNumbers);
}