﻿namespace Devonline.AspNetCore;

/// <summary>
/// 附件及文件操作类服务
/// </summary>
public interface IFileService
{
    /// <summary>
    /// 从 fileName 获取文件扩展名为 extension 的同名文件
    /// </summary>
    /// <param name="fileName">原始文件名</param>
    /// <param name="extension">文件扩展名</param>
    /// <returns></returns>
    string GetSameFileName(string fileName, string? extension = default);
    /// <summary>
    /// 获取附件绝对地址
    /// </summary>
    /// <param name="fileName">附件文件名</param>
    /// <returns></returns>
    string GetAttachmentPath(string fileName);
    /// <summary>
    /// 获取随机的临时文件名
    /// </summary>
    /// <param name="extension"></param>
    /// <returns></returns>
    string GetTempFileName(string? extension = default);
    /// <summary>
    /// 从文件名获取文件绝对地址, 如果文件名是来自互联网的路径, 则下载并保存到本地在返回
    /// </summary>
    /// <param name="fileName">相对文件名或互联网路径</param>
    /// <returns></returns>
    Task<string> GetFileAsync(string fileName);
    /// <summary>
    /// 压缩文件/文件夹, 之后执行委托方法
    /// </summary>
    /// <param name="filePath">文件/文件夹路径</param>
    string Compress(string filePath, Action<string>? action = default);

    /// <summary>
    /// 删除附件物理文件, 请在附件相关操作的事物提交之后执行此方法
    /// </summary>
    /// <param name="fileName">待删除的文件</param>
    void Delete(string fileName);
    /// <summary>
    /// 删除附件物理文件, 请在附件相关操作的事物提交之后执行此方法
    /// </summary>
    /// <param name="fileNames">待删除的文件</param>
    void Delete(params string[] fileNames);

    /// <summary>
    /// 销毁文件, 如果销毁, 此接口将性能不好且文件则无法找回
    /// 销毁附件物理文件, 请在附件相关操作的事物提交之后执行此方法
    /// </summary>
    /// <param name="fileName">待销毁的文件</param>
    Task DestoryAsync(string fileName);
    /// <summary>
    /// 销毁文件, 如果销毁, 此接口将性能不好且文件则无法找回
    /// 销毁附件物理文件, 请在附件相关操作的事物提交之后执行此方法
    /// </summary>
    /// <param name="fileNames">待销毁的文件</param>
    Task DestoryAsync(params string[] fileNames);

    /// <summary>
    /// 从上传的图片文件得到一个合适尺寸的缩略图
    /// </summary>
    /// <param name="fileName">原始文件</param>
    /// <param name="setting">缩略图设置</param>
    /// <returns></returns>
    Task<string?> GetImageThumbnailAsync(string fileName, FileSetting? setting = null);
    /// <summary>
    /// 从上传的图片文件得到一个合适尺寸的裁剪图片
    /// </summary>
    /// <param name="fileName">原始文件</param>
    /// <param name="setting">裁剪设置</param>
    /// <returns></returns>
    Task<string?> GetImageCropAsync(string fileName, FileSetting? setting = null);
}