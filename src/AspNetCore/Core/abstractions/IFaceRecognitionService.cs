﻿namespace Devonline.AspNetCore;

/// <summary>
/// 人脸识别相关接口
/// </summary>
public interface IFaceRecognitionService
{
    /// <summary>
    /// 两张面部图片进行人脸比对
    /// </summary>
    /// <param name="src">原图片</param>
    /// <param name="dist">目标图片</param>
    /// <returns>比对结果</returns>
    Task<(bool, float)> CompareAsync(string src, string dist);
}