﻿namespace Devonline.AspNetCore;

/// <summary>
/// 附件及文件操作类服务
/// </summary>
public interface IDataFileService : IFileService
{
    /// <summary>
    /// 写入数据到文件, 每个对象写一行, 此为大数据量写入处理方法
    /// </summary>
    /// <typeparam name="TEntitySet">写入的数据类型</typeparam>
    /// <param name="entitySets">写入的数据</param>
    /// <param name="fileName">文件名</param>
    /// <param name="converter">对象到数据行的转换方法, 传入参数分别是: 当前行字符串, 当前行号, 总行号</param>
    /// <returns>写入的总行数</returns>
    Task<long> WriteAsync<TEntitySet>(IEnumerable<TEntitySet> entitySets, string? fileName = default, Func<TEntitySet, long, long, string?>? converter = default) where TEntitySet : class;
    /// <summary>
    /// 从文件读取数据, 每行读取到一个对象, 此为大数据量读取处理方法
    /// </summary>
    /// <typeparam name="TEntitySet">读取的数据类型</typeparam>
    /// <param name="fileName">文件名</param>
    /// <param name="converter">数据行到对象的转换方法, 传入参数分别是: 当前行字符串, 当前行号</param>
    /// <returns>读取的数据</returns>
    Task<List<TEntitySet>> ReadAsync<TEntitySet>(string? fileName = default, Func<string, long, TEntitySet?>? converter = default) where TEntitySet : class, new();
}