﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.AspNetCore;

/// <summary>
/// 业务数据带附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IDataWithCollectionService<TDbContext, TEntitySet, TElement, TKey> :
    IDataService<TDbContext, TEntitySet, TKey>,
    IDataCollectionService<TDbContext, TEntitySet, TElement, TKey>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>
    where TElement : class, IEntitySet<TKey>
    where TKey : IConvertible;

/// <summary>
/// 业务数据带附属集合类数据处理服务
/// 字符串作为主键的业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public interface IDataWithCollectionService<TDbContext, TEntitySet, TElement> :
    IDataWithCollectionService<TDbContext, TEntitySet, TElement, string>,
    IDataService<TDbContext, TEntitySet>,
    IDataCollectionService<TDbContext, TEntitySet, TElement>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet
    where TElement : class, IEntitySet;