﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.AspNetCore;

/// <summary>
/// 业务数据带附属集合及附件类数据处理的基础数据服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// 一个业务数据中仅有一个类型为: TAttachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
/// <typeparam name="TAttachment">附件数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IDataWithCollectionAndAttachmentService<TDbContext, TEntitySet, TElement, TAttachment, TKey> :
    IDataWithCollectionService<TDbContext, TEntitySet, TElement, TKey>,
    IDataWithAttachmentService<TDbContext, TEntitySet, TAttachment, TKey>,
    IDataService<TDbContext, TEntitySet, TKey>,
    IDataCollectionService<TDbContext, TEntitySet, TElement, TKey>,
    IDataAttachmentService<TDbContext, TEntitySet, TAttachment, TKey>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>
    where TElement : class, IEntitySet<TKey>
    where TAttachment : class, IAttachment<TKey>
    where TKey : IConvertible;

/// <summary>
/// 业务数据带附属集合及附件类数据处理的基础数据服务
/// 字符串作为主键的默认实现
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// 一个业务数据中仅有一个类型为: Attachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public interface IDataWithCollectionAndAttachmentService<TDbContext, TEntitySet, TElement> :
    IDataWithCollectionAndAttachmentService<TDbContext, TEntitySet, TElement, Attachment, string>,
    IDataWithCollectionService<TDbContext, TEntitySet, TElement>,
    IDataWithAttachmentService<TDbContext, TEntitySet>,
    IDataService<TDbContext, TEntitySet>,
    IDataCollectionService<TDbContext, TEntitySet, TElement>,
    IDataAttachmentService<TDbContext, TEntitySet>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet
    where TElement : class, IEntitySet;