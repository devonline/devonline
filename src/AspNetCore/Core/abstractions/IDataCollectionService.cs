﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.AspNetCore;

/// <summary>
/// 业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IDataCollectionService<TDbContext, TEntitySet, TElement, TKey>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>
    where TElement : class, IEntitySet<TKey>
    where TKey : IConvertible
{
    /// <summary>
    /// 新增业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="entitySet">业务数据</param>
    /// <param name="elements">引用数据集合</param>
    /// <param name="foreignKey">外键字段名</param>
    /// <returns></returns>
    Task AddCollectionAsync(TEntitySet entitySet, IEnumerable<TElement>? elements = default, string? foreignKey = default);
    /// <summary>
    /// 更新业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="entitySet">业务数据</param>
    /// <param name="elements">引用数据集合</param>
    /// <param name="foreignKey">外键字段名</param>
    /// <param name="isLogical">是否逻辑操作, 默认不是</param>
    /// <returns></returns>
    Task UpdateCollectionAsync(TEntitySet entitySet, IEnumerable<TElement>? elements = default, string? foreignKey = default, bool isLogical = false);
    /// <summary>
    /// 删除业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="id">业务数据主键</param>
    /// <param name="foreignKey">外键字段名</param>
    /// <param name="isLogical">是否逻辑操作, 默认不是</param>
    /// <returns></returns>
    Task DeleteCollectionAsync(TKey id, string? foreignKey = default, bool isLogical = false);
}

/// <summary>
/// 字符串作为主键的业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public interface IDataCollectionService<TDbContext, TEntitySet, TElement> :
    IDataCollectionService<TDbContext, TEntitySet, TElement, string>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet
    where TElement : class, IEntitySet;