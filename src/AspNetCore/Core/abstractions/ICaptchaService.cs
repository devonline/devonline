﻿using Microsoft.AspNetCore.Mvc;

namespace Devonline.AspNetCore;

/// <summary>
/// 验证码服务
/// </summary>
public interface ICaptchaService
{
    /// <summary>
    /// 获取验证码
    /// </summary>
    /// <param name="captchaId">验证码编号</param>
    /// <returns></returns>
    IActionResult GetCaptcha(string captchaId);
    /// <summary>
    /// 获取验证码
    /// </summary>
    /// <param name="captchaId">验证码编号</param>
    /// <returns></returns>
    Task<IActionResult> GetCaptchaAsync(string captchaId);

    /// <summary>
    /// 验证验证码
    /// </summary>
    /// <param name="captchaId">验证码编号</param>
    /// <param name="captcha">验证码</param>
    /// <returns></returns>
    bool Validate(string captchaId, string captcha);
    /// <summary>
    /// 验证验证码
    /// </summary>
    /// <param name="captchaId">验证码编号</param>
    /// <param name="captcha">验证码</param>
    /// <returns></returns>
    Task<bool> ValidateAsync(string captchaId, string captcha);
}