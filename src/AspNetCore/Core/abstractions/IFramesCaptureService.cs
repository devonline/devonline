﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devonline.AspNetCore;

/// <summary>
/// 视频抽帧服务
/// </summary>
public interface IFramesCaptureService
{
    /// <summary>
    /// 从视频种捕获图片
    /// </summary>
    /// <param name="fileName">原始视频文件名</param>
    /// <param name="captureCount">捕获数量</param>
    /// <returns></returns>
    string[]? Capture(string fileName, int captureCount = UNIT_ONE);
}