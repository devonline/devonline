﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.AspNetCore;

/// <summary>
/// 业务数据带附件类服务
/// 一个业务数据中仅有一个类型为: TAttachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TAttachment">附件数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IDataWithAttachmentService<TDbContext, TEntitySet, TAttachment, TKey> :
    IDataService<TDbContext, TEntitySet, TKey>,
    IDataAttachmentService<TDbContext, TEntitySet, TAttachment, TKey>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>
    where TAttachment : class, IAttachment<TKey>
    where TKey : IConvertible;

/// <summary>
/// 业务数据带附件类服务
/// 字符串作为主键的默认实现
/// 一个业务数据中仅有一个类型为: Attachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
public interface IDataWithAttachmentService<TDbContext, TEntitySet> :
    IDataWithAttachmentService<TDbContext, TEntitySet, Attachment, string>,
    IDataService<TDbContext, TEntitySet>,
    IDataAttachmentService<TDbContext, TEntitySet>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet;