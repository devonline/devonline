﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.AspNetCore;

/// <summary>
/// 重载的数据库上下文, 提供字符串类型主键的默认实现
/// 此基类上下文因为和身份上下文存在相同两个实体对象模型, 因此不可创建于统一数据库中
/// </summary>
public class BaseDbContext(DbContextOptions options) : BaseDbContext<Attachment, DataIssue, Email, Feedback, IdCard, LimitList, Log, Message, Parameter, Personal, Region, SensitiveWord, Todo, WhiteList, string>(options);

/// <summary>
/// 提供 TKey 作为主键类型的数据库上下文
/// 此基类上下文因为和身份上下文存在相同两个实体对象模型, 因此不可创建于统一数据库中
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class BaseDbContext<TKey>(DbContextOptions options) : BaseDbContext<Attachment<TKey>, DataIssue<TKey>, Email<TKey>, Feedback<TKey>, IdCard<TKey>, LimitList<TKey>, Log<TKey>, Message<TKey>, Parameter<TKey>, Personal<TKey>, Region<TKey>, SensitiveWord<TKey>, Todo<TKey>, WhiteList<TKey>, TKey>(options) where TKey : IEquatable<TKey>, IConvertible;

/// <summary>
/// 包含了基础数据对象模型的数据库上下文
/// 此基类上下文因为和身份上下文存在相同两个实体对象模型, 因此不可创建于统一数据库中
/// </summary>
/// <typeparam name="TAttachment"></typeparam>
/// <typeparam name="TDataIssue"></typeparam>
/// <typeparam name="TEmail"></typeparam>
/// <typeparam name="TFeedback"></typeparam>
/// <typeparam name="TIdCard"></typeparam>
/// <typeparam name="TLimitList"></typeparam>
/// <typeparam name="TLog"></typeparam>
/// <typeparam name="TMessage"></typeparam>
/// <typeparam name="TParameter"></typeparam>
/// <typeparam name="TPersonal"></typeparam>
/// <typeparam name="TRegion"></typeparam>
/// <typeparam name="TSensitiveWord"></typeparam>
/// <typeparam name="TTodo"></typeparam>
/// <typeparam name="TWhiteList"></typeparam>
/// <typeparam name="TKey"></typeparam>
public class BaseDbContext<TAttachment, TDataIssue, TEmail, TFeedback, TIdCard, TLimitList, TLog, TMessage, TParameter, TPersonal, TRegion, TSensitiveWord, TTodo, TWhiteList, TKey>(DbContextOptions options) : DbContext(options)
    where TAttachment : Attachment<TKey>
    where TDataIssue : DataIssue<TKey>
    where TEmail : Email<TKey>
    where TFeedback : Feedback<TKey>
    where TIdCard : IdCard<TKey>
    where TLimitList : LimitList<TKey>
    where TLog : Log<TKey>
    where TMessage : Message<TKey>
    where TParameter : Parameter<TKey>
    where TPersonal : Personal<TKey>
    where TRegion : Region<TKey>
    where TSensitiveWord : SensitiveWord<TKey>
    where TTodo : Todo<TKey>
    where TWhiteList : WhiteList<TKey>
    where TKey : IEquatable<TKey>, IConvertible
{
    #region base entities
    public virtual DbSet<TAttachment> Attachments => Set<TAttachment>();
    public virtual DbSet<TDataIssue> DataIssues => Set<TDataIssue>();
    public virtual DbSet<TEmail> Emails => Set<TEmail>();
    public virtual DbSet<TFeedback> Feedbacks => Set<TFeedback>();
    public virtual DbSet<TIdCard> IdCards => Set<TIdCard>();
    public virtual DbSet<TLimitList> LimitLists => Set<TLimitList>();
    public virtual DbSet<TLog> Logs => Set<TLog>();
    public virtual DbSet<TMessage> Messages => Set<TMessage>();
    public virtual DbSet<TParameter> Parameters => Set<TParameter>();
    public virtual DbSet<TPersonal> Personals => Set<TPersonal>();
    public virtual DbSet<TRegion> Regions => Set<TRegion>();
    public virtual DbSet<TSensitiveWord> SensitiveWords => Set<TSensitiveWord>();
    public virtual DbSet<TTodo> Todos => Set<TTodo>();
    public virtual DbSet<TWhiteList> WhiteLists => Set<TWhiteList>();
    #endregion

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<TAttachment>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TDataIssue>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TEmail>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TFeedback>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TIdCard>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TLimitList>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TLog>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TMessage>(b => b.Property(x => x.Type).HasDefaultValue(MessageType.Text));
        builder.Entity<TParameter>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TPersonal>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TRegion>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TSensitiveWord>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TTodo>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TWhiteList>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
    }
}