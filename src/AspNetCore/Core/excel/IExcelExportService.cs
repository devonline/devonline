﻿using Microsoft.AspNetCore.Mvc;

namespace Devonline.AspNetCore;

/// <summary>
/// excel 导出操作的服务
/// TODO 暂不支持嵌套对象内部字段导出
/// </summary>
public interface IExcelExportService
{
    /// <summary>
    /// 导出数据到 excel
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="excelData">excel 数据对象</param>
    /// <returns>返回当前对象, 以支持连续导出到多张表</returns>
    IExcelExportService Export<TEntitySet>(ExcelData<TEntitySet> excelData);
    /// <summary>
    /// 导出数据到 excel, 此方法会按默认参数构造 ExcelData 并导出
    /// 需要 Data(必须), Columns(非必须) 和 SheetName(非必须) 参数
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="Data">要导出的数据集合</param>
    IExcelExportService Export<TEntitySet>(IEnumerable<TEntitySet> Data);

    /// <summary>
    /// 获取导出处理过程中存在于内存中的 excel 文件数据流
    /// </summary>
    /// <returns></returns>
    Task<byte[]> GetContentsAsync();
    /// <summary>
    /// 使用指定的文件名将 excel 内容另存为文件, 适用于先使用 Export 方法生成了 excel 文件内容, 在将文件内容另存为文件
    /// </summary>
    /// <param name="fileName">excel 文件名</param>
    /// <returns></returns>
    Task SaveAsAsync(string fileName);

    /// <summary>
    /// 在链式循环导出时, 最后一个直接导出为文件的方法
    /// <param name="fileName">导出的 Excel 文件名, 如果为空, 则使用最后一个 ExcelData 的文件名</param>
    /// </summary>
    /// <returns></returns>
    Task<IActionResult> ExportAsync(string? fileName = null);
    /// <summary>
    /// 在链式循环导出时, 使用单个数据集作为最后一个 excel sheet, 直接导出为文件的方法
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="excelData">excel 数据对象</param>
    /// <returns></returns>
    Task<IActionResult> ExportAsync<TEntitySet>(ExcelData<TEntitySet> excelData);
    /// <summary>
    /// 在链式循环导出时, 使用单个数据集作为最后一个 excel sheet, 直接导出为文件的方法
    /// 此方法会按默认参数构造 ExcelData 并导出
    /// 需要 Data(必须), Columns(非必须) 和 SheetName(非必须) 参数
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="Data">要导出的数据集合</param>
    Task<IActionResult> ExportAsync<TEntitySet>(IEnumerable<TEntitySet> Data);
}