﻿using System.Globalization;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace Devonline.AspNetCore;

/// <summary>
/// excel 导入操作的服务
/// </summary>
/// <remarks>
/// 构造函数
/// </remarks>
/// <param name="logger"></param>
/// <param name="httpSetting"></param>
public class ExcelImportService(ILogger<ExcelImportService> logger, HttpSetting httpSetting) : IExcelImportService
{
    protected readonly ILogger<ExcelImportService> _logger = logger;
    protected readonly HttpSetting _httpSetting = httpSetting;
    protected readonly ExcelPackage _excelPackage = new();

    /// <summary>
    /// 获取附件绝对地址
    /// </summary>
    /// <param name="fileName">附件文件名</param>
    /// <returns></returns>
    public virtual string GetAttachmentPath(string fileName) => Path.Combine((_httpSetting.Attachment?.RootPath ?? DEFAULT_ATTACHMENT_PATH).GetAbsolutePath(), fileName);
    /// <summary>
    /// 从 excel 导入数据
    /// 需要 FileName(必须), SheetIndex(非必须) 和 SheetName(非必须) 参数
    /// 导入结果保存在 ExcelData 的 Data 字段, 错误信息保存在 Messages 字段
    /// get data, excel row and column index from 1
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="excelData">excel 数据对象</param>
    public virtual ExcelData<TEntitySet> Import<TEntitySet>(ExcelData<TEntitySet> excelData) where TEntitySet : class, new()
    {
        if (string.IsNullOrWhiteSpace(excelData.FileName))
        {
            throw new BadHttpRequestException("excel 文件无法导入, 未提供要导入的文件名!");
        }

        var fileName = GetAttachmentPath(excelData.FileName);
        if (!File.Exists(fileName))
        {
            throw new BadHttpRequestException("文件不存在!");
        }

        var file = new FileInfo(fileName);
        if (file.Extension.ToLowerInvariant() != DEFAULT_EXCEL_FILE_EXTENSION)
        {
            throw new BadHttpRequestException($"只能导入 {DEFAULT_EXCEL_FILE_EXTENSION} 类型的 Excel 文件!");
        }

        using var stream = file.OpenRead();
        _excelPackage.Load(stream);
        if (_excelPackage == null || _excelPackage.Workbook == null || _excelPackage.Workbook.Worksheets == null || _excelPackage.Workbook.Worksheets.Count == UNIT_ZERO)
        {
            throw new BadHttpRequestException("excel 文件读取失败, 不能读取空的 excel 文件!");
        }

        // read sheet
        if (excelData.SheetIndex >= 0 && excelData.SheetIndex >= _excelPackage.Workbook.Worksheets.Count)
        {
            throw new BadHttpRequestException($"excel 文件读取失败, excel 文件不存在第 {excelData.SheetIndex} 页!");
        }

        using var sheet = string.IsNullOrWhiteSpace(excelData.SheetName) ? _excelPackage.Workbook.Worksheets[excelData.SheetIndex] : _excelPackage.Workbook.Worksheets[excelData.SheetName];
        if (sheet == null)
        {
            throw new BadHttpRequestException($"excel 文件读取失败, 无法读取 excel 表格: {excelData.SheetName ?? excelData.SheetIndex.ToString(CultureInfo.CurrentCulture)} !");
        }

        excelData.GetAttributes();
        if (excelData.Attributes == null || (!excelData.Attributes.Any()))
        {
            throw new BadHttpRequestException($"excel 文件读取失败, 没有配置可以导入的列!");
        }

        if (sheet.Dimension.Columns <= UNIT_ZERO || sheet.Dimension.Rows <= UNIT_ZERO)
        {
            throw new BadHttpRequestException($"excel 文件读取失败, 文件中没有数据!");
        }

        excelData.FileName ??= _excelPackage.File.Name;
        excelData.SheetName ??= sheet.Name;
        var columns = ReadHeader(sheet, excelData);
        if ((excelData.Messages == null || (!excelData.Messages.Any())) && columns.Count > UNIT_ZERO && sheet.Dimension.Rows > UNIT_ONE)
        {
            ReadContent(sheet, excelData, columns);
        }

        return excelData;
    }
    /// <summary>
    /// 使用文件名构造默认 excel 数据对象进行导入
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="fileName">excel 文件名</param>
    public virtual ExcelData<TEntitySet> Import<TEntitySet>(string fileName) where TEntitySet : class, new()
    {
        var excelData = new ExcelData<TEntitySet> { FileName = fileName };
        Import(excelData);
        return excelData;
    }

    /// <summary>
    /// 读取 excel 列头, 返回读出来的列头
    /// </summary>
    /// <typeparam name="TEntitySet"></typeparam>
    /// <param name="sheet"></param>
    /// <returns></returns>
    protected virtual Dictionary<int, string> ReadHeader<TEntitySet>(in ExcelWorksheet sheet, ExcelData<TEntitySet> excelData)
    {
        var attributes = excelData.Attributes;
        var errors = new Dictionary<int, string>();
        var columns = new Dictionary<int, string>();
        for (int index = UNIT_ZERO; index < sheet.Dimension.Columns; index++)
        {
            var header = (sheet.Cells[UNIT_ONE, index + UNIT_ONE].GetValue<string>() ?? string.Empty).Trim();
            var attribute = attributes.FirstOrDefault(x => x.Name == header);
            if (attribute != null && attribute.Field != null)
            {
                columns.Add(index, attribute.Field.Name);
            }
        }

        var columnIndex = sheet.Dimension.Columns + 1;
        foreach (var attribute in attributes.Where(x => x.Required).ToList())
        {
            if (attribute.Field != null && !columns.ContainsValue(attribute.Field.Name))
            {
                errors.Add(columnIndex++, $"缺少必须导入的 <strong>{attribute.Name}</strong> 列!");
            }
        }

        excelData.Messages = errors;
        return columns;
    }
    /// <summary>
    /// 读取 excel 的内容
    /// </summary>
    /// <typeparam name="TEntitySet"></typeparam>
    /// <param name="sheet"></param>
    /// <param name="excelData"></param>
    /// <param name="columns"></param>
    protected virtual void ReadContent<TEntitySet>(in ExcelWorksheet sheet, ExcelData<TEntitySet> excelData, Dictionary<int, string> columns) where TEntitySet : class, new()
    {
        // get data, excel row and column index from 1
        var list = new List<TEntitySet>();
        var errors = new Dictionary<int, string> { };
        var attributes = excelData.Attributes;
        for (int row = UNIT_TWO; row <= sheet.Dimension.Rows; row++)
        {
            // read value
            var t = new TEntitySet();
            var messages = new List<string>();
            var nullValueCount = 0;
            for (int col = UNIT_ONE; col <= sheet.Dimension.Columns; col++)
            {
                if (!columns.ContainsKey(col - UNIT_ONE))
                {
                    continue;
                }

                var field = columns[col - UNIT_ONE];
                var attribute = attributes.FirstOrDefault(x => x.Field?.Name == field);
                if (attribute != null && attribute.Field != null)
                {
                    try
                    {
                        object? value;
                        var cell = sheet.Cells[row, col];
                        var propertyType = attribute.Field.PropertyType;

                        //TODO 对于字符串类型的可空类型尚待验证
                        if (propertyType != typeof(string) && propertyType.IsNullable())
                        {
                            propertyType = propertyType.GetCoreType();
                        }

                        try
                        {
                            cell.InvokeGenericMethod(nameof(cell.GetValue), null, out value, propertyType);
                        }
                        catch (Exception)
                        {
                            value = cell.Value;
                        }

                        if (attribute.Required && value == null)
                        {
                            throw new BadHttpRequestException("不能为空!");
                        }

                        if (value != null)
                        {
                            if (propertyType == typeof(string) && value is string stringValue && attribute.MaxLength > 0 && stringValue.Length > attribute.MaxLength)
                            {
                                throw new BadHttpRequestException($"输入超过最大长度 {attribute.MaxLength} !");
                            }

                            if (propertyType != value.GetType().GetCoreType())
                            {
                                value = GetPropertyValue(attribute.Field, value);
                            }
                        }

                        if (value != null)
                        {
                            // excel cell delegate method call
                            if ((!string.IsNullOrWhiteSpace(attribute.Field.Name)) && excelData.Converters != null && excelData.Converters.ContainsKey(attribute.Field.Name))
                            {
                                //自定义设置方式, 导入时应通过converter 方法直接设置 t 的值
                                excelData.Converters[attribute.Field.Name]?.Invoke(t, value);
                            }
                            else
                            {
                                //默认设置方式
                                attribute.Field.SetValue(t, (value is IConvertible) ? Convert.ChangeType(value, propertyType, CultureInfo.CurrentCulture) : value);
                            }
                        }

                        if (value is null)
                        {
                            nullValueCount++;
                        }
                    }
                    catch (Exception ex)
                    {
                        messages.Add($"<strong>{attribute.Name}</strong> {ex.Message}");
                    }
                }
            }

            if (nullValueCount == sheet.Dimension.Columns)
            {
                //跳过空行
                continue;
            }

            // check repeat
            if (list.Contains(t, excelData.Comparer))
            {
                messages.Add("与其他行重复!");
            }

            try
            {
                // check for whole row
                excelData.Validator?.Invoke(t, list);
            }
            catch (Exception ex)
            {
                messages.Add(ex.Message);
            }

            list.Add(t);

            // process error message
            if (messages.Count != 0)
            {
                if (errors.Count == 0)
                {
                    errors.Add(0, sheet.Name);
                }

                errors.Add(row, string.Join(DEFAULT_SECTION_STRING, messages));
            }
        }

        excelData.Messages = errors;
        excelData.Data = list;
    }
    /// <summary>
    /// 用导入类型得到原始数据类型
    /// </summary>
    /// <param name="propertyInfo"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    protected virtual object? GetPropertyValue(PropertyInfo propertyInfo, object value)
    {
        var strValue = (value.ToString() ?? string.Empty).Trim();
        var result = (value is string) ? strValue : value;
        switch (Type.GetTypeCode(propertyInfo.PropertyType))
        {
            case TypeCode.Boolean:
                if (value is not bool)
                {
                    result = (strValue == "是" || strValue == "否") ? (strValue == "是") : throw new BadHttpRequestException("只能填\"是\"或\"否\"!");
                }
                break;
            case TypeCode.DateTime:
                if (value is double doubleValue)
                {
                    result = DateTime.FromOADate(doubleValue);
                }
                break;
            default:
                if (propertyInfo.PropertyType.IsEnum && (!string.IsNullOrWhiteSpace(strValue)))
                {
                    result = strValue.GetEnumValueByDisplayName(propertyInfo.PropertyType);
                }

                break;
        }

        return result;
    }
}