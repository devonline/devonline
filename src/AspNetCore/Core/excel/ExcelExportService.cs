﻿using System.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace Devonline.AspNetCore;

/// <summary>
/// excel 导出操作的服务
/// TODO 暂不支持嵌套对象内部字段导出
/// </summary>
/// <remarks>
/// 构造函数
/// </remarks>
/// <param name="logger"></param>
public class ExcelExportService(ILogger<ExcelExportService> logger) : IExcelExportService
{
    private string _fileName = string.Empty;
    protected readonly ILogger<ExcelExportService> _logger = logger;
    protected readonly ExcelPackage _excelPackage = new();

    /// <summary>
    /// 导出数据到 excel
    /// 需要 Data(必须), Columns(非必须) SheetName(非必须) 和 Converters(非必须) 参数
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="excelData">excel 数据对象</param>
    public virtual IExcelExportService Export<TEntitySet>(ExcelData<TEntitySet> excelData)
    {
        if (excelData.Data == null || (!excelData.Data.Any()))
        {
            throw new BadHttpRequestException($"excel 文件导出失败, 没有提供导出的数据!");
        }

        var attributes = excelData.GetAttributes();
        if (attributes == null || (!attributes.Any()))
        {
            throw new BadHttpRequestException($"excel 文件导出失败, 没有配置可以导出的列!");
        }

        //转换原数据
        var typeName = typeof(TEntitySet).GetDisplayName();
        excelData.SheetName ??= typeName;
        var sheet = _excelPackage.Workbook.Worksheets.Add(excelData.SheetName);
        sheet.Cells.Style.ShrinkToFit = true;

        _logger.LogDebug("export {typeName} to fill data", typeName);
        var dataTable = ConvertData(excelData);
        var members = attributes.OrderBy(x => x.Index).Select(x => x.Field).ToArray();
        sheet.Cells.LoadFromDataTable(dataTable, true, TableStyles.None);

        //格式化列
        _logger.LogDebug("export {typeName} to format columns", typeName);
        FormatColumns(excelData, sheet);

        //设置样式
        _logger.LogDebug("export {typeName} to set styles", typeName);
        SetStyles(sheet);

        //导出的文件名
        _fileName = string.IsNullOrWhiteSpace(excelData.FileName) ? excelData.SheetName + DEFAULT_EXCEL_FILE_EXTENSION : Path.GetFileName(excelData.FileName);
        _logger.LogInformation($"The user will export {typeName} to file {_fileName}");

        return this;
    }
    /// <summary>
    /// 导出数据到 excel, 此方法会按默认参数构造 ExcelData 并导出
    /// 需要 Data(必须), Columns(非必须) 和 SheetName(非必须) 参数
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="Data">要导出的数据集合</param>
    public virtual IExcelExportService Export<TEntitySet>(IEnumerable<TEntitySet> Data) => Export(new ExcelData<TEntitySet> { Data = Data });

    /// <summary>
    /// 获取导出处理过程中存在于内存中的 excel 文件数据流
    /// </summary>
    /// <returns></returns>
    public virtual async Task<byte[]> GetContentsAsync()
    {
        var bytes = await _excelPackage.GetAsByteArrayAsync().ConfigureAwait(false);
        _excelPackage.Dispose();
        return bytes;
    }
    /// <summary>
    /// 使用指定的文件名将 excel 内容另存为文件, 适用于先使用 Export 方法生成了 excel 文件内容, 在将文件内容另存为文件
    /// </summary>
    /// <param name="fileName">excel 文件名</param>
    /// <returns></returns>
    public virtual async Task SaveAsAsync(string fileName)
    {
        var data = await GetContentsAsync();
        if (data.Length >= 0)
        {
            await File.WriteAllBytesAsync(fileName, data);
        }
    }

    /// <summary>
    /// 在链式循环导出时, 最后一个直接导出为文件的方法
    /// <param name="fileName">导出的 Excel 文件名, 如果为空, 则使用最后一个 ExcelData 的文件名</param>
    /// </summary>
    /// <returns></returns>
    public virtual async Task<IActionResult> ExportAsync(string? fileName = null) => new FileContentResult(await GetContentsAsync(), CONTENT_TYPE_OF_EXCEL) { FileDownloadName = fileName ?? _fileName };
    /// <summary>
    /// 单个数据集直接导出到一个 excel 文件, 并直接返回
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="excelData">excel 数据对象</param>
    /// <returns></returns>
    public virtual async Task<IActionResult> ExportAsync<TEntitySet>(ExcelData<TEntitySet> excelData) => await Export(excelData).ExportAsync();
    /// <summary>
    /// 导出数据到 excel, 此方法会按默认参数构造 ExcelData 并导出
    /// 需要 Data(必须), Columns(非必须) 和 SheetName(非必须) 参数
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="Data">要导出的数据集合</param>
    public virtual async Task<IActionResult> ExportAsync<TEntitySet>(IEnumerable<TEntitySet> Data) => await ExportAsync(new ExcelData<TEntitySet> { Data = Data });

    /// <summary>
    /// 在存在 Converters 的情况下对即将导出的值进行转换
    /// </summary>
    /// <typeparam name="TEntitySet"></typeparam>
    /// <param name="excelData"></param>
    /// <returns>转换结果</returns>
    protected virtual DataTable ConvertData<TEntitySet>(ExcelData<TEntitySet> excelData)
    {
        var dataTable = new DataTable(typeof(TEntitySet).Name + CHAR_UNDERLINE + KeyGenerator.GetKey().ToString());
        if (excelData.Data != null && excelData.Data.Any() && excelData.Columns != null && excelData.Columns.Any())
        {
            //columns
            foreach (var column in excelData.Columns)
            {
                var attribute = excelData.Attributes.FirstOrDefault(x => x.Field?.Name == column);
                if (attribute != null && attribute.ExportType != null)
                {
                    dataTable.Columns.Add(attribute.Name, attribute.ExportType);
                }
            }

            //rows
            var type = typeof(TEntitySet);
            foreach (var entity in excelData.Data)
            {
                var row = dataTable.NewRow();
                foreach (var column in excelData.Columns)
                {
                    var attribute = excelData.Attributes.FirstOrDefault(x => x.Field?.Name == column);
                    if (attribute != null && attribute.Field != null && (!string.IsNullOrWhiteSpace(attribute.Name)))
                    {
                        object? value = attribute.Field.GetValue(entity);
                        if (excelData.Converters?.ContainsKey(column) ?? false)
                        {
                            value = excelData.Converters[column].Invoke(entity, value);
                        }
                        else
                        {
                            //没有 Converters 的情况下, 对 bool 类型和 enum 类型进行默认转换
                            var propertyType = attribute.Field.PropertyType.GetCoreType();
                            if (propertyType == typeof(bool) && value is bool boolean)
                            {
                                value = boolean ? "是" : "否";
                            }
                            else if (value != null && propertyType.IsEnum)
                            {
                                value = ((Enum)value).GetDisplayName();
                            }
                        }

                        row.SetField(attribute.Name, value);
                    }
                }

                dataTable.Rows.Add(row);
            }
        }

        return dataTable;
    }
    /// <summary>
    /// 格式化列
    /// </summary>
    /// <typeparam name="TEntitySet"></typeparam>
    /// <param name="excelData"></param>
    protected virtual void FormatColumns<TEntitySet>(ExcelData<TEntitySet> excelData, in ExcelWorksheet sheet)
    {
        var rowCount = (excelData.Data?.Count() ?? UNIT_ZERO) + UNIT_ONE;
        foreach (var attribute in excelData.Attributes)
        {
            try
            {
                //set width
                sheet.Column(attribute.Index).Width = attribute.Size;

                //set format
                if (rowCount > UNIT_ONE && (!string.IsNullOrWhiteSpace(attribute.Format)))
                {
                    sheet.Cells[UNIT_TWO, attribute.Index, rowCount, attribute.Index].Style.Numberformat.Format = attribute.Format;
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, $"Excel sheet {excelData.SheetName} column {attribute.Name} set failed!");
            }
        }
    }

    /// <summary>
    /// 为导出的 excel 设置默认的样式
    /// </summary>
    /// <param name="sheet"></param>
    protected virtual void SetStyles(in ExcelWorksheet sheet)
    {
        //TODO TBC
    }
}