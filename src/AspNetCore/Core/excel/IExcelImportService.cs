﻿namespace Devonline.AspNetCore;

/// <summary>
/// excel 导入操作的服务
/// </summary>
public interface IExcelImportService
{
    /// <summary>
    /// 获取附件绝对地址
    /// </summary>
    /// <param name="fileName">附件文件名</param>
    /// <returns></returns>
    string GetAttachmentPath(string fileName);
    /// <summary>
    /// 从 excel 导入数据
    /// 需要 FileName(必须), SheetIndex(非必须) 和 SheetName(非必须) 参数
    /// 导入结果保存在 ExcelData 的 Data 字段, 错误信息保存在 Messages 字段
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="excelData">excel 数据对象</param>
    ExcelData<TEntitySet> Import<TEntitySet>(ExcelData<TEntitySet> excelData) where TEntitySet : class, new();
    /// <summary>
    /// 需要 FileName(必须), SheetIndex(非必须) 和 SheetName(非必须) 参数
    /// </summary>
    /// <typeparam name="TEntitySet">业务数据类型</typeparam>
    /// <param name="fileName">excel 文件名</param>
    ExcelData<TEntitySet> Import<TEntitySet>(string fileName) where TEntitySet : class, new();
}