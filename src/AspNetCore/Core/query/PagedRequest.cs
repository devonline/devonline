﻿namespace Devonline.AspNetCore;

/// <summary>
/// http request 基础分页查询请求参数
/// </summary>
public class PagedRequest
{
    public PagedRequest()
    {
        PageIndex = UNIT_ONE;
        PageSize = UNIT_TEN;
    }

    /// <summary>
    /// 当前页码
    /// </summary>
    public virtual int PageIndex { get; set; }
    /// <summary>
    /// 页大小
    /// </summary>
    public virtual int PageSize { get; set; }
    /// <summary>
    /// 查询结果总数目
    /// </summary>
    public virtual bool? Total { get; set; }
    /// <summary>
    /// 查询条件
    /// </summary>
    public virtual string? Filter { get; set; }
    /// <summary>
    /// 排序条件
    /// </summary>
    public virtual string? Orderby { get; set; }
}

/// <summary>
/// 包含时间范围条件的分页查询请求
/// </summary>
public class TimeRangePagedRequest : PagedRequest
{
    public virtual DateTime? StartTime { get; set; }
    public virtual DateTime? EndTime { get; set; }
}