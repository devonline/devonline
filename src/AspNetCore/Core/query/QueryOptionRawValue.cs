﻿using System.Text.Json.Serialization;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace Devonline.AspNetCore;

/// <summary>
/// 字符串表达式的原始查询选项
/// </summary>
public class QueryOptionRawValue : PagedOptions
{
    /// <summary>]
    /// 默认的构造函数
    /// </summary>
    public QueryOptionRawValue() { }
    /// <summary>
    /// 使用 url 字符串表达式的构造函数
    /// TODO, 此处应使用 string
    /// </summary>
    /// <param name="rawValue"></param>
    public QueryOptionRawValue(string rawValue)
    {
        Init(rawValue.ToKeyValuePairs());
    }
    /// <summary>
    /// 接受 request query 作为参数的方式
    /// </summary>
    /// <param name="keyValuePairs"></param>
    public QueryOptionRawValue(IEnumerable<KeyValuePair<string, StringValues>> requestQuery)
    {
        Init(requestQuery.ToDictionary(x => x.Key, x => x.Value.ToString()));
    }

    /// <summary>
    /// 查询条件
    /// </summary>
    [JsonProperty("$filter")]
    [JsonPropertyName("$filter")]
    public string? Filter { get; set; }
    /// <summary>
    /// 排序条件
    /// </summary>
    [JsonProperty("$orderby")]
    [JsonPropertyName("$orderby")]
    public string? Orderby { get; set; }
    /// <summary>
    /// 列筛选条件
    /// </summary>
    [JsonProperty("$select")]
    [JsonPropertyName("$select")]
    public string? Select { get; set; }
    /// <summary>
    /// 扩展选项
    /// TODO TBD 暂不实现
    /// </summary>
    [JsonProperty("$expand")]
    [JsonPropertyName("$expand")]
    public string? Expand { get; set; }

    /// <summary>
    /// init query options
    /// </summary>
    /// <param name="keyValuePairs"></param>
    private void Init(IEnumerable<KeyValuePair<string, string>> keyValuePairs)
    {
        if (keyValuePairs.IsNotNullOrEmpty())
        {
            var type = GetType();

            //count 选项
            var key = type.GetJsonPropertyName(nameof(Count));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Count = Convert.ToBoolean(keyValue.Value);
            }

            //top 选项
            key = type.GetJsonPropertyName(nameof(Top));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Top = Convert.ToInt32(keyValue.Value);
            }

            //skip 选项
            key = type.GetJsonPropertyName(nameof(Skip));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Skip = Convert.ToInt32(keyValue.Value);
            }

            //filter 选项
            key = type.GetJsonPropertyName(nameof(Filter));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Filter = keyValue.Value.ToString();
            }

            //orderby 选项
            key = type.GetJsonPropertyName(nameof(Orderby));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Orderby = keyValue.Value.ToString();
            }

            //select 选项
            key = type.GetJsonPropertyName(nameof(Select));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Select = keyValue.Value.ToString();
            }

            //expand 选项
            key = type.GetJsonPropertyName(nameof(Expand));
            if (keyValuePairs.Any(x => x.Key == key))
            {
                var keyValue = keyValuePairs.FirstOrDefault(x => x.Key == key);
                Expand = keyValue.Value.ToString();
            }
        }
    }
}