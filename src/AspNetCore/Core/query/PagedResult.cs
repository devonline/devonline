﻿using System.Collections;

namespace Devonline.AspNetCore;

/// <summary>
/// 适用于显式的分页参数查询的返回值
/// 未从 IEnumerable 接口实现, 用于 http 接口的返回值
/// 因为 http 接口只会返回 IEnumerable 的内容
/// </summary>
public class PagedResult
{
    public PagedResult()
    {
        PageIndex = UNIT_ONE;
        PageSize = UNIT_TEN;
    }

    /// <summary>
    /// 查询结果总数目
    /// </summary>
    public virtual int Total { get; set; }
    /// <summary>
    /// 当前页码
    /// </summary>
    public virtual int PageIndex { get; set; }
    /// <summary>
    /// 页大小
    /// </summary>
    public virtual int PageSize { get; set; }
    /// <summary>
    /// 当前页数据集合
    /// </summary>
    public virtual IEnumerable? Data { get; set; }
}

/// <summary>
/// 适用于显式的分页参数查询的返回值
/// 从 IEnumerable<> 接口实现, 适用于除 http 接口返回值以外的其他场景
/// </summary>
public class PagedResult<T>
{
    public PagedResult()
    {
        PageIndex = UNIT_ONE;
        PageSize = UNIT_TEN;
    }

    /// <summary>
    /// 查询结果总数目
    /// </summary>
    public virtual int Total { get; set; }
    /// <summary>
    /// 当前页码
    /// </summary>
    public virtual int PageIndex { get; set; }
    /// <summary>
    /// 页大小
    /// </summary>
    public virtual int PageSize { get; set; }
    /// <summary>
    /// 当前页数据集合
    /// </summary>
    public virtual IEnumerable<T>? Data { get; set; }
}