﻿using System.Net;

namespace Devonline.AspNetCore;

/// <summary>
/// http Upload result
/// </summary>
public class UploadResult : UploadResult<string> { }

/// <summary>
/// http Upload result
/// </summary>
public class UploadResult<TKey> : UploadResult<Attachment<TKey>, TKey> where TKey : IConvertible { }

/// <summary>
/// http Upload result
/// </summary>
public class UploadResult<TAttachment, TKey> where TAttachment : class, IAttachment<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 状态码
    /// </summary>
    public HttpStatusCode StatusCode { get; set; }
    /// <summary>
    /// 文件名
    /// </summary>
    public string? FileName { get; set; }
    /// <summary>
    /// 上传结果, 状态码为 200 的, result 返回服务器文件路径, 否则返回失败信息
    /// </summary>
    public string? Result { get; set; }
    /// <summary>
    /// 附件
    /// </summary>
    public TAttachment? Attachment { get; set; }
}