﻿using System.Collections;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.AspNetCore;

/// <summary>
/// 适用于显式的 odata 参数查询的返回值
/// </summary>
public class ODataResult
{
    [JsonProperty("@odata.context")]
    [JsonPropertyName("@odata.context")]
    public string? Context { get; set; }
    [JsonProperty("@odata.count")]
    [JsonPropertyName("@odata.count")]
    public int Count { get; set; }
    [JsonProperty("value")]
    [JsonPropertyName("value")]
    public IEnumerable? Value { get; set; }

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator PagedResult(ODataResult result) => new() { PageIndex = 1, Total = result.Count, Data = result.Value };

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator ODataResult(PagedResult result) => new() { Value = result.Data, Count = result.Total };
}

/// <summary>
/// 适用于显式的 odata 参数查询的返回值
/// </summary>
public class ODataResult<T>
{
    [JsonProperty("@odata.context")]
    [JsonPropertyName("@odata.context")]
    public string? Context { get; set; }
    [JsonProperty("@odata.count")]
    [JsonPropertyName("@odata.count")]
    public int Count { get; set; }
    [JsonProperty("value")]
    [JsonPropertyName("value")]
    public IEnumerable<T>? Value { get; set; }

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator PagedResult<T>(ODataResult<T> result) => new() { PageIndex = 1, Total = result.Count, Data = result.Value };

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator ODataResult<T>(PagedResult<T> result) => new() { Value = result.Data, Count = result.Total };
}