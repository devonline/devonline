﻿using System.Linq.Dynamic.Core;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore;

/// <summary>
/// 业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public class DataWithCollectionService<TDbContext, TEntitySet, TElement, TKey> :
    DataService<TDbContext, TEntitySet, TKey>,
    IDataWithCollectionService<TDbContext, TEntitySet, TElement, TKey>,
    IDataService<TDbContext, TEntitySet, TKey>,
    IDataCollectionService<TDbContext, TEntitySet, TElement, TKey>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TElement : class, IEntitySet<TKey>
    where TKey : IConvertible
{
    protected readonly Type _elementType;
    protected readonly DbSet<TElement> _dbSetElement;
    public DataWithCollectionService(
        ILogger<DataWithCollectionService<TDbContext, TEntitySet, TElement, TKey>> logger,
        TDbContext context,
        IDistributedCache cache,
        IHttpContextAccessor httpContextAccessor,
        HttpSetting httpSetting
        ) : base(logger, context, cache, httpContextAccessor, httpSetting)
    {
        _elementType = typeof(TElement);
        _dbSetElement = _context.Set<TElement>();
    }

    #region 实现自 IDataWithCollectionService 接口的方法
    /// <summary>
    /// 新增业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="entitySet">业务数据</param>
    /// <param name="elements">引用数据集合</param>
    /// <param name="foreignKey">外键字段名</param>
    /// <returns></returns>
    public virtual async Task AddCollectionAsync(TEntitySet entitySet, IEnumerable<TElement>? elements = default, string? foreignKey = default)
    {
        elements ??= entitySet.GetCollectionMember<TEntitySet, TElement>();
        foreignKey ??= _elementType.GetForeignKey<TEntitySet, TKey>();
        if (elements is null || !elements.Any() || string.IsNullOrWhiteSpace(foreignKey))
        {
            return;
        }

        var propertyInfo = _elementType.GetProperty(foreignKey);
        if (propertyInfo is null)
        {
            return;
        }

        foreach (var element in elements)
        {
            Create(element);
            Update(element);
            propertyInfo.SetValue(element, entitySet.Id);
            await _dbSetElement.AddAsync(element);
        }
    }
    /// <summary>
    /// 更新业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="entitySet">业务数据</param>
    /// <param name="elements">引用数据集合</param>
    /// <param name="foreignKey">外键字段名</param>
    /// <param name="isLogical">是否逻辑操作, 默认不是</param>
    /// <returns></returns>
    public virtual async Task UpdateCollectionAsync(TEntitySet entitySet, IEnumerable<TElement>? elements = default, string? foreignKey = default, bool isLogical = false)
    {
        var news = elements ??= entitySet.GetCollectionMember<TEntitySet, TElement>() ?? [];
        foreignKey ??= _elementType.GetForeignKey<TEntitySet, TKey>();
        if (!news.Any() || string.IsNullOrWhiteSpace(foreignKey))
        {
            return;
        }

        var olds = await _dbSetElement.Where($"{foreignKey} == @0", entitySet.Id).ToListAsync();
        if (!news.Any() && olds.Count == 0)
        {
            return;
        }

        var propertyInfo = _elementType.GetProperty(foreignKey);
        if (propertyInfo == null)
        {
            return;
        }

        //TODO TBD, 此处的 AdapterConfig 使用了基类的方法, 并没有办法针对类型做出具体设置, 只是一个通用基类适配方法
        var adapterConfig = GetAdapterConfig<TElement>();
        foreach (var entity in news)
        {
            var old = olds.FirstOrDefault(x => x.Id.Equals(entity.Id));
            if (old == null)
            {
                //新的有旧的没有, 则新增
                Create(entity);
                Update(entity);
                propertyInfo.SetValue(entity, entitySet.Id);
                await _dbSetElement.AddAsync(entity);
            }
            else
            {
                //新的有旧的也有, 则更新
                //先对原数据进行逻辑更新
                Update(old, isLogical);
                entity.Adapt(old, adapterConfig);
                Update(old);
                _dbSetElement.Update(old);
            }
        }

        foreach (var old in olds)
        {
            if (!news.Any(x => x.Id.Equals(old.Id)))
            {
                //旧的有新的没有, 则删除
                Delete(old, isLogical);
            }
        }
    }
    /// <summary>
    /// 删除业务数据对象中 TElement 类型的集合对象
    /// </summary>
    /// <param name="id">业务数据主键</param>
    /// <param name="foreignKey">外键字段名</param>
    /// <param name="isLogical">是否逻辑操作, 默认不是</param>
    /// <returns></returns>
    public virtual async Task DeleteCollectionAsync(TKey id, string? foreignKey = default, bool isLogical = false)
    {
        foreignKey ??= _elementType.GetForeignKey<TEntitySet, TKey>();
        if (string.IsNullOrWhiteSpace(foreignKey))
        {
            return;
        }

        var elements = await _dbSetElement.Where($"{foreignKey} == @0", id).ToListAsync();
        if (elements == null || elements.Count == 0)
        {
            return;
        }

        foreach (var element in elements)
        {
            Delete(element, isLogical);
        }
    }
    #endregion

    #region 重载自 DataService 内部方法
    /// <summary>   
    /// 重载的新增方法
    /// 当前设置限定每种类型的集合在业务数据中只能有一个, 多于一个的时候, 无法自动获取对应的外键值, 因此只能通过 context 中的属性值和外键对应关系实现, 此时 context 为必填项
    /// </summary>
    /// <param name="entitySet">业务数据</param>
    /// <param name="context">数据操作上下文</param>
    /// <returns></returns>
    protected override async Task InternalAddAsync(TEntitySet entitySet, DataServiceContext? context = null)
    {
        _logger.LogInformation($"The user {UserName} will add {_typeName} and it's related collection elements, the content is: {entitySet.ToJsonString()}");

        await base.InternalAddAsync(entitySet, context);
        var elements = entitySet.GetCollectionMembers<TEntitySet, TElement>();
        var foreignKeys = _elementType.GetForeignKeys<TEntitySet, TKey>();
        foreach (var element in elements)
        {
            var foreignKey = (context?.ForeignKeys?.ContainsKey(element.Key) ?? false) ? context.ForeignKeys[element.Key] : foreignKeys.FirstOrDefault(x => x.Key == element.Key).Value;
            await AddCollectionAsync(entitySet, element.Value, foreignKey);
        }
    }
    /// <summary>
    /// 重载的更新方法
    /// 当前设置限定每种类型的集合在业务数据中只能有一个, 多于一个的时候, 无法自动获取对应的外键值, 因此只能通过 context 中的属性值和外键对应关系实现, 此时 context 为必填项
    /// </summary>
    /// <param name="entitySet">业务数据</param>
    /// <param name="context">数据操作上下文</param>
    /// <returns></returns>
    protected override async Task InternalUpdateAsync(TEntitySet entitySet, DataServiceContext? context = null)
    {
        var isLogical = context?.IsLogical ?? false;
        _logger.LogInformation($"The user {UserName} will {GetLogicalString(isLogical)} update {_typeName} and it's related collection elements, the content is: {entitySet.ToJsonString()}");

        await base.InternalUpdateAsync(entitySet, context);

        //TODO TBC 在主数据对象中出现多个同类型集合时, 此处可能会有问题
        var elements = entitySet.GetCollectionMembers<TEntitySet, TElement>();
        var foreignKeys = _elementType.GetForeignKeys<TEntitySet, TKey>();
        foreach (var element in elements)
        {
            var foreignKey = (context?.ForeignKeys?.ContainsKey(element.Key) ?? false) ? context.ForeignKeys[element.Key] : foreignKeys.FirstOrDefault(x => x.Key == element.Key).Value;
            await UpdateCollectionAsync(entitySet, element.Value, foreignKey, isLogical);
        }
    }
    /// <summary>
    /// 重载的删除方法会同时将当前业务数据引用的数据集合一并删除
    /// </summary>
    /// <param name="id">业务数据主键</param>
    /// <param name="context">数据操作上下文</param>
    /// <returns></returns>
    protected override async Task InternalDeleteAsync(TKey id, DataServiceContext? context = null)
    {
        var isLogical = context?.IsLogical ?? false;
        _logger.LogInformation($"The user {UserName} will {GetLogicalString(isLogical)} delete {_typeName} and it's related collection elements, the content id is: {id}");

        await base.InternalDeleteAsync(id, context);

        var foreignKeys = context?.ForeignKeys?.Values ?? _elementType.GetForeignKeys<TEntitySet, TKey>().Values;
        foreach (var foreignKey in foreignKeys)
        {
            await DeleteCollectionAsync(id, foreignKey, isLogical);
        }
    }
    #endregion
}

/// <summary>
/// 字符串作为主键的业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public class DataWithCollectionService<TDbContext, TEntitySet, TElement>(
    ILogger<DataWithCollectionService<TDbContext, TEntitySet, TElement>> logger,
    TDbContext context,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    HttpSetting httpSetting) :
    DataWithCollectionService<TDbContext, TEntitySet, TElement, string>(logger, context, cache, httpContextAccessor, httpSetting),
    IDataWithCollectionService<TDbContext, TEntitySet, TElement>,
    IDataService<TDbContext, TEntitySet>
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet, new()
    where TElement : class, IEntitySet;