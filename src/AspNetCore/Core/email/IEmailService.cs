﻿namespace Devonline.AspNetCore;

/// <summary>
/// Smtp 邮件发送服务
/// </summary>
/// <typeparam name="TEmail">邮件数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IEmailService<TEmail, TKey> : IDisposable where TEmail : Email<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 发送邮件
    /// </summary>
    /// <param name="mail">邮件对象</param>
    void Send(TEmail mail);
    /// <summary>
    /// 发送邮件
    /// </summary>
    /// <param name="mail">邮件对象</param>
    /// <param name="userToken">用户令牌</param>
    void SendAsync(TEmail mail, object? userToken);
    /// <summary>
    /// 发送邮件
    /// </summary>
    /// <param name="mail">邮件对象</param>
    Task SendAsync(TEmail mail);
    /// <summary>
    /// 发送邮件
    /// </summary>
    /// <param name="mail">邮件对象</param>
    /// <param name="cancellationToken">取消执行令牌</param>
    Task SendAsync(TEmail mail, CancellationToken cancellationToken);
}

/// <summary>
/// 发送邮件
/// </summary>
public interface IEmailService : IEmailService<Email, string> { }