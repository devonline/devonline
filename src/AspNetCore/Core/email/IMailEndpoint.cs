﻿namespace Devonline.AspNetCore;

/// <summary>
/// 邮件终结点设置
/// </summary>
public interface IMailEndpoint : IAuthEndpoint
{
    /// <summary>
    /// 发件人/发件地址
    /// </summary>
    string? From { get; set; }
    /// <summary>
    /// 是否需要认证
    /// </summary>
    bool UseCredentials { get; set; }
}