﻿namespace Devonline.AspNetCore;

/// <summary>
/// SMTP 邮件终结点设置
/// </summary>
public class SmtpMailEndpoint : AuthEndpoint, IMailEndpoint
{
    public SmtpMailEndpoint()
    {
        Port = 25;
        UseCredentials = true;
    }

    /// <summary>
    /// 发件人/发件地址
    /// </summary>
    public string? From { get; set; }
    /// <summary>
    /// 是否需要认证
    /// </summary>
    public bool UseCredentials { get; set; }
}