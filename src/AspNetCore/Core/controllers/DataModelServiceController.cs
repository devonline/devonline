﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore;

/// <summary>
/// 基于 DataService 的数据及视图模型增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class DataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>(
    ILogger<DataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>> logger,
    IDataService<TDbContext, TEntitySet, TKey> dataService) :
    ControllerBase
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>
    where TViewModel : class, IViewModel<TKey>
    where TKey : IConvertible
{
    protected readonly ILogger<DataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>> _logger = logger;
    protected readonly IDataService<TDbContext, TEntitySet, TKey> _dataService = dataService;

    /// <summary>
    /// 查询业务对象分页列表, 查询表达式会默认取出分页及过滤等参数
    /// </summary>
    /// <returns></returns>
    [HttpGet, DisplayName("查询"), AccessAuthorize(Code = "IGet")]
    public virtual async Task<IActionResult> GetAsync() => Ok(await _dataService.GetPagedResultAsync());
    /// <summary>
    /// 使用视图模型新增单个数据对象的 post 请求
    /// </summary>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    [HttpPost, DisplayName("新增"), AccessAuthorize(Code = "ICreate")]
    public virtual async Task<IActionResult> CreateAsync(TViewModel viewModel) => Ok(await _dataService.AddAsync(viewModel));
    /// <summary>
    /// 使用视图模型修改单个数据对象的 put 请求
    /// </summary>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    [HttpPut, DisplayName("更新"), AccessAuthorize(Code = "IUpdate")]
    public virtual async Task<IActionResult> UpdateAsync(TViewModel viewModel) => Ok(await _dataService.UpdateAsync(viewModel));
    /// <summary>
    /// 使用 id 删除单个数据对象的 delete 请求
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}"), DisplayName("删除"), AccessAuthorize(Code = "IDelete")]
    public virtual async Task<IActionResult> DeleteAsync(TKey id)
    {
        await _dataService.DeleteAsync(id);
        return Ok();
    }
}

/// <summary>
/// 基于字符串作为主键的 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
[Authorize]
[ApiController]
public abstract class DataModelServiceController<TDbContext, TEntitySet, TViewModel>(
    ILogger<DataModelServiceController<TDbContext, TEntitySet, TViewModel>> logger,
    IDataService<TDbContext, TEntitySet> dataService) :
    DataModelServiceController<TDbContext, TEntitySet, TViewModel, string>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet
    where TViewModel : class, IViewModel;