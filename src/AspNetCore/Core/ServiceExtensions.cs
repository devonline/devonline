﻿using Devonline.AspNetCore.Security;
using Devonline.Caching;
using Devonline.Security;
using Lazy.Captcha.Core;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace Devonline.AspNetCore;

public static class ServiceExtensions
{
    #region application builder extensions
    /// <summary>
    /// 使用 TSetting 类型获取配置文件 Application Setting 节点配置项, 并以单例形式添加到服务容器并返回
    /// </summary>
    /// <typeparam name="TSetting">Application Setting 配置项的类型名称</typeparam>
    /// <param name="builder">WebApplicationBuilder 实例</param>
    /// <param name="section">自定义设置节点名称, 默认配置节点: AppSetting</param>
    /// <param name="setup">在注册到依赖注入容器之前的处理方法</param>
    /// <returns></returns>
    public static TSetting AddSetting<TSetting>(this WebApplicationBuilder builder, string? section = default, Action<TSetting>? setup = default) where TSetting : class, new()
    {
        var setting = (string.IsNullOrWhiteSpace(section) || section == nameof(AppSetting)) ? builder.Configuration.GetSetting<TSetting>() : builder.Configuration.GetSetting<TSetting>(section) ?? new TSetting();
        setup?.Invoke(setting);

        if (setting is HttpSetting httpSetting)
        {
            httpSetting.IdentityDbContext ??= builder.Configuration.GetConnectionString(nameof(httpSetting.IdentityDbContext));
        }

        builder.Services.AddSetting(setting);
        return setting;
    }
    /// <summary>
    /// 配置服务器信息
    /// </summary>
    /// <param name="builder">web host builder</param>
    /// <param name="httpSetting">http setting</param>
    /// <returns></returns>
    public static IWebHostBuilder ConfigureKestrel(this IWebHostBuilder builder, HttpSetting httpSetting)
    {
        return builder.ConfigureKestrel(options => options.Limits.MaxRequestBodySize = httpSetting.Attachment.Upload.Total);
    }
    /// <summary>
    /// 启用全局请求中间件
    /// </summary>
    /// <param name="builder"></param>
    /// <returns></returns>
    public static IApplicationBuilder UseGlobalRequest(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<GlobalRequestMiddleware>();
    }
    #endregion

    #region service extensions
    /// <summary>
    /// 注册默认的基于字符串主键的实体数据对象模型数据操作相关服务
    /// </summary>
    /// <typeparam name="TSetting">Application Setting 配置项的类型名称</typeparam>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="setting">Application Setting 配置项的实例</param>
    /// <returns></returns>
    public static IServiceCollection AddSetting<TSetting>(this IServiceCollection services, TSetting setting) where TSetting : class, new()
    {
        services.AddSingleton(setting);
        if (typeof(TSetting) != typeof(AppSetting) && setting is AppSetting appSetting)
        {
            services.AddSingleton(appSetting);
            if (appSetting is HttpSetting httpSetting)
            {
                services.AddSingleton(httpSetting);
            }
        }

        return services;
    }
    /// <summary>
    /// 注册默认的基于字符串主键的实体数据对象模型数据操作相关服务
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddDataService(this IServiceCollection services)
    {
        services.AddScoped<IFileService, FileService>();
        services.AddScoped(typeof(IDataService<,>), typeof(DataService<,>));
        services.AddScoped(typeof(IAttachmentService<>), typeof(AttachmentService<>));
        services.AddScoped(typeof(IDataWithAttachmentService<,>), typeof(DataWithAttachmentService<,>));
        services.AddScoped(typeof(IDataWithCollectionService<,,>), typeof(DataWithCollectionService<,,>));
        services.AddScoped(typeof(IDataWithCollectionAndAttachmentService<,,>), typeof(DataWithCollectionAndAttachmentService<,,>));
        return services;
    }
    /// <summary>
    /// 注册默认的基于字符串主键的 Excel 导入导出操作相关服务
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddExcelService(this IServiceCollection services)
    {
        services.AddTransient<IExcelExportService, ExcelExportService>();
        services.AddTransient<IExcelImportService, ExcelImportService>();
        return services;
    }

    /// <summary>
    /// 注册默认 MVC 控制器
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="httpSetting">配置项</param>
    /// <returns></returns>
    public static IMvcBuilder AddControllers(this IServiceCollection services, HttpSetting httpSetting) => services.AddControllers(options => options.Filters.Add<ExceptionFilter>()).AddJsonOptions(httpSetting);
    /// <summary>
    /// 注册默认 MVC 控制器及页面
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="httpSetting">配置项</param>
    /// <returns></returns>
    public static IMvcBuilder AddControllersWithViews(this IServiceCollection services, HttpSetting httpSetting) => services.AddControllersWithViews(options => options.Filters.Add<ExceptionFilter>()).AddJsonOptions(httpSetting);

    /// <summary>
    /// 注册安全策略
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="httpSetting">配置项</param>
    public static IServiceCollection AddDefaultSecurityPolicy(this IServiceCollection services, HttpSetting httpSetting)
    {
        //启用跨域策略
        if (!string.IsNullOrWhiteSpace(httpSetting.CorsOrigins))
        {
            services.AddCors(options => options.AddPolicy(DEFAULT_CORS_POLICY, policy => policy.WithOrigins(httpSetting.CorsOrigins.Split(CHAR_COMMA, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)).AllowAnyHeader().AllowAnyMethod()));
        }

        //启用重定向和 Cookie 策略
        if (httpSetting.ProtocolType == ProtocolType.Https && httpSetting.HttpFordwarded)
        {
            services.AddCookiePolicy(options =>
            {
                options.MinimumSameSitePolicy = httpSetting.SameSiteMode;
                options.OnAppendCookie = cookieContext => SetSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext => SetSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });
        }

        return services;
    }
    /// <summary>
    /// 注册默认的认证服务
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="identitySetting">配置项</param>
    /// <returns></returns>
    public static IServiceCollection AddDefaultAuthentication(this IServiceCollection services, IdentitySetting identitySetting)
    {
        System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler.DefaultMapInboundClaims = false;
        services.AddAuthentication(options =>
        {
            options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
        })
        .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme)
        .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
        {
            ArgumentNullException.ThrowIfNull(identitySetting.ResponseType);
            ArgumentNullException.ThrowIfNull(identitySetting.Scope);
            options.Authority = identitySetting.Authority;
            options.ClientId = identitySetting.ClientId;
            options.ClientSecret = identitySetting.ClientSecret;
            options.ResponseType = identitySetting.ResponseType;
            options.ResponseMode = OpenIdConnectResponseMode.FormPost;
            options.Scope.AddRange(identitySetting.Scope.Split(CHAR_COMMA, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries));
            options.SaveTokens = identitySetting.SaveTokens;
            options.RequireHttpsMetadata = identitySetting.RequireHttpsMetadata;
            options.GetClaimsFromUserInfoEndpoint = identitySetting.GetClaimsFromUserInfoEndpoint;
            //options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //options.SignedOutRedirectUri = "/Home/Index";

            //options.CorrelationCookie.Expiration = TimeSpan.FromMinutes(5);
            //options.NonceCookie.Expiration = TimeSpan.FromMinutes(5);
            //if (appSetting.ProtocolType == ProtocolType.Https)
            //{
            //    options.CorrelationCookie.SameSite = SameSiteMode.Lax;
            //    options.CorrelationCookie.SecurePolicy = CookieSecurePolicy.Always;
            //    options.NonceCookie.SameSite = SameSiteMode.Lax;
            //    options.NonceCookie.SecurePolicy = CookieSecurePolicy.Always;
            //}
            //else
            //{
            //    options.CorrelationCookie.HttpOnly = true;
            //    options.NonceCookie.HttpOnly = true;
            //    options.RequireHttpsMetadata = false;
            //}
        });

        return services;
    }
    /// <summary>
    /// 使用 JwtBearer 方式认证服务
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="identitySetting">配置项</param>
    /// <returns></returns>
    public static IServiceCollection AddJwtBearerAuthentication(this IServiceCollection services, IdentitySetting identitySetting)
    {
        ArgumentNullException.ThrowIfNull(identitySetting);
        services.AddAuthentication(options =>
        {
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(options =>
        {
            options.Authority = identitySetting.Authority;
            //options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters { ValidateAudience = false };
            options.Audience = identitySetting.Scope;
            options.RequireHttpsMetadata = identitySetting.RequireHttpsMetadata;
        });

        return services;
    }
    /// <summary>
    /// 注册默认的授权服务
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="identitySetting">配置项</param>
    /// <returns></returns>
    public static IServiceCollection AddDefaultAuthorization(this IServiceCollection services, IdentitySetting identitySetting)
    {
        ArgumentNullException.ThrowIfNull(identitySetting.Scope);
        services.AddAuthorizationBuilder().AddPolicy(DEFAULT_AUTHORIZATION_POLICY, policy =>
        {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim(nameof(identitySetting.Scope).ToLowerInvariant(), identitySetting.Scope.Split(CHAR_COMMA, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries));
        });

        return services;
    }

    /// <summary>
    /// 注册通用服务
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="appSetting">配置项</param>
    /// <returns></returns>
    public static IServiceCollection AddDefaultServices(this IServiceCollection services, AppSetting appSetting)
    {
        services.AddLogging();
        services.AddHttpContextAccessor();
        services.AddResponseCompression();
        services.AddDataService();
        services.AddExcelService();
        services.AddDistributedCache(appSetting.Cache);

        if (appSetting is HttpSetting httpSetting)
        {
            if (httpSetting.Endpoint.Mode == EndpointMode.Distributed && (httpSetting.Endpoint.MachineCode != UNIT_ZERO || httpSetting.Endpoint.DataCenterCode != UNIT_ZERO))
            {
                //分布式配置
                KeyGenerator.Configure(options =>
                {
                    options.MachineCode = httpSetting.Endpoint.MachineCode;
                    options.DataCenterCode = httpSetting.Endpoint.DataCenterCode;
                });
            }

            if (httpSetting.HttpLogging is not null)
            {
                services.AddHttpLogging(options => options = httpSetting.HttpLogging);
            }

            if (httpSetting.EnableAuth)
            {
                services.AddAuthorization();
                if (httpSetting.Identity is not null)
                {
                    if (httpSetting.AuthType == AuthType.Oidc)
                    {
                        services.AddDefaultAuthentication(httpSetting.Identity);
                    }
                    else if (httpSetting.AuthType == AuthType.BearerToken)
                    {
                        services.AddJwtBearerAuthentication(httpSetting.Identity);
                    }
                    else
                    {
                        //不认证
                        services.AddAuthentication();
                    }
                }
            }

            if (httpSetting.ResourcesPath is not null)
            {
                services.AddLocalization(options => options.ResourcesPath = httpSetting.ResourcesPath);
            }

            if (httpSetting.CaptchaExpireTime > UNIT_ZERO)
            {
                services.AddCaptchaService();
            }

#if !DEBUG
            services.AddDefaultSecurityPolicy(httpSetting);
#endif
        }

        services.Configure<ApiBehaviorOptions>(config => config.SuppressModelStateInvalidFilter = true);
        return services;
    }
    /// <summary>
    /// 添加 Http 日志
    /// </summary>
    /// <param name="services">依赖注入容器服务</param>
    /// <param name="loggingSetting">配置项</param>
    /// <returns></returns>
    public static IServiceCollection AddHttpLogging(this IServiceCollection services, HttpLoggingSetting? loggingSetting = default)
    {
        if (loggingSetting is not null)
        {
            services.AddHttpLogging(options => options = loggingSetting);
        }

        return services;
    }
    /// <summary>
    /// 添加验证码服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    public static IServiceCollection AddCaptchaService(this IServiceCollection services, IConfiguration? configuration = default)
    {
        if (configuration is null)
        {
            services.AddCaptcha();
        }
        else
        {
            services.AddCaptcha(configuration);
        }

        services.AddScoped<ICaptcha, RandomCaptcha>();
        services.AddScoped<ICaptchaService, CaptchaService>();
        return services;
    }

    /// <summary>
    /// 添加默认的数据安全服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="dataSecuritySetting"></param>
    /// <returns></returns>
    public static IServiceCollection AddDataSecurityService<TDbContext>(this IServiceCollection services, DataSecuritySetting dataSecuritySetting) where TDbContext : BaseSecurityDbContext
    {
        services.AddSecurityService(dataSecuritySetting);
        services.AddScoped<IDataSecurityService<TDbContext>, DataSecurityService<TDbContext>>();
        return services;
    }
    /// <summary>
    /// 添加默认的数据安全服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="dataSecuritySetting"></param>
    /// <returns></returns>
    public static IServiceCollection AddUserDataSecurityService<TDbContext>(this IServiceCollection services, DataSecuritySetting dataSecuritySetting) where TDbContext : BaseSecurityDbContext
    {
        services.AddSecurityService(dataSecuritySetting);
        services.AddScoped<IUserDataSecurityService<TDbContext>, UserDataSecurityService<TDbContext>>();
        return services;
    }
    #endregion

    #region IApplicationBuilder extensions
    /// <summary>
    /// 启动安全策略
    /// </summary>
    /// <param name="app">IApplicationBuilder</param>
    /// <param name="httpSetting">配置项</param>
    /// <returns></returns>
    public static IApplicationBuilder UseDefaultSecurityPolicy(this IApplicationBuilder app, HttpSetting httpSetting)
    {
        //启用跨域策略
        if (!string.IsNullOrWhiteSpace(httpSetting.CorsOrigins))
        {
            app.UseCors(DEFAULT_CORS_POLICY);
        }

        //启动重定向和 Cookie 策略
        if (httpSetting.ProtocolType == ProtocolType.Https && httpSetting.HttpFordwarded)
        {
            //使用 webserver header 配置进行 http 重定向
            var fordwardedHeaderOptions = new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto };
            fordwardedHeaderOptions.KnownNetworks.Clear();
            fordwardedHeaderOptions.KnownProxies.Clear();
            app.UseForwardedHeaders(fordwardedHeaderOptions);

            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = httpSetting.SameSiteMode,
                OnAppendCookie = cookieContext => SetSameSite(cookieContext.Context, cookieContext.CookieOptions),
                OnDeleteCookie = cookieContext => SetSameSite(cookieContext.Context, cookieContext.CookieOptions)
            });
        }

        return app;
    }
    /// <summary>
    /// 全局默认异常处理方法
    /// 在 WebApplication 下会自动记录异常日志
    /// </summary>
    /// <param name="app">IApplicationBuilder</param>
    /// <param name="action">Exception Handler</param>
    /// <returns></returns>
    public static IApplicationBuilder UseException(this IApplicationBuilder app, Action<HttpContext, Exception?>? action = default) => app.UseExceptionHandler(new ExceptionHandlerOptions
    {
        ExceptionHandler = async context =>
        {
            var message = "服务器内部错误";
            var logger = (app as WebApplication)?.Logger;
            var exception = context.Features.Get<IExceptionHandlerFeature>();
            if (exception != null && exception.Error != null)
            {
                var errorMessage = exception.Error.GetMessage();
                switch (exception.Error)
                {
                    case BadHttpRequestException:
                        //业务类型的错误需要抛出 BadHttpRequestException 异常, 一律按 BadRequest(400) 处理
                        context.Response.StatusCode = StatusCodes.Status400BadRequest;
                        message = exception.Error.Message;
                        logger?.LogInformation(message);
                        break;
                    case UnauthorizedAccessException:
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        message = "用户尚未登陆, 请先登录!";
                        break;
                    case DbUpdateException:
                        context.Response.StatusCode = StatusCodes.Status409Conflict;
                        message = "数据异常, 请联系管理员处理!";
                        logger?.LogWarning(exception.Error, exception.Error.GetMessage());
                        break;
                    default:
                        context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                        logger?.LogError(exception.Error, exception.Error.GetMessage());
                        break;
                }
#if !DEBUG
                message = errorMessage;
#endif
            }

            //context.Response.ContentType = ContentType.Json;
            action?.Invoke(context, exception?.Error);
            await context.Response.WriteAsync(message).ConfigureAwait(false);
        }
    });
    /// <summary>
    /// 启动默认 IApplicationBuilder
    /// </summary>
    /// <param name="app">WebApplication</param>
    /// <param name="httpSetting">配置项</param>
    /// <param name="beforeAuth">在启用认证与授权之前执行的方法委托</param>
    /// <returns></returns>
    public static IApplicationBuilder UseDefaultBuilder(this WebApplication app, HttpSetting httpSetting, Action? beforeAuth = default)
    {
        // Configure the HTTP request pipeline.
        if (app.Environment.IsProduction())
        {
            app.UseExceptionHandler("/Error");
        }
        else
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseException();

        if (httpSetting.UseStaticFiles)
        {
            app.UseStaticFiles();
        }

        if (httpSetting.HttpLogging is not null)
        {
            app.UseHttpLogging();
        }

        app.UseRouting();

#if !DEBUG
        app.UseDefaultSecurityPolicy(httpSetting);
#endif

        beforeAuth?.Invoke();

        app.UseResponseCompression();

        if (httpSetting.EnableAuth)
        {
            app.UseAuthentication();
            app.UseAuthorization();
        }

        app.UseGlobalRequest();
        app.MapControllers();
        app.MapDefaultControllerRoute();
        app.MapControllerRoute(name: "api", pattern: "api/{controller=Home}/{action=Index}/{id?}");

        return app;
    }
    #endregion

    #region 内部方法
    /// <summary>
    /// 设置 Cookie 策略
    /// </summary>
    /// <param name="httpContext"></param>
    /// <param name="options"></param>
    private static void SetSameSite(HttpContext httpContext, CookieOptions options)
    {
        if (options.SameSite == SameSiteMode.None && DisallowsSameSiteNone(httpContext.Request.Headers.UserAgent.ToString()))
        {
            options.SameSite = SameSiteMode.Lax;
        }
    }
    /// <summary>
    /// Checks if the UserAgent is known to interpret an unknown value as Strict.
    /// For those the <see cref="CookieOptions.SameSite" /> property should be
    /// set to <see cref="Unspecified" />.
    /// </summary>
    /// <remarks>
    /// This code is taken from Microsoft:
    /// https://devblogs.microsoft.com/aspnet/upcoming-samesite-cookie-changes-in-asp-net-and-asp-net-core/
    /// </remarks>
    /// <param name="userAgent">The user agent string to check.</param>
    /// <returns>Whether the specified user agent (browser) accepts SameSite=None or not.</returns>
    private static bool DisallowsSameSiteNone(string userAgent)
    {
        // Cover all iOS based browsers here. This includes:
        //   - Safari on iOS 12 for iPhone, iPod Touch, iPad
        //   - WkWebview on iOS 12 for iPhone, iPod Touch, iPad
        //   - Chrome on iOS 12 for iPhone, iPod Touch, iPad
        // All of which are broken by SameSite=None, because they use the
        // iOS networking stack.
        // Notes from Thinktecture:
        // Regarding https://caniuse.com/#search=samesite iOS versions lower
        // than 12 are not supporting SameSite at all. Starting with version 13
        // unknown values are NOT treated as strict anymore. Therefore we only
        // need to check version 12.
        if (userAgent.Contains("CPU iPhone OS 12") || userAgent.Contains("iPad; CPU OS 12"))
        {
            return true;
        }

        // Cover Mac OS X based browsers that use the Mac OS networking stack.
        // This includes:
        //   - Safari on Mac OS X.
        // This does not include:
        //   - Chrome on Mac OS X
        // because they do not use the Mac OS networking stack.
        // Notes from Thinktecture:
        // Regarding https://caniuse.com/#search=samesite MacOS X versions lower
        // than 10.14 are not supporting SameSite at all. Starting with version
        // 10.15 unknown values are NOT treated as strict anymore. Therefore we
        // only need to check version 10.14.
        if (userAgent.Contains("Safari") && userAgent.Contains("Macintosh; Intel Mac OS X 10_14") && userAgent.Contains("Version/"))
        {
            return true;
        }

        // Cover Chrome 50-69, because some versions are broken by SameSite=None
        // and none in this range require it.
        // Note: this covers some pre-Chromium Edge versions,
        // but pre-Chromium Edge does not require SameSite=None.
        // Notes from Thinktecture:
        // We can not validate this assumption, but we trust Microsofts
        // evaluation. And overall not sending a SameSite value equals to the same
        // behavior as SameSite=None for these old versions anyways.
        if (userAgent.Contains("Chrome/5") || userAgent.Contains("Chrome/6"))
        {
            return true;
        }

        return false;
    }
    #endregion
}