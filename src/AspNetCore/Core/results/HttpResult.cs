﻿namespace Devonline.AspNetCore;

/// <summary>
/// 用于构造 web api 接口的统一结构返回值
/// </summary>
public class HttpResult<TValue>
{
    /// <summary>
    /// HTTP Status Code, HTTP 状态码
    /// </summary>
    public int Code { get; set; }
    /// <summary>
    /// 返回的消息, 用于出错或提示时, 指示内容
    /// </summary>
    public string? Message { get; set; }
    /// <summary>
    /// 返回的数据, 是一个值, 数组或对象
    /// </summary>
    public TValue? Data { get; set; }
}