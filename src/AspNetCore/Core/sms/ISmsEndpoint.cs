﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Devonline.AspNetCore;

/// <summary>
/// 短信终结点设置
/// </summary>
public interface ISmsEndpoint : IAuthEndpoint
{
    /// <summary>
    /// 短信验证码有效期, 单位: 分钟
    /// </summary>
    int Expiration { get; set; }
    /// <summary>
    /// 最大发送次数
    /// </summary>
    int SendCount { get; set; }
    /// <summary>
    /// 最大尝试验证次数
    /// </summary>
    int RetryCount { get; set; }
}