﻿namespace Devonline.AspNetCore;

/// <summary>
/// 短信配置模型
/// </summary>
public interface ISmsModel
{
    /// <summary>
    /// 短信签名
    /// </summary>
    string SignName { get; set; }
    /// <summary>
    /// 短信模版编号
    /// </summary>
    string TemplateCode { get; set; }
    /// <summary>
    /// 短信内容模板中的变量和值的集合
    /// </summary>
    Dictionary<string, string> TemplateParam { get; set; }
    /// <summary>
    /// 接收短信的默认手机号码列表
    /// </summary>
    string[]? PhoneNumbers { get; set; }

    /// <summary>
    /// 用途, 用于同一手机号码同一短信模板区分不同用途
    /// </summary>
    string? Purpose { get; set; }
}