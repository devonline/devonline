﻿namespace Devonline.AspNetCore;

/// <summary>
/// 短信终结点设置
/// </summary>
public class SmsEndpoint : AuthEndpoint, ISmsEndpoint
{
    /// <summary>
    /// 短信验证码有效期, 单位: 分钟
    /// </summary>
    public virtual int Expiration { get; set; } = 5;
    /// <summary>
    /// 最大发送次数
    /// </summary>
    public virtual int SendCount { get; set; } = 10;
    /// <summary>
    /// 最大尝试验证次数
    /// </summary>
    public virtual int RetryCount { get; set; } = 5;
}