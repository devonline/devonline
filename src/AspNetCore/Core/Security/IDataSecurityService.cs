﻿using Devonline.Security;

namespace Devonline.AspNetCore.Security;

/// <summary>
/// 使用数据库上下文存储的数据安全服务
/// 证书和密钥的存储会固定使用 Certificate 和 Secret 相关的对象
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IDataSecurityService<TDbContext, TKey> :
    IDataSecurityService,
    ISecurityService
    where TDbContext : BaseSecurityDbContext
    where TKey : IConvertible;

/// <summary>
/// 使用数据库上下文存储的数据安全服务
/// 字符串作为主键的默认实现
/// 证书和密钥的存储会固定使用 Certificate 和 Secret 相关的对象
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
public interface IDataSecurityService<TDbContext> :
    IUserDataSecurityService<TDbContext, string>,
    IDataSecurityService,
    ISecurityService
    where TDbContext : BaseSecurityDbContext
{
    /// <summary>
    /// 设定当前使用者的身份
    /// </summary>
    /// <param name="identityId">身份标识</param>
    /// <param name="identityType">身份类型</param>
    void SetIdentity(string identityId, IdentityType identityType = IdentityType.User);
}