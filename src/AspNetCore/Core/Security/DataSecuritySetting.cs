﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Devonline.Security;

namespace Devonline.AspNetCore.Security;

/// <summary>
/// 数据安全相关设置项
/// </summary>
public class DataSecuritySetting : SecuritySetting
{
    /// <summary>
    /// 本地安全相关文件存储的根目录
    /// </summary>
    public string RootPath { get; set; } = "Security";
    /// <summary>
    /// RSA 密钥大小
    /// </summary>
    public int RSAKeySize { get; set; } = 2048;
    /// <summary>
    /// 签名名称
    /// </summary>
    public string? SignatureName { get; set; }
    /// <summary>
    /// 签名算法名称
    /// </summary>
    public HashAlgorithmName HashAlgorithmName { get; set; } = HashAlgorithmName.SHA256;
    /// <summary>
    /// 对齐方式
    /// </summary>
    public RSASignaturePaddingMode SignaturePadding { get; set; } = RSASignaturePaddingMode.Pkcs1;
    /// <summary>
    /// X509 Key Usage Flags
    /// </summary>
    public X509KeyUsageFlags KeyUsageFlags { get; set; } = X509KeyUsageFlags.DigitalSignature;
    /// <summary>
    /// true if the extension is critical; otherwise, false.
    /// </summary>
    public bool Critical { get; set; } = true;
    /// <summary>
    /// 过期时间, 单位: 月
    /// </summary>
    public int CertificateExpireTime { get; set; } = UNIT_MONTHS_A_YEAR;
    /// <summary>
    /// 证书内容保存类型
    /// </summary>
    public X509ContentType ContentType { get; set; } = X509ContentType.Pfx;
    /// <summary>
    /// 证书密码
    /// </summary>
    public string? Password { get; set; }
}