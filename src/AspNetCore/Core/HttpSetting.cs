﻿global using Devonline.Core;
global using Devonline.Entity;
global using static Devonline.Core.AppSettings;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.Net.Http.Headers;

namespace Devonline.AspNetCore;

/// <summary>
/// 基于 AspNetCore Http Web 项目的基础配置项
/// </summary>
public class HttpSetting : AppSetting
{
    /// <summary>
    /// 启用 Authentication 和 Authorization 功能
    /// </summary>
    public bool EnableAuth { get; set; } = true;
    /// <summary>
    /// 使用 identity 数据库来启用用户身份认证功能
    /// </summary>
    public bool EnableIdentity { get; set; } = true;
    /// <summary>
    /// 使用 IdentityType 来启用用户身份过滤功能, 即存在多重身份无法同时登录使用系统时, 由此来标记启用此功能
    /// 在启用了 IdentityType 过滤功能时, 将从缓存中拿出 ICollection<KeyValuePair<IdentityType, string[]>> 集合来确认允许过滤的身份
    /// </summary>
    public bool EnableIdentityType { get; set; } = false;
    /// <summary>
    /// 使用 AuthorizedAccess 来启用用户授权访问功能, 即用户已登录但身份存在需授权访问时, 会先跳转到授权地址等待授权
    /// </summary>
    public bool EnableAuthorizedAccess { get; set; } = true;
    /// <summary>
    /// 是否使用 Http 协议重定向
    /// </summary>
    public bool HttpFordwarded { get; set; } = true;
    /// <summary>
    /// 默认的时间类型是否输出为字符串格式
    /// </summary>
    public bool DateTimeToString { get; set; } = true;
    /// <summary>
    /// 是否启用静态文件
    /// </summary>
    public bool UseStaticFiles { get; set; } = false;
    /// <summary>
    /// 验证码到期时间, 单位: 秒
    /// </summary>
    public int CaptchaExpireTime { get; set; } = UNIT_SECONDS_A_MINUTE;
    /// <summary>
    /// 版权申明
    /// </summary>
    public string? Copyright { get; set; }
    /// <summary>
    /// 许可证
    /// </summary>
    public string? License { get; set; }
    /// <summary>
    /// 默认的资源文件地址
    /// </summary>
    public string? ResourcesPath { get; set; }
    /// <summary>
    /// 默认密码
    /// </summary>
    public string? DefaultPassword { get; set; }
    /// <summary>
    /// 默认身份数据库连接字符串
    /// </summary>
    public string? IdentityDbContext { get; set; }
    /// <summary>
    /// 内置用户列表
    /// </summary>
    public string BuiltInUsers { get; set; } = USER_MAINTAINERS;
    /// <summary>
    /// 内置角色列表
    /// </summary>
    public string BuiltInRoles { get; set; } = GROUP_MAINTAINERS;
    /// <summary>
    /// 身份组-实名认证用户, 指通过了实名认证, 但是没有进一步操作权限的人
    /// </summary>
    public string IdentityGroupAuthorizedUser { get; set; } = "实名认证用户";
    /// <summary>
    /// 默认的认证类型, Oidc
    /// </summary>
    public AuthType AuthType { get; set; } = AuthType.Oidc;
    /// <summary>
    /// SameSiteMode 配置
    /// </summary>
    public Microsoft.AspNetCore.Http.SameSiteMode SameSiteMode { get; set; } = Microsoft.AspNetCore.Http.SameSiteMode.Lax;
    /// <summary>
    /// 自动条件过滤, 默认不启用
    /// </summary>
    public bool AutoFilter { get; set; }
    /// <summary>
    /// 自动条件过滤时, 针对字符串类型的字段, 默认使用的过滤操作类型, 默认使用包含关系, 也即: 模糊匹配
    /// </summary>
    public OperatorType DefaultOperatorType { get; set; } = OperatorType.Contains;

    /// <summary>
    /// 当前节点设置
    /// </summary>
    public DistributedEndpoint Endpoint { get; set; } = new();
    /// <summary>
    /// 附件相关配置项
    /// </summary>
    public AttachmentSetting Attachment { get; set; } = new();
    /// <summary>
    /// HttpLogging 配置
    /// </summary>
    public HttpLoggingSetting? HttpLogging { get; set; }
    /// <summary>
    /// identity options
    /// </summary>
    public IdentitySetting? Identity { get; set; }
    /// <summary>
    /// user interaction setting
    /// </summary>
    public UserInteractionSetting? UserInteraction { get; set; }
    /// <summary>
    /// https certificate configuration
    /// </summary>
    public CertificateSetting? Certificate { get; set; }
}

/// <summary>
/// 日志相关设置
/// </summary>
public class HttpLoggingSetting
{
    /// <summary>
    /// Gets or sets if the middleware will combine the request, request body, response,
    /// response body, and duration logs into a single log entry. The default is false.
    /// </summary>
    public bool CombineLogs { get; set; } = true;
    /// <summary>
    /// Maximum request body size to log (in bytes). Defaults to 32 KB.
    /// </summary>
    public int RequestBodyLogLimit { get; set; } = UNIT_FOUR * UNIT_EIGHT * UNIT_KILO;
    /// <summary>
    /// Maximum response body size to log (in bytes). Defaults to 32 KB.
    /// </summary>
    public int ResponseBodyLogLimit { get; set; } = UNIT_FOUR * UNIT_EIGHT * UNIT_KILO;
    /// <summary>
    /// HttpLoggingFields 的枚举值
    /// </summary>
    public HttpLoggingFields LoggingFields { get; set; } = HttpLoggingFields.All;
    /// <summary>
    /// 要记录的 Http Request 的 Header 值
    /// Request header values that are allowed to be logged.
    /// If a request header is not present in the Microsoft.AspNetCore.HttpLogging.HttpLoggingOptions.RequestHeaders,
    /// the header name will be logged with a redacted value. Request headers can contain
    /// authentication tokens, or private information which may have regulatory concerns
    /// under GDPR and other laws. Arbitrary request headers should not be logged unless
    /// logs are secure and access controlled and the privacy impact assessed.
    /// </summary>
    public string[] RequestHeaders { get; set; } = [HeaderNames.Cookie, HeaderNames.Authorization, HeaderNames.Origin];
    /// <summary>
    /// 要记录的 Http Response 的 Header 值
    /// Response header values that are allowed to be logged.
    /// If a response header is not present in the Microsoft.AspNetCore.HttpLogging.HttpLoggingOptions.ResponseHeaders,
    /// the header name will be logged with a redacted value.
    /// </summary>
    public string[] ResponseHeaders { get; set; } = [HeaderNames.SetCookie, HeaderNames.Authorization, HeaderNames.Origin, HeaderNames.AccessControlAllowOrigin];
    /// <summary>
    /// Options for configuring encodings for a specific media type.
    /// <para>
    /// If the request or response do not match the supported media type,
    /// the response body will not be logged.
    /// </para>
    /// </summary>
    public string[] MediaTypes { get; set; } = [];

    /// <summary>
    /// 对 HttpLogging 配置进行隐式类型转换
    /// </summary>
    /// <param name="setting"></param>
    public static implicit operator HttpLoggingOptions(HttpLoggingSetting setting)
    {
        var options = new HttpLoggingOptions
        {
            LoggingFields = setting.LoggingFields,
            CombineLogs = setting.CombineLogs,
            RequestBodyLogLimit = setting.RequestBodyLogLimit,
            ResponseBodyLogLimit = setting.ResponseBodyLogLimit
        };

        if (setting.RequestHeaders.Length > 0)
        {
            options.RequestHeaders.AddRangeIfNotContains(setting.RequestHeaders);
        }

        if (setting.ResponseHeaders.Length > 0)
        {
            options.ResponseHeaders.AddRangeIfNotContains(setting.ResponseHeaders);
        }

        if (setting.MediaTypes.Length > 0)
        {
            foreach (var mediaType in setting.MediaTypes)
            {
                options.MediaTypeOptions.AddBinary(mediaType);
            }
        }

        return options;
    }
}

/// <summary>
/// 认证相关配置选项
/// </summary>
public class IdentitySetting
{
    /// <summary>
    /// 认证服务地址
    /// </summary>
    public string Authority { get; set; } = "https://localhost:9527";
    /// <summary>
    /// api 资源名称
    /// </summary>
    [Column("api_name")]
    public string? ApiName { get; set; }
    /// <summary>
    /// api 资源密钥
    /// </summary>
    [Column("api_secret")]
    public string? ApiSecret { get; set; }
    /// <summary>
    /// 客户端编号
    /// </summary>
    [Column("Client_Id")]
    public string? ClientId { get; set; }
    /// <summary>
    /// 客户端密钥
    /// </summary>
    [Column("client_secret")]
    public string? ClientSecret { get; set; }
    /// <summary>
    /// 授权类型
    /// </summary>
    [Column("grant_type")]
    public string? GrantType { get; set; } = "password";
    /// <summary>
    /// 返回值类型
    /// </summary>
    [Column("response_type")]
    public string? ResponseType { get; set; } = "token";
    /// <summary>
    /// 允许访问的 api 资源范围
    /// </summary>
    public string? Scope { get; set; }
    /// <summary>
    /// 获取 token 的地址
    /// </summary>
    public string Token { get; set; } = "/connect/token";
    /// <summary>
    /// 获取用户信息的地址
    /// </summary>
    public string UserInfo { get; set; } = "/connect/userinfo";
    /// <summary>
    /// Defines whether access and refresh tokens should be stored 
    /// in the Microsoft.AspNetCore.Authentication.AuthenticationProperties after a successful authorization. 
    /// This property is set to false by default to reduce the size of the final authentication cookie.
    /// </summary>
    public bool SaveTokens { get; set; } = false;
    /// <summary>
    /// Gets or sets if HTTPS is required for the metadata address or authority. 
    /// The default is true. This should be disabled only in development environments.
    /// </summary>
    public bool RequireHttpsMetadata { get; set; } = false;
    /// <summary>
    /// Boolean to set whether the handler should go to user info endpoint to retrieve additional claims 
    /// or not after creating an identity from id_token received from token endpoint. The default is 'false'.
    /// </summary>
    public bool GetClaimsFromUserInfoEndpoint { get; set; } = true;
}

/// <summary>
/// 用户交互设置
/// </summary>
public class UserInteractionSetting
{
    /// <summary>
    /// 首页地址
    /// </summary>
    public string Home { get; set; } = "/";
    /// <summary>
    /// 登录地址
    /// </summary>
    public string Login { get; set; } = "/Account/Login";
    /// <summary>
    /// 注销地址
    /// </summary>
    public string Logout { get; set; } = "/Account/Logout";
    /// <summary>
    /// 微信授权地址
    /// </summary>
    public string WeixinAuthorize { get; set; } = "/api/Account/WeixinAuthorize";
    /// <summary>
    /// 微信登录地址
    /// </summary>
    public string WeixinLogin { get; set; } = "/api/Account/WeixinLogin";
    /// <summary>
    /// 外部登录回调地址
    /// </summary>
    public string Callback { get; set; } = "/External/Callback";
    /// <summary>
    /// 用户主页地址
    /// </summary>
    public string Profile { get; set; } = "/Account/Profile";
    /// <summary>
    /// 拒绝访问地址
    /// </summary>
    public string AccessDenied { get; set; } = "/Account/AccessDenied";
    /// <summary>
    /// 错误页面地址
    /// </summary>
    public string Error { get; set; } = "/Home/Error";
    /// <summary>
    /// 身份选择页面地址
    /// </summary>
    public string IdentitySelect { get; set; } = "/Account/IdentitySelect";
    /// <summary>
    /// 用户信息获取地址
    /// </summary>
    public string UserInfo { get; set; } = "/api/Account/GetUserInfo";
    /// <summary>
    /// 用户权限校验地址
    /// </summary>
    public string UserPermission { get; set; } = "/api/Account/UserHasPermission";
    /// <summary>
    /// 给当前用户自动添加角色
    /// </summary>
    public string AutoRole { get; set; } = "/api/Account/AutoRole";
    /// <summary>
    /// 外部登录回调结束后绑定手机号码的地址
    /// </summary>
    public string BindPhoneNumber { get; set; } = "/Account/BindPhoneNumber";
    /// <summary>
    /// 业务系统授权访问地址
    /// 已开启授权访问的低级别用户在登录/访问系统资源时, 会跳转到授权地址, 先得到授权后, 才可以访问
    /// </summary>
    public string AuthorizedAccess { get; set; } = "/Account/AuthorizedAccess";
}

/// <summary>
/// https certificate configuration
/// </summary>
public class CertificateSetting
{
    /// <summary>
    /// 域名
    /// </summary>
    public string? Domain { get; set; }
    /// <summary>
    /// 证书地址
    /// </summary>
    public string? Path { get; set; }
    /// <summary>
    /// 证书密码
    /// </summary>
    public string? Password { get; set; }
}

/// <summary>
/// 附件相关选项
/// </summary>
public class AttachmentSetting
{
    public AttachmentSetting()
    {
        Upload = new()
        {
            Enable = true,
            Extensions = DEFAULT_ALLOW_FILE_EXTENSIONS
        };

        Download = new()
        {
            Enable = true,
            Extensions = DEFAULT_ALLOW_FILE_EXTENSIONS
        };

        Thumbnail = new()
        {
            Enable = true,
            Size = DEFAULT_IMAGE_THUMBNAIL_SIZE,
            Prefix = DEFAULT_IMAGE_THUMBNAIL_PREFIX,
            Extensions = DEFAULT_IMAGE_FILE_EXTENSIONS
        };

        Crop = new()
        {
            Enable = false,
            Size = DEFAULT_IMAGE_CROP_SIZE,
            Extensions = DEFAULT_IMAGE_FILE_EXTENSIONS
        };

        Video = new()
        {
            Enable = false,
            Size = DEFAULT_IMAGE_CROP_SIZE,
            Length = DEFAULT_TOTAL_FILE_SIZE,
            Total = UNIT_GIGA,
            Prefix = DEFAULT_VIDEO_CAPTURE_PREFIX,
            Extensions = DEFAULT_VIDEO_FILE_EXTENSIONS
        };
    }

    /// <summary>
    /// 附件根路径
    /// </summary>
    public string RootPath { get; set; } = DEFAULT_ATTACHMENT_PATH;

    /// <summary>
    /// 文件上传限制
    /// </summary>
    public FileSetting Upload { get; set; }
    /// <summary>
    /// 文件下载限制
    /// </summary>
    public FileSetting Download { get; set; }
    /// <summary>
    /// 图片缩略图
    /// </summary>
    public FileSetting Thumbnail { get; set; }
    /// <summary>
    /// 图片裁剪图
    /// </summary>
    public FileSetting Crop { get; set; }
    /// <summary>
    /// 视频附件
    /// </summary>
    public FileSetting Video { get; set; }
}

/// <summary>
/// 文件相关设置
/// </summary>
public class FileSetting
{
    /// <summary>
    /// 启用文件设置
    /// </summary>
    public bool Enable { get; set; } = false;
    /// <summary>
    /// 图片或者视频的最大边长
    /// </summary>
    public int Size { get; set; } = DEFAULT_IMAGE_THUMBNAIL_SIZE;
    /// <summary>
    /// 单个文件大小限制
    /// </summary>
    public int Length { get; set; } = DEFAULT_MAX_FILE_SIZE;
    /// <summary>
    /// 全部/批量文件大小限制
    /// </summary>
    public int Total { get; set; } = DEFAULT_TOTAL_FILE_SIZE;
    /// <summary>
    /// 进行操作时使用的文件前缀
    /// </summary>
    public string Prefix { get; set; } = DEFAULT_IMAGE_THUMBNAIL_PREFIX;
    /// <summary>
    /// 允许使用的文件类型, 文件扩展名
    /// </summary>
    public string Extensions { get; set; } = DEFAULT_IMAGE_FILE_EXTENSIONS;
}