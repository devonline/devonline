﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Devonline.AspNetCore;

/// <summary>
/// AspNetCore type extensions
/// </summary>
public static class TypeExtensions
{
    /// <summary>
    /// 获取类型对于指定类型 T 的第一个引用的外键名称
    /// 取外键顺序遵循 efcore 顺序, ForeignKeyAttribute -> Navigation PropertyInfo.Name + Id
    /// </summary>
    /// <typeparam name="T">主类型参数</typeparam>
    /// <typeparam name="TKey">主键类型参数</typeparam>
    /// <param name="member">包含类型 T 引用的对象的成员</param>
    /// <returns>外键名</returns>
    public static string GetForeignKey<T, TKey>([DisallowNull] this MemberInfo member) where T : IEntitySet<TKey> where TKey : IConvertible => member.GetAttributeValue<ForeignKeyAttribute, string>(nameof(ForeignKeyAttribute.Name)) ?? (member.Name + nameof(IEntitySet<TKey>.Id));
    /// <summary>
    /// 获取类型对于指定类型 T 的第一个引用的外键名称
    /// 取外键顺序遵循 efcore 顺序, ForeignKeyAttribute -> Navigation PropertyInfo.Name + Id
    /// </summary>
    /// <typeparam name="T">主类型参数</typeparam>
    /// <param name="member">包含类型 T 引用的对象的成员</param>
    /// <returns></returns>
    public static string GetForeignKey<T>([DisallowNull] this MemberInfo member) where T : IEntitySet => member.GetForeignKey<T, string>();
    /// <summary>
    /// 获取类型对于指定类型 T 的第一个引用的外键名称
    /// 取外键顺序遵循 efcore 顺序, ForeignKeyAttribute -> Navigation PropertyInfo.Name + Id -> typeof(T).Name + Id
    /// </summary>
    /// <typeparam name="T">主类型参数</typeparam>
    /// <typeparam name="TKey">主键类型参数</typeparam>
    /// <param name="type">包含类型 T 引用的对象</param>
    /// <returns>外键名</returns>
    public static IDictionary<string, string> GetForeignKeys<T, TKey>([DisallowNull] this Type type) where T : IEntitySet<TKey> where TKey : IConvertible
    {
        var dic = new Dictionary<string, string>();
        var propertyInfos = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.PropertyType.IsFromType<T>());
        if (propertyInfos != null && propertyInfos.Any())
        {
            foreach (var propertyInfo in propertyInfos)
            {
                dic.Add(propertyInfo.Name, propertyInfo.GetForeignKey<T, TKey>());
            }
        }
        else
        {
            var name = typeof(T).Name;
            dic.Add(name, name + nameof(IEntitySet<TKey>.Id));
        }

        return dic;
    }
    /// <summary>
    /// 获取类型对于指定类型 T 的第一个引用的外键名称
    /// 取外键顺序遵循 efcore 顺序, ForeignKeyAttribute -> Navigation PropertyInfo.Name + Id -> typeof(T).Name + Id
    /// </summary>
    /// <typeparam name="T">主类型参数</typeparam>
    /// <param name="type">包含类型 T 引用的对象</param>
    /// <returns>外键名</returns>
    public static IDictionary<string, string> GetForeignKeys<T>([DisallowNull] this Type type) where T : IEntitySet => type.GetForeignKeys<T, string>();

    /// <summary>
    /// 获取类型 type 中对于类型 T 的第一个引用的外键名称
    /// </summary>
    /// <param name="type">包含类型 T 引用的对象</param>
    /// <returns>外键名</returns>
    public static string? GetForeignKey<T, TKey>([DisallowNull] this Type type) where T : class, IEntitySet<TKey> where TKey : IConvertible
    {
        var propertyInfo = type.GetProperties().FirstOrDefault(x => x.PropertyType.IsFromType<T>());
        if (propertyInfo is not null)
        {
            var foreignKey = propertyInfo.GetCustomAttribute<ForeignKeyAttribute>();
            return foreignKey is not null ? foreignKey.Name : propertyInfo.Name + nameof(IEntitySet<TKey>.Id);
        }

        return null;
    }
    /// <summary>
    /// 获取类型 type 中对于类型 T 的第一个引用的外键名称
    /// </summary>
    /// <param name="type">包含类型 T 引用的对象</param>
    /// <returns>外键名</returns>
    public static string? GetForeignKey<T>([DisallowNull] this Type type) where T : class, IEntitySet => type.GetForeignKey<T, string>();
    /// <summary>
    /// 获取类型 type 中对于类型 t 的第一个引用的外键名称
    /// </summary>
    /// <param name="type">包含类型 t 引用的对象</param>
    /// <param name="t">类型 t</param>
    /// <returns>外键名</returns>
    public static string? GetForeignKey<TKey>([DisallowNull] this Type type, [DisallowNull] Type t) where TKey : IConvertible
    {
        var propertyInfo = type.GetProperties().FirstOrDefault(x => x.PropertyType.IsFromType(t));
        if (propertyInfo is not null)
        {
            var foreignKey = propertyInfo.GetCustomAttribute<ForeignKeyAttribute>();
            return foreignKey is not null ? foreignKey.Name : propertyInfo.Name + nameof(IEntitySet<TKey>.Id);
        }

        return null;
    }
    /// <summary>
    /// 获取类型 type 中对于类型 T 的第一个引用的外键名称
    /// </summary>
    /// <param name="type">包含类型 T 引用的对象</param>
    /// <returns>外键名</returns>
    public static string? GetForeignKey([DisallowNull] this Type type, [DisallowNull] Type t) => type.GetForeignKey<string>(t);

    /// <summary>
    /// 根据外键属性获取导航属性 
    /// 如获取 Person 的外键 IdCardId 对应的导航属性方式为: idCardIdPropertyInfo.GetNavigationPropertyByForeignKey() => IdCard
    /// 如获取 PersonAdditional 的外键 PersonId 对应的导航属性方式为: personIdPropertyInfo.GetNavigationPropertyByForeignKey() => Person
    /// 外键对于的导航属性为将外键设置为 ForeignKey 特性的属性或者末尾加上 Id 等于外键列名的属性
    /// </summary>
    /// <param name="property">外键属性</param>
    /// <returns>导航属性</returns>
    public static PropertyInfo? GetNavigationPropertyByForeignKey<TKey>([DisallowNull] this PropertyInfo property)
    {
        var type = property.DeclaringType;
        if (type == null)
        {
            throw new ArgumentNullException(nameof(property), $"The type of property {property.PropertyType.FullName} has no DeclaringType");
        }

        var prop = type.GetProperties().FirstOrDefault(x => x.GetCustomAttributes<ForeignKeyAttribute>().Any(a => a.Name == property.Name));
        if (prop == null)
        {
            prop = type.GetProperty(property.Name[..^nameof(IEntitySet<TKey>.Id).Length]);
        }

        return prop;
    }
    /// <summary>
    /// 根据导航属性获取外键属性
    /// 如获取 Person 的导航属性 IdCard 对应的外键属性方式为: idCardPropertyInfo.GetForeignKeyByNavigationProperty() => IdCardId
    /// 如获取 PersonAdditional 的导航属性 Person 对应的外键属性方式为: personPropertyInfo.GetForeignKeyByNavigationProperty() => PersonId
    /// 外键对于的导航属性为将外键设置为 ForeignKey 特性的属性或者末尾加上 Id 等于外键列名的属性
    /// </summary>
    /// <param name="property">导航属性</param>
    /// <returns>外键属性</returns>
    public static PropertyInfo? GetForeignKeyByNavigationProperty<TKey>([DisallowNull] this PropertyInfo property) where TKey : IConvertible
    {
        var type = property.DeclaringType ?? throw new ArgumentNullException(nameof(property), $"The type of property {property.PropertyType.FullName} has no DeclaringType");
        var foreignKey = type.GetForeignKey<TKey>(property.PropertyType);
        return !string.IsNullOrWhiteSpace(foreignKey) ? type.GetProperty(foreignKey) : null;
    }

    /// <summary>
    /// 获取由 NavigableAttribute 指定的导航属性列表
    /// </summary>
    /// <param name="type">要获取NavigableAttribute类型</param>
    /// <returns>对象的NavigableAttribute指定的列</returns>
    public static List<PropertyInfo> GetNavigationProperties<TKey>([DisallowNull] this Type type) => type.GetProperties().Where(x => x.HasAttribute<NavigableAttribute>() && (x.PropertyType.IsFromType<IEntitySet<TKey>>() || x.PropertyType.IsFromType<IEnumerable<IEntitySet<TKey>>>())).ToList();
    /// <summary>
    /// 获取由 NavigableAttribute 指定的非导航属性列的值
    /// </summary>
    /// <param name="type">要获取NavigableAttribute类型</param>
    /// <returns>对象的NavigableAttribute指定的列</returns>
    public static PropertyInfo? GetNavigationProperty<TKey>([DisallowNull] this Type type) => type.GetProperties().FirstOrDefault(x => x.HasAttribute<NavigableAttribute>() && !x.PropertyType.IsFromType<IEntitySet<TKey>>() && !x.PropertyType.IsFromType<IEnumerable<IEntitySet<TKey>>>());

    /// <summary>
    /// 根据 controller 的类型配置路由申明, 获取路由模板地址
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string GetControllerRoutePath(this Type type)
    {
        var attirbute = type.GetCustomAttribute<RouteAttribute>();
        if (attirbute is not null)
        {
            if (attirbute.Template.Contains("[controller]"))
            {
                return attirbute.Template.Replace("[controller]", type.Name.Replace("controller", string.Empty, StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                return attirbute.Template;
            }
        }

        return type.Name;
    }

    /// <summary>
    /// 根据 action 的类型配置路由申明, 获取路由模板地址
    /// </summary>
    /// <param name="methodInfo"></param>
    /// <returns></returns>
    public static string GetActionRoutePath(this MethodInfo methodInfo)
    {
        var pathPrefix = methodInfo.ReflectedType!.GetControllerRoutePath();
        var pathRoute = string.Empty;
        var attirbute = methodInfo.GetCustomAttribute<RouteAttribute>();
        if (attirbute is not null)
        {
            pathRoute = attirbute.Template;
        }
        else
        {
            var httpMethod = methodInfo.GetCustomAttribute<HttpMethodAttribute>();
            if (httpMethod is not null)
            {
                pathRoute = httpMethod.Template ?? string.Empty;
            }
        }

        if (!string.IsNullOrWhiteSpace(pathRoute))
        {
            if (pathRoute.StartsWith(CHAR_SLASH))
            {
                return pathPrefix + pathRoute;
            }
            else
            {
                return pathPrefix + CHAR_SLASH + pathRoute;
            }
        }

        return pathPrefix;
    }

    /// <summary>
    /// 获取当前模型对象程序集中所有可缓存类型
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <param name="type"></param>
    /// <returns></returns>
    public static List<Type> GetCacheTypes<TKey>([DisallowNull] this Type type)
    {
        var types = type.Assembly.GetTypes().Where(x => x.HasAttribute<CacheableAttribute>() && x.IsFromType<IEntitySet<TKey>>()).ToList();
        var cacheTypes = types.Where(x => x.GetProperties().All(a => !a.HasAttribute<NavigableAttribute>())).ToList();
        types = types.Where(x => !cacheTypes.Contains(x)).ToList();
        GetNavigableTypes(types, cacheTypes);
        return cacheTypes;
    }
    /// <summary>
    /// 获取类型的导航属性列表
    /// </summary>
    /// <param name="types"></param>
    /// <param name="cacheTypes"></param>
    private static void GetNavigableTypes(List<Type> types, List<Type> cacheTypes)
    {
        if (types.IsNotNullOrEmpty())
        {
            var temps = new List<Type>();
            foreach (var type in types)
            {
                if (type.GetProperties().Where(x => x.HasAttribute<NavigableAttribute>()).All(x =>
                {
                    var genericType = x.PropertyType.GetGenericType();
                    return genericType != null && cacheTypes.Contains(genericType);
                }))
                {
                    temps.Add(type);
                }
            }

            if (temps.IsNotNullOrEmpty())
            {
                foreach (var type in temps)
                {
                    types.Remove(type);
                    cacheTypes.Add(type);
                }

                GetNavigableTypes(types, cacheTypes);
            }
        }
    }
}