﻿using System.Collections;

namespace Devonline.AspNetCore.AntDesign;

/// <summary>
/// ant design data type
/// </summary>
public class AntDesignData
{
    /// <summary>
    /// data list of type
    /// </summary>
    public IEnumerable? List { get; set; }
    /// <summary>
    /// current page index
    /// </summary>
    public int? Current { get; set; }
    /// <summary>
    /// page size
    /// </summary>
    public int? PageSize { get; set; }
    /// <summary>
    /// total rows of data
    /// </summary>
    public int? Total { get; set; }

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator PagedResult(AntDesignData result) => new() { PageIndex = result.Current ?? 1, PageSize = result.PageSize ?? 0, Total = result.Total ?? 0, Data = result.List };

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator AntDesignData(PagedResult result) => new() { List = result.Data, Current = result.PageIndex, PageSize = result.PageSize, Total = result.Total };
}

/// <summary>
/// ant design data type
/// </summary>
/// <typeparam name="T"></typeparam>
public class AntDesignData<T>
{
    /// <summary>
    /// data list of type
    /// </summary>
    public IEnumerable<T>? List { get; set; }
    /// <summary>
    /// current page index
    /// </summary>
    public int? Current { get; set; }
    /// <summary>
    /// page size
    /// </summary>
    public int? PageSize { get; set; }
    /// <summary>
    /// total rows of data
    /// </summary>
    public int? Total { get; set; }

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator PagedResult<T>(AntDesignData<T> result) => new() { PageIndex = result.Current ?? 1, PageSize = result.PageSize ?? 0, Total = result.Total ?? 0, Data = result.List };

    /// <summary>
    /// 定义与 PagedResult 的强制类型转换
    /// </summary>
    /// <param name="result"></param>
    public static implicit operator AntDesignData<T>(PagedResult<T> result) => new() { List = result.Data, Current = result.PageIndex, PageSize = result.PageSize, Total = result.Total };
}