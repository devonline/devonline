﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore.AntDesign;

/// <summary>
/// 基于 DataService 的数据增删改查带导出数据到 excel 的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class AntDesignDataModelExportServiceController<TDbContext, TEntitySet, TViewModel, TKey>(
    ILogger<AntDesignDataModelExportServiceController<TDbContext, TEntitySet, TViewModel, TKey>> logger,
    IDataService<TDbContext, TEntitySet, TKey> dataService,
    IExcelExportService excelExportService) :
    DataModelExportServiceController<TDbContext, TEntitySet, TViewModel, TKey>(logger, dataService, excelExportService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TViewModel : class, IViewModel<TKey>, new()
    where TKey : IConvertible
{
    /// <summary>
    /// 查询业务对象分页列表, 查询表达式会默认取出分页及过滤等参数
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public override async Task<IActionResult> GetAsync() => Ok((AntDesignData<TEntitySet>)await _dataService.GetPagedResultAsync());
}

/// <summary>
/// 基于 DataService 的数据增删改查带导出数据到 excel 的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
[Authorize]
[ApiController]
public abstract class AntDesignDataModelExportServiceController<TDbContext, TEntitySet, TViewModel>(
    ILogger<AntDesignDataModelExportServiceController<TDbContext, TEntitySet, TViewModel>> logger,
    IDataService<TDbContext, TEntitySet> dataService,
    IExcelExportService excelExportService) :
    AntDesignDataModelExportServiceController<TDbContext, TEntitySet, TViewModel, string>(logger, dataService, excelExportService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet, new()
    where TViewModel : class, IViewModel, new()
{ }