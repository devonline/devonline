﻿namespace Devonline.AspNetCore.AntDesign;

/// <summary>
/// error display type： 0 silent; 1 message.warn; 2 message.error; 4 notification; 9 page
/// </summary>
public enum ErrorDisplayType
{
    /// <summary>
    /// silent
    /// </summary>
    Silent = 0,
    /// <summary>
    /// warn
    /// </summary>
    Warn = 1,
    /// <summary>
    /// error
    /// </summary>
    Error = 2,
    /// <summary>
    /// notification
    /// </summary>
    Notification = 4,
    /// <summary>
    /// page
    /// </summary>
    Page = 9
}