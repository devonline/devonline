﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore.AntDesign;

/// <summary>
/// 基于 DataService 的数据及视图模型增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class AntDesignDataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>(
    ILogger<AntDesignDataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>> logger,
    IDataService<TDbContext, TEntitySet, TKey> dataService) :
    DataModelServiceController<TDbContext, TEntitySet, TViewModel, TKey>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>
    where TViewModel : class, IViewModel<TKey>
    where TKey : IConvertible
{
    /// <summary>
    /// 查询业务对象分页列表, 查询表达式会默认取出分页及过滤等参数
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public override async Task<IActionResult> GetAsync() => Ok((AntDesignData<TEntitySet>)await _dataService.GetPagedResultAsync());
}

/// <summary>
/// 基于字符串作为主键的 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
[Authorize]
[ApiController]
public abstract class AntDesignDataModelServiceController<TDbContext, TEntitySet, TViewModel>(
    ILogger<AntDesignDataModelServiceController<TDbContext, TEntitySet, TViewModel>> logger,
    IDataService<TDbContext, TEntitySet> dataService) :
    AntDesignDataModelServiceController<TDbContext, TEntitySet, TViewModel, string>(logger, dataService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet
    where TViewModel : class, IViewModel
{ }