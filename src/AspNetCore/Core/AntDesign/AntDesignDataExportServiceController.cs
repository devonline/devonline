﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Devonline.AspNetCore.AntDesign;

/// <summary>
/// 基于 DataService 的数据增删改查带导出数据到 excel 的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class AntDesignDataExportServiceController<TDbContext, TEntitySet, TKey>(
    ILogger<AntDesignDataExportServiceController<TDbContext, TEntitySet, TKey>> logger,
    IDataService<TDbContext, TEntitySet, TKey> dataService,
    IExcelExportService excelExportService) :
    DataExportServiceController<TDbContext, TEntitySet, TKey>(logger, dataService, excelExportService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TKey : IConvertible
{
    /// <summary>
    /// 查询业务对象分页列表, 查询表达式会默认取出分页及过滤等参数
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public override async Task<IActionResult> GetAsync() => Ok((AntDesignData<TEntitySet>)await _dataService.GetPagedResultAsync());
}

/// <summary>
/// 基于 DataService 的数据增删改查带导出数据到 excel 的基础控制器
/// </summary>
/// <typeparam name="TDbContext">数据库上下文</typeparam>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
[Authorize]
[ApiController]
public abstract class AntDesignDataExportServiceController<TDbContext, TEntitySet>(
    ILogger<AntDesignDataExportServiceController<TDbContext, TEntitySet>> logger,
    IDataService<TDbContext, TEntitySet> dataService,
    IExcelExportService excelExportService) :
    AntDesignDataExportServiceController<TDbContext, TEntitySet, string>(logger, dataService, excelExportService)
    where TDbContext : DbContext
    where TEntitySet : class, IEntitySet, new()
{ }