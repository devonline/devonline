﻿namespace Devonline.AspNetCore.AntDesign;

/// <summary>
/// the default string data of ant design response type
/// </summary>
public class AntDesignResponse : AntDesignResponse<string> { }

/// <summary>
/// ant design response type
/// </summary>
/// <typeparam name="T"></typeparam>
public class AntDesignResponse<T>
{
    /// <summary>
    /// boolean; // if request is success
    /// </summary>
    public bool Success { get; set; }
    /// <summary>
    /// any; // response data
    /// </summary>
    public T? Data { get; set; }
    /// <summary>
    /// string; // code for errorType
    /// </summary>
    public string? ErrorCode { get; set; }
    /// <summary>
    /// string; // message display to user
    /// </summary>
    public string? ErrorMessage { get; set; }
    /// <summary>
    /// number; // error display type： 0 silent; 1 message.warn; 2 message.error; 4 notification; 9 page
    /// </summary>
    public ErrorDisplayType ShowType { get; set; }
    /// <summary>
    /// string; // Convenient for back-end Troubleshooting: unique request ID
    /// </summary>
    public string? TraceId { get; set; }
    /// <summary>
    /// string; // onvenient for backend Troubleshooting: host of current access server
    /// </summary>
    public string? Host { get; set; }
}