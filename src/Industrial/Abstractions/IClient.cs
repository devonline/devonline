﻿namespace Devonline.Industrial.Abstractions;

/// <summary>
/// 客户端接口
/// </summary>
/// <typeparam name="T">客户端传输的数据类型</typeparam>
public interface IClient<T>
{
    /// <summary>
    /// 客户端连接终端
    /// </summary>
    IClientEndpoint Endpoint { get; }
    /// <summary>
    /// 连接状态
    /// </summary>
    ConnectionState State { get; }

    /// <summary>
    /// 初始化事件处理方法, 在客户端初始化阶段(包括通过消息流的二次初始化)调用
    /// </summary>
    event EventHandler? Initial;
    /// <summary>
    /// 当客户端连接或打开前执行的事件委托, 发生于每次发起连接/打开请求时, 以及断线重连/重新打开时
    /// </summary>
    event EventHandler? Connecting;
    /// <summary>
    /// 当客户端成功连接或打开后执行的事件委托, 发生于每次发起连接/打开请求后, 以及断线重连/重新打开成功后
    /// </summary>
    event EventHandler? Connected;
    /// <summary>
    /// 通讯间隔监控触发时执行的事件处理委托方法
    /// </summary>
    event EventHandler? Monitor;
    /// <summary>
    /// 当通讯结束(包括连接终止/强制终止)时执行的事件委托
    /// </summary>
    event EventHandler? Abort;
    /// <summary>
    /// 收到来自客户端消息时执行的事件处理委托方法
    /// </summary>
    event EventHandler<T>? Receive;
    /// <summary>
    /// 当客户端发送消息时执行的事件处理委托方法
    /// </summary>
    event EventHandler<T>? Send;
    /// <summary>
    /// 通讯出现错误时执行的事件处理委托方法
    /// </summary>
    event EventHandler<Exception>? Error;

    /// <summary>
    /// 启动通讯
    /// </summary>
    /// <returns></returns>
    Task StartAsync();
    /// <summary>
    /// 停止通讯
    /// </summary>
    /// <returns></returns>
    Task StopAsync();
    /// <summary>
    /// 发送数据到通讯终端
    /// </summary>
    /// <returns></returns>
    Task SendAsync(T t);
    /// <summary>
    /// 从服务器端接受数据
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    Task ReceiveAsync(Func<T, Task> onReceive);
}

/// <summary>
/// 以字符串作为传输内容的客户端
/// </summary>
public interface IClient : IClient<string> { }

/// <summary>
/// 以二进制数据流作为传输内容的客户端
/// </summary>
public interface IStreamClient : IClient<byte[]> { }