﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Industrial.Abstractions;

/// <summary>
/// 连接状态
/// </summary>
[Description("连接状态")]
public enum ConnectionState
{
    /// <summary>
    /// 已断开
    /// </summary>
    [Display(Name = "已断开")]
    Closed,
    /// <summary>
    /// 连接中
    /// </summary>
    [Display(Name = "连接中")]
    Connecting,
    /// <summary>
    /// 已连接
    /// </summary>
    [Display(Name = "已连接")]
    Connected
}