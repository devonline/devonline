﻿using Devonline.Entity;

namespace Devonline.Industrial.Abstractions;

public interface IClientEndpoint : IEndpoint { }