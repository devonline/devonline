﻿namespace Devonline.Industrial.Abstractions;

public abstract class Client(IClientEndpoint endpoint) : IClient
{
    /// <summary>
    /// 连接终端
    /// </summary>
    public IClientEndpoint Endpoint { get; } = endpoint;
    /// <summary>
    /// 连接状态
    /// </summary>
    public ConnectionState State { get; private set; }

    public event EventHandler? Initial;
    public event EventHandler? Connecting;
    public event EventHandler? Connected;
    public event EventHandler? Monitor;
    public event EventHandler? Abort;
    public event EventHandler<string>? Receive;
    public event EventHandler<string>? Send;
    public event EventHandler<Exception>? Error;

    public Task ReceiveAsync(Func<string, Task> onReceive)
    {
        throw new NotImplementedException();
    }

    public Task SendAsync(string t)
    {
        throw new NotImplementedException();
    }

    public Task StartAsync()
    {
        throw new NotImplementedException();
    }

    public Task StopAsync()
    {
        throw new NotImplementedException();
    }
}