﻿using Devonline.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace Devonline.CloudService.Aliyun;

public static class ServiceExtensions
{
    /// <summary>
    /// 注册默认短消息服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="endpoint"></param>
    /// <returns></returns>
    public static IServiceCollection AddSmsService(this IServiceCollection services, ISmsEndpoint? endpoint = default)
    {
        if (endpoint is not null)
        {
            services.AddSingleton(endpoint);
            services.AddSingleton<ISmsService, SmsService>();
        }

        return services;
    }

    /// <summary>
    /// 注册短信验证码服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="endpoint"></param>
    /// <param name="smsModel"></param>
    /// <returns></returns>
    public static IServiceCollection AddSmsCaptchaService(this IServiceCollection services, ISmsEndpoint? endpoint = default, ISmsModel? smsModel = default)
    {
        if (endpoint is not null && smsModel is not null)
        {
            services.AddSmsService(endpoint);
            services.AddSingleton(smsModel);
            services.AddSingleton<ISmsCaptchaService, SmsCaptchaService>();
        }

        return services;
    }
}