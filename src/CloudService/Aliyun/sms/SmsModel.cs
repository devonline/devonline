﻿using Devonline.AspNetCore;
using Devonline.Core;

namespace Devonline.CloudService.Aliyun;

/// <summary>
/// 阿里云短信模型
/// </summary>
public class SmsModel : ISmsModel
{
    public SmsModel()
    {
        TemplateParam = new Dictionary<string, string>();
    }

    /// <summary>
    /// 短信签名
    /// </summary>
    public string SignName { get; set; } = null!;
    /// <summary>
    /// 短信模版编号
    /// </summary>
    public string TemplateCode { get; set; } = null!;
    /// <summary>
    /// 短信内容模板中的变量和值的集合
    /// </summary>
    public Dictionary<string, string> TemplateParam { get; set; }
    /// <summary>
    /// 接收短信的默认手机号码列表
    /// </summary>
    public string[]? PhoneNumbers { get; set; }
    /// <summary>
    /// 用途, 用于同一手机号码同一短信模板区分不同用途
    /// </summary>
    public string? Purpose { get; set; }

    /// <summary>
    /// 格式化内容的短消息模型
    /// </summary>
    /// <returns></returns>
    public override string ToString() => $"use sign name: {SignName} and template code: {TemplateCode} to send params: {TemplateParam.ToJsonString()} to phone numbers: {PhoneNumbers?.ToString<string>()}";
}