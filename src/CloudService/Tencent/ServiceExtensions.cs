﻿using System.Net;
using System.Net.Security;
using Devonline.CloudService.Tencent.Weixin;
using Devonline.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;

namespace Devonline.CloudService.Tencent;

public static class ServiceExtensions
{
    /// <summary>
    /// 注册微信公众号服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="endpoint"></param>
    /// <returns></returns>
    public static IServiceCollection AddWeixinMediaPlatformService(this IServiceCollection services, IMediaPlatformEndpoint? endpoint = default)
    {
        if (endpoint is not null)
        {
            services.AddHttpClient(nameof(WeixinService), httpClient =>
            {
                if (endpoint.Protocol == ProtocolType.Https)
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback((sender, cert, chain, errors) => true);
                }

                ArgumentNullException.ThrowIfNull(endpoint.Host);
                httpClient.BaseAddress = new Uri(endpoint.Host);
                httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, ContentType.Json);
            });

            services.AddSingleton<IMediaPlatformEndpoint>(endpoint);
            services.AddScoped<IMediaPlatformService, MediaPlatformService>();
        }

        return services;
    }
    /// <summary>
    /// 注册微信小程序服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="endpoint"></param>
    /// <returns></returns>
    public static IServiceCollection AddWeixinMiniProgramService(this IServiceCollection services, IMiniProgramEndpoint? endpoint = default)
    {
        if (endpoint is not null)
        {
            services.AddHttpClient(nameof(WeixinService), httpClient =>
            {
                if (endpoint.Protocol == ProtocolType.Https)
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback((sender, cert, chain, errors) => true);
                }

                ArgumentNullException.ThrowIfNull(endpoint.Host);
                httpClient.BaseAddress = new Uri(endpoint.Host);
                httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, ContentType.Json);
            });

            services.AddSingleton<IMiniProgramEndpoint>(endpoint);
            services.AddScoped<IMiniProgramService, MiniProgramService>();
        }

        return services;
    }
    /// <summary>
    /// 注册微信开放平台服务
    /// </summary>
    /// <param name="services"></param>
    /// <param name="endpoint"></param>
    /// <returns></returns>
    public static IServiceCollection AddWeixinOpenPlatformService(this IServiceCollection services, IOpenPlatformEndpoint? endpoint = default)
    {
        if (endpoint is not null)
        {
            services.AddHttpClient(nameof(WeixinService), httpClient =>
            {
                if (endpoint.Protocol == ProtocolType.Https)
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback((sender, cert, chain, errors) => true);
                }

                ArgumentNullException.ThrowIfNull(endpoint.Host);
                httpClient.BaseAddress = new Uri(endpoint.Host);
                httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, ContentType.Json);
            });

            services.AddSingleton<IOpenPlatformEndpoint>(endpoint);
            services.AddScoped<IOpenPlatformService, OpenPlatformService>();
        }

        return services;
    }
}