﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 事件消息
/// </summary>
public class EventMessage : WeixinMessage
{
    public EventMessage()
    {
        MsgType = Core.MessageType.Event;
    }

    /// <summary>
    /// 事件类型，subscribe(订阅/关注)、unsubscribe(取消订阅/关注), SCAN(扫码/关注), CLICK(点击菜单), VIEW(跳转连接)
    /// </summary>
    public string? Event { get; set; }
    /// <summary>
    /// 事件KEY值，qrscene_为前缀，后面为二维码的参数值
    /// </summary>
    public string? EventKey { get; set; }
    /// <summary>
    /// 二维码的ticket，可用来换取二维码图片
    /// </summary>
    public string? Ticket { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    public string? Status { get; set; }
}