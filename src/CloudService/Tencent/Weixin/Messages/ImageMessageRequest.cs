﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 回复图片消息
/// </summary>
public class ImageMessageRequest : WeixinMessage
{
    /// <summary>
    /// 图片消息
    /// </summary>
    public InternalRequest Image { get; set; } = null!;
}