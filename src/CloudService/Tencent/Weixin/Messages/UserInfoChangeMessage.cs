﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 授权用户信息变更参数消息
/// </summary>
public class UserInfoChangeMessage : WeixinMessage
{
    /// <summary>
    /// 授权用户 OpenID
    /// </summary>
    [JsonProperty("openid"), JsonPropertyName("openid"), Display(Name = "授权用户 OpenId")]
    public string OpenId { get; set; } = null!;
    /// <summary>
    /// 移动应用的 AppID
    /// </summary>
    [JsonProperty("appid"), JsonPropertyName("appid"), Display(Name = "移动应用的 AppID")]
    public string AppID { get; set; } = null!;
    /// <summary>
    /// 用户撤回的授权信息，301:用户撤回移动应用所有授权信息
    /// </summary>
    [JsonProperty("revokeinfo"), JsonPropertyName("revokeinfo"), Display(Name = "用户撤回的授权信息")]
    public string? RevokeInfo { get; set; }
}