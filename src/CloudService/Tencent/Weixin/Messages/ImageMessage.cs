﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 文本消息
/// </summary>
public class ImageMessage : MediaMessage
{
    public ImageMessage()
    {
        MsgType = Core.MessageType.Image;
    }

    /// <summary>
    /// 图片链接（由系统生成）
    /// </summary>
    public string? PicUrl { get; set; }
    /// <summary>
    /// 图片消息媒体id，可以调用获取临时素材接口拉取数据。
    /// </summary>
    public string? MediaId { get; set; }
}