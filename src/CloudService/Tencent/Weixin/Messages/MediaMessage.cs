﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 多媒体消息
/// </summary>
public class MediaMessage : WeixinMessage
{
    /// <summary>
    /// 消息的数据ID（消息如果来自文章时才有）
    /// </summary>
    public string? MsgDataId { get; set; }
    /// <summary>
    /// 多图文时第几篇文章，从1开始（消息如果来自文章时才有）
    /// </summary>
    public string? Idx { get; set; }
}