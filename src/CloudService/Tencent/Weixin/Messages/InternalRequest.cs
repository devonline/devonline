﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 消息内部结构
/// </summary>
public class InternalRequest
{
    /// 图片消息媒体id，可以调用获取临时素材接口拉取数据。
    /// </summary>
    public string? MediaId { get; set; }
}