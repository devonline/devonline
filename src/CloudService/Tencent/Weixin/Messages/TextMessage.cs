﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 文本消息
/// </summary>
public class TextMessage : MediaMessage
{
    public TextMessage()
    {
        MsgType = Core.MessageType.Text;
    }

    /// <summary>
    /// 文本消息内容
    /// </summary>
    public string? Content { get; set; }
}