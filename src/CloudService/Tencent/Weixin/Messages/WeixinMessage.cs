﻿using Devonline.Core;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信消息
/// </summary>
public class WeixinMessage
{
    /// <summary>
    /// 开发者微信号
    /// </summary>
    public string? ToUserName { get; set; }
    /// <summary>
    /// 发送方账号（一个OpenID）
    /// </summary>
    public string FromUserName { get; set; } = null!;
    /// <summary>
    /// 消息创建时间 （整型）
    /// </summary>
    public DateTime CreateTime { get; set; }
    /// <summary>
    /// 消息类型，文本为text
    /// </summary>
    public MessageType MsgType { get; set; }
    /// <summary>
    /// 消息id，64位整型
    /// </summary>
    public long MsgId { get; set; }
    /// <summary>
    /// 消息密文
    /// </summary>
    public string? Encrypt { get; set; }
}