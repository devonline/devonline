﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.CloudService.Tencent.Weixin;

[Description("微信登录类型")]
public enum WeixinLoginType
{
    /// <summary>
    /// 网页登录
    /// </summary>
    [Display(Name = "网页")]
    Website,
    /// <summary>
    /// App登录
    /// </summary>
    [Display(Name = "App")]
    App,
    /// <summary>
    /// 小程序登录
    /// </summary>
    [Display(Name = "小程序")]
    MiniProgram,
    /// <summary>
    /// 公众号登录
    /// </summary>
    [Display(Name = "公众号")]
    Offiaccount,

    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "其他")]
    Other = 99
}