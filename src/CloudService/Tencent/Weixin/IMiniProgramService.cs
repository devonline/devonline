﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信小程序接入相关接口
/// </summary>
public interface IMiniProgramService : IMediaPlatformService
{
    /// <summary>
    ///小程序GetSessionKey
    /// docs:
    /// miniprogram: https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-login/code2Session.html
    /// </summary>
    /// <param name="code">code</param>
    /// <returns></returns>
    Task SessionKeyAuthorizeAsync(string code);
}