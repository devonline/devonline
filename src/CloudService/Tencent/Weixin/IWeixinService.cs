﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信接入相关接口
/// </summary>
public interface IWeixinService
{
    /// <summary>
    /// 微信登录
    /// docs:
    /// app: https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_Login/Development_Guide.html
    /// web: https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Wechat_Login.html
    /// </summary>
    /// <param name="code"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    /// <exception cref="UnauthorizedAccessException"></exception>
    Task AuthorizeAsync(string code, string? state = default);
    /// <summary>
    /// 获取 access token
    /// docs:
    /// 公众号: https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_access_token.html
    /// 小程序: https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/mp-access-token/getAccessToken.html
    /// </summary>
    /// <param name="code">code</param>
    /// <returns></returns>
    Task<WeixinAccessToken> GetAccessTokenAsync(string? code = default);
    /// <summary>
    /// 获取稳定版 access token
    /// docs: 
    /// 公众号: https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/getStableAccessToken.html
    /// 小程序: https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/mp-access-token/getStableAccessToken.html
    /// </summary>
    /// 是否强制获取, 默认使用 false。
    /// 1. force_refresh = false 时为普通调用模式，access_token 有效期内重复调用该接口不会更新 access_token；
    /// 2. 当force_refresh = true 时为强制刷新模式，会导致上次获取的 access_token 失效，并返回新的 access_token
    /// </param>
    /// <returns></returns>
    Task<WeixinAccessToken> GetStableAccessTokenAsync(bool? force = false);
    /// <summary>
    /// 获取刷新 token
    /// </summary>
    /// <returns></returns>
    Task<WeixinAccessToken> GetRefreshTokenAsync();

    /// <summary>
    /// 获取用户信息
    /// 微信公众平台需要先从客户端提交用户信息到服务器, 服务器端进行查找后, 在返回客户端
    /// 微信开放平台调用会直接从微信服务器端获取用户信息, 并进行查找后, 在返回客户端
    /// </summary>
    /// <param name="accessToken">access token</param>
    /// <param name="openId">微信用户的 openid</param>
    /// <returns></returns>
    Task<WeixinUserInfo?> GetUserInfoAsync(string accessToken, string openId);
}