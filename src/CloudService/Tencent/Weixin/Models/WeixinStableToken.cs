﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 获取稳定版 token 的请求参数
/// </summary>
internal class WeixinStableToken
{
    /// <summary>
    /// 账号唯一凭证，即 AppID，可在「微信公众平台 - 设置 - 开发设置」页中获得。（需要已经成为开发者，且账号没有异常状态）
    /// </summary>
    [JsonProperty("app_id"), JsonPropertyName("app_id"), DisplayName("应用编号")]
    public string AppId { get; set; } = null!;
    /// <summary>
    /// 账号唯一凭证密钥，即 AppSecret，获取方式同 appid
    /// </summary>
    [JsonProperty("secret"), JsonPropertyName("secret"), DisplayName("应用密码")]
    public string Secret { get; set; } = null!;
    /// <summary>
    /// 授权类型
    /// 填写 client_credential
    /// </summary>
    [JsonProperty("grant_type"), JsonPropertyName("grant_type"), DisplayName("授权类型")]
    public string GrantType { get; set; } = "client_credential";
    /// <summary>
    /// 默认使用 false。
    /// 1. force_refresh = false 时为普通调用模式，access_token 有效期内重复调用该接口不会更新 access_token；
    /// 2. 当force_refresh = true 时为强制刷新模式，会导致上次获取的 access_token 失效，并返回新的 access_token
    /// </summary>
    [JsonProperty("force_refresh"), JsonPropertyName("force_refresh"), Display(Name = "是否强制获取")]
    public bool ForceRefresh { get; set; } = false;
}