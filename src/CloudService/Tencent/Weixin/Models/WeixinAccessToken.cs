﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// access token
/// </summary>
public class WeixinAccessToken
{
    /// <summary>
    /// 接口调用凭证
    /// </summary>
    [JsonProperty("access_token"), JsonPropertyName("access_token"), Display(Name = "接口调用凭证")]
    public string AccessToken { get; set; } = null!;
    /// <summary>
    /// access_token 接口调用凭证超时时间，单位（秒）
    /// </summary>
    [JsonProperty("expires_in"), JsonPropertyName("expires_in"), Display(Name = "超时时间")]
    public int ExpiresIn { get; set; }
    /// <summary>
    /// 用户刷新 access_token
    /// </summary>
    [JsonProperty("refresh_token"), JsonPropertyName("用户刷新 access_token"), Display(Name = "用户标识")]
    public string RefreshToken { get; set; } = null!;
    /// <summary>
    /// 授权用户唯一标识
    /// </summary>
    [JsonProperty("openid"), JsonPropertyName("openid"), Display(Name = "授权用户唯一标识")]
    public string OpenId { get; set; } = null!;
    /// <summary>
    /// 用户授权的作用域，使用逗号（,）分隔
    /// </summary>
    [JsonProperty("scope"), JsonPropertyName("scope"), Display(Name = "用户授权的作用域")]
    public string Scope { get; set; } = null!;
    /// <summary>
    /// 当且仅当该网站应用已获得该用户的userinfo授权时，才会出现该字段。
    /// </summary>
    [JsonProperty("unionid"), JsonPropertyName("unionid"), Display(Name = "统一标识")]
    public string? UnionId { get; set; }
}
