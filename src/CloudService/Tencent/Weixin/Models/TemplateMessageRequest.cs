﻿using System.Text.Json.Serialization;
using Devonline.Core;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 模板消息请求参数
/// </summary>
public class TemplateMessageRequest
{
    [JsonProperty("touser"), JsonPropertyName("touser")]
    public string ToUser { get; set; } = null!;
    [JsonProperty("template_id"), JsonPropertyName("template_id")]
    public string TemplateId { get; set; } = null!;
    [JsonProperty("client_msg_id"), JsonPropertyName("client_msg_id")]
    public string ClientMessageId { get; set; } = KeyGenerator.GetStringKey();
    /// <summary>
    /// 详情地址
    /// </summary>
    public string? Url { get; set; }

    /// <summary>
    /// 模板消息参数
    /// </summary>
    public Dictionary<string, TemplateMessageParam> Data { get; set; } = null!;
}

/// <summary>
/// 模板消息模板数据
/// </summary>
public class TemplateMessageParam
{
    /// <summary>
    /// 模板消息的参数值
    /// </summary>
    public string Value { get; set; } = null!;
}