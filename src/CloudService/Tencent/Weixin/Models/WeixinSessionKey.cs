﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// access token
/// </summary>
public class WeixinSessionKey
{
    /// <summary>
    /// 会话密钥
    /// </summary>
    [JsonProperty("session_key"), JsonPropertyName("session_key"), Display(Name = "会话密钥")]
    public string SessionKey { get; set; } = null!;
    /// <summary>
    /// 当且仅当该网站应用已获得该用户的userinfo授权时，才会出现该字段。
    /// </summary>
    [JsonProperty("unionid"), JsonPropertyName("unionid"), Display(Name = "统一标识")]
    public string? UnionId { get; set; }
    /// <summary>
    /// 错误信息
    /// </summary>
    [JsonProperty("errmsg"), JsonPropertyName("errmsg"), Display(Name = "错误信息")]
    public string? ErrMsg { get; set; }
    /// <summary>
    /// 授权用户唯一标识
    /// </summary>
    [JsonProperty("openid"), JsonPropertyName("openid"), Display(Name = "授权用户唯一标识")]
    public string OpenId { get; set; } = null!;
    /// <summary>
    /// 错误信息
    /// </summary>
    [JsonProperty("errcode"), JsonPropertyName("errcode"), Display(Name = "错误码")]
    public int ErrCode { get; set; }
}
