﻿using Devonline.Entity;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 模板消息基类模型
/// </summary>
public class TemplateMessage : ViewModel
{
    /// <summary>
    /// 模板名称
    /// </summary>
    public string TemplateName { get; set; } = null!;
    /// <summary>
    /// 公众号平台账号
    /// </summary>
    public string Account { get; set; } = null!;
    /// <summary>
    /// 微信的 OpenId
    /// </summary>
    public string OpenId { get; set; } = null!;
    /// <summary>
    /// 详情地址
    /// </summary>
    public string? Url { get; set; }
}