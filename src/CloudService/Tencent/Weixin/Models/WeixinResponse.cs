﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信接口返回值
/// </summary>
public class WeixinResponse
{
    [JsonProperty("errcode"), JsonPropertyName("errcode")]
    public int ErrorCode { get; set; }
    [JsonProperty("errmsg"), JsonPropertyName("errmsg")]
    public string? ErrorMessage { get; set; }
    [JsonProperty("msgid"), JsonPropertyName("msgid")]
    public long MessageId { get; set; }
}