﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Devonline.Core;
using Newtonsoft.Json;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信用户信息
/// 内容参考微信开放平台用户信息接口返回值
/// {
///   "openid": "OPENID",
///   "nickname": "NICKNAME",
///   "sex": 1,
///   "province": "PROVINCE",
///   "city": "CITY",
///   "country": "COUNTRY",
///   "headimgurl": "https://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
///   "privilege": ["PRIVILEGE1", "PRIVILEGE2"],
///   "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"
/// }
/// </summary>
public class WeixinUserInfo
{
    /// <summary>
    /// 用户标识，对当前开发者帐号唯一
    /// </summary>
    [JsonProperty("openid"), JsonPropertyName("openid"), Display(Name = "用户标识")]
    public string? OpenId { get; set; }
    /// <summary>
    /// 昵称
    /// </summary>
    [JsonProperty("nickname"), JsonPropertyName("nickname"), Display(Name = "昵称")]
    public string? NickName { get; set; }
    /// <summary>
    /// 1 为男性，2 为女性
    /// </summary>
    [JsonProperty("sex"), JsonPropertyName("sex"), Display(Name = "性别")]
    public int Sex
    {
        get
        {
            return (int)Gender;
        }
        set
        {
            if (value == 1)
            {
                Gender = Gender.Male;
            }
            else
            {
                Gender = Gender.Female;
            }
        }
    }
    /// <summary>
    /// 性别
    /// </summary>
    [JsonProperty("gender"), JsonPropertyName("gender"), Display(Name = "性别")]
    public Gender Gender { get; private set; }
    /// <summary>
    /// 省份
    /// </summary>
    [JsonProperty("province"), JsonPropertyName("province"), Display(Name = "省份")]
    public string? Province { get; set; }
    /// <summary>
    /// 城市
    /// </summary>
    [JsonProperty("city"), JsonPropertyName("city"), Display(Name = "城市")]
    public string? City { get; set; }
    /// <summary>
    /// 国籍
    /// </summary>
    [JsonProperty("country"), JsonPropertyName("country"), Display(Name = "国籍")]
    public string? Country { get; set; }
    /// <summary>
    /// 微信头像
    /// </summary>
    [JsonProperty("headimgurl"), JsonPropertyName("headimgurl"), Display(Name = "微信头像")]
    public string? HeadImage { get; set; }
    /// <summary>
    /// 统一标识
    /// 针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。
    /// 开发者最好保存 unionID 信息，以便以后在不同应用之间进行用户信息互通。
    /// </summary>
    [JsonProperty("unionid"), JsonPropertyName("unionid"), Display(Name = "统一标识")]
    public string? UnionId { get; set; }
    /// <summary>
    /// 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
    /// </summary>
    [JsonProperty("privilege"), JsonPropertyName("privilege"), Display(Name = "用户特权信息")]
    public string[]? Privilege { get; set; }
}