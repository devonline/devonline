﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Xml.Linq;
using Devonline.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信公众平台接入接口
/// </summary>
/// <param name="logger"></param>
/// <param name="endpoint">微信服务器接口配置</param>
/// <param name="httpContextAccessor"></param>
/// <param name="cache"></param>
/// <param name="httpClientFactory">http client 工厂, 需要在全局配置微信服务器接口地址</param>
public class MediaPlatformService(
    ILogger<MediaPlatformService> logger,
    IMediaPlatformEndpoint endpoint,
    IHttpContextAccessor httpContextAccessor,
    IDistributedCache cache,
    IHttpClientFactory httpClientFactory
    ) : WeixinService(logger, endpoint, httpContextAccessor, cache, httpClientFactory), IMediaPlatformService, IWeixinService
{
    protected readonly IMediaPlatformEndpoint _platformEndpoint = endpoint;
    protected readonly HttpRequest _request = httpContextAccessor.HttpContext!.Request;

    /// <summary>
    /// 微信服务器认证
    /// 用于开始交互前, 微信服务器调用应用服务器, 用以验证服务器配置有效
    /// docs: https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Access_Overview.html
    /// </summary>
    /// <param name="signature">微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。</param>
    /// <param name="timestamp">时间戳</param>
    /// <param name="nonce">随机数</param>
    /// <param name="echostr">随机字符串</param>
    /// <returns></returns>
    public virtual string Authenticate(string signature, string timestamp, string nonce, string echostr)
    {
        //string? signature = _request.GetRequestOption<string>(nameof(signature));
        //string? timestamp = _request.GetRequestOption<string>(nameof(timestamp));
        //string? nonce = _request.GetRequestOption<string>(nameof(nonce));
        //string? echostr = _request.GetRequestOption<string>(nameof(echostr));
        //if (string.IsNullOrWhiteSpace(signature) || string.IsNullOrWhiteSpace(timestamp) || string.IsNullOrWhiteSpace(nonce) || string.IsNullOrWhiteSpace(echostr))
        //{
        //    throw new UnauthorizedAccessException(nameof(signature));
        //}

        ArgumentException.ThrowIfNullOrWhiteSpace(_endpoint.Token);
        _logger.LogDebug($"即将进行微信服务器认证, nonce: {nonce}, echostr: {echostr}");
        var signArray = new string[] { _endpoint.Token, timestamp, nonce }.OrderBy(x => x);
        var signString = string.Join(string.Empty, signArray);
        var signResult = SHA1.Create().GetHashValue(signString).ToHexString(char.MinValue);
        var result = signResult.Equals(signature, StringComparison.InvariantCultureIgnoreCase);

        _logger.LogInformation("微信服务器认证" + (result ? "完成" : "失败, 签名不符!"));
        return result ? echostr : throw new UnauthorizedAccessException();
    }

    /// <summary>
    /// 接受微信消息
    /// </summary>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public virtual async Task<WeixinMessage?> OnReceiveMessageAsync()
    {
        _logger.LogDebug("收到来自微信的消息!");
        using var stream = new StreamReader(_request.Body);
        var xml = await stream.ReadToEndAsync();
        if (string.IsNullOrWhiteSpace(xml))
        {
            _logger.LogWarning("微信消息内容为空!");
            return null;
        }

        _logger.LogInformation("收到来自微信的消息: " + Environment.NewLine + xml);
        var doc = XDocument.Parse(xml);
        var nodes = doc.Descendants();
        var typeNode = nodes.FirstOrDefault(x => x.Name == nameof(WeixinMessage.MsgType));
        var messageType = string.IsNullOrWhiteSpace(typeNode?.Value) ? MessageType.Text : Enum.Parse<MessageType>(typeNode.Value, true);

        WeixinMessage message = messageType switch
        {
            MessageType.Image => new ImageMessage(),
            MessageType.Event => new EventMessage(),
            _ => new TextMessage()
        };

        _logger.LogDebug($"已读取 msgType 节点值为: {typeNode?.Value}, 解析为枚举值: {messageType}, 已创建类型为: {message.MsgType.GetDisplayName()} 的微信消息!");
        foreach (var node in nodes)
        {
            if (node != typeNode)
            {
                message.SetPropertyValue(node.Name.LocalName, node.Value, true);
            }
        }

        message.CreateTime = DateTime.SpecifyKind(message.CreateTime, DateTimeKind.Utc);

        _logger.LogDebug("收到来自微信的消息解析后的内容为: " + message.ToJsonString());
        return message;
    }
    /// <summary>
    /// 发送模板消息
    /// 模板消息接口的 http request body 仅支持 StringContent 且为: text/plain 类型
    /// </summary>
    /// <param name="message">模板消息</param>
    /// <returns></returns>
    public virtual async Task SendTemplateMessageAsync(TemplateMessage message)
    {
        var messageTemplate = _platformEndpoint.MessageTemplates.FirstOrDefault(x => x.Name.Equals(message.TemplateName, StringComparison.InvariantCultureIgnoreCase));
        if (messageTemplate is null)
        {
            _logger.LogError("尚未配置消息模板: " + message.TemplateName);
            return;
        }

        _logger.LogInformation($"将使用消息模板 {message.TemplateName} 向 {message.OpenId} 发送模板消息: {message.Id}");
        messageTemplate = messageTemplate.Copy();
        var templateMessage = new TemplateMessageRequest
        {
            ToUser = message.OpenId!,
            TemplateId = messageTemplate.Id,
            ClientMessageId = message.Id,
            Data = []
        };

        if (!string.IsNullOrWhiteSpace(message.Url))
        {
            //使用 damain 配置项作为主域名地址
            templateMessage.Url = _endpoint.Domain + message.Url;
        }

        _logger.LogDebug("将根据配置项, 构造模板消息参数!");
        var attributes = message.GetType().GetFieldAttributes();
        if (!attributes.Any())
        {
            _logger.LogError($"消息模板: {message.TemplateName} 使用的数据对象模型配置有误!");
            return;
        }

        foreach (var attribute in attributes)
        {
            var value = attribute.Property.GetValue(message);
            if (value is not null && value is IConvertible convertibleValue)
            {
                templateMessage.Data.Add(attribute.Name, new TemplateMessageParam { Value = (convertibleValue is DateTime dateTime) ? dateTime.ToString(attribute.Format) : (convertibleValue.To<string>() ?? string.Empty) });
            }
        }

        _logger.LogDebug("将发起模板消息调用请求!");
        using var httpClient = _httpClientFactory.CreateClient(nameof(WeixinService));
        var accessToken = await GetAccessTokenAsync();
        var url = $"/cgi-bin/message/template/send?access_token={accessToken}";
        using var httpContent = new StringContent(templateMessage.ToJsonString(), new MediaTypeHeaderValue(ContentType.Txt));
        using var httpResponse = await httpClient.PostAsync(url, httpContent);
        if (httpResponse.IsSuccessStatusCode)
        {
            var response = await httpResponse.Content.ReadFromJsonAsync<WeixinResponse>() ?? throw new Exception($"使用消息模板 {message.TemplateName} 向 {message.OpenId} 发送模板消息: {message.Id} 返回值转换失败!");
            if (response.ErrorCode == AppSettings.UNIT_ZERO)
            {
                _logger.LogInformation($"使用消息模板 {message.TemplateName} 向 {message.OpenId} 发送模板消息: {message.Id} 成功!");
                return;
            }

            throw new Exception($"使用消息模板 {message.TemplateName} 向 {message.OpenId} 发送模板消息: {message.Id} 失败, 错误码: {response.ErrorCode}, 错误描述: {response.ErrorMessage}");
        }

        throw new Exception($"使用消息模板 {message.TemplateName} 向 {message.OpenId} 发送模板消息: {message.Id} 请求失败, HTTP 状态码: {httpResponse.StatusCode}, 错误详情: " + await httpResponse.Content.ReadAsStringAsync());
    }
}