﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信公众平台接入相关接口
/// </summary>
public interface IMediaPlatformService : IWeixinService
{
    /// <summary>
    /// 微信服务器认证
    /// 用于微信公众号开始交互前, 微信服务器调用应用服务器, 用以验证服务器配置有效
    /// docs: https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Access_Overview.html
    /// </summary>
    /// <param name="signature">微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。</param>
    /// <param name="timestamp">时间戳</param>
    /// <param name="nonce">随机数</param>
    /// <param name="echostr">随机字符串</param>
    /// <returns></returns>
    string Authenticate(string signature, string timestamp, string nonce, string echostr);

    /// <summary>
    /// 接受微信消息
    /// </summary>
    /// <returns></returns>
    Task<WeixinMessage?> OnReceiveMessageAsync();
    /// <summary>
    /// 发送模板消息
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    Task SendTemplateMessageAsync(TemplateMessage message);
}