﻿namespace Devonline.CloudService.Tencent.Weixin;

/// <summary>
/// 微信开放平台接入相关接口
/// </summary>
public interface IOpenPlatformService : IWeixinService;