﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// parameter.s67c9c78c.service_kind	string	是	请求功能类型，取值范围为：face_compare
/// </summary>
public class RequestParameter
{
    /// <summary>
    /// parameter.s67c9c78c.service_kind	string	是	请求功能类型，取值范围为：face_compare
    /// </summary>
    public ParameterS67c9c78cModel S67c9c78c => new();
}

/// <summary>
/// parameter.s67c9c78c.service_kind	string	是	请求功能类型，取值范围为：face_compare
/// </summary>
public class ParameterS67c9c78cModel
{
    /// <summary>
    /// parameter.s67c9c78c.service_kind	string	是	请求功能类型，取值范围为：face_compare
    /// </summary>
    [JsonPropertyName("service_kind")]
    public string ServiceKind => "face_compare";
    /// <summary>
    /// parameter.s67c9c78c.face_compare_result	object	是	用于上传响应数据参数
    /// </summary>
    [JsonPropertyName("face_compare_result")]
    public RequestResult FaceCompareResult => new();
}

/// <summary>
/// parameter.s67c9c78c.face_compare_result	object	是	用于上传响应数据参数
/// </summary>
public class RequestResult
{
    /// <summary>
    /// parameter.s67c9c78c.face_compare_result.encoding	string	否	文本编码，可选值：utf8（默认值）
    /// </summary>
    public string Encoding => "utf8";
    /// <summary>
    /// parameter.s67c9c78c.face_compare_result.compress	string	否	文本压缩格式，可选值：raw（默认值）
    /// </summary>
    public string Compress => "raw";
    /// <summary>
    /// parameter.s67c9c78c.face_compare_result.format	string	否	文本格式，可选值：json（默认值）
    /// </summary>
    public string Format => "json";
}