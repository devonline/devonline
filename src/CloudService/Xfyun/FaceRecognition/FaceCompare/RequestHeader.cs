﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// header	object	是	用于上传平台参数
/// <paramref name="appId"></paramref>
/// </summary>
public class RequestHeader(string appId)
{
    /// <summary>
    /// header.app_id	string	是	在平台申请的appid信息
    /// </summary>
    [JsonPropertyName("app_id")]
    public string AppId => appId;
    /// <summary>
    /// header.status	string	是	请求状态，取值范围为：3（一次传完）
    /// </summary>
    public int Status => 3;
}