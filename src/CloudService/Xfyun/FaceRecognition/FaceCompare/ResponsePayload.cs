﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// payload	object	数据段，用于携带响应的数据
/// </summary>
public class ResponsePayload
{
    [JsonPropertyName("face_compare_result")]
    public ResponseResult? FaceCompareResult { get; set; }
}