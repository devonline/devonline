﻿using System.Globalization;
using Devonline.AspNetCore;
using Devonline.Entity;

namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// 人脸比对终结点设置
/// </summary>
public interface IFaceCompareEndpoint : IAuthEndpoint
{
    /// <summary>
    /// 主键
    /// </summary>
    string Key { get; set; }
    /// <summary>
    /// 鉴权 Host 参数
    /// </summary>
    string AuthorizationHost { get; set; }
    /// <summary>
    /// 鉴权 request-line 参数
    /// </summary>
    string RequestLine { get; set; }
    /// <summary>
    /// 鉴权 date 参数
    /// </summary>
    string AuthorizationDate { get; }
    /// <summary>
    /// 人脸比对阈值, 根据算法建议, 设定的门槛值
    /// </summary>
    float FaceCompareThresholdValue { get; set; }
    /// <summary>
    /// 图片文件限制
    /// </summary>
    FileSetting ImageLimit { get; set; }
}

/// <summary>
/// 人脸比对终结点设置
/// </summary>
public class FaceCompareEndpoint : AuthEndpoint, IFaceCompareEndpoint
{
    public FaceCompareEndpoint()
    {
        FaceCompareThresholdValue = 0.67f;
        ImageLimit = new()
        {
            Enable = true,
            Size = DEFAULT_IMAGE_CROP_SIZE,
            Length = UNIT_MEGA,
            Total = UNIT_MEGA,
            Prefix = DEFAULT_IMAGE_CROP_PREFIX,
            Extensions = DEFAULT_IMAGE_FILE_EXTENSIONS
        };
    }

    /// <summary>
    /// 主键
    /// </summary>
    public string Key { get; set; } = null!;

    /// <summary>
    /// 鉴权 Host 参数
    /// </summary>
    public string AuthorizationHost { get; set; } = "api.xf-yun.com";
    /// <summary>
    /// 鉴权 request-line 参数
    /// </summary>
    public string RequestLine { get; set; } = "POST /v1/private/s67c9c78c HTTP/1.1";
    /// <summary>
    /// 鉴权 date 参数
    /// </summary>
    public string AuthorizationDate => DateTime.UtcNow.ToString(DateTimeFormatInfo.InvariantInfo.RFC1123Pattern, CultureInfo.CreateSpecificCulture("en-US"));
    /// <summary>
    /// 人脸比对阈值, 根据算法建议, 设定的门槛值
    /// </summary>
    public float FaceCompareThresholdValue { get; set; }
    /// <summary>
    /// 图片文件限制
    /// </summary>
    public FileSetting ImageLimit { get; set; }
}