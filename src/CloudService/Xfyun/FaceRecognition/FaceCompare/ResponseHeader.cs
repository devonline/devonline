﻿namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// header	object	协议头部，用于描述平台特性的参数
/// </summary>
public class ResponseHeader
{
    /// <summary>
    /// code	int	返回码，0表示成功，其它表示异常，详情请参考错误码。
    /// </summary>
    public int Code { get; set; }
    /// <summary>
    /// message	string	描述信息
    /// </summary>
    public string? Message { get; set; }
    /// <summary>
    /// sid	string	本次会话的id，只在第一帧请求时返回
    /// </summary>
    public string? Sid { get; set; }
}