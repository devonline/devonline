﻿namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// payload.face_compare_result	object	人脸比对响应数据块
/// </summary>
public class ResponseResult
{
    /// <summary>
    /// payload.face_compare_result.compress	string	文本压缩格式，仅在设置了parameter.s67c9c78c.face_compare_result.compress参数时返回
    /// </summary>
    public string? Encoding { get; set; }
    /// <summary>
    /// payload.face_compare_result.encoding	string	文本编码，仅在设置了parameter.s67c9c78c.face_compare_result.encoding参数时返回
    /// </summary>
    public string? Compress { get; set; }
    /// <summary>
    /// payload.face_compare_result.format	string	文本格式，仅在设置了parameter.s67c9c78c.face_compare_result.format参数时返回
    /// </summary>
    public string? Format { get; set; }
    /// <summary>
    /// payload.face_compare_result.text	string	人脸比对返回结果，需要对其进行base64解码，解码后的返回字段如下
    /// </summary>
    public string Text { get; set; } = null!;
    /// <summary>
    /// 比对结果
    /// </summary>
    public ResultModel? Result => System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Text)).ToJsonObject<ResultModel>();
}