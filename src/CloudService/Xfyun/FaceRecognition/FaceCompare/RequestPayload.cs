﻿namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// payload	object	是	用于上传请求数据
/// </summary>
public class RequestPayload(string image1, string image2)
{
    /// <summary>
    /// payload.input1	object	是	用于上传第一张图像数据
    /// </summary>
    public PayloadInput Input1 => new(image1);
    /// <summary>
    /// payload.input2	object	是	用于上传第二张图像数据
    /// </summary>
    public PayloadInput Input2 => new(image2);
}

/// <summary>
/// payload.input	object	是	用于上传一张图像数据
/// </summary>
public class PayloadInput(string image)
{
    /// <summary>
    /// payload.input.encoding	string	否	图像编码
    /// 可选值：
    /// jpg:jpg格式(默认值)
    /// jpeg:jpeg格式
    /// png:png格式
    /// bmp:bmp格式
    /// </summary>
    public string Encoding => Path.GetExtension(image)[UNIT_ONE..];
    /// <summary>
    /// payload.input.image	string	是	第一张图像数据，base64编码，需保证图像文件大小base64编码后不超过4MB
    /// </summary>
    public string Image => Convert.ToBase64String(File.ReadAllBytes(image));
    /// <summary>
    /// payload.input.status	int	否	第一张数据状态，取值范围为：3（一次传完）
    /// </summary>
    public int Status => 3;
}