﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// payload.face_compare_result	object	人脸比对响应数据块
/// </summary>
public class ResultModel
{
    /// <summary>
    /// ret	int	内部服务返回值	ret=0表示请求成功，否则请参考错误码表
    /// </summary>
    [JsonPropertyName("ret")]
    public int Return { get; set; }
    /// <summary>
    /// score	float	人脸相似度	最小值:0 最大值:1。
    /// 建议高于0.67认为是同一个人。
    /// 建议使用仅包含一张人脸的照片进行比对，若照片中含有多张人脸，引擎会选择其中人脸置信度最高的人脸进行比较，可能会影响比对结果。
    /// </summary>
    public float Score { get; set; }
}