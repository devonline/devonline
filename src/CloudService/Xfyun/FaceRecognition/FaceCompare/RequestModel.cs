﻿namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// 人脸对比请求模型
/// </summary>
/// <param name="appId"></param>
/// <param name="image1"></param>
/// <param name="image2"></param>
public class RequestModel(string appId, string image1, string image2)
{
    /// <summary>
    /// header	object	是	用于上传平台参数
    /// </summary>
    public RequestHeader Header => new(appId);
    /// <summary>
    /// parameter.s67c9c78c	object	是	用于上传功能参数
    /// </summary>
    public RequestParameter Parameter => new();
    /// <summary>
    /// payload	object	是	用于上传请求数据
    /// </summary>
    public RequestPayload Payload => new(image1, image2);
}