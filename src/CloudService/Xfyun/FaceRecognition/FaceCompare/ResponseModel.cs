﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;

/// <summary>
/// 人脸比对返回值
/// </summary>
public class ResponseModel
{
    /// <summary>
    /// header	object	协议头部，用于描述平台特性的参数
    /// </summary>
    [JsonPropertyName("header")]
    public ResponseHeader Header { get; set; } = null!;
    /// <summary>
    /// payload	object	数据段，用于携带响应的数据
    /// </summary>
    [JsonPropertyName("payload")]
    public ResponsePayload? Payload { get; set; }
}