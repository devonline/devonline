﻿global using Devonline.Core;
global using static Devonline.Core.AppSettings;
using System.Net;
using System.Net.Security;
using Devonline.AspNetCore;
using Devonline.CloudService.Xfyun.FaceRecognition.FaceCompare;
using Devonline.CloudService.Xfyun.IdCard;
using Devonline.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;

namespace Devonline.CloudService.Xfyun;

public static class ServiceExtensions
{
    /// <summary>
    /// 添加请求头
    /// </summary>
    /// <typeparam name="TParam"></typeparam>
    /// <param name="request"></param>
    /// <param name="endpoint"></param>
    /// <param name="param"></param>
    internal static void AddRequestHeaders<TParam>(this HttpRequestMessage request, IAuthEndpoint endpoint, TParam param) where TParam : class => new AuthorizationModel<TParam>(endpoint, param).AddHeaders(request.Headers);

    /// <summary>
    /// 注册身份证在线识别接口
    /// </summary>
    /// <param name="services">依赖注入服务容器</param>
    /// <param name="endpoint">身份证相关的云接口接入终结点</param>
    /// <returns></returns>
    public static IServiceCollection AddIdCardService<TDbContext>(this IServiceCollection services, IIdCardEndpoint? endpoint = default) where TDbContext : DbContext
    {
        if (endpoint is not null)
        {
            services.AddSingleton(endpoint);
            services.AddHttpClient<IdCardService<TDbContext>>(httpClient =>
            {
                if (endpoint.Protocol == ProtocolType.Https)
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback((sender, cert, chain, errors) => true);
                }

                ArgumentNullException.ThrowIfNull(endpoint.Host);
                httpClient.BaseAddress = new Uri(endpoint.Host);
                httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, ContentType.Json);
            });

            services.AddScoped<IIdCardService, IdCardService<TDbContext>>();
        }

        return services;
    }

    /// <summary>
    /// 人脸比对云接口
    /// </summary>
    /// <param name="services">依赖注入服务容器</param>
    /// <param name="endpoint">人脸比对接口接入终结点</param>
    /// <returns></returns>
    public static IServiceCollection AddFaceCompareService(this IServiceCollection services, IFaceCompareEndpoint? endpoint = default)
    {
        if (endpoint is not null)
        {
            services.AddSingleton(endpoint);
            services.AddHttpClient<FaceCompareService>(nameof(FaceCompareService), httpClient =>
            {
                if (endpoint.Protocol == ProtocolType.Https)
                {
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback((sender, cert, chain, errors) => true);
                }

                ArgumentNullException.ThrowIfNull(endpoint.Host);
                httpClient.BaseAddress = new Uri(endpoint.Host);
                httpClient.DefaultRequestHeaders.Add(HeaderNames.Accept, ContentType.Json);
            });

            services.AddScoped<IFaceRecognitionService, FaceCompareService>();
        }

        return services;
    }
}