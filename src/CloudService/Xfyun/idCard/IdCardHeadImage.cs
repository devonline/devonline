﻿namespace Devonline.CloudService.Xfyun.IdCard;

/// <summary>
/// 适用于 讯飞 的身份证识别结果对象模型
/// 身份证头像返回值内容
/// </summary>
public class IdCardHeadImage
{
    public float Height { get; set; }
    public float Width { get; set; }
    public float Top { get; set; }
    public float Left { get; set; }

    /// <summary>
    /// head_portrait.image	身份证正面头像截图	身份证正面头像截图，base64编码
    /// </summary>
    public string? Image { get; set; }
}