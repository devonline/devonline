﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.IdCard;

/// <summary>
/// 适用于 讯飞 的身份证识别结果对象模型
/// 身份证接口请求参数
/// </summary>
public class IdCardParam
{
    /// <summary>
    /// engine_type	string	是	引擎类型，固定为idcard
    /// </summary>
    [JsonPropertyName("engine_type")]
    public string EngineType { get; set; } = "idcard";
    /// <summary>
    /// head_portrait	string	否	是否返回头像图片：默认head_portrait=0，即不返回头像图片，head_portrait=1，则返回身份证头像照片（Base64编码）
    /// </summary>
    [JsonPropertyName("head_portrait")]
    public string? HeadPortrait { get; set; } = "1";
    /// <summary>
    /// crop_image	string	否	是否返回切片图，默认crop_image=0，1表示返回身份证切片照片（Base64编码）
    /// </summary>
    [JsonPropertyName("crop_image")]
    public string? CropImage { get; set; } = "1";
    /// <summary>
    /// id_number_image	string	否	是否返回身份证号码区域截图，默认id_number_image=0，即不返回身份号码区域截图，1表示返回证件号区域截图（Base64编码）
    /// </summary>
    [JsonPropertyName("id_number_image")]
    public string? IdCodeImage { get; set; }
    /// <summary>
    /// recognize_mode	string	否	是否先对图片进行切片后再识别，默认recognize_mode=0，即直接对图片进行识别，1表示采用先切片后识别的模式
    /// </summary>
    [JsonPropertyName("recognize_mode")]
    public string? RecognizeMode { get; set; }
}