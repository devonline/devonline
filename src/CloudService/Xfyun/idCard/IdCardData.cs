﻿using System.Text.Json.Serialization;

namespace Devonline.CloudService.Xfyun.IdCard;

/// <summary>
/// 适用于 讯飞 的身份证识别结果对象模型
/// 正面返回值
/// </summary>
public class IdCardData
{
    public const string IDCARD_TYPE_FRONT = "第二代身份证";
    public const string IDCARD_TYPE_BACK = "第二代身份证背面";

    /// <summary>
    /// name	姓名	身份证上的姓名识别结果（正面）
    /// </summary>
    public string? Name { get; set; }
    /// <summary>
    /// id_number	身份证号	身份证上的身份证号码识别结果（正面）
    /// </summary>
    [JsonPropertyName("id_number")]
    public string? IdCode { get; set; }
    /// <summary>
    /// birthday	出生日期	身份证上的出生日期识别结果（正面）
    /// </summary>
    public string? BirthDay { get; set; }
    /// <summary>
    /// sex	性别	身份证上的性别识别结果（正面）
    /// </summary>
    public string? Sex { get; set; }
    /// <summary>
    /// people	民族	身份证上的民族识别结果（正面）
    /// </summary>
    [JsonPropertyName("people")]
    public string? Nation { get; set; }
    /// <summary>
    /// address	住址	身份证上的住址识别结果（正面）
    /// </summary>
    public string? Address { get; set; }
    /// <summary>
    /// issue_authority	签发机关	身份证上的签发机关识别结果（反面）
    /// </summary>
    [JsonPropertyName("issue_authority")]
    public string? IssuedBy { get; set; }
    /// <summary>
    /// validity	有效期	身份证上的有效期识别结果（反面）
    /// </summary>
    public string? Validity { get; set; }
    /// <summary>
    /// cropped_image	身份证切边图片	身份证正面或反面的切边图片，base64编码
    /// </summary>
    [JsonPropertyName("cropped_image")]
    public string? CroppedImage { get; set; }
    /// <summary>
    /// id_number_image	身份证号码截图	身份证正面身份证号截图，base64编码
    /// </summary>
    [JsonPropertyName("id_number_image")]
    public string? IdCodeImage { get; set; }
    /// <summary>
    /// head_portrait	身份证正面头像信息	身份证正面头像信息，json类型
    /// </summary>
    [JsonPropertyName("head_portrait")]
    public IdCardHeadImage? HeadPortrait { get; set; }
    /// <summary>
    /// type	类型	身份证正反面类型
    /// 当是身份证正面时，type=第二代身份证
    /// 当是身份证背面时，type=第二代身份证背面
    /// 当是临时身份证时，type=临时身份证。
    /// </summary>
    public string? Type { get; set; }
    /// <summary>
    /// gray_image	黑白图像	gray_image=true 则表示证件判断为黑白
    /// </summary>
    [JsonPropertyName("gray_image")]
    public bool GrayImage { get; set; }
    /// <summary>
    /// border_covered	边缘遮挡	border_covered=true 则表示证件边缘判断为不完整
    /// </summary>
    [JsonPropertyName("border_covered")]
    public bool BorderCovered { get; set; }
    /// <summary>
    /// head_blurred	头像模糊	head_blurred =true 则表示证件头像判断模糊
    /// </summary>
    [JsonPropertyName("head_blurred")]
    public bool HeadBlurred { get; set; }
    ///// <summary>
    ///// head_covered	头像模糊	head_blurred =true 则表示证件头像判断模糊
    ///// </summary>
    //[JsonProperty("head_covered")]
    //public bool HeadCovered { get; set; }
    /// <summary>
    /// error_code	错误码	识别错误码
    /// </summary>
    [JsonPropertyName("error_code")]
    public int ErrorCode { get; set; }
    /// <summary>
    /// error_msg	错误信息	错误原因描述
    /// </summary>
    [JsonPropertyName("error_msg")]
    public string? ErrorMessage { get; set; }
}