﻿using Devonline.AspNetCore;
using Devonline.Entity;

namespace Devonline.CloudService.Xfyun.IdCard;

/// <summary>
/// 身份证识别终结点设置
/// </summary>
public interface IIdCardEndpoint : IAuthEndpoint
{
    /// <summary>
    /// 图片文件限制
    /// </summary>
    FileSetting ImageLimit { get; set; }
}

/// <summary>
/// 身份证识别终结点设置
/// </summary>
public class IdCardEndpoint : AuthEndpoint, IIdCardEndpoint
{
    public IdCardEndpoint()
    {
        ImageLimit = new()
        {
            Enable = true,
            Size = DEFAULT_IMAGE_CROP_SIZE,
            Length = UNIT_MEGA,
            Total = UNIT_MEGA,
            Prefix = DEFAULT_IMAGE_CROP_PREFIX,
            Extensions = DEFAULT_IMAGE_FILE_EXTENSIONS
        };
    }

    /// <summary>
    /// 图片文件限制
    /// </summary>
    public FileSetting ImageLimit { get; set; }
}