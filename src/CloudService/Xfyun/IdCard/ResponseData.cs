﻿namespace Devonline.CloudService.Xfyun.IdCard;

/// <summary>
/// 适用于 讯飞 的 身份证识别结果对象模型 接口调用返回值
/// </summary>
/// <typeparam name="T">对象类型</typeparam>
public class ResponseData<T> where T : class
{
    /// <summary>
    /// code	响应状态    0 为成功
    /// </summary>
    public string? Code { get; set; }
    /// <summary>
    ///
    /// </summary>
    public string? Desc { get; set; }
    /// <summary>
    ///
    /// </summary>
    public string? Sid { get; set; }

    /// <summary>
    /// 成功时为数据信息
    /// </summary>
    public T? Data { get; set; }
}