﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Devonline.Identity.Database.PostgreSQL.Identity
{
    /// <inheritdoc />
    public partial class InitialIdentity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "attachment",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    business_type = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    business_key = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    length = table.Column<long>(type: "bigint", nullable: false),
                    extension = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: false),
                    content_type = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    path = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_attachment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "level",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    value = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_level", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "parameter",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    index = table.Column<int>(type: "integer", nullable: false),
                    key = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    value = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    text = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    parent_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_parameter", x => x.id);
                    table.ForeignKey(
                        name: "FK_parameter_parameter_parent_id",
                        column: x => x.parent_id,
                        principalTable: "parameter",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "region",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    index = table.Column<int>(type: "integer", nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    code = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    parent_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_region", x => x.id);
                    table.ForeignKey(
                        name: "FK_region_region_parent_id",
                        column: x => x.parent_id,
                        principalTable: "region",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    alias = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    image = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    normalized_name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    concurrency_stamp = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "resource",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    system_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    parent_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    owner_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    identity_type = table.Column<string>(type: "varchar(16)", nullable: false),
                    resource_type = table.Column<string>(type: "varchar(16)", nullable: false),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    code = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    title = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    content = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: false),
                    access_level = table.Column<string>(type: "varchar(16)", nullable: false),
                    level_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_resource", x => x.id);
                    table.ForeignKey(
                        name: "FK_resource_level_level_id",
                        column: x => x.level_id,
                        principalTable: "level",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_resource_resource_parent_id",
                        column: x => x.parent_id,
                        principalTable: "resource",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_resource_resource_system_id",
                        column: x => x.system_id,
                        principalTable: "resource",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "group",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    parent_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    level_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    region_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group", x => x.id);
                    table.ForeignKey(
                        name: "FK_group_group_parent_id",
                        column: x => x.parent_id,
                        principalTable: "group",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_group_level_level_id",
                        column: x => x.level_id,
                        principalTable: "level",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_group_region_region_id",
                        column: x => x.region_id,
                        principalTable: "region",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "role_claim",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    role_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    claim_type = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    claim_value = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_claim", x => x.id);
                    table.ForeignKey(
                        name: "FK_role_claim_role_role_id",
                        column: x => x.role_id,
                        principalTable: "role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "access_rule",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    resource_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    identity_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    identity_type = table.Column<string>(type: "varchar(16)", nullable: false),
                    code = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    is_allow = table.Column<string>(type: "varchar(16)", nullable: false),
                    priority = table.Column<int>(type: "integer", nullable: false),
                    expire_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    access_count = table.Column<int>(type: "integer", nullable: false),
                    condition = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_access_rule", x => x.id);
                    table.ForeignKey(
                        name: "FK_access_rule_resource_resource_id",
                        column: x => x.resource_id,
                        principalTable: "resource",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    group_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    level_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    lockout_end = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    two_factor_enabled = table.Column<bool>(type: "boolean", nullable: false),
                    phone_number_confirmed = table.Column<bool>(type: "boolean", nullable: false),
                    phone_number = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    concurrency_stamp = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    security_stamp = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    password_hash = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    email_confirmed = table.Column<bool>(type: "boolean", nullable: false),
                    normalized_email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    normalized_user_name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    user_name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    lockout_enabled = table.Column<bool>(type: "boolean", nullable: false),
                    access_failed_count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_group_group_id",
                        column: x => x.group_id,
                        principalTable: "group",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_user_level_level_id",
                        column: x => x.level_id,
                        principalTable: "level",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "access_apply",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    resource_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    access_rule_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    apply_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    authorizer_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    authorized_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    is_allow = table.Column<string>(type: "varchar(16)", nullable: false),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_access_apply", x => x.id);
                    table.ForeignKey(
                        name: "FK_access_apply_access_rule_access_rule_id",
                        column: x => x.access_rule_id,
                        principalTable: "access_rule",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_access_apply_resource_resource_id",
                        column: x => x.resource_id,
                        principalTable: "resource",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_access_apply_user_authorizer_id",
                        column: x => x.authorizer_id,
                        principalTable: "user",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_access_apply_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "oauth_user",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    gender = table.Column<string>(type: "varchar(8)", nullable: false),
                    auth_type = table.Column<string>(type: "varchar(16)", nullable: false),
                    open_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    region_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    union_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    privileges = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_oauth_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_oauth_user_region_region_id",
                        column: x => x.region_id,
                        principalTable: "region",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_oauth_user_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "real_name_info",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    updated_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    description = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    alias = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    type = table.Column<string>(type: "varchar(36)", nullable: false),
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    is_authed = table.Column<bool>(type: "boolean", nullable: false),
                    auth_phase = table.Column<string>(type: "varchar(16)", nullable: false),
                    id_code = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    phone_number = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    captcha = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    send_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    validate_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    face_image = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    auth_video = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    auth_video_thumbnail = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    face_match_score = table.Column<float>(type: "numeric(18,12)", nullable: true),
                    verify_result = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    phone_number_validate_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    id_card_validate_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    face_compare_validate_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    face_detection_validate_time = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    phone_number_validate_failed_count = table.Column<int>(type: "integer", nullable: false),
                    id_card_validate_failed_count = table.Column<int>(type: "integer", nullable: false),
                    face_compare_validate_failed_count = table.Column<int>(type: "integer", nullable: false),
                    face_detection_validate_failed_count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_real_name_info", x => x.id);
                    table.ForeignKey(
                        name: "FK_real_name_info_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "user_claim",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    claim_type = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    claim_value = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_claim", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_claim_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_group",
                columns: table => new
                {
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    group_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    role_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_group", x => new { x.user_id, x.group_id });
                    table.ForeignKey(
                        name: "FK_user_group_group_group_id",
                        column: x => x.group_id,
                        principalTable: "group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user_group_role_role_id",
                        column: x => x.role_id,
                        principalTable: "role",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_user_group_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_login",
                columns: table => new
                {
                    login_provider = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    provider_key = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    provider_display_name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_login", x => new { x.login_provider, x.provider_key });
                    table.ForeignKey(
                        name: "FK_user_login_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_role",
                columns: table => new
                {
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    role_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_role", x => new { x.user_id, x.role_id });
                    table.ForeignKey(
                        name: "FK_user_role_role_role_id",
                        column: x => x.role_id,
                        principalTable: "role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_user_role_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_token",
                columns: table => new
                {
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    login_provider = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    name = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    value = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_token", x => new { x.user_id, x.login_provider, x.name });
                    table.ForeignKey(
                        name: "FK_user_token_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "access_record",
                columns: table => new
                {
                    id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    row_version = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    state = table.Column<string>(type: "varchar(16)", nullable: false, defaultValue: "Available"),
                    created_on = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    created_by = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    user_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    resource_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    access_rule_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    access_apply_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: true),
                    access_count = table.Column<int>(type: "integer", nullable: false),
                    allowed_count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_access_record", x => x.id);
                    table.ForeignKey(
                        name: "FK_access_record_access_apply_access_apply_id",
                        column: x => x.access_apply_id,
                        principalTable: "access_apply",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_access_record_access_rule_access_rule_id",
                        column: x => x.access_rule_id,
                        principalTable: "access_rule",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "FK_access_record_resource_resource_id",
                        column: x => x.resource_id,
                        principalTable: "resource",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_access_record_user_user_id",
                        column: x => x.user_id,
                        principalTable: "user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_access_apply_access_rule_id",
                table: "access_apply",
                column: "access_rule_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_apply_authorizer_id",
                table: "access_apply",
                column: "authorizer_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_apply_resource_id",
                table: "access_apply",
                column: "resource_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_apply_user_id",
                table: "access_apply",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_record_access_apply_id",
                table: "access_record",
                column: "access_apply_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_record_access_rule_id",
                table: "access_record",
                column: "access_rule_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_record_resource_id",
                table: "access_record",
                column: "resource_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_record_user_id",
                table: "access_record",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_access_rule_resource_id",
                table: "access_rule",
                column: "resource_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_level_id",
                table: "group",
                column: "level_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_name",
                table: "group",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_group_parent_id",
                table: "group",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_region_id",
                table: "group",
                column: "region_id");

            migrationBuilder.CreateIndex(
                name: "IX_oauth_user_open_id",
                table: "oauth_user",
                column: "open_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_oauth_user_region_id",
                table: "oauth_user",
                column: "region_id");

            migrationBuilder.CreateIndex(
                name: "IX_oauth_user_user_id",
                table: "oauth_user",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_parameter_index",
                table: "parameter",
                column: "index",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_parameter_parent_id",
                table: "parameter",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_real_name_info_id_code",
                table: "real_name_info",
                column: "id_code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_real_name_info_phone_number",
                table: "real_name_info",
                column: "phone_number");

            migrationBuilder.CreateIndex(
                name: "IX_real_name_info_user_id",
                table: "real_name_info",
                column: "user_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_region_parent_id",
                table: "region",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_resource_code",
                table: "resource",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_resource_level_id",
                table: "resource",
                column: "level_id");

            migrationBuilder.CreateIndex(
                name: "IX_resource_parent_id",
                table: "resource",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_resource_system_id",
                table: "resource",
                column: "system_id");

            migrationBuilder.CreateIndex(
                name: "IX_role_name",
                table: "role",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "role",
                column: "normalized_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_role_claim_role_id",
                table: "role_claim",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_email",
                table: "user",
                column: "normalized_email");

            migrationBuilder.CreateIndex(
                name: "IX_user_group_id",
                table: "user",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_level_id",
                table: "user",
                column: "level_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_phone_number",
                table: "user",
                column: "phone_number");

            migrationBuilder.CreateIndex(
                name: "IX_user_user_name",
                table: "user",
                column: "normalized_user_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_user_name1",
                table: "user",
                column: "user_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_claim_user_id",
                table: "user_claim",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_group_group_id",
                table: "user_group",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_group_role_id",
                table: "user_group",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_login_user_id",
                table: "user_login",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_role_role_id",
                table: "user_role",
                column: "role_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "access_record");

            migrationBuilder.DropTable(
                name: "attachment");

            migrationBuilder.DropTable(
                name: "oauth_user");

            migrationBuilder.DropTable(
                name: "parameter");

            migrationBuilder.DropTable(
                name: "real_name_info");

            migrationBuilder.DropTable(
                name: "role_claim");

            migrationBuilder.DropTable(
                name: "user_claim");

            migrationBuilder.DropTable(
                name: "user_group");

            migrationBuilder.DropTable(
                name: "user_login");

            migrationBuilder.DropTable(
                name: "user_role");

            migrationBuilder.DropTable(
                name: "user_token");

            migrationBuilder.DropTable(
                name: "access_apply");

            migrationBuilder.DropTable(
                name: "role");

            migrationBuilder.DropTable(
                name: "access_rule");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "resource");

            migrationBuilder.DropTable(
                name: "group");

            migrationBuilder.DropTable(
                name: "level");

            migrationBuilder.DropTable(
                name: "region");
        }
    }
}