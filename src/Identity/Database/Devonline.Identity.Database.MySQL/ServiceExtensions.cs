﻿using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Devonline.AspNetCore;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Storage;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Devonline.Identity.Database.MySQL;

public static class ServiceExtensions
{
    /// <summary>
    /// 注册默认的 MySQL IdentityServer
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="httpSetting"></param>
    /// <returns></returns>
    public static IIdentityServerBuilder AddMySQLIdentityServer(this IHostApplicationBuilder builder, HttpSetting httpSetting)
    {
        var service = builder.Services.AddMySQL(httpSetting);
        var identityServerBuilder = service.AddIdentityServer(options =>
        {
            ArgumentNullException.ThrowIfNull(httpSetting.UserInteraction);
            options.UserInteraction.LoginUrl = httpSetting.UserInteraction.Login;
            options.UserInteraction.LogoutUrl = httpSetting.UserInteraction.Logout;
            options.UserInteraction.ErrorUrl = httpSetting.UserInteraction.Error;
            //options.Cors.CorsPolicyName = DEFAULT_CORS_POLICY;
            //var cors = httpSetting.CorsOrigins!.Split(CHAR_COMMA, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            //foreach (var item in cors)
            //{
            //    options.Cors.CorsPaths.Add(item);
            //}
            //options.Cors.CorsPaths.Add("/api/Account/SendCaptcha");
        })
        .AddAspNetIdentity<User>()
        .AddConfigurationStore<ConfigurationDbContext>(options => options.ConfigureDbContext = db => db.Build(httpSetting.IdentityDbContext))
        .AddOperationalStore(options =>
        {
            options.ConfigureDbContext = db => db.Build(httpSetting.IdentityDbContext);
            options.EnableTokenCleanup = true;
        });

        if (builder.Environment.IsDevelopment())
        {
            identityServerBuilder.AddDeveloperSigningCredential();
        }
        else
        {
            ArgumentException.ThrowIfNullOrWhiteSpace(httpSetting.Certificate?.Path);
            identityServerBuilder.AddSigningCredential(new X509Certificate2(httpSetting.Certificate.Path, httpSetting.Certificate.Password));
        }

        return identityServerBuilder;
    }

    /// <summary>
    /// 注册认证数据库上下文
    /// </summary>
    /// <param name="services">依赖注入服务容器</param>
    /// <param name="contextLifetime">数据库上下文的生命周期</param>
    /// <returns></returns>
    public static IServiceCollection AddMySQL(this IServiceCollection services, HttpSetting httpSetting, ServiceLifetime contextLifetime = ServiceLifetime.Scoped)
    {
        if (!string.IsNullOrWhiteSpace(httpSetting.IdentityDbContext))
        {
            var assembly = Assembly.GetExecutingAssembly().FullName;
            services.AddDbContext<IdentityDbContext>(builder => builder.Build(httpSetting.IdentityDbContext), contextLifetime);
            services.AddConfigurationDbContext(options => options.ConfigureDbContext = builder => builder.Build(httpSetting.IdentityDbContext));
            services.AddOperationalDbContext(options => options.ConfigureDbContext = builder => builder.Build(httpSetting.IdentityDbContext));
        }

        return services;
    }
    /// <summary>
    /// 注册基于安全模型的认证数据库上下文
    /// </summary>
    /// <param name="services">依赖注入服务容器</param>
    /// <param name="contextLifetime">数据库上下文的生命周期</param>
    /// <returns></returns>
    public static IServiceCollection AddMySQLSecurity(this IServiceCollection services, HttpSetting httpSetting, ServiceLifetime contextLifetime = ServiceLifetime.Scoped)
    {
        if (!string.IsNullOrWhiteSpace(httpSetting.IdentityDbContext))
        {
            services.AddDbContext<SecurityIdentityDbContext>(builder => builder.UseMySql(httpSetting.IdentityDbContext, ServerVersion.AutoDetect(httpSetting.IdentityDbContext), options => options.UseMicrosoftJson().MigrationsAssembly(Assembly.GetExecutingAssembly().FullName)), contextLifetime);
        }

        return services;
    }

    /// <summary>
    /// 注册数据库上下文创建对象
    /// </summary>
    /// <param name="builder">依赖注入服务容器</param>
    /// <param name="connectionString">数据库连接字符串</param>
    /// <returns></returns>
    private static DbContextOptionsBuilder Build(this DbContextOptionsBuilder builder, string? connectionString = default)
    {
        return builder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString), options => options.UseMicrosoftJson().MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
    }
}