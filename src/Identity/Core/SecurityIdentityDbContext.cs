﻿using Microsoft.EntityFrameworkCore;

namespace Devonline.Identity;

/// <summary>
/// 重载的安全身份上下文, 提供字符串类型主键的默认实现
/// </summary>
public class SecurityIdentityDbContext(DbContextOptions<SecurityIdentityDbContext> options) : SecurityIdentityDbContext<User, Role, UserClaim, UserRole, UserLogin, RoleClaim, UserToken, RealNameInfo, OAuthUser, Group, UserGroup, Level, Region, Resource, AccessRule, AccessApply, AccessRecord, Parameter, Attachment, Certificate, Secret, DataSecret, ResourceSecret, string>(options);

/// <summary>
/// 重载的安全身份上下文, 提供字符串类型主键的默认实现
/// </summary>
/// <typeparam name="TKey"></typeparam>
public class SecurityIdentityDbContext<TKey>(DbContextOptions<SecurityIdentityDbContext<TKey>> options) : SecurityIdentityDbContext<User<TKey>, Role<TKey>, UserClaim<TKey>, UserRole<TKey>, UserLogin<TKey>, RoleClaim<TKey>, UserToken<TKey>, RealNameInfo<TKey>, OAuthUser<TKey>, Group<TKey>, UserGroup<TKey>, Level<TKey>, Region<TKey>, Resource<TKey>, AccessRule<TKey>, AccessApply<TKey>, AccessRecord<TKey>, Parameter<TKey>, Attachment<TKey>, Certificate<TKey>, Secret<TKey>, DataSecret<TKey>, ResourceSecret<TKey>, TKey>(options) where TKey : IEquatable<TKey>, IConvertible;

/// <summary>
/// 重载的安全身份上下文
/// </summary>
/// <typeparam name="TUser"></typeparam>
/// <typeparam name="TRole"></typeparam>
/// <typeparam name="TUserClaim"></typeparam>
/// <typeparam name="TUserRole"></typeparam>
/// <typeparam name="TUserLogin"></typeparam>
/// <typeparam name="TRoleClaim"></typeparam>
/// <typeparam name="TUserToken"></typeparam>
/// <typeparam name="TRealNameInfo"></typeparam>
/// <typeparam name="TOAuthUser"></typeparam>
/// <typeparam name="TGroup"></typeparam>
/// <typeparam name="TUserGroup"></typeparam>
/// <typeparam name="TLevel"></typeparam>
/// <typeparam name="TRegion"></typeparam>
/// <typeparam name="TResource"></typeparam>
/// <typeparam name="TAccessRule"></typeparam>
/// <typeparam name="TAccessApply"></typeparam>
/// <typeparam name="TAccessRecord"></typeparam>
/// <typeparam name="TParameter"></typeparam>
/// <typeparam name="TAttachment"></typeparam>
/// <typeparam name="TCertificate"></typeparam>
/// <typeparam name="TSecret"></typeparam>
/// <typeparam name="TDataSecret"></typeparam>
/// <typeparam name="TResourceSecret"></typeparam>
/// <typeparam name="TKey"></typeparam>
public class SecurityIdentityDbContext<TUser, TRole, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken, TRealNameInfo, TOAuthUser, TGroup, TUserGroup, TLevel, TRegion, TResource, TAccessRule, TAccessApply, TAccessRecord, TParameter, TAttachment, TCertificate, TSecret, TDataSecret, TResourceSecret, TKey>(DbContextOptions options) :
    IdentityDbContext<TUser, TRole, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken, TRealNameInfo, TOAuthUser, TGroup, TUserGroup, TLevel, TRegion, TResource, TAccessRule, TAccessApply, TAccessRecord, TParameter, TAttachment, TKey>(options)
    where TUser : User<TKey>
    where TRole : Role<TKey>
    where TUserClaim : UserClaim<TKey>
    where TUserRole : UserRole<TKey>
    where TUserLogin : UserLogin<TKey>
    where TRoleClaim : RoleClaim<TKey>
    where TUserToken : UserToken<TKey>
    where TRealNameInfo : RealNameInfo<TKey>
    where TOAuthUser : OAuthUser<TKey>
    where TGroup : Group<TKey>
    where TUserGroup : UserGroup<TKey>
    where TLevel : Level<TKey>
    where TRegion : Region<TKey>
    where TResource : Resource<TKey>
    where TAccessRule : AccessRule<TKey>
    where TAccessApply : AccessApply<TKey>
    where TAccessRecord : AccessRecord<TKey>
    where TParameter : Parameter<TKey>
    where TAttachment : Attachment<TKey>
    where TCertificate : Certificate<TKey>
    where TSecret : Secret<TKey>
    where TDataSecret : DataSecret<TKey>
    where TResourceSecret : ResourceSecret<TKey>
    where TKey : IEquatable<TKey>, IConvertible
{
    #region security identity entities
    /// <summary>
    /// Certificate
    /// </summary>
    public virtual DbSet<TCertificate> Certificates => Set<TCertificate>();
    /// <summary>
    /// Secret
    /// </summary>
    public virtual DbSet<TSecret> Secrets => Set<TSecret>();
    /// <summary>
    /// DataSecret
    /// </summary>
    public virtual DbSet<TDataSecret> DataSecrets => Set<TDataSecret>();
    /// <summary>
    /// ResourceSecret
    /// </summary>
    public virtual DbSet<TResourceSecret> ResourceSecrets => Set<TResourceSecret>();
    #endregion

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<TCertificate>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TSecret>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TDataSecret>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
        builder.Entity<TResourceSecret>(b => b.Property(x => x.State).HasDefaultValue(DataState.Available));
    }
}