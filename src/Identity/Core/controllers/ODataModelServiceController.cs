﻿using Devonline.AspNetCore.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 基于 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataModelServiceController<TEntitySet, TViewModel, TKey>(
    ILogger<ODataModelServiceController<TEntitySet, TViewModel, TKey>> logger,
    IDataService<TEntitySet, TKey> dataService) :
    ODataModelServiceController<IdentityDbContext, TEntitySet, TViewModel, TKey>(logger, dataService)
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TViewModel : class, IViewModel<TKey>, new()
    where TKey : IConvertible;

/// <summary>
/// 基于字符串作为主键的 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TViewModel">业务数据类型的视图模型类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataModelServiceController<TEntitySet, TViewModel>(
    ILogger<ODataModelServiceController<TEntitySet, TViewModel>> logger,
    IDataService<TEntitySet> dataService) :
    ODataModelServiceController<TEntitySet, TViewModel, string>(logger, dataService)
    where TEntitySet : class, IEntitySet, new()
    where TViewModel : class, IViewModel, new();