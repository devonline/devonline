﻿using Devonline.AspNetCore.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 基于 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataServiceController<TEntitySet, TKey>(
    ILogger<ODataServiceController<TEntitySet, TKey>> logger,
    IDataService<TEntitySet, TKey> dataService) :
    ODataServiceController<IdentityDbContext, TEntitySet, TKey>(logger, dataService)
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TKey : IConvertible;

/// <summary>
/// 基于字符串作为主键的 OData 和 DataService 的数据增删改查的基础控制器
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
[Authorize]
[ApiController]
public abstract class ODataServiceController<TEntitySet>(
    ILogger<ODataServiceController<TEntitySet>> logger,
    IDataService<TEntitySet> dataService) :
    ODataServiceController<TEntitySet, string>(logger, dataService)
    where TEntitySet : class, IEntitySet, new();