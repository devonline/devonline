﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Devonline.Identity;

/// <summary>
/// 密钥
/// </summary>
public class DefaultKey : EntitySet
{
    /// <summary>
    /// 密钥
    /// </summary>
    [Column("key"), DisplayName("密钥"), Excel]
    public string? Key { get; set; }
    /// <summary>
    /// 创建时间
    /// </summary>
    [Column("create_time"), DisplayName("创建时间"), Excel]
    public DateTime CreateTime { get; set; }
    /// <summary>
    /// 过期时间, 密钥的有效期内
    /// </summary>
    [Column("expire_time"), DisplayName("过期时间"), Excel]
    public DateTime? ExpireTime { get; set; }
}
