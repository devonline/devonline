﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

/// <summary>
/// 用户的访问令牌 字符串类型的默认实现
/// </summary>
[Table("user_token"), DisplayName("用户访问令牌")]
public class UserToken : UserToken<string>;

/// <summary>
/// 用户访问令牌
/// </summary>
[Table("user_token"), DisplayName("用户访问令牌")]
public class UserToken<TKey> : IdentityUserToken<TKey> where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// 用户编号
    /// </summary>
    [Column("user_id"), DisplayName("用户编号"), Key, Required, MaxLength(36), Excel]
    public override TKey UserId { get; set; } = default!;
    //
    // 摘要:
    //     Gets or sets the LoginProvider this token is from.
    [Column("login_provider"), DisplayName("登录提供者"), Key, Required, MaxLength(36), Excel]
    public override string LoginProvider { get; set; } = null!;
    //
    // 摘要:
    //     Gets or sets the name of the token.
    [Column("name"), DisplayName("访问令牌的名字"), Key, Required, MaxLength(36), Excel]
    public override string Name { get; set; } = null!;
    //
    // 摘要:
    //     Gets or sets the token value.
    [ProtectedPersonalData]
    [Column("value"), DisplayName("访问令牌的值"), MaxLength(1024), Excel]
    public override string? Value { get; set; }
}