﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Devonline.Identity;

/// <summary>
/// 用户组织, 字符串类型的默认实现
/// </summary>
[Table("user_group"), DisplayName("用户组织")]
public class UserGroup : UserGroup<string>;

/// <summary>
/// 用户组织
/// </summary>
[Table("user_group"), DisplayName("用户组织")]
public class UserGroup<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 用户编号
    /// </summary>
    [Column("user_id"), DisplayName("用户编号"), Key, Required, MaxLength(36), Excel]
    public virtual TKey UserId { get; set; } = default!;
    /// <summary>
    /// 组织编号
    /// </summary>
    [Column("group_id"), DisplayName("组织编号"), Key, Required, MaxLength(36), Excel]
    public virtual TKey GroupId { get; set; } = default!;
    /// <summary>
    /// 角色编号
    /// 角色是用户在组织中的身份
    /// </summary>
    [Column("role_id"), DisplayName("角色编号"), MaxLength(36), Excel]
    public virtual TKey? RoleId { get; set; }
}