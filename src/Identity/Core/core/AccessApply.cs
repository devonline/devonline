﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Devonline.Identity;

/// <summary>
/// 访问申请, 字符串类型的默认实现
/// 暂时无法满足命中多条访问规则的情况
/// </summary>
[Table("access_apply"), DisplayName("访问申请")]
public class AccessApply : AccessApply<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 申请用户
    /// </summary>
    public virtual User? User { get; set; }
    /// <summary>
    /// 申请的资源
    /// </summary>
    public virtual Resource? Resource { get; set; }
    /// <summary>
    /// 对应的访问规则, 最匹配的一条规则
    /// </summary>
    public virtual AccessRule? AccessRule { get; set; }
    /// <summary>
    /// 授权者
    /// </summary>
    public virtual User? Authorizer { get; set; }
}

/// <summary>
/// 访问申请
/// </summary>
/// <typeparam name="TKey"></typeparam>
[Table("access_apply"), DisplayName("访问申请")]
public class AccessApply<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible, IEquatable<TKey>
{
    /// <summary>
    /// 申请者编号
    /// </summary>
    [Column("user_id"), DisplayName("申请者编号"), Required, MaxLength(36), Excel]
    public virtual TKey UserId { get; set; } = default!;
    /// <summary>
    /// 资源编号
    /// </summary>
    [Column("resource_id"), DisplayName("资源编号"), Required, MaxLength(36), Excel]
    public virtual TKey ResourceId { get; set; } = default!;
    /// <summary>
    /// 访问规则编号, 最匹配的一条规则
    /// </summary>
    [Column("access_rule_id"), DisplayName("访问规则编号"), MaxLength(36), Excel]
    public virtual TKey? AccessRuleId { get; set; }
    /// <summary>
    /// 申请时间
    /// </summary>
    [Column("apply_time"), DisplayName("申请时间"), Excel]
    public virtual DateTime ApplyTime { get; set; }
    /// <summary>
    /// 授权人编号
    /// </summary>
    [Column("authorizer_id"), DisplayName("访问规则编号"), MaxLength(36), Excel]
    public virtual TKey? AuthorizerId { get; set; }
    /// <summary>
    /// 授权时间
    /// </summary>
    [Column("authorized_time"), DisplayName("访问规则编号"), Excel]
    public virtual DateTime? AuthorizedTime { get; set; }
    /// <summary> 
    /// 是否允许访问 AllowType 枚举值
    /// 申请阶段值为: Authorize, 意为待授权, 授权完毕为 Allow 和 Forbid 其中之一
    /// </summary>
    [Column("is_allow", TypeName = "varchar(16)"), DisplayName("是否允许"), DefaultValue(AllowType.Allow), Excel]
    public virtual AllowType IsAllow { get; set; }
    /// <summary>
    /// 描述, 受理意见等
    /// </summary>
    [Column("description"), DisplayName("描述"), MaxLength(255), Excel]
    public virtual string? Description { get; set; }
}