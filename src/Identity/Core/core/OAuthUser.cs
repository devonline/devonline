﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Devonline.Identity;

/// <summary>
/// 基于 OAuth 的认证用户信息, 字符主键的默认实现
/// OAuth 用户信息每个用户可能有多个
/// </summary>
[Table("oauth_user"), DisplayName("基于 OAuth 的认证用户"), Index(nameof(OpenId), IsUnique = true)]
public class OAuthUser : OAuthUser<string>, IIdentity, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 用户
    /// </summary>
    public virtual User? User { get; set; }
    /// <summary>
    /// 所在区域
    /// </summary>
    public virtual Region? Region { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped Attachments 用于记录实体对象上上传的附件
    /// </summary>
    [NotMapped]
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 基于 OAuth 的认证用户信息
/// OAuth 用户信息每个用户可能有多个
/// </summary>
[Table("oauth_user"), DisplayName("基于 OAuth 的认证用户"), Index(nameof(OpenId), IsUnique = true)]
public abstract class OAuthUser<TKey> : Identity<TKey>, IIdentity<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 性别
    /// </summary>
    [Column("gender", TypeName = "varchar(8)"), DisplayName("性别"), Required, DefaultValue(Gender.Male), Excel(Size = DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual Gender Gender { get; set; } = Gender.Male;
    /// <summary>
    /// 认证方式
    /// </summary>
    [Column("auth_type", TypeName = "varchar(16)"), DisplayName("认证方式"), DefaultValue(AuthType.Weixin), Excel]
    public virtual AuthType AuthType { get; set; } = AuthType.Weixin;
    /// <summary>
    /// OpenId
    /// </summary>
    [Column("open_id"), DisplayName("OpenId"), Unique, Required, MaxLength(36), Excel]
    public virtual string OpenId { get; set; } = null!;
    /// <summary>
    /// 用户编号
    /// </summary>
    [Column("user_id"), DisplayName("用户编号"), MaxLength(36), Excel]
    public virtual TKey? UserId { get; set; }
    /// <summary>
    /// 地区, 所在区域
    /// </summary>
    [Column("region_id"), DisplayName("所在区域"), MaxLength(36), Excel]
    public virtual TKey? RegionId { get; set; }
    /// <summary>
    /// 统一标识
    /// 针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。
    /// 开发者最好保存 unionID 信息，以便以后在不同应用之间进行用户信息互通。
    /// </summary>
    [Column("union_id"), Display(Name = "统一标识"), MaxLength(36), Excel]
    public virtual string? UnionId { get; set; }
    /// <summary>
    /// 用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
    /// </summary>
    [Column("privileges"), Display(Name = "用户特权信息"), MaxLength(1024), Excel]
    public virtual string? Privileges { get; set; }
}
