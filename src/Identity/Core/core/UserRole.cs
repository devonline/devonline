﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

/// <summary>
/// 用户角色, 字符串类型的默认实现
/// </summary>
[Table("user_role"), DisplayName("用户角色")]
public class UserRole : UserRole<string>;

/// <summary>
/// 用户角色
/// </summary>
[Table("user_role"), DisplayName("用户角色")]
public class UserRole<TKey> : IdentityUserRole<TKey> where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// 用户编号
    /// </summary>
    [Column("user_id"), DisplayName("用户编号"), Key, Required, MaxLength(36), Excel]
    public override TKey UserId { get; set; } = default!;
    /// <summary>
    /// 角色编号
    /// </summary>
    [Column("role_id"), DisplayName("角色编号"), Key, Required, MaxLength(36), Excel]
    public override TKey RoleId { get; set; } = default!;
}