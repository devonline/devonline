﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

/// <summary>
/// 用户申明, 字符串类型的默认实现
/// </summary>
[Table("user_claim"), DisplayName("用户申明")]
public class UserClaim : UserClaim<string>, IEntitySet<int>;

/// <summary>
/// 用户申明
/// </summary>
[Table("user_claim"), DisplayName("用户申明")]
public class UserClaim<TKey> : IdentityUserClaim<TKey>, IEntitySet<int> where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// 数据主键
    /// </summary>
    [Column("id"), DisplayName("编号"), DatabaseGenerated(DatabaseGeneratedOption.Identity), Key, Excel]
    public override int Id { get; set; }
    /// <summary>
    /// 用户编号
    /// </summary>
    [Column("user_id"), DisplayName("用户编号"), Required, MaxLength(36), Excel]
    public override TKey UserId { get; set; } = default!;
    //
    // 摘要:
    //     Gets or sets the claim type for this claim.
    [Column("claim_type"), DisplayName("申明类型"), MaxLength(128), Excel]
    public override string? ClaimType { get; set; }
    //
    // 摘要:
    //     Gets or sets the claim value for this claim.
    [Column("claim_value"), DisplayName("申明内容"), MaxLength(255), Excel]
    public override string? ClaimValue { get; set; }
}