﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

/// <summary>
/// 用户登录提供者, 字符串类型的默认实现
/// </summary>
[Table("user_login"), DisplayName("用户登录提供者")]
public class UserLogin : UserLogin<string>;

/// <summary>
/// 用户登录提供者
/// </summary>
[Table("user_login"), DisplayName("用户登录提供者")]
public class UserLogin<TKey> : IdentityUserLogin<TKey> where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// 用户编号
    /// </summary>
    [Column("user_id"), DisplayName("用户编号"), Required, MaxLength(36), Excel]
    public override TKey UserId { get; set; } = default!;
    /// <summary>
    /// Gets or sets the LoginProvider this token is from.
    /// eg: Weixin
    /// </summary>
    [Column("login_provider"), DisplayName("登录提供者"), Key, Required, MaxLength(36), Excel]
    public override string LoginProvider { get; set; } = null!;
    /// <summary>
    /// Gets or sets the unique provider identifier for this login.
    /// eg: OpenId
    /// </summary>
    [Column("provider_key"), DisplayName("提供者编号"), Key, Required, MaxLength(36), Excel]
    public override string ProviderKey { get; set; } = null!;
    /// <summary>
    /// Gets or sets the friendly name used in a UI for this login.
    /// </summary>
    [Column("provider_display_name"), DisplayName("提供者显示名称"), MaxLength(128), Excel]
    public override string? ProviderDisplayName { get; set; }
}