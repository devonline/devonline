﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Devonline.Identity;

/// <summary>
/// 访问记录, 字符串类型的默认实现
/// 暂时无法满足命中多条访问规则的情况
/// </summary>
[Table("access_record"), DisplayName("访问记录")]
public class AccessRecord : AccessRecord<string>, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 申请用户
    /// </summary>
    public virtual User? User { get; set; }
    /// <summary>
    /// 申请的资源
    /// </summary>
    public virtual Resource? Resource { get; set; }
    /// <summary>
    /// 对应的访问规则, 最匹配的一条规则
    /// </summary>
    public virtual AccessRule? AccessRule { get; set; }
    /// <summary>
    /// 访问申请
    /// </summary>
    public virtual AccessApply? AccessApply { get; set; }
}

/// <summary>
/// 访问记录, 用户的普通访问记录只记录访问次数, 和最后一次的访问时间, 授权访问的类型, 每次请求都需要记录
/// </summary>
/// <typeparam name="TKey"></typeparam>
[Table("access_record"), DisplayName("访问记录")]
public class AccessRecord<TKey> : EntitySetWithCreate<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 访问者编号
    /// </summary>
    [Column("user_id"), DisplayName("访问者编号"), Required, MaxLength(36), Excel]
    public virtual TKey UserId { get; set; } = default!;
    /// <summary>
    /// 资源编号
    /// </summary>
    [Column("resource_id"), DisplayName("资源编号"), Required, MaxLength(36), Excel]
    public virtual TKey ResourceId { get; set; } = default!;
    /// <summary>
    /// 访问规则编号, 最匹配的一条规则
    /// </summary>
    [Column("access_rule_id"), DisplayName("访问规则编号"), MaxLength(36), Excel]
    public virtual TKey? AccessRuleId { get; set; }
    /// <summary>
    /// 访问申请编号(如果有申请)
    /// </summary>
    [Column("access_apply_id"), DisplayName("访问申请编号"), MaxLength(36), Excel]
    public virtual TKey? AccessApplyId { get; set; }
    /// <summary>
    /// 当前用户访问总次数, 默认 0
    /// </summary>
    [Column("access_count"), DisplayName("总访问次数"), Excel]
    public virtual int AccessCount { get; set; }
    /// <summary>
    /// 当前用户被允许的访问次数, 默认 0
    /// </summary>
    [Column("allowed_count"), DisplayName("允许的访问次数"), Excel]
    public virtual int AllowedCount { get; set; }
}