﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

/// <summary>
/// 角色申明, 字符串类型的默认实现
/// </summary>
[Table("role_claim"), DisplayName("角色申明")]
public class RoleClaim : RoleClaim<string>;

/// <summary>
/// 角色申明
/// </summary>
[Table("role_claim"), DisplayName("角色申明")]
public class RoleClaim<TKey> : IdentityRoleClaim<TKey> where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// 数据主键
    /// </summary>
    [Column("id"), DisplayName("编号"), DatabaseGenerated(DatabaseGeneratedOption.Identity), Key, Excel]
    public override int Id { get; set; }
    /// <summary>
    /// 角色编号
    /// </summary>
    [Column("role_id"), DisplayName("角色编号"), Required, MaxLength(36), Excel]
    public override TKey RoleId { get; set; } = default!;
    //
    // 摘要:
    //     Gets or sets the claim type for this claim.
    [Column("claim_type"), DisplayName("申明类型"), Required, MaxLength(128), Excel]
    public override string? ClaimType { get; set; }
    //
    // 摘要:
    //     Gets or sets the claim value for this claim.
    [Column("claim_value"), DisplayName("申明内容"), Required, MaxLength(255), Excel]
    public override string? ClaimValue { get; set; }
}