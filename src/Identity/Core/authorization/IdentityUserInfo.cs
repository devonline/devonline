﻿namespace Devonline.Identity;

/// <summary>
/// identity service response userinfo
/// </summary>
public class IdentityUserInfo : EntitySet
{
    /// <summary>
    /// ids4 subject field
    /// </summary>
    public string? Sub { get; set; }
    public string? UserId { get; set; }
    public string? Name { get; set; }
    public string? UserName { get; set; }
    public string? Role { get; set; }
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
}