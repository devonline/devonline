﻿using System.ComponentModel;

namespace Devonline.Identity;

/// <summary>
/// 用户上下文
/// </summary>
[DisplayName("用户上下文")]
public class UserContext
{
    /// <summary>
    /// 是否当前登录用户
    /// </summary>
    public bool IsCurrentUser { get; init; }
    /// <summary>
    /// 用户是否已通过认证
    /// </summary>
    public bool IsAuthenticated { get; init; }
    /// <summary>
    /// 是否系统用户
    /// </summary>
    public bool IsSystem => User.Type == AuthorizeType.System;
    /// <summary>
    /// 是否超级管理员
    /// </summary>
    public bool IsAdministrator => User.Type == AuthorizeType.Administrator;
    /// <summary>
    /// 是否开发者
    /// </summary>
    public bool IsDeveloper => User.Type == AuthorizeType.Developer;
    /// <summary>
    /// 是否授权用户
    /// </summary>
    public bool IsAuthorizer => User.Type >= AuthorizeType.Authorizer;
    /// <summary>
    /// 是否匿名用户
    /// </summary>
    public bool IsAnonymous => User.Type == AuthorizeType.Anonymous;
    /// <summary>
    /// 是否业务管理员
    /// </summary>
    public bool IsBusinessAdministrator => User.Type == AuthorizeType.BusinessAdministrator;
    /// <summary>
    /// 是否授权访问用户
    /// </summary>
    public bool IsAuthorizedAccessor => User.Type == AuthorizeType.AuthorizedAccessor || Group?.Type == AuthorizeType.AuthorizedAccessor || Level?.Type == AuthorizeType.AuthorizedAccessor || Roles.Any(x => x.Type == AuthorizeType.AuthorizedAccessor) || Groups.Any(x => x.Type == AuthorizeType.AuthorizedAccessor);

    /// <summary>
    /// 用户编号
    /// </summary>
    public string UserId => User.Id;
    /// <summary>
    /// 用户名
    /// </summary>
    public string UserName => User.UserName!;
    /// <summary>
    /// 是否已取得访问授权
    /// </summary>
    public bool HasAuthorizedAccess { get; internal set; }
    /// <summary>
    /// 访问时间, 记录每次的访问时间, 用于对于过长时间没有访问过的用户数据进行清理
    /// </summary>
    public DateTime AccessTime { get; internal set; } = DateTime.UtcNow;
    /// <summary>
    /// 用户
    /// </summary>
    public UserViewModel User { get; init; } = null!;
    /// <summary>
    /// 用户直属组织
    /// </summary>
    public GroupViewModel? Group { get; init; }
    /// <summary>
    /// 用户级别
    /// </summary>
    public LevelViewModel? Level { get; init; }
    /// <summary>
    /// 用户所有的角色
    /// </summary>
    public virtual ICollection<RoleViewModel> Roles { get; init; } = [];
    /// <summary>
    /// 用户所有的组织
    /// </summary>
    public virtual ICollection<GroupViewModel> Groups { get; init; } = [];
    /// <summary>
    /// 用户授权资源列表, 为: { 系统: { 资源地址: 资源 }} 的结构, 用户级别的资源只记录可访问的资源, 资源类型小于 Page 级别的
    /// </summary>
    public virtual Dictionary<string, Dictionary<string, ResourceViewModel>> Resources { get; init; } = [];
    /// <summary>
    /// 用户授权资源树, 为: { 系统: 资源树 } 的结构
    /// </summary>
    public virtual Dictionary<string, ResourceViewModel> ResourceTrees { get; init; } = [];
    /// <summary>
    /// 用户身份, 每种身份类型已获取的身份数据, 用户选择的身份, 用于授权后的身份选择后的二次权限过滤
    /// TODO TBD 在二次身份过滤中, 只能选择可能存在多种身份类型情况的 Role 和 Group, 不能选择其他身份类型?
    /// </summary>
    public virtual Dictionary<IdentityType, string[]>? Identities { get; init; }
}