﻿using System.ComponentModel;

namespace Devonline.Identity;

/// <summary>
/// 用户及其常用数据定义
/// </summary>
[DisplayName("用户及其常用数据定义")]
public class UserInfo
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string UserName => User?.UserName!;
    /// <summary>
    /// 访问令牌
    /// </summary>
    public IdentityToken? Token { get; set; }
    /// <summary>
    /// 用户
    /// </summary>
    public UserViewModel? User { get; set; }
    /// <summary>
    /// 用户直属组织信息
    /// </summary>
    public GroupViewModel? Group { get; set; }
    /// <summary>
    /// 数据隔离编号
    /// </summary>
    public string? DataIsolateId { get; set; }
    /// <summary>
    /// 用户授权资源列表
    /// </summary>
    public ICollection<ResourceViewModel>? Resources { get; set; }
    /// <summary>
    /// 用户授权资源树
    /// </summary>
    public ICollection<ResourceViewModel>? ResourceTree { get; set; }
}