﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 认证用户视图模型
/// </summary>
public class RealNameViewModel : IdentityViewModel
{
    /// <summary>
    /// 用户编号
    /// </summary>
    [DisplayName("用户编号"), MaxLength(36), Excel]
    public virtual string? UserId { get; set; }
    /// <summary>
    /// 是否已通过认证
    /// </summary>
    [DisplayName("是否已认证"), Excel]
    public virtual bool IsAuthed { get; set; }
    /// <summary>
    /// 认证阶段
    /// </summary>
    [DisplayName("认证阶段"), DefaultValue(AuthPhase.None), Excel]
    public virtual AuthPhase AuthPhase { get; set; } = AuthPhase.None;
    /// <summary>
    /// 身份证号码
    /// </summary>
    [DisplayName("身份证号码"), MaxLength(255), Excel]
    public virtual string? IdCode { get; set; }
    /// <summary>
    /// 联系方式
    /// </summary>
    [MaxLength(255), DisplayName("手机号码"), Excel]
    public virtual string? PhoneNumber { get; set; }
    /// <summary>
    /// 验证消息, 验证消息有值, 则当前状态不可继续认证
    /// </summary>
    public virtual string? Message { get; set; }
}