﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 访问申请
/// </summary>
[DisplayName("访问申请")]
public class AccessApplyViewModel : AccessApplyViewModel<string>, IViewModel, IEntitySet { }

/// <summary>
/// 访问申请
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("访问申请")]
public class AccessApplyViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 申请者编号
    /// </summary>
    [DisplayName("申请者编号"), Required, MaxLength(36), Excel]
    public TKey? UserId { get; set; }
    /// <summary>
    /// 资源编号
    /// </summary>
    [DisplayName("资源编号"), Required, MaxLength(36), Excel]
    public TKey? ResourceId { get; set; }
    /// <summary>
    /// 访问规则编号, 最匹配的一条规则
    /// </summary>
    [DisplayName("访问规则编号"), MaxLength(36), Excel]
    public TKey? AccessRuleId { get; set; }
    /// <summary>
    /// 申请时间
    /// </summary>
    [DisplayName("申请时间"), Excel]
    public DateTime ApplyTime { get; set; }
    /// <summary>
    /// 授权人编号
    /// </summary>
    [DisplayName("访问规则编号"), MaxLength(36), Excel]
    public TKey? AuthorizerId { get; set; }
    /// <summary>
    /// 授权时间
    /// </summary>
    [DisplayName("访问规则编号"), Excel]
    public DateTime? AuthorizedTime { get; set; }
    /// <summary> 
    /// 是否允许访问 AllowType 枚举值
    /// 申请阶段值为: Authorize, 意为待授权, 授权完毕为 Allow 和 Forbid 其中之一
    /// </summary>
    [DisplayName("是否允许"), Excel]
    public AllowType? IsAllow { get; set; }
    /// <summary>
    /// 描述, 受理意见等
    /// </summary>
    [DisplayName("描述"), MaxLength(255), Excel]
    public string? Description { get; set; }
}