﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

/// <summary>
/// 资源的定义
/// </summary>
[DisplayName("资源")]
public class ResourceViewModel : ResourceViewModel<string>, IIdentity, IViewModel, IEntitySet
{
    /// <summary>
    /// 子级资源
    /// </summary>
    public ICollection<ResourceViewModel>? Children { get; set; }
    /// <summary>
    /// 当前资源访问规则
    /// </summary>
    public ICollection<AccessRuleViewModel>? AccessRules { get; set; }
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 资源的定义
/// </summary>
[DisplayName("资源")]
public class ResourceViewModel<TKey> : IdentityViewModel<TKey>, IIdentity<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 所属系统编号, 用于标记当前资源属于那个系统, 以简化资源内容描述
    /// </summary>
    [DisplayName("所属系统编号"), MaxLength(36), Excel]
    public TKey? SystemId { get; set; }
    /// <summary>
    /// 父级资源编号
    /// </summary>
    [DisplayName("父级资源编号"), MaxLength(36), Excel]
    public TKey? ParentId { get; set; }
    /// <summary>
    /// 资源所有者编号
    /// </summary>
    [DisplayName("资源所有者"), MaxLength(36), Excel]
    public TKey? OwnerId { get; set; }
    /// <summary>
    /// 资源所有者类型 IdentityType 枚举值
    /// </summary>
    [DisplayName("资源所有者类型"), Excel]
    public IdentityType IdentityType { get; set; }
    /// <summary>
    /// 资源类型 ResourceType 枚举类型的值
    /// </summary>
    [DisplayName("资源类型"), Excel]
    public ResourceType ResourceType { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    [Required, Unique, MaxLength(255), DisplayName("名称"), Excel]
    public override string? Name { get; set; } = null!;
    /// <summary>
    /// 资源编号, 用于内部命名, 排序等
    /// 资源访问的统一规则制定的总体约束, 形如: R35, #35
    /// </summary>
    [DisplayName("资源编号"), MaxLength(16), Excel]
    public string Code { get; set; } = null!;
    /// <summary>
    /// 资源标题
    /// </summary>
    [DisplayName("资源标题"), MaxLength(36), Excel]
    public string Title { get; set; } = null!;
    /// <summary>
    /// 资源保存的具体内容, 可能是密文
    /// </summary>
    [ProtectedPersonalData]
    [DisplayName("资源内容"), MaxLength(1024), Excel]
    public string Content { get; set; } = null!;
    /// <summary>
    /// 资源定义的可访问级别 AccessLevel 枚举类型的值
    /// </summary>
    [DisplayName("访问级别"), Excel]
    public AccessLevel AccessLevel { get; set; }
    /// <summary>
    /// 资源级别, 即资源定义的最低访问级别
    /// </summary>
    [DisplayName("资源级别"), MaxLength(36), Excel]
    public TKey? LevelId { get; set; }

    [DisplayName("是否已取得权限"), Excel]
    public bool HasPermission { get; set; }
}