﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 基于 OAuth 的认证用户信息, 字符主键的默认实现
/// OAuth 用户信息每个用户可能有多个
/// </summary>
public class OAuthUserViewModel : OAuthUserViewModel<string>, IViewModel, IEntitySet, IIdentity
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public virtual ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 基于 OAuth 的认证用户信息
/// OAuth 用户信息每个用户可能有多个
/// </summary>
public abstract class OAuthUserViewModel<TKey> : IdentityViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey>, IIdentity<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 身份授权类型
    /// </summary>
    [DisplayName("授权类型"), Excel(Size = DEFAULT_EXCEL_COLUMN_WIDTH)]
    public virtual AuthType AuthType { get; set; } = AuthType.Weixin;
    /// <summary>
    /// OpenId
    /// </summary>
    [DisplayName("OpenId"), Required, MaxLength(36), Excel]
    public virtual string OpenId { get; set; } = null!;
}