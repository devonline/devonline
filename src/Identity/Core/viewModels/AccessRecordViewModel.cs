﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 访问记录
/// </summary>
[DisplayName("访问记录")]
public class AccessRecordViewModel : AccessRecordViewModel<string>, IViewModel, IEntitySet { }

/// <summary>
/// 访问记录
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("访问记录")]
public class AccessRecordViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 访问者编号
    /// </summary>
    [DisplayName("访问者编号"), Required, MaxLength(36), Excel]
    public TKey? UserId { get; set; }
    /// <summary>
    /// 资源编号
    /// </summary>
    [DisplayName("资源编号"), Required, MaxLength(36), Excel]
    public TKey? ResourceId { get; set; }
    /// <summary>
    /// 访问记录编号, 最匹配的一条规则
    /// </summary>
    [DisplayName("访问记录编号"), MaxLength(36), Excel]
    public TKey? AccessRuleId { get; set; }
    /// <summary>
    /// 访问申请编号(如果有申请)
    /// </summary>
    [DisplayName("访问申请编号"), MaxLength(36), Excel]
    public TKey? AccessApplyId { get; set; }
}