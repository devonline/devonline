﻿namespace Devonline.Identity;

/// <summary>
/// 短信验证码数据模型
/// </summary>
public class CaptchaViewModel : ViewModel, IViewModel
{
    /// <summary>
    /// 手机号
    /// </summary>
    public string PhoneNumber { get; set; } = null!;
    /// <summary>
    /// 验证码
    /// </summary>
    public string Captcha { get; set; } = null!;
    /// <summary>
    /// 手机验证码发送时间, 记录最后一次验证码验证通过和不通过的时间
    /// </summary>
    public DateTime SendTime { get; set; }
    /// <summary>
    /// 过期时间
    /// </summary>
    public DateTime Expiration { get; set; }
    /// <summary>
    /// 手机验证码发送次数, 用于限制无限尝试
    /// </summary>
    public int SendCount { get; set; }
    /// <summary>
    /// 手机验证码验证失败次数, 用于限制无限尝试
    /// </summary>
    public int FailedCount { get; set; }
    /// <summary>
    /// 是否已经验证通过
    /// </summary>
    public bool IsValid { get; set; }

    /// <summary>
    /// 验证错误信息
    /// </summary>
    public string? ErrorMessage { get; set; }
}