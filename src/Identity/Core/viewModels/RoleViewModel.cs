﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 角色
/// </summary>
[DisplayName("角色")]
public class RoleViewModel : RoleViewModel<string>, IIdentity, IViewModel, IEntitySet
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 角色
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("角色")]
public class RoleViewModel<TKey> : IdentityViewModel<TKey>, IIdentity<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    [MaxLength(64), DisplayName("归一角色名"), Excel]
    public string? NormalizedName { get; set; }
}