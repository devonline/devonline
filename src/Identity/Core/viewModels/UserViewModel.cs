﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 用户
/// </summary>
[DisplayName("用户")]
public class UserViewModel : UserViewModel<string>, IIdentity, IViewModel, IEntitySet
{
    /// <summary>
    /// 通用附件集合, NotMapped 用于记录实体对象上上传的附件
    /// </summary>
    public ICollection<Attachment>? Attachments { get; set; }
}

/// <summary>
/// 用户
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("用户")]
public class UserViewModel<TKey> : IdentityViewModel<TKey>, IIdentity<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    [Unique, DisplayName("用户名"), MaxLength(255), Excel]
    public string? UserName { get; set; }
    [DisplayName("手机号码"), MaxLength(255), Excel]
    public string? PhoneNumber { get; set; }
    [DisplayName("邮箱"), MaxLength(255), Excel]
    public string? Email { get; set; }
    /// <summary>
    /// 用户直属部门编号
    /// </summary>
    [DisplayName("用户直属部门编号"), MaxLength(36), Excel]
    public TKey? GroupId { get; set; }
    [DisplayName("用户级别编号"), MaxLength(36), Excel]
    public TKey? LevelId { get; set; }
    [DisplayName("归一用户名"), MaxLength(255), Excel]
    public string? NormalizedUserName { get; set; }
    [DisplayName("手机号码是否已确认"), Excel]
    public bool PhoneNumberConfirmed { get; set; }
    [DisplayName("邮箱是否已确认"), Excel]
    public bool EmailConfirmed { get; set; }
    [DisplayName("是否锁定"), Excel]
    public bool LockoutEnabled { get; set; }
    [DisplayName("访问失败次数"), Excel]
    public int AccessFailedCount { get; set; }
    [DisplayName("是否实名认证"), Excel]
    public bool? HasRealNameAuthentication { get; set; }
}