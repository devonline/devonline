﻿namespace Devonline.Identity;

/// <summary>
/// 用户手机号码确认视图模型
/// </summary>
public class PhoneNumberConfirmedViewModel : ViewModel
{
    /// <summary>
    /// 手机号码
    /// </summary>
    public string PhoneNumber { get; set; } = null!;
    /// <summary>
    /// 验证码
    /// </summary>
    public string Code { get; set; } = null!;
    /// <summary>
    /// 手机验证码验证失败次数, 用于限制无限尝试
    /// </summary>
    public int FailedCount { get; set; }
    /// <summary>
    /// 返回地址
    /// </summary>
    public string? ReturnUrl { get; set; }
    /// <summary>
    /// 错误消息
    /// </summary>
    public string? ErrorMessage { get; set; }
}