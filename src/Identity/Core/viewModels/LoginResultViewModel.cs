﻿namespace Devonline.Identity;

/// <summary>
/// 用户登录结果模型
/// </summary>
public class LoginResultViewModel
{
    /// <summary>
    /// 登录类型
    /// </summary>
    public LoginType Type { get; set; }
    /// <summary>
    /// 安全级别要求
    /// </summary>
    public SecurityLevel SecurityLevel { get; set; }
    /// <summary>
    /// 用户名/手机号
    /// </summary>
    public string UserName { get; set; } = null!;
    /// <summary>
    /// 是否登录成功
    /// </summary>
    public bool Success { get; set; }
    /// <summary>
    /// 重定向地址, 重定向地址有值, 则需要客户端根据地址进行重定向
    /// </summary>
    public string? RedirectUri { get; set; }
    /// <summary>
    /// 失败信息
    /// </summary>
    public string? ErrorMessage { get; set; }
}