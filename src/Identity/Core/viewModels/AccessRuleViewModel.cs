﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity;

/// <summary>
/// 访问规则
/// </summary>
[DisplayName("访问规则")]
public class AccessRuleViewModel : AccessRuleViewModel<string>, IViewModel, IEntitySet { }

/// <summary>
/// 访问规则
/// </summary>
/// <typeparam name="TKey">主键类型</typeparam>
[DisplayName("访问规则")]
public class AccessRuleViewModel<TKey> : ViewModel<TKey>, IViewModel<TKey>, IEntitySet<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 资源编号
    /// </summary>
    [DisplayName("资源编号"), MaxLength(36), Excel]
    public TKey? ResourceId { get; set; }
    /// <summary>
    /// 授权对象
    /// 资源授权使用的主体
    /// </summary>
    [DisplayName("授权对象编号"), MaxLength(36), Excel]
    public TKey? IdentityId { get; set; }
    /// <summary>
    /// 授权对象类型, IdentityType 枚举值
    /// </summary>
    [DisplayName("授权对象类型"), Excel]
    public IdentityType IdentityType { get; set; }
    /// <summary>
    /// 规则编号
    /// 针对资源访问的统一规则制定的总体约束, 形如: R35, #35
    /// </summary>
    [DisplayName("规则编号"), MaxLength(16), Excel]
    public string? Code { get; set; }
    /// <summary>
    /// 是否允许 AllowType 枚举值
    /// </summary>
    [DisplayName("是否允许"), Excel]
    public AllowType IsAllow { get; set; }
    /// <summary>
    /// 优先级
    /// </summary>
    [DisplayName("优先级"), Excel]
    public int Priority { get; set; }
    /// <summary>
    /// 过期时间限制, 访问授权的有效期
    /// </summary>
    [DisplayName("过期时间"), Excel]
    public DateTime? ExpireTime { get; set; }
    /// <summary>
    /// 访问次数
    /// </summary>
    [DisplayName("访问次数"), Excel]
    public int? AccessCount { get; set; }
    /// <summary>
    /// 访问次数限制, 即最大可访问次数
    /// </summary>
    [DisplayName("访问次数限制"), Excel]
    public int? LimitAccessCount { get; set; }
    /// <summary>
    /// 条件
    /// 符合条件则执行 AllowType 策略
    /// 规则构成为简单的 {变量 条件 值} 形式, 一行一个, 暂不支持复杂表达式
    /// 如: {userName eq 'admin'}
    /// 支持的条件表达式详见枚举类型: OperatorType
    /// </summary>
    [DisplayName("条件"), MaxLength(1024), Excel]
    public string? Condition { get; set; }

    [DisplayName("资源编号集合"), MaxLength(4096), Excel]
    public string? ResourceIds { get; set; }
}