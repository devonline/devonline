using System.ComponentModel.DataAnnotations.Schema;

namespace Devonline.Identity;

/// <summary>
/// 用户登录模型
/// </summary>
public class UserLoginModel
{
    /// <summary>
    /// 登录类型
    /// </summary>
    public LoginType Type { get; set; }
    /// <summary>
    /// 用户名/手机号
    /// </summary>
    [Column("username")]
    public string? UserName { get; set; }
    /// <summary>
    /// 密码/验证码
    /// </summary>
    [Column("password")]
    public string? Password { get; set; }
    /// <summary>
    /// 回调地址
    /// </summary>
    public string? ReturnUrl { get; set; }
    /// <summary>
    /// 是否记住登录
    /// </summary>
    public bool RememberLogin { get; set; }
    /// <summary>
    /// 验证码编号
    /// </summary>
    public string? CaptchaId { get; set; }
    /// <summary>
    /// 验证码的值
    /// </summary>
    public string? CaptchaCode { get; set; }
}