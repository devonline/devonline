﻿using Devonline.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 字符串作为主键的业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public interface IDataCollectionService<TEntitySet, TElement> :
    IDataCollectionService<IdentityDbContext, TEntitySet, TElement>
    where TEntitySet : class, IEntitySet, new()
    where TElement : class, IEntitySet;

/// <summary>
/// 业务数据带附属集合类数据处理服务
/// 字符串作为主键的业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public interface IDataWithCollectionService<TEntitySet, TElement> :
    IDataWithCollectionService<IdentityDbContext, TEntitySet, TElement>,
    IDataService<TEntitySet>,
    IDataCollectionService<TEntitySet, TElement>
    where TEntitySet : class, IEntitySet, new()
    where TElement : class, IEntitySet;

/// <summary>
/// 字符串作为主键的业务数据附属集合类数据处理服务
/// 不提供一次性新增或修改所有附属集合的功能, 因为不明确附属集合类型的数量, 实际业务中需要的时候调用即可
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public class DataWithCollectionService<TEntitySet, TElement>(
    IdentityDbContext context,
    ILogger<DataWithCollectionService<TEntitySet, TElement>> logger,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    HttpSetting httpSetting) :
    DataWithCollectionService<IdentityDbContext, TEntitySet, TElement>(logger, context, cache, httpContextAccessor, httpSetting),
    IDataService<TEntitySet>,
    IDataWithCollectionService<TEntitySet, TElement>,
    IDataCollectionService<TEntitySet, TElement>
    where TEntitySet : class, IEntitySet, new()
    where TElement : class, IEntitySet;