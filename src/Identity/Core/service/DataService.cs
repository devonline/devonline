﻿using Devonline.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 认证类数据处理服务接口
/// 字符串作为主键的默认实现接口
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public interface IDataService<TEntitySet, TKey> :
    IDataService<IdentityDbContext, TEntitySet, TKey>
    where TEntitySet : class, IEntitySet<TKey>
    where TKey : IConvertible;

/// <summary>
/// 认证类数据处理服务接口
/// 字符串作为主键的默认实现接口
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
public interface IDataService<TEntitySet> :
    IDataService<TEntitySet, string>
    where TEntitySet : class, IEntitySet;

/// <summary>
/// 认证类数据处理服务
/// 字符串作为键的默认实现
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TKey">主键类型</typeparam>
public class DataService<TEntitySet, TKey>(
    ILogger<DataService<TEntitySet, TKey>> logger,
    IdentityDbContext context,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    HttpSetting httpSetting) :
    DataService<IdentityDbContext, TEntitySet, TKey>(logger, context, cache, httpContextAccessor, httpSetting),
    IDataService<TEntitySet, TKey>
    where TEntitySet : class, IEntitySet<TKey>, new()
    where TKey : IConvertible;

/// <summary>
/// 认证类数据处理服务
/// 字符串作为键的默认实现
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
public class DataService<TEntitySet>(
    ILogger<DataService<TEntitySet>> logger,
    IdentityDbContext context,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    HttpSetting httpSetting) :
    DataService<TEntitySet, string>(logger, context, cache, httpContextAccessor, httpSetting),
    IDataService<TEntitySet>
    where TEntitySet : class, IEntitySet, new();