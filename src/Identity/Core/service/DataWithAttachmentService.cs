﻿using Devonline.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 业务数据附件类服务
/// 字符串作为主键的默认实现
/// 一个业务数据中仅有一个类型为: Attachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
public interface IDataAttachmentService<TEntitySet> : IDataAttachmentService<IdentityDbContext, TEntitySet> where TEntitySet : class, IEntitySet, new();

/// <summary>
/// 业务数据带附件类服务
/// 字符串作为主键的默认实现
/// 一个业务数据中仅有一个类型为: Attachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
public interface IDataWithAttachmentService<TEntitySet> :
    IDataWithAttachmentService<IdentityDbContext, TEntitySet>,
    IDataService<TEntitySet>,
    IDataAttachmentService<TEntitySet>
    where TEntitySet : class, IEntitySet, new();

/// <summary>
/// 业务数据及其附件操作类服务
/// 字符串作为主键的默认实现
/// 业务数据的附件全都在名为 Attachments 的集合中, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
public class DataWithAttachmentService<TEntitySet>(
    ILogger<DataWithAttachmentService<TEntitySet>> logger,
    IdentityDbContext context,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    IFileService fileService,
    HttpSetting httpSetting) :
    DataWithAttachmentService<IdentityDbContext, TEntitySet>(logger, context, cache, httpContextAccessor, fileService, httpSetting),
    IDataService<TEntitySet>,
    IDataWithAttachmentService<TEntitySet>,
    IDataAttachmentService<TEntitySet>
    where TEntitySet : class, IEntitySet, new();