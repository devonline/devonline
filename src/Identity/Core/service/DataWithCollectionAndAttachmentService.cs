﻿using Devonline.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 数据带集合及附件的基础数据服务
/// 字符串作为主键的默认实现
/// 一个业务数据中仅有一个类型为: Attachment 的业务数据的附件集合, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public interface IDataWithCollectionAndAttachmentService<TEntitySet, TElement> :
    IDataWithCollectionAndAttachmentService<IdentityDbContext, TEntitySet, TElement>,
    IDataWithAttachmentService<TEntitySet>,
    IDataCollectionService<TEntitySet, TElement>,
    IDataAttachmentService<TEntitySet>,
    IDataService<TEntitySet>
    where TEntitySet : class, IEntitySet, new()
    where TElement : class, IEntitySet;

/// <summary>
/// 数据带集合及附件的基础数据服务
/// 字符串作为主键的默认实现
/// 业务数据的附件全都在名为 Attachments 的集合中, 根据 BusinessType 字段区分用途
/// </summary>
/// <typeparam name="TEntitySet">业务数据类型</typeparam>
/// <typeparam name="TElement">集合元素数据类型</typeparam>
public class DataWithCollectionAndAttachmentService<TEntitySet, TElement>(
    ILogger<DataWithCollectionAndAttachmentService<TEntitySet, TElement>> logger,
    IdentityDbContext context,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    IFileService fileService,
    HttpSetting httpSetting,
    IDataWithCollectionAndAttachmentService<IdentityDbContext, TEntitySet, TElement> attachmentService) :
    DataWithCollectionAndAttachmentService<IdentityDbContext, TEntitySet, TElement>(logger, context, cache, httpContextAccessor, fileService, httpSetting, attachmentService),
    IDataWithCollectionAndAttachmentService<TEntitySet, TElement>,
    IDataWithAttachmentService<TEntitySet>,
    IDataCollectionService<TEntitySet, TElement>,
    IDataAttachmentService<TEntitySet>,
    IDataService<TEntitySet>
    where TEntitySet : class, IEntitySet, new()
    where TElement : class, IEntitySet;