﻿using Devonline.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Devonline.Identity;

/// <summary>
/// 附件及文件操作类服务
/// 字符串作为主键的默认实现
/// </summary>
public interface IAttachmentService : IAttachmentService<IdentityDbContext>;

/// <summary>
/// 附件及文件操作类服务
/// </summary>
public class AttachmentService(
    ILogger<AttachmentService> logger,
    IdentityDbContext context,
    IDistributedCache cache,
    IHttpContextAccessor httpContextAccessor,
    IFileService fileService,
    IServiceProvider serviceProvider,
    HttpSetting httpSetting) :
    AttachmentService<IdentityDbContext>(logger, context, cache, httpContextAccessor, fileService, serviceProvider, httpSetting),
    IAttachmentService;