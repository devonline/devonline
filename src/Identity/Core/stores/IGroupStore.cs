﻿namespace Devonline.Identity;

/// <summary>
/// Store 接口
/// </summary>
/// <typeparam name="TGroup"></typeparam>
/// <typeparam name="TKey"></typeparam>
public interface IGroupStore<TGroup, TKey> : IIdentityStore<TGroup, TKey> where TGroup : Group<TKey> where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// Find and return parent Group, if any, who has the specified <paramref name="id"/>.
    /// </summary>
    /// <param name="id">The Group id to search for.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>
    /// The <see cref="Task"/> that represents the asynchronous operation, containing the groups matching the specified <paramref name="id"/> if it exists.
    /// </returns>
    Task<TGroup?> GetParentGroupAsync(TKey id, CancellationToken cancellationToken = default);

    /// <summary>
    /// Finds and returns parent Groups, if any, who has the specified <paramref name="id"/>.
    /// </summary>
    /// <param name="id">The Group id to search for.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>
    /// The <see cref="Task"/> that represents the asynchronous operation, containing the groups matching the specified <paramref name="id"/> if it exists.
    /// </returns>
    Task<IList<TGroup>> GetParentGroupsAsync(TKey id, CancellationToken cancellationToken = default);

    /// <summary>
    /// Finds and returns children Group, if any, who has the specified <paramref name="id"/>.
    /// </summary>
    /// <param name="id">The Group id to search for.</param>
    /// <param name="recursion">To recursion it's children or not.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>
    /// The <see cref="Task"/> that represents the asynchronous operation, containing the groups matching the specified <paramref name="id"/> if it exists.
    /// </returns>
    Task<IList<TGroup>> GetChildrenGroupsAsync(TKey id, bool recursion = false, CancellationToken cancellationToken = default);
}
