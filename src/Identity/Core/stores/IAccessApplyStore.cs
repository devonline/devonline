﻿namespace Devonline.Identity;

/// <summary>
/// 访问申请存储接口
/// </summary>
/// <typeparam name="TUser">申请用户</typeparam>
/// <typeparam name="TResource">申请的资源</typeparam>
/// <typeparam name="TKey"></typeparam>
public interface IAccessApplyStore<TUser, TResource, TKey> : IDisposable where TUser : class where TResource : class where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// user allpy a resource
    /// </summary>
    /// <param name="user">the apply user</param>
    /// <param name="resource">the resource of apply</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task ApplyAsync(TUser user, TResource resource, CancellationToken cancellationToken);
}
