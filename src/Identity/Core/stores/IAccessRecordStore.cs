﻿namespace Devonline.Identity;

/// <summary>
/// 访问记录存储接口
/// </summary>
/// <typeparam name="TUser"></typeparam>
/// <typeparam name="TResource"></typeparam>
/// <typeparam name="TKey"></typeparam>
public interface IAccessRecordStore<TUser, TResource, TKey> : IDisposable where TUser : class where TResource : class where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// record a user access a resource
    /// </summary>
    /// <param name="user">the access user</param>
    /// <param name="resource">the resource of access</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task RecordAsync(TUser user, TResource resource, CancellationToken cancellationToken);
}