﻿namespace Devonline.Identity;

/// <summary>
/// 访问控制存储接口
/// </summary>
/// <typeparam name="TAccessRule"></typeparam>
/// <typeparam name="TKey"></typeparam>
public interface ILevelStore<TIdentity, TKey> : IDisposable where TIdentity : class where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// set a level for identity
    /// </summary>
    /// <param name="identity">The AccessRule id to search for.</param>
    /// <param name="level">The AccessRule id to search for.</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task SetLevelAsync(TIdentity identity, string level, CancellationToken cancellationToken);
}