﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Devonline.Identity;

/// <summary>
/// Creates a new instance of a persistence store for roles.
/// </summary>
public class RoleStore : RoleStore<IdentityDbContext>
{
    /// <summary>
    /// Constructs a new instance of <see cref="RoleStore{TRole}"/>.
    /// </summary>
    /// <param name="context">The <see cref="IdentityDbContext"/>.</param>
    /// <param name="describer">The <see cref="IdentityErrorDescriber"/>.</param>
    public RoleStore(IdentityDbContext context, DefaultIdentityErrorDescriber describer) : base(context, describer) { }
}

/// <summary>
/// Creates a new instance of a persistence store for roles.
/// </summary>
/// <typeparam name="TContext">The type of the data context class used to access the store.</typeparam>
public class RoleStore<TContext> : RoleStore<Role, TContext, string, UserRole, RoleClaim> where TContext : DbContext
{
    /// <summary>
    /// Constructs a new instance of <see cref="RoleStore{TRole, TContext, TKey}"/>.
    /// </summary>
    /// <param name="context">The <see cref="DbContext"/>.</param>
    /// <param name="describer">The <see cref="IdentityErrorDescriber"/>.</param>
    public RoleStore(TContext context, DefaultIdentityErrorDescriber describer) : base(context, describer) { }
}