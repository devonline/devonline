﻿namespace Devonline.Identity;

/// <summary>
/// identity secret interfacem
/// </summary>
/// <typeparam name="TIdentity"></typeparam>
/// <typeparam name="TKey"></typeparam>
public interface ISecretStore<TIdentity, TKey> : IIdentityStore<TIdentity, TKey> where TIdentity : class, IIdentity<TKey> where TKey : IEquatable<TKey>
{
    /// <summary>
    /// use the default secret to set the secret of identity
    /// </summary>
    /// <param name="identity">The resource id to change for</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task SetDefaultSecret(TIdentity identity, CancellationToken cancellationToken = default);
    /// <summary>
    /// 使用 secret 重置当前 identity 密钥
    /// </summary>
    /// <param name="identity"></param>
    /// <param name="secret"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task ResetSecret(TIdentity identity, string secret, CancellationToken cancellationToken = default);
    /// <summary>
    /// 使用 identity 自有密钥加密明文, 返回密文
    /// </summary>
    /// <param name="identity">当前身份</param>
    /// <param name="plaintext">明文</param>
    /// <returns></returns>
    string Encrypt(TIdentity identity, string plaintext);
    /// <summary>
    /// 使用 identity 自有密钥解密密文, 返回明文
    /// </summary>
    /// <param name="identity">当前身份</param>
    /// <param name="ciphertext">密文</param>
    /// <returns></returns>
    string Decrypt(TIdentity identity, string ciphertext);
}