﻿using Microsoft.AspNetCore.Identity;

namespace Devonline.Identity;

public interface IResourceAccessRuleStore<TResource, TKey> : IDisposable where TResource : class where TKey : IEquatable<TKey>, IConvertible
{
    /// <summary>
    /// Add the specified <paramref name="identity"/> to the named role.
    /// </summary>
    /// <param name="identity">The user to remove the named resource from.</param>
    /// <param name="resource">The access rule of the resource to remove.</param>
    /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
    Task<IdentityResult> AddAccessRuleAsync(IIdentity<TKey> identity, TResource resource, CancellationToken cancellationToken);
    /// <summary>
    /// Add the specified <paramref name="identity"/> to the named role.
    /// </summary>
    /// <param name="identity">The user to remove the named resource from.</param>
    /// <param name="resources">The access rule of the resource to remove.</param>
    /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
    Task<IdentityResult> AddAccessRulesAsync(IIdentity<TKey> identity, IList<TResource> resources, CancellationToken cancellationToken);

    /// <summary>
    /// Remove the specified <paramref name="identity"/> from the named role.
    /// </summary>
    /// <param name="identity">The user to remove the named resource from.</param>
    /// <param name="resource">The access rule of the resource to remove.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
    Task<IdentityResult> RemoveAccessRuleAsync(IIdentity<TKey> identity, TResource resource, CancellationToken cancellationToken);
    /// <summary>
    /// Remove the specified <paramref name="identity"/> from the named role.
    /// </summary>
    /// <param name="identity">The user to remove the named resource from.</param>
    /// <param name="resources">The access rule of the resource to remove.</param>
    /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
    /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
    Task<IdentityResult> RemoveAccessRulesAsync(IIdentity<TKey> identity, IList<TResource> resources, CancellationToken cancellationToken);
}