﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Devonline.Identity;

/// <summary>
/// Creates a new instance of a persistence store for the specified user type.
/// </summary>
/// <typeparam name="TUser">The type representing a user.</typeparam>
public class ResourceAccessStore : ResourceAccessStore<User, Resource, Level, AccessRule, IdentityDbContext, string>
{
    /// <summary>
    /// Constructs a new instance of <see cref="UserStore{TUser}"/>.
    /// </summary>
    /// <param name="context">The <see cref="IdentityDbContext"/>.</param>
    /// <param name="describer">The <see cref="IdentityErrorDescriber"/>.</param>
    public ResourceAccessStore(IdentityDbContext context, DefaultIdentityErrorDescriber describer) : base(context, describer) { }
}

/// <summary>
/// Represents a new instance of a persistence store for the specified user and role types.
/// </summary>
/// <typeparam name="TUser">The type representing a user.</typeparam>
/// <typeparam name="TRole">The type representing a role.</typeparam>
/// <typeparam name="TContext">The type of the data context class used to access the store.</typeparam>
/// <typeparam name="TKey">The type of the primary key for a role.</typeparam>
public class ResourceAccessStore<TUser, TResource, TLevel, TAccessRule, TContext, TKey> :
    IResourceStore<TResource, TKey>,
    ILevelStore<TResource, TKey>,
    IAccessRuleStore<TAccessRule, TKey>,
    IResourceAccessRuleStore<TResource, TKey>,
    IAccessApplyStore<TUser, TResource, TKey>,
    IAccessRecordStore<TUser, TResource, TKey>
    where TKey : IEquatable<TKey>, IConvertible
    where TUser : User<TKey>
    where TResource : Resource<TKey>
    where TLevel : Level<TKey>
    where TAccessRule : AccessRule<TKey>, new()
    where TContext : DbContext
{
    protected readonly TContext _context;
    protected readonly DefaultIdentityErrorDescriber _errorDescriber;
    protected readonly DbSet<TResource> _resources;
    protected readonly DbSet<TAccessRule> _accessRules;
    private bool _disposed;

    /// <summary>
    /// Constructs a new instance of <see cref="UserStore{TUser, TRole, TContext, TKey}"/>.
    /// </summary>
    /// <param name="context">The <see cref="DbContext"/>.</param>
    /// <param name="describer">The <see cref="DefaultIdentityErrorDescriber"/>.</param>
    public ResourceAccessStore(TContext context, DefaultIdentityErrorDescriber describer)
    {
        _context = context;
        _errorDescriber = describer;
        _resources = _context.Set<TResource>();
        _accessRules = _context.Set<TAccessRule>();
    }

    #region implement from IIdentityStore
    /// <summary>
    /// find a resource by id
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<TResource> FindByIdAsync(TKey id, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (id.Equals(default))
        {
            throw new ArgumentNullException(nameof(id));
        }

        return (await _resources.FindAsync(new object[] { id }, cancellationToken)) ?? throw new ArgumentNullException(nameof(id), $"the user of id {id} not exist!");
    }
    /// <summary>
    /// find a resource by name
    /// </summary>
    /// <param name="name"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<TResource?> FindByNameAsync(string name, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (name.IsNullOrWhiteSpace())
        {
            throw new ArgumentNullException(nameof(name));
        }

        return (await _resources.FirstOrDefaultAsync(x => x.Name == name, cancellationToken)) ?? throw new ArgumentNullException(nameof(name), $"the user of name {name} not exist!");
    }

    /// <summary>
    /// set resource name
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="name"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetNameAsync(TResource resource, string name, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.Name = name;
        return Task.CompletedTask;
    }
    /// <summary>
    /// set resource alice
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="alias"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetAliasAsync(TResource resource, string alias, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.Alias = alias;
        return Task.CompletedTask;
    }
    /// <summary>
    /// set resource image
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="image"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetImageAsync(TResource resource, string image, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.Image = image;
        return Task.CompletedTask;
    }
    /// <summary>
    /// 修改资源类型
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="authorizeType"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual Task SetAuthorizeTypeAsync(TResource resource, AuthorizeType authorizeType, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.Type = authorizeType;
        return Task.CompletedTask;
    }
    #endregion

    #region implement from IResourceStore
    /// <summary>
    /// get resources by owner id
    /// </summary>
    /// <param name="ownerId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<IList<TResource>?> GetByOwnerAsync(TKey ownerId, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (ownerId.Equals(default))
        {
            throw new ArgumentNullException(nameof(ownerId));
        }

        return await _resources.Where(x => x.OwnerId != null && x.OwnerId.Equals(ownerId)).ToListAsync(cancellationToken);
    }
    /// <summary>
    /// get children resources by current resource id
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<IList<TResource>> GetChildrenResourcesAsync(TKey id, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (id.Equals(default))
        {
            throw new ArgumentNullException(nameof(id));
        }

        return await _resources.Where(x => x.ParentId != null && x.ParentId.Equals(id)).ToListAsync(cancellationToken);
    }
    /// <summary>
    /// get parent resource of current resource
    /// </summary>
    /// <param name="id"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<TResource?> GetParentResourceAsync(TKey id, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (id.Equals(default))
        {
            throw new ArgumentNullException(nameof(id));
        }

        var resource = await FindByIdAsync(id, cancellationToken);
        if (resource is null)
        {
            throw new ArgumentException($"the id of resource {id} not found!");
        }

        return await _resources.FirstOrDefaultAsync(x => x.Id.Equals(resource.ParentId), cancellationToken);
    }
    /// <summary>
    /// get the resources that user has been authorized
    /// TODO TBD this function need to be defined
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual Task<IList<TResource>?> GetUserAuthorizedResourcesAsync(TKey userId, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (userId.Equals(default))
        {
            throw new ArgumentNullException(nameof(userId));
        }

        throw new NotImplementedException();
    }
    /// <summary>
    /// set resource owner
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="ownerId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual Task SetOwnerAsync(TResource resource, TKey ownerId, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.OwnerId = ownerId;
        return Task.CompletedTask;
    }
    /// <summary>
    /// set resource parent
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="parentId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual Task SetParentAsync(TResource resource, TKey parentId, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.ParentId = parentId;
        return Task.CompletedTask;
    }
    /// <summary>
    /// set resource content
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="content"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual Task SetContentAsync(TResource resource, string content, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.Content = content;
        return Task.CompletedTask;
    }
    /// <summary>
    /// set resource type
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="resourceType"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetResourceTypeAsync(TResource resource, ResourceType resourceType, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.ResourceType = resourceType;
        return Task.CompletedTask;
    }
    /// <summary>
    /// set resource access level
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="accessLevel"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetAccessLevelAsync(TResource resource, AccessLevel accessLevel, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.AccessLevel = accessLevel;
        return Task.CompletedTask;
    }
    /// <summary>
    /// 设置资源级别
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="level"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task SetLevelAsync(TResource resource, string level, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        var existLevel = await _context.Set<TLevel>().FirstOrDefaultAsync(x => x.Name == level, cancellationToken);
        if (existLevel is null)
        {
            throw new ArgumentNullException(nameof(level));
        }

        resource.LevelId = existLevel.Id;
    }
    #endregion

    #region implement from IAccessRuleStore
    /// <summary>
    /// 获取当前资源的所有访问规则
    /// </summary>
    /// <param name="resourceId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<IList<TAccessRule>> GetByResourceIdAsync(TKey resourceId, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (resourceId.Equals(default))
        {
            throw new ArgumentNullException(nameof(resourceId));
        }

        return await _accessRules.Where(x => resourceId.Equals(x.ResourceId)).ToListAsync(cancellationToken);
    }
    /// <summary>
    /// 按授权对象获取所有访问规则列表
    /// </summary>
    /// <param name="identityId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<IList<TAccessRule>> GetByIdentityIdAsync(TKey identityId, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (identityId.Equals(default))
        {
            throw new ArgumentNullException(nameof(identityId));
        }

        return await _accessRules.Where(x => identityId.Equals(x.IdentityId)).ToListAsync(cancellationToken);
    }

    /// <summary>
    /// 允许一个访问规则
    /// </summary>
    /// <param name="accessRule"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task AllowAsync(TAccessRule accessRule, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (accessRule is null)
        {
            throw new ArgumentNullException(nameof(accessRule));
        }

        accessRule.IsAllow = AllowType.Allow;
        return Task.CompletedTask;
    }
    /// <summary>
    /// 禁止一个访问规则
    /// </summary>
    /// <param name="accessRule"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task ForbidAsync(TAccessRule accessRule, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (accessRule is null)
        {
            throw new ArgumentNullException(nameof(accessRule));
        }

        accessRule.IsAllow = AllowType.Forbid;
        return Task.CompletedTask;
    }
    /// <summary>
    /// 设置访问规则的优先级
    /// </summary>
    /// <param name="accessRule"></param>
    /// <param name="priority"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetPriorityAsync(TAccessRule accessRule, int priority, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (accessRule is null)
        {
            throw new ArgumentNullException(nameof(accessRule));
        }

        accessRule.Priority = priority;
        return Task.CompletedTask;
    }
    /// <summary>
    /// 设置访问规则的额外条件
    /// </summary>
    /// <param name="accessRule"></param>
    /// <param name="condition"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task SetConditionAsync(TAccessRule accessRule, string condition, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (accessRule is null)
        {
            throw new ArgumentNullException(nameof(accessRule));
        }

        accessRule.Condition = condition;
        return Task.CompletedTask;
    }
    #endregion

    #region implement from IResourceAccessRuleStore
    /// <summary>
    /// 对当前访问对象添加访问资源的权限
    /// </summary>
    /// <typeparam name="TIdentity"></typeparam>
    /// <param name="identity"></param>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> AddAccessRuleAsync(IIdentity<TKey> identity, TResource resource, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (identity is null || identity.Id.Equals(default))
        {
            throw new ArgumentNullException(nameof(identity));
        }

        if (resource is null || resource.Id.Equals(default))
        {
            throw new ArgumentNullException(nameof(resource));
        }

        var identityType = identity.GetIdentityType();
        if (identity.Name is not null && resource.Name is not null && await _accessRules.AnyAsync(x => x.IdentityType == identityType && identity.Id.Equals(x.IdentityId) && resource.Id.Equals(x.ResourceId), cancellationToken))
        {
            return IdentityResult.Failed(identity switch
            {
                User<TKey> => _errorDescriber.UserAlreadyInAccessRule(identity.Name, resource.Name),
                Role<TKey> => _errorDescriber.RoleAlreadyInAccessRule(identity.Name, resource.Name),
                Group<TKey> => _errorDescriber.GroupAlreadyInAccessRule(identity.Name, resource.Name),
                _ => _errorDescriber.DefaultError()
            });
        }

        var accessRule = new TAccessRule
        {
            ResourceId = resource.Id,
            IdentityId = identity.Id,
            IdentityType = identity.GetIdentityType()
        };

        accessRule.Create();
        accessRule.Update();

        try
        {
            _context.Add(accessRule);
            await _context.SaveChangesAsync(cancellationToken);
        }
        catch (Exception)
        {
            return IdentityResult.Failed(_errorDescriber.DefaultError());
        }

        return IdentityResult.Success;
    }
    /// <summary>
    /// 对当前访问对象添加访问资源列表的权限
    /// </summary>
    /// <typeparam name="TIdentity"></typeparam>
    /// <param name="identity"></param>
    /// <param name="resources"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> AddAccessRulesAsync(IIdentity<TKey> identity, IList<TResource> resources, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (identity is null || identity.Id.Equals(default))
        {
            throw new ArgumentNullException(nameof(identity));
        }

        if (resources.IsNullOrEmpty() || resources.Any(x => x.Id.Equals(default)))
        {
            throw new ArgumentNullException(nameof(resources));
        }

        var identityType = identity.GetIdentityType();
        var resoourceIds = await _accessRules.Where(x => x.IdentityType == identityType && identity.Id.Equals(x.IdentityId) && resources.Select(a => a.Id).Contains(x.ResourceId)).Select(x => x.ResourceId).ToListAsync(cancellationToken);
        if (resoourceIds.IsNotNullOrEmpty())
        {
            resources = resources.Where(x => !resoourceIds.Contains(x.Id)).ToList();
        }

        if (resources.IsNotNullOrEmpty())
        {
            try
            {
                foreach (var resource in resources)
                {
                    var accessRule = new TAccessRule
                    {
                        ResourceId = resource.Id,
                        IdentityType = identityType,
                        IdentityId = identity.Id
                    };

                    accessRule.Create();
                    accessRule.Update();

                    _context.Add(accessRule);
                }

                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception)
            {
                return IdentityResult.Failed(_errorDescriber.DefaultError());
            }
        }

        return IdentityResult.Success;
    }
    /// <summary>
    /// 对当前访问对象移除资源访问的权限
    /// </summary>
    /// <typeparam name="TIdentity"></typeparam>
    /// <param name="identity"></param>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> RemoveAccessRuleAsync(IIdentity<TKey> identity, TResource resource, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (identity is null || identity.Id.Equals(default))
        {
            throw new ArgumentNullException(nameof(identity));
        }

        if (resource is null || resource.Id.Equals(default))
        {
            throw new ArgumentNullException(nameof(resource));
        }

        var identityType = identity.GetIdentityType();
        var accessRule = await _accessRules.FirstOrDefaultAsync(x => x.IdentityType == identityType && identity.Id.Equals(x.IdentityId) && resource.Id.Equals(x.ResourceId), cancellationToken);
        if (accessRule is null && identity.Name is not null && resource.Name is not null)
        {
            return IdentityResult.Failed(identity switch
            {
                User<TKey> => _errorDescriber.UserNotInAccessRule(identity.Name, resource.Name),
                Role<TKey> => _errorDescriber.RoleNotInAccessRule(identity.Name, resource.Name),
                Group<TKey> => _errorDescriber.GroupNotInAccessRule(identity.Name, resource.Name),
                _ => _errorDescriber.DefaultError()
            });
        }

        if (accessRule is not null)
        {
            try
            {
                _context.Remove(accessRule);
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException)
            {
                return IdentityResult.Failed(_errorDescriber.ConcurrencyFailure());
            }
        }

        return IdentityResult.Success;
    }
    /// <summary>
    /// 对当前访问对象移除列表中资源访问的权限
    /// </summary>
    /// <typeparam name="TIdentity"></typeparam>
    /// <param name="identity"></param>
    /// <param name="resources"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> RemoveAccessRulesAsync(IIdentity<TKey> identity, IList<TResource> resources, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();

        if (identity is null || identity.Id.Equals(default))
        {
            throw new ArgumentNullException(nameof(identity));
        }

        if (resources.IsNullOrEmpty() || resources.Any(x => x.Id.Equals(default)))
        {
            throw new ArgumentNullException(nameof(resources));
        }

        var identityType = identity.GetIdentityType();
        var accessRules = await _accessRules.Where(x => x.IdentityType == identityType && identity.Id.Equals(x.IdentityId) && resources.Select(a => a.Id).Contains(x.ResourceId)).ToListAsync(cancellationToken);
        if (accessRules.IsNotNullOrEmpty())
        {
            _accessRules.RemoveRange(accessRules);

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException)
            {
                return IdentityResult.Failed(_errorDescriber.ConcurrencyFailure());
            }
        }

        return IdentityResult.Success;
    }
    #endregion

    #region 数据操作方法
    /// <summary>
    /// Creates the specified resource in the store.
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> CreateAsync(TResource resource, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        resource.Create();
        resource.Update();
        _resources.Add(resource);
        await _context.SaveChangesAsync(cancellationToken);
        return IdentityResult.Success;
    }
    /// <summary>
    /// Updates the specified resource in the store.
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> UpdateAsync(TResource resource, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        _resources.Attach(resource);
        resource.Update();
        _resources.Update(resource);

        try
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
        catch (DbUpdateConcurrencyException)
        {
            return IdentityResult.Failed(_errorDescriber.ConcurrencyFailure());
        }

        return IdentityResult.Success;
    }
    /// <summary>
    /// Delete the specified resource from the store.
    /// </summary>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public virtual async Task<IdentityResult> DeleteAsync(TResource resource, CancellationToken cancellationToken = default)
    {
        cancellationToken.ThrowIfCancellationRequested();
        ThrowIfDisposed();
        if (resource is null)
        {
            throw new ArgumentNullException(nameof(resource));
        }

        _resources.Remove(resource);

        try
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
        catch (DbUpdateConcurrencyException)
        {
            return IdentityResult.Failed(_errorDescriber.ConcurrencyFailure());
        }

        return IdentityResult.Success;
    }

    /// <summary>
    /// TODO TBI
    /// </summary>
    /// <param name="user"></param>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task ApplyAsync(TUser user, TResource resource, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
    /// <summary>
    /// TODO TBI
    /// </summary>
    /// <param name="user"></param>
    /// <param name="resource"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task RecordAsync(TUser user, TResource resource, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    #endregion
    /// <summary>
    /// Throws if this class has been disposed.
    /// </summary>
    protected void ThrowIfDisposed()
    {
        if (_disposed)
        {
            throw new ObjectDisposedException(GetType().Name);
        }
    }
    /// <summary>
    /// Dispose the stores
    /// </summary>
    public void Dispose()
    {
        _disposed = true;
        GC.SuppressFinalize(this);
    }
}