﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// identity change owner view model
/// </summary>
public class ChangeOwnerViewModel : ViewModel
{
    /// <summary>
    /// 所有者编号
    /// </summary>
    public string? OwnerId { get; set; }
    /// <summary>
    /// 所属类型 IdentityType 枚举值
    /// </summary>
    public IdentityType IdentityType { get; set; }
}