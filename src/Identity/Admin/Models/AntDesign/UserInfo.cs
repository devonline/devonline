﻿using Newtonsoft.Json;

namespace Devonline.Identity.Admin.Models.AntDesign;

public class UserInfo
{
    [JsonProperty("name")]
    public string? Name { get; set; }
    [JsonProperty("avatar")]
    public string? Avatar { get; set; }
    [JsonProperty("userid")]
    public string? Userid { get; set; }
    [JsonProperty("email")]
    public string? Email { get; set; }
    [JsonProperty("signature")]
    public string? Signature { get; set; }
    [JsonProperty("title")]
    public string? Title { get; set; }
    [JsonProperty("group")]
    public string? Group { get; set; }
    [JsonProperty("tags")]
    public ICollection<KeyLabelPair>? Tags { get; set; }
    [JsonProperty("notifyCount")]
    public int? NotifyCount { get; set; }
    [JsonProperty("unreadCount")]
    public int? UnreadCount { get; set; }
    [JsonProperty("country")]
    public string? Country { get; set; }
    [JsonProperty("access")]
    public string? Access { get; set; }
    [JsonProperty("geographic")]
    public GeoGraphic? Geographic { get; set; }
    [JsonProperty("address")]
    public string? Address { get; set; }
    [JsonProperty("phone")]
    public string? Phone { get; set; }
}

/// <summary>
/// 地理位置
/// </summary>
public class GeoGraphic
{
    [JsonProperty("province")]
    public KeyLabelPair? Province { get; set; }
    [JsonProperty("city")]
    public KeyLabelPair? City { get; set; }
}

/// <summary>
/// key and label pair
/// </summary>
public class KeyLabelPair
{
    public string? Key { get; set; }
    public string? Label { get; set; }
}