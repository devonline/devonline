﻿using Newtonsoft.Json;

namespace Devonline.Identity.Admin.Models.AntDesign;

public class LoginResult
{
    [JsonProperty("status")]
    public string? Status { get; set; }
    [JsonProperty("type")]
    public string? Type { get; set; }
    [JsonProperty("currentAuthority")]
    public string? CurrentAuthority { get; set; }
    [JsonProperty("userInfo")]
    public UserInfo? UserInfo { get; set; }
}