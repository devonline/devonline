﻿namespace Devonline.Identity.Admin.Models.AntDesign;

/// <summary>
/// 验证码
/// </summary>
public class CaptchaModel
{
    /// <summary>
    /// 验证码
    /// </summary>
    public string? Code { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    public string? Status { get; set; }
}