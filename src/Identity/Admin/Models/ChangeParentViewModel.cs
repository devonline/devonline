﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// entity change parent view model
/// </summary>
public class ChangeParentViewModel : ViewModel
{
    /// <summary>
    /// parent id
    /// </summary>
    public string? ParentId { get; set; }
}