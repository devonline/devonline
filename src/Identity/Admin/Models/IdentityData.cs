﻿using IdentityServer4.Models;
using Client = IdentityServer4.Models.Client;

namespace Devonline.Identity.Admin.Models;

public class IdentityData
{
    public List<Client> Clients { get; set; } = [];
    public List<IdentityResource> IdentityResources { get; set; } = [];
    public List<ApiResource> ApiResources { get; set; } = [];
    public List<ApiScope> ApiScopes { get; set; } = [];
}