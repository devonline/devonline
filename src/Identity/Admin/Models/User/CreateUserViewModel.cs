﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 创建用户模型, 供管理员使用
/// </summary>
public class CreateUserViewModel : IdentityViewModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 邮箱
    /// </summary>
    public string? Email { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string? PhoneNumber { get; set; }
}