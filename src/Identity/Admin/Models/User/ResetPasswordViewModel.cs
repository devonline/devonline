﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 重置密码视图模型, 供用户使用
/// </summary>
public class ResetPasswordViewModel : ViewModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 重置密码
    /// </summary>
    public string? Password { get; set; }
    /// <summary>
    /// 操作令牌
    /// </summary>
    public string? Token { get; set; }
}