﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 用户查询视图模型
/// </summary>
public class UserIdentityViewModel : IdentityViewModel, IViewModel, IEntitySet
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 邮箱
    /// </summary>
    public string? Email { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string? PhoneNumber { get; set; }
}