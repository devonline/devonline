﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 修改密码视图模型, 供用户使用
/// </summary>
public class ChangePasswordViewModel : ViewModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 原始密码
    /// </summary>
    public string? Password { get; set; }
    /// <summary>
    /// 新密码
    /// </summary>
    public string? NewPassword { get; set; }
}