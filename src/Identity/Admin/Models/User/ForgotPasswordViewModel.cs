﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 忘记密码视图模型, 供用户使用
/// </summary>
public class ForgotPasswordViewModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string? PhoneNumber { get; set; }
}