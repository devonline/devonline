﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 修改电子邮件视图模型, 供用户使用
/// </summary>
public class ChangeEmailViewModel : ViewModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string? Email { get; set; }
    /// <summary>
    /// 操作令牌
    /// </summary>
    public string? Token { get; set; }
}