﻿namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 修改手机号码视图模型, 供用户使用
/// </summary>
public class ChangePhoneNumberViewModel : ViewModel
{
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 手机号码
    /// </summary>
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 操作令牌
    /// </summary>
    public string? Token { get; set; }
}