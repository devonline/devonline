﻿namespace Devonline.Identity.Admin.Models;

public class GroupWithChildrenViewModel : Group
{
    public string? ParentName => Parent != null ? Parent.Name : string.Empty;
    public ICollection<GroupWithChildrenViewModel>? Children { get; set; }
}