﻿namespace Devonline.Identity.Admin.Models;

public class UserGroupViewModel
{
    public string? UserName { get; set; }
    public string? GroupName { get; set; }
}