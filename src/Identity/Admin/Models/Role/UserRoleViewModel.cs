﻿namespace Devonline.Identity.Admin.Models;

public class UserRoleViewModel : ViewModel
{
    public string? UserName { get; set; }
    public string? RoleName { get; set; }
}