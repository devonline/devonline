﻿namespace Devonline.Identity.Admin.Models;

public class ResourceChangeViewModel : IdentityViewModel
{
    /// <summary>
    /// 资源标题
    /// </summary>
    public string? Title { get; set; }
    /// <summary>
    /// 资源访问地址
    /// </summary>
    public string? Content { get; set; }
    /// <summary>
    /// parent id
    /// </summary>
    public string? ParentId { get; set; }
    /// <summary>
    /// 资源类型 ResourceType 枚举类型的值
    /// </summary>
    public ResourceType ResourceType { get; set; }
}
