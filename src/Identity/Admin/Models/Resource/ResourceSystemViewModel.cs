﻿namespace Devonline.Identity.Admin.Models;

public class ResourceSystemViewModel
{
    public List<Devonline.Identity.ResourceViewModel>? Resources { get; set; }
}