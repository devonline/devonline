﻿namespace Devonline.Identity.Admin.Models;

public class ResourceChangeLevelViewModel : ViewModel
{
    /// <summary>
    /// 资源级别
    /// </summary>
    public string? LevelName { get; set; }
    /// <summary>
    /// 资源定义的受保护级别 AccessLevel 枚举类型的值
    /// </summary>
    public AccessLevel AccessLevel { get; set; }
}