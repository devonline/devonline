﻿namespace Devonline.Identity.Admin.Models;

public class ResourceViewModel : IdentityViewModel
{
    /// <summary>
    /// 资源标题
    /// </summary>
    public string? Title { get; set; }
    /// <summary>
    /// 资源内容
    /// </summary>
    public string? Content { get; set; }
    /// <summary>
    /// 上级组织
    /// </summary>
    public string? ParentId { get; set; }
    /// <summary>
    /// 资源所有者编号
    /// </summary>
    public string? OwnerId { get; set; }
    /// <summary>
    /// 资源级别, 即资源定义的最低访问级别
    /// </summary>
    public string? LevelId { get; set; }
    /// <summary>
    /// 资源类型 ResourceType 枚举类型的值
    /// </summary>
    public ResourceType ResourceType { get; set; }
    /// <summary>
    /// 资源所属类型 IdentityType 枚举值
    /// </summary>
    public IdentityType IdentityType { get; set; }
    /// <summary>
    /// 资源级别, 即资源定义的最低访问级别
    /// </summary>
    public AccessLevel AccessLevel { get; set; }
}