﻿using System.ComponentModel;

namespace Devonline.Identity.Admin.Models;

[DisplayName("资源")]
public class ResourceExportModel : IdentityViewModel
{
    /// <summary>
    /// 父级资源编号
    /// </summary>
    [DisplayName("父级资源"), Excel]
    public string? ParentName => Parent?.Name;
    /// <summary>
    /// 资源所有者类型 IdentityType 枚举值
    /// </summary>
    [DisplayName("资源所有者类型"), Excel]
    public IdentityType IdentityType { get; set; }
    /// <summary>
    /// 资源所有者编号
    /// </summary>
    [DisplayName("资源所有者"), Excel]
    public string? OwnerName { get; set; }
    /// <summary>
    /// 资源类型 ResourceType 枚举类型的值
    /// </summary>
    [DisplayName("资源类型"), Excel]
    public ResourceType ResourceType { get; set; }
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("资源名称"), Excel]
    public override string? Name { get; set; } = null!;
    /// <summary>
    /// 资源编号, 用于内部命名, 排序等
    /// 资源访问的统一规则制定的总体约束, 形如: R35, #35
    /// </summary>
    [DisplayName("资源编号"), Excel(Size = DEFAULT_EXCEL_COLUMN_WIDTH)]
    public string? Code { get; set; }
    /// <summary>
    /// 资源标题
    /// </summary>
    [DisplayName("资源标题"), Excel]
    public string? Title { get; set; }
    /// <summary>
    /// 资源保存的具体内容, 可能是密文
    /// </summary>
    [DisplayName("资源内容"), Excel(Size = UNIT_FOUR * UNIT_TEN)]
    public string? Content { get; set; }
    /// <summary>
    /// 资源定义的可访问级别 AccessLevel 枚举类型的值
    /// </summary>
    [DisplayName("访问级别"), Excel]
    public AccessLevel AccessLevel { get; set; }
    /// <summary>
    /// 资源级别, 即资源定义的最低访问级别
    /// </summary>
    [DisplayName("资源级别"), Excel(Size = DEFAULT_EXCEL_COLUMN_WIDTH)]
    public string? LevelName => Level?.Name;

    /// <summary>
    /// 此处可用任意存在导出属性的类型
    /// </summary>
    public IdentityViewModel? Parent { get; set; }
    /// <summary>
    /// 此处可用任意存在导出属性的类型
    /// </summary>
    public IdentityViewModel? Level { get; set; }
}