﻿namespace Devonline.Identity.Admin.Models;

public class PersistedGrantViewModel
{
    public string? SubjectId { get; set; }
    public string? SubjectName { get; set; }
}