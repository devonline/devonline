﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Devonline.Identity.Admin.Models;

public class AttachmentViewModel : ViewModel
{
    /// <summary>
    /// 附件名
    /// </summary>
    [DisplayName("附件名"), MaxLength(128), Excel]
    public virtual string Name { get; set; } = null!;
    /// <summary>
    /// 文件大小
    /// </summary>
    [DisplayName("文件大小"), Excel]
    public virtual long Size { get; set; }

    public virtual string Extension { get; set; } = null!;
    /// <summary>
    /// 文件的 content type
    /// </summary>
    [DisplayName("内容类型"), MaxLength(128), Excel]
    public virtual string Type { get; set; } = null!;
    /// <summary>
    /// 附件地址, 相对地址加上文件名
    /// </summary>
    [DisplayName("附件地址"), MaxLength(255), Excel]
    public virtual string Path { get; set; } = null!;
}