﻿namespace Devonline.Identity.Admin.Models;

public class ProfileViewModel
{
    public string? ClientId { get; set; }
    public string? ClientName { get; set; }
    public string? ClientUri { get; set; }
}