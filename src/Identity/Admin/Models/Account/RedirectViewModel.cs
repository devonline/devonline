namespace Devonline.Identity.Admin.Models;

public class RedirectViewModel
{
    public string? RedirectUrl { get; set; }
}