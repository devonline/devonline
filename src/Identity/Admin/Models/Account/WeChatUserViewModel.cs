using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 微信开放平台用户信息参数
/// </summary>
public class WeChatUserViewModel : ViewModel
{
    /// <summary>
    /// 用户标识，对当前开发者帐号唯一
    /// </summary>
    [JsonProperty("openid"), Display(Name = "用户标识")]
    public string? OpenId { get; set; }
    /// <summary>
    /// 昵称
    /// </summary>
    [JsonProperty("nickname"), Display(Name = "昵称")]
    public string? NickName { get; set; }
    /// <summary>
    /// 性别
    /// </summary>
    private string? _sex;
    /// <summary>
    /// 1 为男性，2 为女性
    /// </summary>
    [JsonProperty("sex"), Display(Name = "性别")]
    public string? Sex
    {
        get => _sex == "1" ? Gender.Male.ToString() : Gender.Female.ToString();
        set => _sex = value;
    }
    /// <summary>
    /// 省份
    /// </summary>
    [JsonProperty("province"), Display(Name = "省份")]
    public string? Province { get; set; }
    /// <summary>
    /// 城市
    /// </summary>
    [JsonProperty("city"), Display(Name = "城市")]
    public string? City { get; set; }
    /// <summary>
    /// 国籍
    /// </summary>
    [JsonProperty("country"), Display(Name = "国籍")]
    public string? Country { get; set; }
    /// <summary>
    /// 微信头像
    /// </summary>
    [JsonProperty("headimgurl"), Display(Name = "微信头像")]
    public string? HeadimgUrl { get; set; }
    /// <summary>
    /// 特权信息
    /// </summary>
    [JsonProperty("privilege"), Display(Name = "特权信息")]
    public string? Privilege { get; set; }
    /// <summary>
    /// 统一标识
    /// 针对一个微信开放平台帐号下的应用，同一用户的 unionid 是唯一的。
    /// 开发者最好保存 unionID 信息，以便以后在不同应用之间进行用户信息互通。
    /// </summary>
    [JsonProperty("unionid"), Display(Name = "统一标识")]
    public string? UnionId { get; set; }
}