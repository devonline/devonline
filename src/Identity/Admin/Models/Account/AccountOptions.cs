﻿namespace Devonline.Identity.Admin.Models;

public class AccountOptions
{
    public static bool AllowLocalLogin = true;
    public static bool AllowRememberLogin = true;
    public static TimeSpan RememberMeLoginDuration = TimeSpan.FromDays(30);

    public static bool ShowLogoutPrompt = true;
    public static bool AutomaticRedirectAfterSignOut = false;

    public static string ErrorMessageInvalidCredentials = "用户名或密码不正确, 请重新输入!";
    public static string ErrorMessageLockedOut = "当前用户已被锁定, 请联系管理员处理!";
    public static string ErrorMessageNotAllowed = "当前用户不允许登陆, 请联系管理员处理!";
    public static string ErrorMessagePhoneNumberUserNotFound = "当前手机号码的用户不存在!";
    public static string ErrorMessageSmsCaptchaExpired = "短信验证码已过期!";
    public static string ErrorMessageSmsCaptchaError = "短信验证码错误, 请重新输入!";
}