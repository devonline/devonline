﻿namespace Devonline.Identity.Admin.Models;

public class ExternalProvider
{
    /// <summary>
    /// 显示名称
    /// </summary>
    public string? DisplayName { get; set; }
    /// <summary>
    /// 认证 Scheme
    /// </summary>
    public string? AuthenticationScheme { get; set; }
    /// <summary>
    /// 图标
    /// </summary>
    public string? Icon { get; set; }
}