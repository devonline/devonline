﻿namespace Devonline.Identity.Admin.Models;

public class LogoutInputModel
{
    public string? LogoutId { get; set; }
}