using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 微信开放平台接口参数
/// </summary>
public class WeChatAppAuthViewModel : ViewModel
{
    /// <summary>
    /// 调用授权关系接口的调用凭证,有效期 2 H
    /// </summary>
    [JsonProperty("access_token"), Display(Name = "用户刷新 access_token")]
    public string? AccessToken { get; set; }
    /// <summary>
    /// 调用凭证超时时间
    /// </summary>
    [JsonProperty("expires_in"), Display(Name = "调用凭证超时时间")]
    public string? ExpiresIn { get; set; }
    /// <summary>
    /// 刷新 access_token ,有效期（30 天），失效后用户重新授权
    /// </summary>
    [JsonProperty("refresh_token"), Display(Name = "用户刷新 access_token")]
    public string? RefreshToken { get; set; }
    /// <summary>
    /// open id 用户唯一标识
    /// </summary>
    [JsonProperty("openid"), Display(Name = "微信编号")]
    public string? OpenId { get; set; }
    /// <summary>
    /// 有效范围
    /// </summary>
    [JsonProperty("scope"), Display(Name = "有效范围")]
    public string? Scope { get; set; }

    /// <summary>
    /// 用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明
    /// </summary>
    [JsonProperty("unionid"), Display(Name = "用户在开放平台的唯一标识符")]
    public string? Unionid { get; set; }
    /// <summary>
    /// 错误码
    /// </summary>
    [JsonProperty("errcode"), Display(Name = "错误码")]
    public int ErrorCode { get; set; }
    /// <summary>
    /// 错误信息
    /// </summary>
    [JsonProperty("errmsg"), Display(Name = "错误信息")]
    public string? ErrorMessage { get; set; }
}
