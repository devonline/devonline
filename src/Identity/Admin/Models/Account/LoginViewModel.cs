using AspNet.Security.OAuth.Weixin;

namespace Devonline.Identity.Admin.Models;

public class LoginViewModel : LoginModel
{
    public bool AllowRememberLogin { get; set; } = true;
    public bool EnableLocalLogin { get; set; } = true;
    public string? ErrorMessage { get; set; }
    /// <summary>
    /// OAuth State
    /// </summary>
    public string? State { get; set; }

    public IEnumerable<ExternalProvider> ExternalProviders { get; set; } = Enumerable.Empty<ExternalProvider>();
    public bool IsExternalLoginOnly => EnableLocalLogin == false && ExternalProviders?.Count() == 1;
    public bool HasWeixinLogin => ExternalProviders?.Any(x => x.AuthenticationScheme == WeixinAuthenticationDefaults.AuthenticationScheme) ?? false;
    public bool HasExternalLogin => ExternalProviders?.Any(x => x.AuthenticationScheme != WeixinAuthenticationDefaults.AuthenticationScheme) ?? false;
    public string? ExternalLoginScheme => IsExternalLoginOnly ? ExternalProviders?.SingleOrDefault()?.AuthenticationScheme : null;
}