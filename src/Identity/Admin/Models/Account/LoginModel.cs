namespace Devonline.Identity.Admin.Models;

/// <summary>
/// 登录模型
/// </summary>
public class LoginModel
{
    public LoginType Type { get; set; }
    public string? UserName { get; set; }
    public string? Password { get; set; }
    public string? ReturnUrl { get; set; }
    public bool RememberLogin { get; set; }
    /// <summary>
    /// 验证码编号
    /// </summary>
    public string CaptchaId { get; } = KeyGenerator.GetStringKey();
    /// <summary>
    /// 验证码的值
    /// </summary>
    public string? CaptchaCode { get; set; }
}