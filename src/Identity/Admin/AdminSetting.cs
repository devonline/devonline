﻿using Devonline.CloudService.Aliyun;
using Devonline.CloudService.Tencent.Weixin;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;

namespace Devonline.Identity.Admin;

/// <summary>
/// 认证配置项
/// </summary>
public class AdminSetting : HttpSetting
{
    public AdminSetting()
    {
        UseStaticFiles = true;
        Name = "统一认证与授权中心";
        Copyright = "www.devonline.cn";
        License = "陕ICP备14006366号-1";
    }

    /// <summary>
    /// 微信开放平台 网站应用 接入配置
    /// </summary>
    public OpenPlatformEndpoint? WeixinWebSite { get; set; }
    /// <summary>
    /// 微信开放平台 移动应用 接入配置
    /// </summary>
    public OpenPlatformEndpoint? WeixinApp { get; set; }
    /// <summary>
    /// 微信 小程序 配置
    /// </summary>
    public MiniProgramEndpoint? MiniProgram { get; set; }
    /// <summary>
    /// Github 认证配置
    /// </summary>
    public AuthOptions? GitHub { get; set; }
    /// <summary>
    /// Gitee 认证配置
    /// </summary>
    public AuthOptions? Gitee { get; set; }

    /// <summary>
    /// DataProtection
    /// </summary>
    public DataProtectionOptions? DataProtection { get; set; }

    /// <summary>
    /// 短信账户配置
    /// </summary>
    public SmsEndpoint? Sms { get; set; }
    /// <summary>
    /// 短信验证码模板配置
    /// </summary>
    public SmsModel? SmsCaptcha { get; set; }
    /// <summary>
    /// 登录二次验证短信模板
    /// </summary>
    public SmsModel? SmsAuthorize { get; set; }
    /// <summary>
    /// 短信验证码二次验证登录
    /// </summary>
    public SmsAuthorizeLogin SmsAuthorizeLogin { get; set; } = new();
}

/// <summary>
/// auth options
/// </summary>
public class AuthOptions
{
    /// <summary>
    /// 图标
    /// </summary>
    public string? Icon { get; set; }
    //
    // 摘要:
    //     Gets or sets the provider-assigned client id.
    public string? ClientId { get; set; }
    //
    // 摘要:
    //     Gets or sets the provider-assigned client secret.
    public string? ClientSecret { get; set; }
    //
    // 摘要:
    //     Gets the list of permissions to request.
    public string[]? Scope { get; set; }

    /// <summary>
    /// 自配置方法
    /// </summary>
    /// <param name="options"></param>
    public void Configure(OAuthOptions options)
    {
        ArgumentNullException.ThrowIfNull(ClientId);
        ArgumentNullException.ThrowIfNull(ClientSecret);
        options.ClientId = ClientId;
        options.ClientSecret = ClientSecret;
        options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        if (Scope is not null)
        {
            options.Scope.AddRange(Scope);
        }
    }
}

/// <summary>
/// 短信验证码二次验证登录
/// </summary>
public class SmsAuthorizeLogin
{
    /// <summary>
    /// 启用此功能
    /// </summary>
    public bool Enable { get; set; }
    /// <summary>
    /// 登录校验人员是否需要二次验证
    /// </summary>
    public bool AuthorizeOneself { get; set; } = true;
    /// <summary>
    /// 忽略登录短信验证码二次验证的人员名单/不进行校验的名单
    /// </summary>
    public string? AuthorizeIgnoreUsers { get; set; }
    /// <summary>
    /// 需要登录短信验证码二次验证的顶级单位名称
    /// </summary>
    public string? AuthorizeGroups { get; set; }
    /// <summary>
    /// 登录校验人员角色名
    /// </summary>
    public string? AuthorizorRoles { get; set; }
}