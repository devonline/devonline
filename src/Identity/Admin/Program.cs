﻿global using Devonline.AspNetCore;
global using Devonline.Core;
global using Devonline.Entity;
global using Devonline.Identity.Admin.Models;
global using static Devonline.Core.AppSettings;
using Devonline.AspNetCore.OData;
using Devonline.AuxiliaryTools.AITools;
using Devonline.CloudService.Aliyun;
using Devonline.CloudService.Tencent;
using Devonline.Communication.Messages;
using Devonline.Identity;
using Devonline.Identity.Admin;
using Devonline.Identity.Database.MySQL;
using Devonline.Identity.Database.PostgreSQL;
using Devonline.Logging;
using OfficeOpenXml;

#if DEBUG
ThreadPool.SetMinThreads(UNIT_HUNDRED, UNIT_HUNDRED);
#endif
ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

var builder = WebApplication.CreateBuilder(args);
var appSetting = builder.AddSetting<AdminSetting>();
builder.Host.AddLogging();
builder.WebHost.ConfigureKestrel(options => options.Limits.MaxRequestBodySize = appSetting.Attachment.Upload.Total);
builder.AddCommunicator();
var services = builder.Services;
var mvcBuilder = services.AddIdentityControllersWithViews(appSetting).AddDefaultOData<IdentityDbContext>();

#if DEBUG
mvcBuilder.AddRazorRuntimeCompilation();
#endif

services.AddDefaultServices(appSetting);
services.AddIdentityDataService();
services.AddDefaultIdentity();
services.AddTransient<AuthorizationService>();
services.AddTransient<AuthenticationService>();
services.AddDataProtection(appSetting.DataProtection);
services.AddSmsCaptchaService(appSetting.Sms, appSetting.SmsCaptcha);
services.AddWeixinOpenPlatformService(appSetting.WeixinApp);
services.AddWeixinMiniProgramService(appSetting.MiniProgram);
services.AddTransient<PhoneNumberCaptchaService>();
services.AddTransient<WeixinIdentityService>();
services.AddFramesCaptureService();
services.AddIdentityAuthentication(appSetting);

if (appSetting.DatabaseType == DatabaseType.MySQL)
{
    builder.AddMySQLIdentityServer(appSetting);
}
else
{
    builder.AddPostgreSQLIdentityServer(appSetting);
}

var app = builder.Build();
app.UseDefaultBuilder(appSetting, () => app.UseIdentityServer());
await app.Services.GetRequiredService<IMessageCommunicator>().StartAsync();
app.Run();