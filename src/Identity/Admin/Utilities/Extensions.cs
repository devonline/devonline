using AspNet.Security.OAuth.Weixin;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin;

public static class Extensions
{
    /// <summary>
    /// Checks if the redirect URI is for a native client.
    /// </summary>
    /// <returns></returns>
    public static bool IsNativeClient(this AuthorizationRequest context) => !context.RedirectUri.StartsWith("https", StringComparison.Ordinal) && !context.RedirectUri.StartsWith("http", StringComparison.Ordinal);

    public static IActionResult LoadingPage(this Controller controller, string viewName, string redirectUri)
    {
        controller.HttpContext.Response.StatusCode = 200;
        controller.HttpContext.Response.Headers.Location = "";

        return controller.View(viewName, new RedirectViewModel { RedirectUrl = redirectUri });
    }

    /// <summary>
    /// 注入默认的身份认证相关服务
    /// </summary>
    /// <param name="services">依赖注入服务容器</param>
    /// <param name="appSetting">认证配置项</param>
    /// <returns></returns>
    public static AuthenticationBuilder AddIdentityAuthentication(this IServiceCollection services, AdminSetting appSetting)
    {
        var builder = services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme);

        if (appSetting.WeixinWebSite is not null)
        {
            //实际上微信扫码登录时, 因无法预先写入 OAuth2 10.12 CSRF Cookie 问题, 无法通过 state 验证, 因此仅限微信扫码登录, 且跳过此验证
            builder.AddOAuth<WeixinAuthenticationOptions, WeixinAuthenticationHandler>(WeixinAuthenticationDefaults.AuthenticationScheme, WeixinAuthenticationDefaults.DisplayName, appSetting.WeixinWebSite!.Configure);
        }

        if (appSetting.GitHub is not null)
        {
            builder.AddGitHub(appSetting.GitHub.Configure);
        }

        if (appSetting.Gitee is not null)
        {
            builder.AddGitee(appSetting.Gitee.Configure);
        }

        return builder;
    }
}