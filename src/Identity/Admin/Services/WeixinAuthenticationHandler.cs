﻿using System.Text.Encodings.Web;
using AspNet.Security.OAuth.Weixin;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;

namespace Devonline.Identity.Admin;

/// <summary>
/// 微信认证处理的 handler, 用于调式模式使用
/// </summary>
public class WeixinAuthenticationHandler : AspNet.Security.OAuth.Weixin.WeixinAuthenticationHandler
{
    public WeixinAuthenticationHandler(IOptionsMonitor<WeixinAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder) : base(options, logger, encoder) { }

    /// <summary>
    /// DEBUG 模式无视 OAuth2 10.12 CSRF Cookie 写入问题
    /// </summary>
    /// <param name="properties"></param>
    /// <returns></returns>
    protected override bool ValidateCorrelationId(AuthenticationProperties properties) => true;
}