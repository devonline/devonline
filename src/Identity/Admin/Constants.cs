﻿namespace Devonline.Identity.Admin;

/// <summary>
/// 系统常量
/// </summary>
public static class Constants
{
    /// <summary>
    /// 全资源列表缓存
    /// </summary>
    public const string CACHE_FULL_RESOURCES_LIST = CACHE_APPLICATION + "FULL_RESOURCES_LIST";
    /// <summary>
    /// 全资源树缓存
    /// </summary>
    public const string CACHE_FULL_RESOURCES_TREE = CACHE_APPLICATION + "FULL_RESOURCES_TREE";
    /// <summary>
    /// 用户缓存
    /// </summary>
    public const string CACHE_USER_INFO = "userInfo";
}