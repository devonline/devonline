﻿/**
 * 表格打印预览
 */
class PrintTable {
    constructor(el) {
        this.el = el;
        this.$content = this.el.$el.find('.print-content');
        this.$header = this.$content.find('.print-header');
        this.$table = this.$content.find('.print-table');
        this.$footer = this.$content.find('.print-footer');
        this.$opener = this.el.options.opener;
        this.grid = undefined;

        if (!this.$content.attr('id')) {
            this.$content.attr('id', 'print-content_' + $.com.getSerial());
        }

        this.init();
    }

    /**
     * 初始化
     */
    init() {
        if ($.com.isJqueryObject(this.$opener)) {
            var form = this.$opener.data(_config_.form.namespace);
            if ($.com.hasValue(form) && form.$grid.hasValue()) {
                var grid = form.$grid.data(_config_.grid.namespace);
                if ($.com.hasValue(grid)) {
                    this.grid = grid;
                    this.columns = grid.role.columns.filter(x => x.hidden != true);
                    if (this.columns.length > 0) {
                        var data = grid.role.dataSource.data();
                        this.data = $.com.cleanData(data);
                        if ($.com.isArray(this.data) && this.data.length > 0) {
                            this.drawHeader();
                            this.drawData();
                            this.drawSummary();
                            this.drawFooter();
                        }
                    }
                }
            }
        }
    }

    /**
     * 绘制表头
     */
    drawHeader() {
        debugger;
        this.$header.find('.print-title').html(this.title)

        this.header = [];
        this.summary = [];
        var $tr = "<tr>";
        this.columns.forEach(column => {
            $tr += ($.com.hasValue(column.field) ? "<td>" : "<td style='width:35px'>") + column.title + "</td>";

            if (!!column.hidden) {
                this.header.push({ field: column.field, displayName: column.title });
            }

            if (!!column.summary) {
                this.summary.push(column.field);
            }
        });

        $tr += "</tr>";
        this.$table.append($tr);
    }

    /**
     * 绘制表格
     */
    drawData() {
        this.data.forEach(item => {
            var $tr = "<tr>";
            columns.forEach(column => {
                if ($.com.hasValue(column.field)) {
                    var value = item[column.field];
                    if ($.com.hasValue(column.dataFrom)) {
                        var dataSource = $.com.eval(column.dataFrom);
                        if ($.com.isArray(dataSource)) {
                            value = $.com.commonFormatter(dataSource, item[column.field], item);
                        }
                    } else if (column.formatter) {
                        var fun = $.com.getFunction(column.formatter);
                        if ($.com.isFunction(fun)) {
                            value = fun(column.field, item[column.field], item);
                        }
                    } else if (column.type === 'date' || column.type === 'time' || column.type === 'datetime') {
                        value = $.com.dateTimeFormatter(column.field, _config_.format.monemt[column.type]);
                    }

                    value = value || '';
                } else {
                    value = item._rowIndex;
                }

                $tr += "<td>" + value + "</td>";
            });

            $tr += "</tr>";
            this.$table.append($tr);
        })
    }

    /**
     * 绘制表格底部统计信息
     */
    drawSummary() {
        var summaryColumns = this.columns.filter(x => x.summary == true);
        if ($.com.isArray(summaryColumns)) {
            var $totalTr = "<tr>";
            columns.forEach(column => {
                if ($.com.hasValue(column.field)) {
                    if (summaryColumns.filter(x => x.field == column.field).length > 0) {
                        var total = $.com.toFixed(data.select(column.field).sum(), 2);
                        $totalTr += "<td>" + total + "</td>";
                    } else {
                        $totalTr += "<td></td>";
                    }
                } else {
                    $totalTr += "<td>总计</td>";
                }
            });

            $totalTr += "</tr>";
            this.$table.append($totalTr);
        }
    }

    /**
     * 绘制底部
     */
    drawFooter() {
        this.$footer.html(new Date().format('ll'));
    }
}