﻿/*****
* 系统客户端全局配置文件
*/

var _config_ = new function () {
    'use strict';
    var that = this;

    this.debug = true;

    this.default = {
        placeholder: '',
        pluginNamespacePrefix: 'devonline.plugin',
        parameterPrefix: 'parameterPrefix',
        parameterPrefixValue: '#:',
        arrayTypeName: 'array',
        kendoValidator: 'kendoValidator',
        kendoValidatorPlugin: 'jQuery.fn.kendoValidator',
        layout: 'horizontal',
        ignoreFields: '@odata.etag,serial,uid,dirty,removable',
        withAttachments: 'attachments',
        attachmentGet: '/api/Attachments',
        attachmentUpload: '/api/Attachments/Upload',
        attachmentDownload: '/api/Attachments/Download?fileName=',
        attachmentPath: '/api/Attachments/Files',
        fileicoPath: "/lib/devonline/img/fileico",
        imageIcoFileName: "img",
        imageIcoFileExtension: ".png",
        imageThumbnailPrefix: "thumb_",
        imageFileSize: 240,
        imageThumbnailSize: 48,
        displayFileNameLength: 15,
        dataState: {
            available: 'available',
            unavailable: 'unavailable',
            frozen: 'frozen',
            updated: 'updated',
            deleted: 'deleted',
            draft: 'draft',
            obsoleted: 'obsoleted',
            destroyed: 'destroyed',
            canceled: 'canceled'
        },
        auditState: {
            pending: "Pending",
            rejected: "Rejected",
            passed: "Passed",
            terminated: "Terminated"
        },
        dataSource: {
            orderby: "orderby"
        }
    };

    this.attribute = {
        serial: 'data-serial',
        id: 'data-id',
        value: 'data-value',
        initial: 'data-initial',
        node: 'node',
        template: 'node="template"',
        parameterPrefix: 'data-parameter-prefix',
        parameterArg: 'data-parameter-arg',
        parameterArgNode: 'node',
        parameterArgNodeType: 'node-type',
        required: 'required',
        plugin: 'data-toggle',
        field: 'data-field',
        fieldset: 'data-fieldset',
        button: 'data-button',
        queryButton: 'data-button="query"',
        form: 'data-toggle="form"',
        formArray: 'data-type="array"',
        formAttr: 'data-form',
        grid: 'data-toggle="grid"',
        popup: 'data-popup',
        popupWindow: 'data-toggle="popup"',
        tooltip: 'data-toggle="tooltip"',
        withAttachments: 'data-with-attachments',
        maxLength: 'data-max-length',
        minLength: 'data-min-length',
        inputInvalidMsg: 'k-invalid-msg"',
        namedPrefix: 'named-prefix',
        dataNamedPrefix: 'data-named-prefix',
        autoStartup: 'data-auto-startup',
        popupParameter: 'data-popup-parameter',
        openerProperty: 'data-opener-property'
    };

    this.selector = {
        'default': 'xxxxx',
        serial: '[data-serial]',
        id: '[data-id]',
        required: '[required]',
        node: '[node]',
        template: '[node="template"]',
        initial: '[data-initial]',
        plugin: '[data-toggle]',
        field: '[data-field]',
        fieldset: '[data-fieldset]',
        button: '[data-button]',
        form: '[data-toggle="form"][data-field]',
        formArray: '[data-toggle="form"][data-type="array"][data-field]',
        formAttr: '[data-form]',
        grid: '[data-toggle="grid"]',
        popup: '[data-popup]',
        popupWindow: '[data-toggle="popup"]',
        tooltip: '[data-toggle="tooltip"]',
        //notForm: '[data-toggle="grid"],[data-toggle="popup"],[node]',
        notForm: '[data-toggle="grid"],[node]',
        postButton: '[data-button="post"]',
        putButton: '[data-button="put"]',
        deleteButton: '[data-button="delete"]',
        queryButton: '[data-button="query"]',
        okButton: '[data-button="ok"]',
        apiPostButton: '[data-button="apiPost"]',
        closeButton: '[data-button="close"]',
        addButton: '[data-button="add"]',
        editButton: '[data-button="edit"]',
        enterPressButton: '[data-button][data-enter-press="true"]',
        closeOnSuccess: '[data-button][data-close-on-success="true"]',
        withAttachments: ':file[data-with-attachments],div[data-type="img"]',
        inputFormGroup: '.form-group',
        inputInvalidMsg: '.k-invalid-msg',
        inputDescMsg: '.k-desc-msg',
        maxLength: '[data-max-length]',
        minLength: '[data-min-length]',
        namedPrefix: '[named-prefix],[data-named-prefix]',
        autoStartup: '[data-auto-startup]',
        showbox: '[data-toggle="showbox"]',
        popupParameter: '[data-popup-parameter]',
        openerProperty: '[data-opener-property]'
    };

    this.dataSource = {
        defaults: {
            type: 'odata-v4',
            autoQuery: false,
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10
        },
        noPaging: {
            type: 'odata-v4',
            autoQuery: false,
            serverFiltering: true,
            serverSorting: true,
            serverPaging: false,
            pageSize: 999999
        },
        grid: {
            type: 'odata-v4',
            autoQuery: true,        //是否在初始化阶段开始自动查询
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
            sort: {
                field: "updatedOn", dir: "desc"
            },
            schema: {
                model: {
                    id: "id",
                    fields: {}
                },
                parse: function (response) {
                    response = $.com.odataAdapter(response);
                    if ($.com.hasValue(response) && $.com.isArray(response.value) && $.com.isArray(this.navigationColumns)) {
                        for (var i = 0; i < response.value.length; i++) {
                            var data = response.value[i];
                            for (var j = 0; j < this.navigationColumns.length; j++) {
                                var column = this.navigationColumns[j];
                                //key = key.replace(/\[\d+\]/, '');
                                var key = column.field;

                                // TODO ***** 处理 parent 作为字段名的特殊情况:
                                // 因为 KendoUI 的 datasource 组件将 parent 定义为一个方法, 因此 parent 作为字段名时只能换一个写法, 此处使用 Parent 替代
                                if (key.startsWith("_parent.")) {
                                    //key = key.replace("_parent.", "parent.");
                                    data._parent = $.extend({}, data.parent);
                                }

                                // 为 {outer: {key: value}} 的情况增加字段值为: {outer: {key: value}, outer.key: value}
                                var value = $.com.getValue(data, key);
                                value = $.com.hasValue(value) ? value : that.default.placeholder;
                                $.com.setValue(data, column.field, value);
                            }
                        }
                    }

                    return response;
                }
            },
            requestStart: function (e) {
                if (e && !e.sender.options.autoQuery) {
                    e.preventDefault();
                }
            }
        }
    };

    this.field = {
        namespace: 'devonline.plugin.field',
        defaults: {
            field: undefined,
            type: 'text',
            title: undefined,                                   //组件上的文字描述, 标题
            label: undefined,                                   //组件上的文字描述, 第一副标题, 优先显示于组件标签的文字
            text: undefined,                                    //组件上的文字描述, 第二副标题, 优先显示于组件本身的文字
            value: undefined,
            placeholder: undefined,                             //文本类的组件的 placeholder 内容
            valueFrom: undefined,                               //value 值来自某个字段, 如果值不为空则, value 值不取 field 字段而取 value from 的值
            role: undefined,                                    //*****组件可用性级别, 分以下4种, 但是如果组件属性跟role定义的组件属性冲突, 优先以role定义为主: 
            enable: true,                                       //enable: 可用(可编辑, 可触发事件, 反之均不能)
            editable: true,                                     //editable: 可编辑(指可输入或选择, 但不可触发事件)
            readonly: false,                                    //readonly: 只读(不可编辑, 但可触发事件)
            display: true,                                      //display: 可见(不可见状态不能人为触发事件, 仅能通过脚本触发)
            withGroup: false,
            inline: true,                                       //withGroup=true 且 inline=true 则使用内联样式, 即内部块横排布局, 否则都按竖排布局
            unique: false,
            message: undefined,
            valid: undefined,                                   //undefined, true, false
            side: 'client',
            url: undefined,
            data: undefined,
            alwaysInitial: false,                               //始终使用初始值
            initialWhenEmpty: true,                             //当value值为空值时使用初始值
            validateWhenDisplay: false,                         //可见则验证, 即仅元素可见时验证, 默认 false, 设置为 true, 则隐藏时不验证

            format: undefined,                                  //格式化时使用的格式描述, 此时 format 是名词
            dataFrom: undefined,                                //指示当前 field 是一个需要被格式化的字段, 格式化使用的的数据来自 dataFrom 属性指定的值, 默认来自 cache, 格式化方法来自 formatter 属性指定的值, 默认格式化方法为: $.com.commonFormatter

            //布局选项
            template: '#__TemplateField',                       //布局模版
            fieldset: true,                                     //是否使用布局
            layout: 'horizontal',                               //布局方式, 水平: horizontal, 垂直: vertical
            size: '12,3,8',                                     //布局尺寸: <整体宽度, 标签宽度, 表单元素宽度>, 默认: 垂直布局(12, 12, 12), 水平布局(12, 3, 8)
            required: false,                                    //是否必填
            icon: undefined,                                    //图标样式: 自定义
            iconTemplate: '#__TemplateIcon',                    //图标模板
            tooltip: false,                                     //使用tooltips
            tooltipIcon: 'glyphicon glyphicon-question-sign',   //tooltip图标样式
            tooltipPlacement: 'right',                          //tooltip位置
            tooltipTitle: undefined,                            //tooltip文字

            //filter 扩展选项
            filterable: true,                                   //可查询状态, 标记一个 field 可用于 grid 的查询区的查询组件, 查询区不作为查询条件的 field 需要设置该标记为 false 的值
            dataType: 'string',
            logic: 'and',
            operator: 'eq',
            filters: undefined,
            autoQuery: false,                                   //用于在自动匹配框获得焦点时自动触发查询

            //通用数据绑定选项
            dataIdField: 'id',                                  //当前表单绑定的对象的数据主键字段
            dataKeyField: 'key',                                //当前表单绑定的对象的取值键字段
            dataValueField: 'value',                            //当前表单绑定的对象的取值值字段
            dataTextField: 'text',                              //当前表单绑定的对象的取值显示文本字段

            //dropdownlist 扩展选项
            optionLabel: '---请选择---',
            selectedValue: undefined,

            single: false,                                      //是否单一文件上传, 单一文件上传情况比较特殊, 默认使用了基类名叫 Attachments 的共享集合保存附件, 因此需要根据 businessType 的值区分字段值
            withAttachments: undefined,                         //是否有附件, 如果有, 则会根据当前 form data dataIdField 指定列查出已有附件, 并绑定到当前列上, 如果 withAttachments 指定了列名, 则使用 withAttachments 指定名字的集合, 仅对 type 为: file 和 img 有效
            businessType: undefined,
            imageSize: 240,
            imageViewer: true,                                  //是否使用 image viewer 呈现图片的预览, 仅在 type=img 时有效
            fileName: undefined,                                //下载的文件名称

            //popup 合成组件扩展选项, 详见 popup 部分
            popup: undefined,

            //field 联动
            next: undefined,                                    //联动的下一个元素

            loading: undefined,
            loaded: undefined,
            binding: undefined,
            bound: undefined,
            clicking: undefined,
            clicked: undefined,
            changing: undefined,
            changed: undefined,
            blurring: undefined,
            blurred: undefined,
            selecting: undefined,
            selected: undefined,
            keyPressing: undefined,
            keyPressed: undefined,
            querying: undefined,
            queried: undefined,
            focusing: undefined,
            focused: undefined,
            iconClicking: undefined,
            iconClicked: undefined,
            dataAdapter: $.com.dataAdapter,
            /**
             * dataFrom 存在时格式化方法格式为: formatter(el.options.data, el.options.value, el.options);
             * 不存在时格式化方法格式为: formatter(el, el.options.value, el.options.format);
             */
            formatter: undefined,
            validator: undefined
        },
        methods: [
            'init',
            'reInit',
            'getData',
            'getValue',
            'setValue',
            'readonly',
            'display',
            'clear',
            'reset',
            'refresh',
            'writeBack',
            'getQueryExpression',
            'drawAttachments',
            'getFormData',
            'validate',
            'destroy'
        ],
        events: [
            'click',
            'change',
            'blur',
            'select',
            'query',
            'iconClick'
        ],
        /*
         * kendo 组件赋值本地数据源需指定到 dataSource 字段
         */
        initials: {
            dataType: 'string',
            logic: 'and',
            operator: 'eq',
            role: 'textbox',
            dataValueField: 'value',
            dataTextField: 'text',
            optionLabel: '---请选择---',
            noDataTemplate: '无数据显示',
            dataSource: this.dataSource.defaults
        }
    };

    this.button = {
        namespace: 'devonline.plugin.button',
        defaults: {
            button: undefined,
            action: undefined,
            icon: undefined,
            type: 'POST',
            dataType: 'json',
            label: undefined,
            textField: 'id',
            valueField: 'id',
            role: undefined,                //*****组件可用性级别, 分以下4种, 但是如果组件属性跟role定义的组件属性冲突, 优先以role定义为主: 
            enable: true,                   //enable: 可用(可编辑, 可触发事件, 反之均不能)
            editable: false,                //editable: 可编辑(指可输入或选择, 但不可触发事件)
            readonly: true,                 //readonly: 只读(不可编辑, 但可触发事件)
            display: true,                  //display: 可见(不可见状态不能人为触发事件, 仅能通过脚本触发)
            side: 'server',
            url: undefined,
            target: undefined,              //组件操作目标, link 类型, 默认为 target="_blank", 在新页面打开
            withFormData: true,             //用于指示提交请求到服务器时, 是否需要附带当前 form 的数据, 默认是, 提交数据时的数据来源, 支持 true, false, 及来自 devonline 的组件; 默认 true 情况下, button 作为容器组件的子组件, 数据从父级容器而来, false 则来自当前组件自己
            keepReference: undefined,       //是否保留对象引用, 适用于提交服务器之前, 是否将当前对象作为参数提交服务器, 在 post/put 请求中为保留引用为默认情况, 在 get/delete 请求中, url 末尾不保留引用的情况下将只传递 id 作为参数
            validationInSbumit: true,       //提交时验证表单
            template: '#__TemplateButton',
            withGroup: false,
            enterPress: false,              //默认不响应回车, 如果设置为 true, 则会在 form 范围内按下回车键时触发

            confirm: false,                 //确认, 事件触发前, 是否先弹出确认一次?
            confirmTitle: '请确认',
            confirmText: '请确认是否要执行当前操作?',
            prompt: true,                   //弹出提示, 事件完成后, 是否弹出提示
            successTitle: '',
            successText: '保存成功!',
            errorTitle: '',
            errorText: '保存失败!',
            displayInEditable: undefined,   //在编辑页面, 是否显示当前按钮, 用于区分新增和编辑模式
            isAuditing: false,              //是否是审核业务,用来区分页面上按钮是否需要按流程状态来显示
            isAlteration: true,             //审核完成后是否还可以编辑
            closeOnSuccess: true,           //作为弹出框的按钮, 执行事件处理方法成功后, 自动关闭弹出框

            authorize: false,               //按钮是否要经过授权
            authorizeLogic: 'or',           //functionid 和 authorizeMethod 条件的授权逻辑 and or
            functionid: undefined,          //授权的functionId, 多个以逗号隔开
            authorizeMethod: undefined,     //授权方式
            isAuthenticated: undefined,     //指示组件是否已通过授权认证

            style: 'popup',                 //针对高级查询区域的显示方式, 默认 popup, 可选方式还有 dropdown
            autoShrink: true,               //针对高级查询区域的收缩方式, 点击查询事件后, 是否自动收缩查询区域

            importView: undefined,          //导入预览的 popup windows

            dataAdapter: $.com.dataAdapter,
            loading: undefined,
            loaded: undefined,
            clicking: undefined,
            clicked: undefined,
            beforeSubmit: undefined,
            beforeSend: undefined,
            success: undefined,
            error: undefined,
            complete: undefined
        },
        methods: [
            'init',
            'getData',
            'getValue',
            'setValue',
            'display',
            'destroy'
        ],
        events: [
            'click'
        ],
        actions: {
            'default': { name: 'default', action: undefined, 'class': "btn-default" },
            //确定按钮
            ok: { name: 'ok', action: undefined, label: '确 定', 'class': "btn-primary", icon: 'fa fa-check' },
            //关闭按钮
            close: { name: 'close', action: undefined, label: '关 闭', 'class': "btn-default", icon: 'fa fa-times' },
            remove: { name: 'remove', action: undefined, label: '移 除', 'class': "btn-danger", icon: 'fa fa-times' },
            print: { name: 'print', action: 'onPrint', label: '打 印', 'class': "btn-primary", icon: 'fa fa-print', successText: '打印完成!', errorText: '打印失败!' },
            printView: { name: 'printView', action: undefined, label: '打 印', 'class': "btn-primary", icon: 'fa fa-print' },
            showbox: { name: 'showbox', action: undefined, label: '打 开', 'class': undefined, icon: undefined },

            add: { name: 'add', action: undefined, label: '新 增', 'class': "btn-primary", icon: 'fa fa-plus' },
            edit: { name: 'edit', action: undefined, label: '编 辑', 'class': "btn-primary", icon: 'fa fa-edit', isAlteration: true },
            link: { name: 'link', action: 'onLink', 'class': "btn-default", icon: 'fa fa-link', target: "_blank" },
            back: { name: 'back', action: 'onLink', label: '返 回', 'class': "btn-default", icon: 'fa fa-chevron-left' },
            detail: { name: 'detail', action: undefined, 'class': "btn-success", icon: 'fa fa-eye' },

            approve: { name: 'approve', action: 'onApprove,onSubmit', label: '同 意', 'class': "btn-success", icon: 'fa fa-check', url: '/api/Audits/Approve' },
            reject: { name: 'reject', action: 'onReject,onSubmit', label: '拒 绝', 'class': "btn-warning", icon: 'fa fa-warning', url: '/api/Audits/Reject' },
            finish: { name: 'finish', action: 'onSubmit', label: '完 成', 'class': "btn-success", icon: 'fa fa-check', successText: '操作完成!', errorText: '操作失败!' },
            revise: { name: 'revise', action: 'onSubmit', label: '打回修改', 'class': "btn-warning", icon: 'fa fa-warning', successText: '操作完成!', errorText: '操作失败!', validationInSbumit: false },
            abort: { name: 'abort', action: 'onSubmit', label: '作废变更', 'class': "btn-warning", icon: 'fa fa-stop', type: 'POST', keepReference: false, isAuditing: true, confirm: true, confirmText: '请确认是否要作废当前变更?', successText: '操作完成!', errorText: '操作失败!' },
            cancel: { name: 'cancel', action: 'onSubmit', label: '终 止', 'class': "btn-warning", icon: 'fa fa-warning', keepReference: false, isAuditing: true, confirm: true, confirmText: '请确认是否要终止当前流程?', successText: '操作完成!', errorText: '操作失败!' },
            terminate: { name: 'terminate', action: 'onSubmit', label: '撤销申请', 'class': "btn-warning", icon: 'fa fa-stop', type: 'POST', keepReference: false, isAuditing: true, confirm: true, confirmText: '请确认是否要撤销当前申请?', successText: '操作完成!', errorText: '操作失败!' },
            ////仅在有流程且流程为驳回状态时出现
            //rejectEdit: { name: 'rejectEdit', action: undefined, label: '编 辑', 'class': "btn-primary", icon: 'fa fa-edit', isAuditing: true },

            clear: { name: 'clear', action: 'onClear', label: '清 除', 'class': "btn-default", icon: 'fa fa-times' },
            reset: { name: 'reset', action: 'onReset', label: '重 置', 'class': "btn-default", icon: 'fa fa-undo' },
            query: { name: 'query', action: 'onQuery', label: '查 询', 'class': "btn-primary", icon: 'fa fa-search', enterPress: true },
            clearAndQuery: { name: 'clearAndQuery', action: 'onClear,onQuery', label: '清除并查询', 'class': "btn-default", icon: 'fa fa-times' },
            resetAndQuery: { name: 'resetAndQuery', action: 'onReset,onQuery', label: '重置并查询', 'class': "btn-default", icon: 'fa fa-undo' },
            //高级查询按钮
            advancedSearch: { name: 'advancedSearch', action: 'onAdvancedSearch', label: '高级查询', 'class': "", style: "dropdown" },

            'import': {
                name: 'import', action: 'onImport', 'class': 'btn-primary', icon: 'fa fa-cloud-upload',
                template: '#__TemplateUpload',
                type: 'post',
                label: '导入',
                prompt: false,
                success: function (el, data, xhr) {
                    if ($.com.hasValue(data)) {
                        var $popup = $.com.hasValue(el.options.importView) ? $(el.options.importView) : $('[data-toggle="popup"][data-type="ImportView"]');
                        if ($popup.hasValue()) {
                            $popup.trigger('open', $.extend({
                                title: '导入预览',
                                opener: el.$el,
                                confirming: el.options.confirming,
                                confirmed: el.options.confirmed
                            }, that.popupWindow.size.importView));
                            var popup = $popup.data(that.popupWindow.namespace);
                            if ($.com.hasValue(popup) && popup.$grid.hasValue()) {
                                var grid = popup.$grid.data(that.grid.namespace);
                                if (!$.com.hasValue(grid) || !$.com.hasValue(grid.role)) {
                                    popup.$grid.grid();
                                    grid = popup.$grid.data(that.grid.namespace);
                                }

                                if ($.com.hasValue(grid) && $.com.hasValue(grid.role)) {
                                    if ($.com.isArray(data)) {
                                        //返回数组的形式时, 使用自定义预览窗口, 此时, 只需要植入数据
                                        //for (var i = 0; i < data.length; i++) {
                                        //    $.each(data[i], function (k, v) {
                                        //        var f = k.replace(/[\(|\)]/g, '_');
                                        //        if (i === 0) {
                                        //            columns.push({ title: k, field: f, attributes: { 'class': 'popup' } });
                                        //        }
                                        //    });
                                        //}

                                        //options = { dataSource: data };
                                        grid.role.dataSource.data(data);
                                    } else if ($.com.isArray(data.Header) && $.com.isArray(data.Body)) {
                                        //返回ExcelEntitySet的形式
                                        var options = {};
                                        if ($.com.isArray(data.Header)) {
                                            for (var i = 0; i < data.Header.length; i++) {
                                                data.Header[i].attributes = { 'class': 'popup' };
                                            }
                                        }

                                        options = { columns: data.Header, dataSource: data.Body };
                                        options.columns.insert(that.grid.rowIndex, 0);
                                        grid.role.setOptions(options);
                                    }

                                    grid.resize();
                                    popup.$grid.show();
                                    popup.$buttons.filter('[data-button="ok"]').show();
                                    popup.$el.find('.import-error').hide();
                                }
                            }
                        }
                    }
                },
                error: function (el, xhr) {
                    if ($.com.hasValue(xhr)) {
                        var msg = $.com.getValue(xhr, 'responseJSON.ExceptionMessage') || xhr.responseText || '';
                        var $popup = $('[data-toggle="popup"][data-type="ImportView"]');
                        if ($popup.hasValue()) {
                            $popup.trigger('open', $.extend({ title: '导入错误!' }, that.popupWindow.size.importView));
                            var popup = $popup.data(that.popupWindow.namespace);
                            if ($.com.hasValue(popup)) {
                                popup.$grid.hide();
                                //var grid = popup.$grid.data(that.grid.namespace);
                                //if ($.com.hasValue(grid)) {
                                //    grid.destroy();
                                //}

                                popup.$buttons.filter('[data-button="ok"]').hide();
                                popup.$el.find('.import-error').show().html(msg);
                            }
                        }
                    }
                }
            },
            'export': { name: 'export', action: 'onExport', label: '导 出', 'class': 'btn-primary', icon: 'fa fa-cloud-download', type: 'GET', successText: '导出成功!', errorText: '导出失败!' },
            upload: { name: 'upload', action: 'onUpload', label: '上 传', 'class': 'btn-default', icon: 'fa fa-upload', template: '#__TemplateUpload' },
            download: { name: 'download', action: 'onDownload', label: '下 载', 'class': 'btn-default', icon: 'fa fa-download' },
            addForm: { name: 'addForm', action: 'onAddForm', label: '新 增', 'class': "btn-primary", icon: 'fa fa-plus' },
            removeForm: { name: 'removeFrom', action: 'onRemoveForm', 'class': "btn-default", icon: 'fa fa-times' },

            'delete': { name: 'delete', action: 'onSubmit', label: '删 除', 'class': "btn-danger", icon: 'fa fa-trash', type: 'DELETE', keepReference: false, confirm: true, confirmText: '请确认是否要删除当前数据?', successText: '删除成功!', errorText: '删除失败!' },
            get: { name: 'get', action: 'onSubmit', label: '获 取', 'class': "btn-default", icon: 'fa fa-search', type: 'GET', keepReference: false },
            //新增
            post: { name: 'post', action: 'onSubmit', label: '新 增', 'class': "btn-success", icon: 'fa fa-file-text', type: 'POST', displayInEditable: false, successText: '新增成功!', errorText: '新增失败!' },
            //编辑保存
            put: { name: 'put', action: 'onSubmit', label: '保 存', 'class': "btn-primary", icon: 'fa fa-save', type: 'PUT', displayInEditable: true, successText: '保存成功!', errorText: '保存失败!' },
            //申请(除提交备案和变更以外的提交)
            apply: { name: 'apply', action: 'onSubmit', label: '申 请', 'class': "btn-purple", icon: 'fa fa-check', type: 'POST', successText: '申请成功!', errorText: '申请失败!' },
            //提交
            submit: { name: 'submit', action: 'onSubmit', label: '提 交', 'class': "btn-purple", icon: 'fa fa-check', type: 'POST', displayInEditable: true, successText: '提交成功!', errorText: '提交失败!' },

            //以下三个为有流程的业务所需要的按钮
            //提交备案
            register: { name: 'register', action: 'onSubmit', label: '申请备案', 'class': "btn-purple", icon: 'fa fa-check', type: 'POST', displayInEditable: true, confirm: true, confirmText: '请确认是否要申请备案?', successText: '申请成功!', errorText: '申请失败!' },
            //变更保存
            save: { name: 'save', action: 'onSubmit', label: '保 存', 'class': "btn-primary", icon: 'fa fa fa-save', type: 'POST', displayInEditable: true, successText: '保存成功!', errorText: '保存失败!' },
            //变更提交
            submitChange: { name: 'submitChange', action: 'onSubmit', label: '提交变更', 'class': "btn-purple", icon: 'fa fa-check', type: 'POST', displayInEditable: true, confirm: true, confirmText: '请确认是否要提交当前变更?', successText: '提交成功!', errorText: '提交失败!' },
        }
    };

    this.form = {
        namespace: 'devonline.plugin.form',
        defaults: {
            //properties
            type: 'form',
            readonly: undefined,
            display: true,
            valid: true,                    //undefined, true, false
            url: undefined,
            side: 'client',
            httpMethod: 'get',
            data: undefined,
            dataSource: undefined,
            autoStartup: false,             //是否自动初始化
            alwaysInitial: false,           //始终使用初始值

            detailable: false,              //详情状态, 用于标记表单是否处于可编辑模式, true 则为详情状态, 整个表单用不可编辑的组件绘制. false, 则使表单切换到 editable 模式
            editable: false,                //可编辑状态, 用于标记表单是否处于编辑状态, false 则为新增状态. 正常情况下表单是新增和编辑状态共享的
            removable: true,                //是否删除一个 form 可选值： true(物理删除, 同时删除元素和数据), false(逻辑删除, 既不删除元素也不删除数据, 只标记数据状态为 Deleted, 表示已处于被删除状态, 再次删除则可直接删掉)
            filterable: false,              //可查询状态, 标记一个 form 是用于 grid 上的查询区, 查询区的 form 需要设置该标记为 true 的值

            autoDraw: false,                //是否自动绘制子组件
            drawPlacement: 'append',        //自动绘制位置在最前, append 则会绘制到末尾位置, 否则认为为 prepend, 则会绘制到起始位置, 其余位置则需自定义
            template: undefined,            //自动绘制子组件使用的模板
            validateWhenDisplay: false,     //可见则验证, 即仅元素可见时验证, 默认 false, 设置为 true, 则隐藏时不验证
            autoRefreshLayout: true,        //是否在子 form 增删之后自动刷新布局样式
            objectToArray: false,           //是否将单个对象中的字段以键值对数组的形式自动构造出来
            dataIdField: 'id',              //当前表单绑定的对象的数据主键字段
            dataIdType: 'string',           //当前表单绑定的对象的数据主键数据类型
            dataStateField: 'state',        //当前表单绑定的对象的数据状态字段
            dataOrderByField: 'index',      //当前表单绑定的对象的数据排序字段
            ignoreFields: undefined,        //当前表单提交时忽略的字段名
            withAttachments: undefined,     //是否有附件, 如果有, 则会在初始阶段加载数据的同时加载数据的附件, 如不设置, 会在初始化时, 根据表单中是否有附件组件元素来判断

            //methods
            dataAdapter: $.com.dataAdapter,
            loading: undefined,
            loaded: undefined,
            binding: undefined,
            bound: undefined,
            changing: undefined,
            changed: undefined,
            validator: undefined,           //kendo ui validator elements
            validate: undefined             //custom validate
        },
        methods: [
            'init',
            'reInit',
            'getData',
            'getValue',
            'setValue',
            'readonly',
            'display',
            'clear',
            'reset',
            'refresh',
            'addForm',
            'addString',
            'removeForm',
            'refreshForm',
            'refreshLayout',
            'writeBack',
            'getFormData',
            'getQueryExpression',
            'onChange',
            'validate',
            'destroy'
        ],
        events: []
    };

    this.grid = {
        namespace: 'devonline.plugin.grid',
        defaults: {
            remember: false,
            resizable: true,
            type: 'grid',
            side: 'server',
            role: "kendoGrid",
            url: undefined,
            related: undefined,
            dataField: undefined,               //指定如果使用客户端数据的话, 取上层或关联 form 对象的那个字段的数据, 默认取关联对象的 data 值
            dataStateField: 'state',            //当前表格绑定的对象的数据状态字段
            dataSource: {},
            columns: [],
            filters: [],
            pageable: {},
            data: [],

            //以下字段为 columns 的字段
            //formatter: undefined,             //格式化时使用的方法, 格式为: formatter('fieldName', fieldValue, rowData);
            //dataFrom: undefined,              //指示当前 column 值是一个需要被格式化的字段, 格式为: formatter(dataFrom, fieldValue, rowData); 配合 formatter 属性来使用, 格式化的数据来自 dataFrom 属性指定的值, 默认来自 cache, 格式化方法来自 formatter 属性指定的值, 默认格式化方法为: $.com.commonFormatter

            loading: undefined,
            loaded: undefined,
            changing: undefined,
            changed: undefined,
            clicking: undefined,
            clicked: undefined,
            dblclicking: undefined,
            dblclicked: undefined,
            binding: undefined,
            bound: undefined,

            //column 字段
            contentPopup: true,               //当单元格内容超长时鼠标移动到单元格上是否弹出完整内容, 默认 true, 如果为 false 则不弹出内容
        },
        initials: {
            role: 'kendoGrid',
            tableHeight: 'auto',                // auto(自动计算尺寸) 或者一个具体像素值数字
            minRowCount: 10,                    // 设置最小显示多少行, 不管有多少行数据
            maxRowCount: 30,                    // 设置最大显示多少行, 使行内不出现滚动条
            allowCopy: true,

            // 选择模式, true: 可选择,
            // "row" - the user can select a single row.行
            // "cell" - the user can select a single cell.项
            // "multiple, row" - the user can select multiple rows.多行
            // "multiple, cell" - the user can select multiple cells.多项
            selectable: true,
            multiple: false,                    // 是否多选模式, 由 selectable 的值判断而来
            persistSelection: true,
            filterable: false,
            sortable: false,
            resizable: true,
            scrollable: true,
            rowIndex: true,                     // 是否启用rowIndex显示, 如启用, 将在每行第二列显示行号
            columnMenu: true,
            dataSource: this.dataSource.grid,
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 30, 50, 100],
                pageSize: 10,
                buttonCount: 10
            }
        },
        methods: [
            'init',
            'resize',
            'getData',
            'getValue',
            'setValue',
            'query',
            'select',
            'refresh',
            'destroy'
        ],
        events: [
            'change',
            'click',
            'dblclick'
        ],
        rowIndex: {
            title: '序号',
            width: 48,
            menu: false,
            template: function (e) {
                if ($.com.hasValue(e)) {
                    return e._rowIndex;
                }
            }
        },
        rowSelect: {
            width: 30,
            menu: false,
            selectable: true,
            filterable: false,
            sortable: false
        }
    };

    this.popupWindow = {
        namespace: 'devonline.plugin.popupWindow',
        // parameter 数据结构 {transfer: 'in', source: 'Code', target: 'Name', value: '123'} 数组, transfer 默认 out
        // 简单模式: Name 即将 Name 列绑定到 target 属性, transfer 默认 out ,  source 默认当前绑定属性(field), value 默认当前绑定属性值
        // 完全模式: [{transfer: 'in', source: 'Code', target: 'Name', value: '123'}, {transfer: 'out', source: 'Code', target: 'Name', value: '123'}]
        defaults: {
            role: 'kendoWindow',
            popup: undefined,
            type: undefined,
            form: undefined,
            data: undefined,
            opener: undefined,
            parameter: undefined,
            loading: undefined,
            loaded: undefined,
            opening: undefined,
            opened: undefined,
            confirming: undefined,
            confirmed: undefined,
            closing: undefined,
            closed: undefined,
            isAutoQuery: true
        },
        initials: {
            modal: true,
            pinned: false,
            iframe: false,
            autoFocus: true,
            visible: false,
            resizable: false,
            draggable: true,
            scrollable: true,
            isGrid: true,
            isMultiple: false,
            multiple: 'row',
            size: 'default',
            minWidth: 320,
            minHeight: 180,
            actions: [
                "Minimize",
                "Maximize",
                "Close"
            ]
        },
        methods: [
            'init',
            'select',
            'getData',
            'getValue',
            'setValue',
            'destroy'
        ],
        events: [
            'open',
            'close',
            'ok',
            'dblclick',
            'query'
        ],
        //尺寸设定默认比例: 16:9
        size: {
            //默认的弹出页面, medium 中号的 (50倍)
            'default': {
                width: '800',
                height: '450'
            },
            //UHD 尺寸的, 4K 寸尺显示的, 信息量极大的页面 (240倍)
            uhd: {
                width: '3840',
                height: '2160'
            },
            //WQHD 尺寸的, 2K 寸尺显示的, 信息量超大的页面 (160倍)
            wqhd: {
                width: '2560',
                height: '1440'
            },
            //FHD 尺寸的, 超大号的, 信息量很大的页面 (120倍)
            fhd: {
                width: '1920',
                height: '1080'
            },
            //HD 尺寸的, 超大号的, 信息量大的页面 (80倍)
            hd: {
                width: '1280',
                height: '720'
            },
            //大号的, 信息量较大的页面 (60倍)
            large: {
                width: '960',
                height: '540'
            },
            //中号的, 尺寸同 default, 显示一般信息量的常规内容 (50倍)
            medium: {
                width: '800',
                height: '450'
            },
            //小号的, 字段不多的小页面 (40倍)
            small: {
                width: '640',
                height: '360'
            },
            //最小页面（比如：提示页面）(20倍)
            mini: {
                width: '320',
                height: '180'
            },
            print: {
                width: '820',
                height: '860'
            },
            receipt: {
                width: '1120',
                height: '700'
            }
        }
    };

    /*
    *   popupParameter 属性, 有以下属性
    *   opener: undefined, 打开者 / 接收者, 默认取当前组件所在 form;
    *   parameter: 传入传出参数列表, 默认情况下将使用当前字段自动构造
    *   inout: [], 这个参数是原来 parameter 参数的简写形式, 由左侧 in 属性名 = 右侧 out 属性名/值来构成; 如果无此参数, 则默认用当前元素构造出来
    *   formData: 指示使用当前 form data 作为传入参数
    *   fillFormData: 指示是否填充整个表单数据
    */
    this.popup = {
        namespace: 'devonline.plugin.popup',
        defaults: {
            popup: undefined,
            opener: undefined,
            source: undefined,
            target: undefined,
            parameter: undefined,
            popupParameter: undefined,
            role: undefined,                                //*****组件可用性级别, 分以下4种, 但是如果组件属性跟role定义的组件属性冲突, 优先以role定义为主: 
            enable: true,                                   //enable: 可用(可编辑, 可触发事件, 反之均不能)
            editable: false,                                //editable: 可编辑(指可输入或选择, 但不可触发事件)
            readonly: true,                                 //readonly: 只读(不可编辑, 但可触发事件)
            display: true,                                  //display: 可见(不可见状态不能人为触发事件, 仅能通过脚本触发)
            withFormData: false,                            //withFormData: 是否携带当前 form data, 指示打开弹出框传递数据时, 是否使用当前弹出框启动器所在 form data 作为原始数据构造参数
            fillFormData: false,                            //fillFormData: 是否填充到当前 form data, 指示关闭弹出框传递数据时, 是否填充当前弹出框启动器所在 form data 的所有匹配字段, 当且仅当没有指定传入传出参数时使用, 即 popupParameter.parameter 或者 inout 参数没有值时使用
            isAutoQuery: true                               //isAutoQuery: 打开时如果有grid是否执行查询
        },
        methods: [
            'init',
            'getData',
            'getValue',
            'setValue'
        ],
        events: ['open']
    };

    this.showbox = {
        namespace: 'devonline.plugin.showbox',
        defaults: {
            showbox: undefined,
            showboxParameter: undefined,
            template: '#__TemplateShowBox',
            tagTemplate: '#__TemplateTag',
            showField: undefined,
            button: 'showbox',
            clicking: undefined,
            clicked: undefined,
        },
        methods: [
            'init',
            'getData',
            'getValue',
            'setValue'
        ],
        events: ['click']
    };

    this.alert = {
        namespace: 'devonline.plugin.alert',
        defaults: {
            alert: undefined,
            autoClose: false,
            timeout: 3000,
            template: '#__ModalAlertTemplate',

            loading: undefined,
            loaded: undefined,
            opening: undefined,
            opened: undefined,
            confirming: undefined,
            confirmed: undefined,
            closing: undefined,
            closed: undefined,
            cancelling: undefined,
            cancelled: undefined
        },
        methods: [
            'init',
            'getData',
            'getValue',
            'setValue'
        ],
        events: [
            'open',
            'close',
            'ok'
        ]
    };

    this.uploader = {
        initials: {
            enable: true,
            viewable: true,
            editable: false,
            deletable: true,
            single: false,
            multiple: true,
            fieldset: true,
            role: 'kendoUpload',
            type: 'file',
            template: undefined,
            uploadTemplate: '#__TemplateFileAttachment',
            label: undefined,
            title: undefined,
            layout: 'horizontal',               //布局方式, 水平: horizontal, 垂直: vertical
            //size: '12,3,9',
            fileSize: 10240,                    //文件大小, 单位 KB, 默认 10M
            fileCount: 10,               //文件数量
            fileExtension: '.pdf,.txt,.log,.md,.png,.jpg,.jpeg,.gif,.bmp,.ico,.xlsx,.xls,.docx,.doc,.pptx,.ppt,.csv,.mp3,.wma,.wav,.m4a,.mp4,.mkv,.avi,.mov,.flv,.zip,.rar,.7z,.gz',   //文件扩展名, 多个扩展名用","隔开, * 指任意文件;  
            async: {
                saveUrl: "/api/Attachments/Upload",
                removeUrl: "/api/Attachments/Delete",
                autoUpload: true,
                removeVerb: "DELETE"
            },
            localization: {
                select: "请选择"
            },
            loading: function (el) {
                //设定已有附件绑定到组件上
                var files = [];
                if ($.com.hasValue(el) && $.com.isJqueryObject(el.$form)) {
                    var attachments = el.getFieldAttachments();
                    if ($.com.isArray(attachments)) {
                        for (var i = 0; i < attachments.length; i++) {
                            var attachment = attachments[i];
                            if ($.com.hasValue(attachment)) {
                                //attachment._fileName = attachment.name;
                                //attachment.fileName = attachment.path;
                                files.push({
                                    name: attachment.name,
                                    size: attachment.length,
                                    path: attachment.path,
                                    extension: attachment.extension
                                });
                            }
                        }
                    }
                }

                el.initials.files = files;
            },
            loaded: function (el) {
                //设定组件的显示内容
                var $uploader = el.$el.closest('.k-upload');
                if ($uploader.hasValue()) {
                    var $uploadFiles = $uploader.find('.k-upload-files');
                    var $dropzone = $uploader.find('.k-dropzone');
                    if ($uploadFiles.hasValue() && $dropzone.hasValue() && el.options.enable) {
                        $uploadFiles.insertBefore($dropzone);
                        if (el.options.single) {
                            $dropzone.hide();
                        }
                    } else {
                        if (!el.options.enable) {
                            $dropzone.remove();

                            if (!$uploadFiles.hasValue()) {
                                //$uploader.append('<span data-field="_UploadFile">无附件</span>');
                                $uploader.append('<span style="color:#336199;font-weight: 400;">无附件</span>');
                            }
                        } else if (el.options.single) {
                            $dropzone.show();
                        }
                    }

                    var $buttons = $uploader.find('.k-upload-files .k-file-success .k-upload-status');
                    if ($buttons.hasValue() && $.com.isArray(el.initials.files)) {
                        $buttons.each(function () {
                            var $this = $(this);
                            $this.children().eq(0).hide();
                            //$this.children().eq(1).append('<span class="k-icon ion-eye" data-button="download" data-file-name="' + $this.attr('data-path') + '" data-file-name="' + $this.prev().attr('title') + '" title="查看" aria-label="查看"></span>').hide();
                            //$this.children().eq(2).append('<span class="k-icon ion-edit" title="编辑" aria-label="编辑"></span>').hide();

                            if (el.options.deletable) {
                                $this.children().eq(0).show();
                            }
                            //if (el.options.viewable) {
                            //    $this.children().eq(1).show().children().button();
                            //}
                            //if (el.options.editable) {
                            //    $this.children().eq(2).show();
                            //}

                            //$this.children().eq(0).insertAfter($this.children().eq(2));
                        });
                    }
                }
            },
            selected: function (el, e) {
                //选择附件后触发
                if ($.com.hasValue(el.options) && $.com.hasValue(e) && $.com.isArray(e.files)) {
                    var valid = $.com.hasValue(el.role);
                    if (valid && $.com.isJqueryObject(el.$form)) {
                        var attachments = el.getFieldAttachments();
                        if ($.com.isArray(attachments)) {
                            if (el.options.single) {
                                //单文件情况
                                //单文件直接判断第一个
                                let attachment = attachments.first();
                                if (!attachment) {
                                    valid = e.files.exist(x => x.name === attachment.name);
                                }

                                //移除超出文件
                                if (e.files.length + attachments.length > 1) {
                                    app.warning('只能上传 1 个文件!');
                                    valid = false;
                                }
                            } else {
                                //多文件情况
                                //移除重复文件
                                for (var i = 0; i < e.files.length; i++) {
                                    var name = e.files[i].name;
                                    if (attachments.exist(x => x.name === name)) {
                                        e.files.splice(i, 1);
                                    }
                                }

                                //移除超出文件
                                if ($.com.hasValue(el.options.fileCount) && (e.files.length + attachments.length > el.options.fileCount)) {
                                    app.warning('只能上传 ' + el.options.fileCount + ' 个文件!');
                                    while (e.files.length + attachments.length > el.options.fileCount) {
                                        e.files.splice(e.files.length - 1, 1);
                                    }
                                }
                            }

                            if ($.com.isArray(e.files)) {
                                //验证文件类型
                                if (el.options.fileExtension !== '*') {
                                    var exts = el.options.fileExtension + ',';
                                    if (!e.files.exist(x => exts.includes(x.extension.toLowerCase() + ','))) {
                                        valid = false;
                                        app.warning('只能上传 ' + el.options.fileExtension + ' 类型的文件!');
                                    }
                                }

                                //验证文件大小
                                if (valid && e.files.exist(x => x.size > el.options.fileSize * 1024)) {
                                    valid = false;

                                    var size = el.initials.fileSize;
                                    if ((size / 1024) >= 1) {
                                        size = $.com.toFixed(size / 1024, 2) + 'MB';
                                    } else {
                                        size = size + 'KB';
                                    }

                                    app.warning('单个文件不能超过 ' + size + ' 大小!');
                                }
                            }
                        }
                    }

                    if (!valid) {
                        while (e.files.length > 0) {
                            e.files.splice(0, 1);
                        }
                    }
                }
            },
            remove: function (e) {
                //移除附件后触发
                if ($.com.hasValue(e) && $.com.isArray(e.files) && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.element)) {
                    var $field = e.sender.element;
                    var files = e.files;
                    var field = $field.data(_config_.field.namespace);
                    if ($.com.hasValue(field)) {
                        let attachments = field.getFormAttachments() || [];
                        if ($.com.isArray(attachments)) {
                            let removed = false;
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                var attachment = attachments.first(x => x.path === file.path);
                                if (!$.com.hasValue(attachment)) {
                                    attachment = attachments.first(x => x.name === file.name || x.name === file.name);
                                    if ($.com.hasValue(attachment)) {
                                        file.name = attachment.path;
                                        file.path = attachment.path;
                                    }
                                }

                                if ($.com.hasValue(attachment)) {
                                    attachments.remove(attachment, 'path');
                                    removed = true;
                                }
                            }

                            if (removed) {
                                if ($.com.hasValue(field.role) && $.com.hasValue(file.uid) && typeof field.role.removeFileByUid === 'function') {
                                    field.role._removeFileByUid(file.uid, false);
                                }

                                field.execute(field.options.loaded);
                            }
                        }
                    }

                    //remove 将不执行服务器删除方法, 删除附件在客户端提交后, 有服务器完成
                    e.preventDefault();
                }
            },
            upload: function (e) {
                //上传时触发
                if ($.com.hasValue(e) && $.com.isArray(e.files) && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.element)) {
                    //由于kendoUpload组件上传时移除了组件数据, 此处需重新修正组件数据引用
                    var $field = e.sender.element;
                    var field = this[that.field.namespace];
                    if ($.com.isJqueryObject($field) && $.com.hasValue(field)) {
                        if (field.options.single) {
                            e.sender.wrapper.find('.k-dropzone').hide();
                        }

                        field.execute(field.options.recalculate);
                        if (field.$form.isForm()) {
                            let attachments = field.getFieldAttachments();
                            if ($.com.isArray(attachments) && attachments.exist(x => x.Name === e.files[0].name)) {
                                e.preventDefault();
                            }
                        } else {
                            e.files.splice(0, 1);
                            e.preventDefault();
                        }
                    }
                }
            },
            success: function (e) {
                //上传成功后会执行此方法
                //TODO 多文件上传暂未验证
                if ($.com.hasValue(e) && $.com.hasValue(e.response) && $.com.isArray(e.response) && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.element) && $.com.isJqueryObject(e.sender.wrapper)) {
                    var file = e.response[0];
                    if ((file.statusCode.toLowerCase() === 'ok' || file.statusCode === 200) && $.com.hasValue(file.attachment)) {
                        var $uploader = e.sender.wrapper;
                        var $uploadFiles = $uploader.find('.k-upload-files');
                        if ($uploadFiles.prev().hasValue()) {
                            $uploadFiles.insertBefore($uploadFiles.prev());
                        }

                        $uploader.find('.k-file-progress .file-icon').attr('src', $.com.getAttachmentFileName(file.fileName, file.result));
                        $uploader.find('.k-file-progress .k-upload-status').attr('data-path', file.result);
                        $uploader.find('.k-file-progress .k-upload-status').attr('data-file-name', file.fileName);

                        var $field = e.sender.element;
                        var field = $field.data(that.field.namespace);
                        if ($.com.isJqueryObject($field) && $.com.hasValue(field)) {
                            let formData = field.getFormData();
                            if ($.com.hasValue(formData)) {
                                let attachment = file.attachment;
                                attachment.businessKey = formData[field.options.dataIdField];
                                attachment.businessType = field.options.businessType;

                                var attachments = field.getFormAttachments() || [];
                                attachments.push(file.attachment);
                                if (field.options.single) {
                                    //单文件的情况下
                                    field.options.value = attachment.path;
                                }

                                if ($.com.isFunction(field.options.success) && typeof field.execute === 'function') {
                                    field.execute(field.options.success, file);
                                }
                            }
                        }
                    }
                }
            },
            error: function (e) {
                //失败后会执行此方法
                if ($.com.hasValue(e) && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.wrapper)) {
                    var $uploader = e.sender.wrapper;
                    var $uploadFiles = $uploader.find('.k-upload-files');
                    if ($uploadFiles.prev().hasValue()) {
                        $uploadFiles.insertBefore($uploadFiles.prev());
                    }
                }
            },
            complete: function (e) {
                //成功失败最终都会执行此方法
                if ($.com.hasValue(e) && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.wrapper) && $.com.hasValue(e.sender.options)) {
                    var $uploader = e.sender.wrapper;
                    var $success = $uploader.find('.k-upload-files .k-file-success');
                    if ($success.hasValue()) {
                        $success = $success.filter((i, x) => !$(x).find('.k-progress').is(':hidden'));
                        if ($success.hasValue()) {
                            $success.each(function () {
                                var $this = $(this);
                                var $status = $this.find('.k-upload-status');
                                var $buttons = $this.find('.k-upload-status button');
                                if ($status.hasValue() && $buttons.hasValue() && $buttons.children().length !== 3) {
                                    $buttons.eq(0).hide();
                                    //$buttons.eq(1).append('<span class="k-icon ion-eye" data-button="download" data-path="' + $status.attr('data-path') + '" data-file-name="' + $status.attr('data-path') + '" title="查看" aria-label="查看"></span>').hide();
                                    //$buttons.eq(2).append('<span class="k-icon ion-edit" title="编辑" aria-label="编辑"></span>').hide();
                                    //$buttons.eq(0).insertAfter($buttons.eq(2));
                                    if (e.sender.options.deletable) {
                                        $buttons.eq(0).show();
                                    }
                                    //if (e.sender.options.viewable) {
                                    //    $buttons.eq(1).show().children().button();
                                    //}
                                    //if (e.sender.options.editable) {
                                    //    $buttons.eq(2).show();
                                    //}
                                }
                            });
                        }
                    }
                }
            },
            recalculate: function (el) {
                //由于kendoUpload组件上传时移除了组件数据, 此处需重新修正组件数据引用
                if ($.com.hasValue(el) && $.com.isJqueryObject(el.$fieldset)) {
                    var $field = el.$fieldset.find('#' + el.options.id);
                    if ($.com.isJqueryObject($field)) {
                        el.$el = $field;
                        el.validator.element = $field;
                        $field.data(that.field.namespace, el);
                        var form = el.$form.data(that.form.namespace);
                        if ($.com.hasValue(form)) {
                            var $el = form.$fields.filter('[data-field="' + el.options.field + '"]');
                            if ($.com.isJqueryObject($el)) {
                                var index = form.$fields.getIndex($el);
                                form.$fields.splice(index, 1, $field[0]);
                            }
                        }
                    }
                }
            }
        },
        'import': {
            enable: true,
            viewable: false,
            editable: false,
            deletable: false,
            multiple: false,
            fieldset: false,
            role: 'kendoUpload',
            type: 'file',
            template: undefined,
            uploadTemplate: undefined,
            size: '12,3,9',
            fileSize: 102400,                    //文件大小, 单位 KB
            fileCount: 1,                       //文件数量
            fileExtension: '.xlsx',             //文件扩展名, * 指任意文件, 不指定扩展名; 多个扩展名用 , 隔开 
            async: {
                saveUrl: "/api/Attachments/Upload",
                autoUpload: true
            },
            localization: {
                select: "导入"
            },
            selected: function (el, e) {
                if ($.com.hasValue(el.options) && $.com.hasValue(e) && $.com.isArray(e.files)) {
                    if (e.files.length === 1) {
                        var file = e.files[0];
                        var exts = el.options.fileExtension.split(',');
                        if (!exts.exist(function (x) { return x === file.extension.toLowerCase(); })) {
                            e.files.splice(0, 1);
                            //$.com.WarningDialog('只能导入 ' + el.options.fileExtension + ' 类型的文件!');
                            app.warning('只能导入 ' + el.options.fileExtension + ' 类型的文件!');
                        }

                        if (file.size > el.options.fileSize * 1024) {
                            e.files.splice(0, 1);
                            //$.com.WarningDialog('只能导入不超过 ' + el.options.fileSize + 'KB 大小的文件!');
                            app.warning('只能导入不超过 ' + el.options.fileSize + 'KB 大小的文件!');
                        }
                    } else {
                        while (e.files.length > 0) {
                            e.files.splice(0, 1);
                        }

                        //$.com.WarningDialog('每次只能导入一个文件!');
                        app.warning('每次只能导入一个文件!');
                    }
                }
            },
            success: function (e) {
                //成功后会执行此方法
                if ($.com.hasValue(e) && $.com.hasValue(e.response) && $.com.isArray(e.response) && e.response.length === 1 && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.element) && $.com.isJqueryObject(e.sender.wrapper)) {
                    var fileName = e.response[0].result;
                    var $uploader = e.sender.wrapper;
                    var $button = $uploader.closest('[data-button="import"]');
                    if ($button.hasValue()) {
                        $button.button('setValue', 'options.fileName', fileName);
                        $button.click();
                    }
                }
            },
            complete: function (e) {
                //成功失败最终都会执行此方法
                if ($.com.hasValue(e) && $.com.hasValue(e.sender) && $.com.isJqueryObject(e.sender.wrapper)) {
                    var $uploader = e.sender.wrapper;
                    $uploader.find('.k-upload-files').remove();
                    var $button = $uploader.closest('[data-button="import"]');
                    if ($button.hasValue()) {
                        $button.button('setValue', 'options.fileName', null);
                    }
                }
            }
        }
    };

    this.validator = {
        defaults: {
            validateOnBlur: false,
            //errorTemplate: '<span class="' + that.attribute.inputInvalidMsg + '">#=message#</span>'
        },
        requiredDisplay: {
            rules: {
                requiredDisplay: function ($el) {
                    var field = $el.data(that.field.namespace);
                    if ($.com.hasValue(field)) {
                        return field.options.display === false || (field.options.display && $.com.hasValue($el.val()));
                    }

                    return $el.is(':hidden') || (!$el.is(':hidden') && $.com.hasValue($el.val()));
                }
            },
            messages: {
                requiredDisplay: function ($el) {
                    return $el.attr('name') + ' 为必填项';
                }
            }
        },
        maxLength: {
            rules: {
                maxLength: function ($el) {
                    if ($el.is(that.selector.maxLength)) {
                        return $el.val().length <= Number($el.attr(that.attribute.maxLength) || 0);
                    }

                    return true;
                }
            },
            messages: {
                maxLength: function ($el) {
                    return $el.attr('name') + ' 最多可以输入 ' + $el.attr(that.attribute.maxLength) + ' 个字符!';
                }
            }
        },
        minLength: {
            rules: {
                minLength: function ($el) {
                    if ($el.is(that.selector.minLength)) {
                        return $el.val().length >= Number($el.attr(that.attribute.minLength) || 0);
                    }

                    return true;
                }
            },
            messages: {
                minLength: function ($el) {
                    return $el.attr('name') + ' 必须至少输入 ' + $el.attr(that.attribute.minLength) + ' 个字符';
                }
            }
        },
        groupRequired: {
            rules: {
                required: function ($el) {
                    var el;
                    if (!$el.is(that.selector.field)) {
                        el = $el.parents(that.selector.field).first();
                        if (el.hasValue()) {
                            el = el.data(that.field.namespace);
                        }
                    } else {
                        el = $el.data(that.field.namespace);
                    }

                    if ($.com.hasValue(el) && $.com.hasValue(el.options) && el.options.withGroup && (el.options.role === 'checkbox' || el.options.role === 'radio')) {
                        return $.com.hasValue(el.options.value);
                    }

                    return true;
                }
            },
            messages: {
                required: function ($el) {
                    return $el.parents(that.selector.field).attr('name') + ' 必须至少选择其中一项';
                }
            }
        },
        attachmentRequired: {
            rules: {
                required: function ($el) {
                    var result = $.com.isJqueryObject($el) && $el[0].files.length > 0;
                    if (!result) {
                        var field = $el.data(that.field.namespace);
                        if ($.com.hasValue(field)) {
                            var data = $.com.getFormData(field.$form);
                            if ($.com.hasValue(data) && $.com.hasValue(field.options.businessType) && $.com.isArray(data.attachments)) {
                                result = data.attachments.exist(x => x.businessType == field.options.businessType);
                            }
                        }
                    }

                    return result;
                }
            },
            messages: {
                required: function ($el) {
                    var name = "附件";
                    var field = $el.data(that.field.namespace);
                    if ($.com.hasValue(field) && $.com.hasValue(field.options.name)) {
                        name = field.options.name;
                    }

                    return name + " 为必填项";
                }
            }
        }
    };

    this.format = {
        'default': {
            timezone: 'Asia/Shanghai',
            date: 'yyyy-MM-dd',
            time: 'HH:mm:ss',
            datetime: 'yyyy-MM-dd HH:mm:ss'
        },
        'monemt': {
            date: 'YYYY-MM-DD',
            time: 'HH:mm:ss',
            datetime: 'YYYY-MM-DD HH:mm:ss'
        },
        kendo: {
            date: '{0: yyyy-MM-dd}',
            time: '{0: HH:mm:ss}',
            datetime: '{0: yyyy-MM-dd HH:mm:ss}'
        },
        grid: {
            date: '{0: yyyy/MM/dd}',
            time: '{0: HH:mm:ss}',
            datetime: '{0: yyyy/MM/dd HH:mm:ss}'
        },
        filter: {
            date: '{1:yyyy-MM-dd}',
            time: '{1:HH:mm:ss}',
            datetime: '{1:yyyy-MM-ddTHH:mm:ss+00:00}'
        }
    };
};