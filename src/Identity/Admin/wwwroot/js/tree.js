﻿/**
* 树
*/
var Tree = new function () {
    //新增
    this.onTreeLoad = (el) => {
        loadingResourceTree(el, false, false);
    };

    //编辑
    this.onTreeLoadChecked = el => {
        loadingResourceTree(el, true, false);
    };

    //详情
    this.onTreeLoadCheckDetail = el => {
        loadingResourceTree(el, true, true);
    }

    function loadingResourceTree(el, getCheck, isDetail) {
        var $tree = el.$form.find('[data-toggle="tree"]');
        if ($.com.isJqueryObject($tree)) {
            var tree = $tree.data("kendoTreeView");
            if (tree) {
                tree.destroy();
                $tree.empty();
            }

            generateResourceTree($tree, getCheck, isDetail);
        }
    };

    function generateResourceTree($tree, getCheck, isDetail) {
        $.api.get("/odata/Resources?$filter=state eq 'Available'&$orderBy=code", data => {
            if ($.com.hasValue(data) && data.value.length > 0) {
                var resources = data.value;
                var resourceTree = resources.where(x => x.parentId == null);
                if (resourceTree.length > 0) {
                    this._displayName = $tree.data().displayName;
                    for (var i = 0; i < resourceTree.length; i++) {
                        getResourceTree(resourceTree[i], resources);
                    }

                    var options = {
                        dataTextField: "dispayName",
                        dataSource: resourceTree,
                        animation: {
                            expand: false
                        },
                    };

                    if ($tree.data().checkboxes) {
                        options = $.extend({
                            check: onCheck,
                            checkboxes: { checkChildren: true }
                        }, options);
                    }

                    $tree.kendoTreeView(options);
                }
            }

            //详情页面checkbox不可用
            if (isDetail) {
                $tree.find('.k-checkbox').attr('disabled', 'disabled');
            }
        });
    }

    function getResourceTree(resourceTree, allResources) {
        resourceTree.dispayName = (resourceTree[this._displayName] || resourceTree.title) + "(" + resourceTree.content + ")";
        let children = allResources.where(function (x) { return x.parentId == resourceTree.id });
        if (children.length > 0) {
            resourceTree.items = children;
            resourceTree.expanded = true;
            resourceTree.hasChildren = true;
            for (let index = 0; index < children.length; index++) {
                getResourceTree(children[index], allResources);
            }
        }
    }

    function onCheck(el) {
        let checkedNodes = [];
        var $tree = $(el.sender.element);
        var data = $tree.data("kendoTreeView").dataSource.data();
        if (data && data.length > 0) {
            getCheckedNodes(data, checkedNodes);
            var ids = checkedNodes.select("id");
            if ($.com.isArray(ids)) {
                let $form = $tree.closest(_config_.selector.form);
                $form.find('[data-field="resourceIds"]').val(ids.join()).change();
            }
        }
    };

    /**
     * 获取选中项列表
     * @param {any} nodes
     * @param {any} checkedNodes
     */
    function getCheckedNodes(nodes, checkedNodes) {
        for (var index = 0; index < nodes.length; index++) {
            let node = nodes[index];
            if (node.checked && node.parentId) {
                checkedNodes.push(node);
                continue;
            }

            if (node.hasChildren && node.items.length > 0) {
                getCheckedNodes(node.items, checkedNodes);
            }
        }
    };
}