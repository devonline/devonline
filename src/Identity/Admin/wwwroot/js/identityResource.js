﻿var IdentityResource = new function () {
    'use strict';

    //组织单位页面选择组织单位后 刷新右侧详情
    this.groupSelect = (e, $form) => {
        //选中后右侧出现详情,及新增按钮
        $form.find("#detail").show();
        let form = $form.data(_config_.form.namespace);
        if ($.com.hasValue(form)) {
            var dataItem = $form.find("#groupTree").data("kendoTreeView").dataItem(e.node);
            form.refresh(dataItem);
        }
    }

    //选中角色后加载对应的权限树
    this.roleSelect = (el, e, identity) => {
        IdentityResource.getResourceData('Role', identity.id);
    }

    //选中组织单位后加载对应的权限树
    this.groupResourceSelect = (e, $form) => {
        var $treeview = $form.find("#groupTree");
        var data = $treeview.data("kendoTreeView").dataItem(e.node);
        IdentityResource.getResourceData('Group', data.id);
    }

    //加载权限树
    this.getResourceData = (type, id) => {
        $.api.get(`/api/AccessRules/GetIdentityResource/${type}/${id}`, data => {
            var $treeview = $("#assginTree");
            var treeview = $treeview.data("kendoTreeView");
            if (treeview) {
                treeview.destroy();
                $treeview.empty();
            }

            if (data && data.length > 0) {
                getResourceTree(data);
                $treeview.kendoTreeView({
                    dataTextField: "displayName",
                    dataSource: data
                });
            } else {
                $treeview.html('<span class="k-in">没有权限!</span>');
            }
        });
    }

    //组织单位页面 加载组织单位树
    this.groupFormLoaded = (el) => {
        IdentityResource.initGroupTree(el.$el, IdentityResource.groupSelect);
    }

    //组织单位可访问资源页面 加载组织单位树
    this.groupResourceFormLoaded = (el) => {
        IdentityResource.initGroupTree(el.$el, IdentityResource.groupResourceSelect);
    }

    //加载组织单位树
    this.initGroupTree = ($form, fun, selectValue) => {
        var $treeview = $form.find("#groupTree");
        var treeview = $treeview.data("kendoTreeView");
        $.api.get(`/api/Groups/GetGroupTreeData`, data => {
            if (treeview) {
                treeview.destroy();
                $treeview.empty();
            }

            if (data && data.length > 0) {
                getResourceTree(data);
                $treeview.kendoTreeView({
                    dataTextField: "name",
                    dataSource: data,
                    select: e => fun(e, $form)
                });

                if ($.com.hasValue(selectValue)) {
                    treeview = $treeview.data("kendoTreeView");
                    var treeNodeData = treeview.dataSource.get(selectValue);
                    if ($.com.hasValue(treeNodeData)) {
                        var bar = treeview.findByUid(treeNodeData.uid);
                        treeview.select(bar);
                        let form = $form.data(_config_.form.namespace);
                        if ($.com.hasValue(form)) {
                            form.refresh(treeNodeData);
                        }
                    }
                }
            } else {
                $treeview.html('<span class="k-in">没有组织机构数据!</span>');
            }
        });

        return treeview;
    }

    //刷新组织单位树
    this.refreshTree = (el) => {
        var popup = el.$popup.data(_config_.popupWindow.namespace);
        if ($.com.hasValue(popup)) {
            var $form = popup.options.opener;
            var data = popup.options.formData;
            IdentityResource.initGroupTree($form, IdentityResource.groupSelect, data.id);
        }
    }

    this.deleteRefreshTree = (el) => {
        el.$form.find("#detail").hide();
        IdentityResource.initGroupTree(el.$form, IdentityResource.groupSelect);
    }

    function getResourceTree(resources) {
        for (let index = 0; index < resources.length; index++) {
            let node = resources[index];
            node.expanded = true;
            node.items = node.children;
            node.displayName = node.title + "(" + node.content + ")";
            delete node.children;

            node.hasChildren = false;
            if (node.items && node.items.length > 0) {
                node.hasChildren = true;
                getResourceTree(node.items);
            }
        }
    }
}