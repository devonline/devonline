﻿function userSelect(el, event, user) {
    if (user && user.userName) {
        $.api.get(`/api/accessRules/GetUserInfo?userName=${user.userName}`, data => {
            var $treeview = $("#assginTree");
            var treeview = $treeview.data("kendoTreeView");
            if (treeview) {
                treeview.destroy();
                $treeview.empty();
            }

            if (data.resourceTree && data.resourceTree.length > 0) {
                getResourceTree(data.resourceTree);
                $treeview.kendoTreeView({
                    dataTextField: "displayName",
                    dataSource: data.resourceTree
                });
            } else {
                $treeview.html('<span class="k-in">没有权限!</span>');
            }
        });
    }
}

function getResourceTree(resources) {
    for (let index = 0; index < resources.length; index++) {
        let node = resources[index];
        node.expanded = true;
        node.items = node.children;
        node.displayName = node.title + "(" + node.content + ")";
        delete node.children;

        node.hasChildren = false;
        if (node.items && node.items.length > 0) {
            node.hasChildren = true;
            getResourceTree(node.items);
        }
    }
}