﻿app.loaded.set(99, () => Profile.onLoaded());

var Profile = new function () {
    'use strict';

    this.onLoad = el => {
        $('#_profile_detail_panel').show();
        $('#_profile_edit_panel').hide();
        $('#_footer_').after($('#_header_'));
        var phoneNumber = $('#_profile_edit_panel [data-field="phoneNumber"]').field('getData');
        phoneNumber.$fieldset.append($('#_btn_edit_phoneNumber'));
        phoneNumber.$fieldset.append($('#_btn_submit_phoneNumber'));
        var email = $('#_profile_edit_panel [data-field="email"]').field('getData');
        email.$fieldset.append($('#_btn_edit_email'));
        email.$fieldset.append($('#_btn_submit_email'));
    }

    this.onBound = el => {
        $('#_changePassword_panel [data-toggle="form"] [data-field="userName"]').val(el.options.data.userName).change();
    }

    this.onEdit = el => {
        $('#_profile_detail_panel').hide();
        $('#_profile_edit_panel').show();
        $('#_profile_edit_panel [data-toggle="form"]').form('refresh', el.$form.form('getFormData'));
    };

    this.onBack = el => {
        $('#_profile_detail_panel').show();
        $('#_profile_edit_panel').hide();
    };

    this.onEditSuccess = el => {
        $('#_profile_detail_panel [data-toggle="form"]').form('init');
    };

    this.onLoaded = () => {
        var formData = $('#_profile_edit_panel [data-toggle="form"]').form('getFormData');
        $('#_profile_edit_panel [data-toggle="form"] [data-field="userName"]').field('refresh', formData.userName);
        $('#_txt_edit_phoneNumber').field('readonly', true);
        $('#_txt_edit_email').field('readonly', true);
        $('#_btn_submit_phoneNumber').button('display', false);
        $('#_btn_submit_email').button('display', false);
    };

    this.onEditPhoneNumber = el => {
        $.api.get('/api/UserProfiles/GetChangePhoneNumberToken', token => {
            $('#_profile_edit_panel [data-toggle="form"][data-field="UserPhoneNumber"] [data-field="token"]').field('refresh', token);
            $('#_txt_edit_phoneNumber').field('readonly', false);
            $('#_btn_edit_phoneNumber').button('display', false);
            $('#_btn_submit_phoneNumber').button('display', true);
        });
    };

    this.onAfterEditPhoneNumber = el => {
        $('#_profile_edit_panel [data-toggle="form"][data-field="UserPhoneNumber"] [data-field="token"]').field('clear');
        $('#_txt_edit_phoneNumber').field('readonly', true);
        $('#_btn_edit_phoneNumber').button('display', true);
        $('#_btn_submit_phoneNumber').button('display', false);
    };

    this.onEditEmail = el => {
        $.api.get('/api/UserProfiles/GetChangeEmailToken', token => {
            $('#_profile_edit_panel [data-toggle="form"][data-field="UserEmail"] [data-field="token"]').field('refresh', token);
            $('#_txt_edit_email').field('readonly', false);
            $('#_btn_edit_email').button('display', false);
            $('#_btn_submit_email').button('display', true);
        });
    };

    this.onAfterEditEmail = el => {
        $('#_profile_edit_panel [data-toggle="form"][data-field="UserEmail"] [data-field="token"]').field('clear');
        $('#_txt_edit_email').field('readonly', true);
        $('#_btn_edit_email').button('display', true);
        $('#_btn_submit_email').button('display', false);
    };

    this.onEditError = el => {
        var formData = el.$form.form('getFormData');
        formData[el.options.field];
    };

    this.onBeforeChangePassword = el => {
        var password = el.$form.find('[data-field="password"]').val();
        var newPassword = el.$form.find('[data-field="newPassword"]').val();
        var confirmNewPassword = el.$form.find('[data-field="confirmNewPassword"]').val();
        if (!$.com.hasValue(password)) {
            app.warning("原始密码不能为空!");
            return false;
        }

        if (!$.com.hasValue(newPassword)) {
            app.warning("新密码不能为空!");
            return false;
        }

        if (!$.com.hasValue(confirmNewPassword)) {
            app.warning("确认新密码不能为空!");
            return false;
        }

        if (newPassword !== confirmNewPassword) {
            app.warning("新密码和确认新密码不一致!");
            return false;
        } else if (password === newPassword) {
            app.warning("新密码和原始密码不能相同!");
            return false;
        }
    }
}