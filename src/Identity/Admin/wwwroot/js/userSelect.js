﻿var _userAssign = {
    user: undefined,
    assgin: []
};

function userSelect(el, event, user) {
    if (user && user.userName) {
        _userAssign.user = user;
        var isRole = $.com.isJqueryObject($('#assginFilter[data-field="Role"]'));
        $.api.get(`/api/users/${isRole ? 'GetUserRoles' : 'GetUserGroups'}/${user.userName}`, data => {
            _userAssign.assgin = data;

            var grid = $('#assginGrid').data(_config_.grid.namespace);
            if ($.com.hasValue(grid)) {
                grid.role.clearSelection();

                if (data && data.length > 0) {
                    var selectedRows = [];
                    grid.role.items().each((index, el) => {
                        var row = grid.role.dataItem(el)
                        if (data.exist(x => x == row.name)) {
                            selectedRows.push(el);
                        }
                    });

                    grid.role.select(selectedRows);
                }
            }
        });
    }
}

function assginSelect(el, event, assgins) {
    _userAssign.assgin = [];
    if ($.com.isArray(assgins)) {
        assgins.where(x => _userAssign.assgin.push(x.name));
    }
}

function userChangeAssign(el) {
    el.options.url = el.initials.url + '/' + _userAssign.user.userName;
    el.options.data = _userAssign.assgin;
}