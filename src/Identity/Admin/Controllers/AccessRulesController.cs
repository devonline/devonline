﻿using System.ComponentModel;
using Devonline.Communication.Messages;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Devonline.Identity.Admin.Controllers;

[SecurityHeaders]
[ApiController]
[Route("api/[controller]")]
[Description("访问规则管理")]
[Authorize(Roles = GROUP_MAINTAINERS)]
[AccessAuthorize(Code = "AS0XAccessRules", Name = "访问规则管理", Content = "/api/AccessRules", ResourceType = ResourceType.Service)]
public class AccessRulesController(
    AuthorizationService authorizationService,
    ILogger<AccessRulesController> logger,
    IMessageCommunicator communicator,
    IDataService<AccessRule> dataService,
    IDataService<User> userService,
    IDataService<Resource> resourceService,
    IDataService<Role> roleService) :
    ODataModelServiceController<AccessRule, AccessRuleViewModel>(logger, dataService)
{
    private readonly IDataService<User> _userService = userService;
    private readonly AuthorizationService _authorizationService = authorizationService;
    private readonly IMessageCommunicator _communicator = communicator;
    private readonly IDataService<Resource> _resourceService = resourceService;
    private readonly IDataService<Role> _roleService = roleService;

    [HttpPost, DisplayName("创建用户信息"), AccessAuthorize(Code = "AS0XAccessRulesICreate")]
    public override async Task<IActionResult> CreateAsync(AccessRuleViewModel viewModel)
    {
        var name = "";
        switch (viewModel.IdentityType)
        {
            case IdentityType.All:
                viewModel.IdentityId = null;
                name = "All";
                break;
            case IdentityType.Anonymous:
            case IdentityType.System:
                viewModel.IdentityId = _userService.GetQueryable(x => x.UserName == viewModel.IdentityType.ToString().ToLowerInvariant()).Select(x => x.Id).FirstOrDefault();
                name = viewModel.IdentityType.ToString();
                break;
            case IdentityType.Role:
                var role = await _roleService.FirstOrDefaultAsync(x => x.Id == viewModel.IdentityId);
                name = role?.Type.ToString();
                break;
            default:
                if (string.IsNullOrWhiteSpace(viewModel.IdentityId))
                {
                    return BadRequest("必须选择一个授权对象!");
                }
                break;
        }

        string[]? resources = null;
        if (!string.IsNullOrWhiteSpace(viewModel.ResourceIds))
        {
            resources = viewModel.ResourceIds.Split(CHAR_COMMA, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            if (resources.Length == 1)
            {
                viewModel.ResourceId = resources.FirstOrDefault();
            }
        }

        if (resources?.Length > 1)
        {
            var addAccessRules = new List<AccessRule>();
            if (string.IsNullOrWhiteSpace(viewModel.Code))
            {
                //如果没有规则编号则自动生成
                var allResource = await _resourceService.GetQueryable(x => x.State == DataState.Available).ToListAsync();
                for (int index = 0; index < resources.Length; index++)
                {
                    var systemId = allResource.FirstOrDefault(x => x.Id == resources[index])?.SystemId;
                    var systemCode = allResource.FirstOrDefault(x => x.Id == systemId)?.Code;
                    var prefix = CHAR_SHARP + systemCode + CHAR_UNDERLINE + name + CHAR_UNDERLINE + viewModel.Priority + CHAR_UNDERLINE;
                    var add = viewModel.CopyTo<AccessRule>();
                    add.Id = KeyGenerator.GetStringKey();
                    add.ResourceId = resources[index];
                    add.Code = prefix + (index + 1).ToString("D4");
                    addAccessRules.Add(add);
                }
            }
            else
            {
                for (int index = 0; index < resources.Length; index++)
                {
                    var add = viewModel.CopyTo<AccessRule>();
                    add.Id = KeyGenerator.GetStringKey();
                    add.ResourceId = resources[index];
                    add.Code = viewModel.Code + (index + 1).ToString("D4");
                    addAccessRules.Add(add);
                }
            }

            if (addAccessRules.Any())
            {
                await _dataService.AddsAsync(addAccessRules);
                await RefreshUserInfoAsync(viewModel.IdentityType, viewModel.IdentityId);
            }
        }
        else
        {
            await _dataService.AddAsync(viewModel);
            await RefreshUserInfoAsync(viewModel.IdentityType, viewModel.IdentityId);
        }

        return Ok(viewModel);
    }
    [HttpPut, DisplayName("更新用户信息"), AccessAuthorize(Code = "AS0XAccessRulesIUpdate")]
    public override async Task<IActionResult> UpdateAsync(AccessRuleViewModel viewModel)
    {
        if (viewModel.IdentityType == IdentityType.All)
        {
            viewModel.IdentityId = null;
        }

        if (viewModel.IdentityType == IdentityType.Anonymous || viewModel.IdentityType == IdentityType.System)
        {
            viewModel.IdentityId = _userService.GetQueryable(x => x.UserName == viewModel.IdentityType.ToString().ToLowerInvariant()).Select(x => x.Id).FirstOrDefault();
        }

        await _dataService.UpdateAsync(viewModel);
        await RefreshUserInfoAsync(viewModel.IdentityType, viewModel.IdentityId);
        return Ok(viewModel);
    }
    [HttpDelete("{id}"), DisplayName("删除用户信息"), AccessAuthorize(Code = "AS0XAccessRulesIDelete")]
    public override async Task<IActionResult> DeleteAsync(string id)
    {
        var entitySet = await _dataService.GetIfExistAsync(id);
        await _dataService.DeleteAsync(id);
        await RefreshUserInfoAsync(entitySet.IdentityType, entitySet.IdentityId);
        return Ok();
    }

    /// <summary>
    /// 此处的获取用户信息会自动刷新缓存
    /// </summary>
    /// <param name="userName"></param>
    /// <returns></returns>
    [HttpGet("GetUserInfo"), DisplayName("获取用户信息"), AccessAuthorize(Code = "AS0XAccessRulesIGetUserInfo")]
    public async Task<IActionResult> GetUserInfoAsync(string userName)
    {
        _logger.LogDebug("user {user} get user info forced", HttpContext.GetUserName());
        var userContext = await _authorizationService.GetUserContextAsync(userName, true);
        if (userContext is null)
        {
            return NotFound();
        }

        await RefreshUserInfoAsync(IdentityType.User, userContext.UserId);
        return Ok(new UserInfo { User = userContext.User.Desensitize(), ResourceTree = userContext.ResourceTrees.Values });
    }
    /// <summary>
    /// 根据身份类型和身份类别编号获取对应的授权资源
    /// </summary>
    /// <param name="identityType"></param>
    /// <param name="identityId"></param>
    /// <returns></returns>
    [HttpGet("GetIdentityResource/{identityType}/{identityId}"), DisplayName("按身份获取授权资源树"), AccessAuthorize(Code = "AS0XAccessRulesIGetIdentityResource")]
    public async Task<IActionResult> GetIdentityResourceAsync(IdentityType identityType, string identityId)
    {
        var resources = await _authorizationService.GetResourceByIdentityAsync(identityType, identityId);
        await RefreshUserInfoAsync(identityType, identityId);
        return Ok(resources.Values);
    }

    [HttpGet("UserHasPermission/{userName}"), DisplayName("用户是否有资源访问权限查询"), AccessAuthorize(Code = "AS0XAccessRulesIUserHasPermission")]
    public async Task<IActionResult> UserHasPermissionAsync(string userName, string resource) => Ok(await _authorizationService.UserHasPermissionAsync(resource, userName));

    /// <summary>
    /// 刷新用户缓存
    /// </summary>
    /// <param name="identityType"></param>
    /// <param name="identityId"></param>
    /// <returns></returns>
    private async Task RefreshUserInfoAsync(IdentityType identityType, string? identityId)
    {
        var identityIds = new List<string>();
        if (!string.IsNullOrWhiteSpace(identityId))
        {
            identityIds.Add(identityId);
        }

        await _authorizationService.RefreshUserInfoAsync(_communicator, identityType, identityIds.ToArray());
    }
}