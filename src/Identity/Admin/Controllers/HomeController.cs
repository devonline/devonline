﻿using System.ComponentModel;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin.Controllers;

[DisplayName("基础服务")]
[Authorize(Roles = GROUP_MAINTAINERS)]
[AccessAuthorize(Code = "AS0XHome", Name = "统一认证与授权中心-基础服务", Content = "/Home", ResourceType = ResourceType.Service)]
public class HomeController(
    IIdentityServerInteractionService interaction,
    IWebHostEnvironment environment) : Controller
{
    private readonly IIdentityServerInteractionService _interaction = interaction;
    private readonly IWebHostEnvironment _environment = environment;

    /// <summary>
    /// 主页
    /// </summary>
    /// <returns></returns>
    [AccessAuthorize(Code = "AS0PIndex", ResourceType = ResourceType.Page), DisplayName("主页")]
    public IActionResult Index()
    {
        return View();
    }
    /// <summary>
    /// Shows the error page
    /// </summary>
    [AccessAuthorize(Code = "AS0PError", ResourceType = ResourceType.Page), DisplayName("错误页面")]
    public async Task<IActionResult> Error(string errorId)
    {
        var vm = new ErrorViewModel();

        // retrieve error details from identityserver
        var message = await _interaction.GetErrorContextAsync(errorId);
        if (message != null)
        {
            vm.Error = message;

            if (!_environment.IsDevelopment())
            {
                // only show in development
                message.ErrorDescription = null;
            }
        }

        return View("Error", vm);
    }
}