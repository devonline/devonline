using System.ComponentModel;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin.Controllers;

[AllowAnonymous]
[AccessAuthorize(Code = "AS0XExternal", Name = "外部服务", Content = "/External", ResourceType = ResourceType.Service)]
public class ExternalController(
    IIdentityServerInteractionService interaction,
    ILogger<ExternalController> logger,
    HttpSetting httpSetting,
    SignInManager<User> signInManager,
    AuthenticationService authenticationService) : Controller
{
    private readonly IIdentityServerInteractionService _interaction = interaction;
    private readonly ILogger<ExternalController> _logger = logger;
    private readonly HttpSetting _httpSetting = httpSetting;
    private readonly SignInManager<User> _signInManager = signInManager;
    private readonly AuthenticationService _authenticationService = authenticationService;

    /// <summary>
    /// initiate roundtrip to external authentication provider
    /// </summary>
    [HttpGet("External/Challenge")]
    [Description("外部接口质询")]
    [AccessAuthorize(Code = "AS0XExternalIChallenge")]
    public IActionResult Challenge(string scheme, string returnUrl)
    {
        if (string.IsNullOrEmpty(returnUrl)) returnUrl = "~/";

        // validate returnUrl - either it is a valid OIDC URL or back to a local page
        if (Url.IsLocalUrl(returnUrl) == false && _interaction.IsValidReturnUrl(returnUrl) == false)
        {
            // user might have clicked on a malicious link - should be logged
            throw new Exception("invalid return URL");
        }

        // start challenge and roundtrip the return URL and scheme 
        var props = new AuthenticationProperties
        {
            RedirectUri = _httpSetting.UserInteraction!.Callback,
            Items =
            {
                { nameof(returnUrl), returnUrl },
                { nameof(scheme), scheme },
            }
        };

        return Challenge(props, scheme);
    }
    /// <summary>
    /// Post processing of external authentication
    /// </summary>
    [HttpGet("External/Callback")]
    [Description("外部接口回调")]
    [AccessAuthorize(Code = "AS0XExternalICallback")]
    public async Task<IActionResult> CallbackAsync()
    {
        // read external identity from the temporary cookie
        var scheme = CookieAuthenticationDefaults.AuthenticationScheme;
        var result = await HttpContext.AuthenticateAsync(scheme);
        if (!(result?.Succeeded ?? false))
        {
            throw new Exception("External authentication error");
        }

        if (_logger.IsEnabled(Microsoft.Extensions.Logging.LogLevel.Debug))
        {
            var externalClaims = result.Principal?.Claims.Select(c => $"{c.Type}: {c.Value}");
            _logger.LogDebug("External claims: {@claims}", externalClaims);
        }

        var user = await _authenticationService.LoginAsync();
        var principal = await _signInManager.CreateUserPrincipalAsync(user);
        var claims = await _authenticationService.GetUserClaimsAsync(user);
        claims.AddRange(principal.Claims);
        var isuser = new IdentityServer4.IdentityServerUser(user.Id)
        {
            DisplayName = user.UserName,
            IdentityProvider = result.Properties.Items["scheme"],
            AdditionalClaims = claims
        };

        result.Properties.IsPersistent = false;
        await HttpContext.SignInAsync(isuser, result.Properties);

        // delete temporary cookie used during external authentication
        //await HttpContext.SignOutAsync(scheme);

        // retrieve return URL
        //string? returnUrl = result.Properties.Items[nameof(returnUrl)];
        var returnUrl = Request.Cookies[CLAIM_TYPE_REDIRECT_URL];
        var redirectUrl = _authenticationService.GetRedirectUrl(user, returnUrl);

        // check if external login is in the context of an OIDC request
        var context = await _interaction.GetAuthorizationContextAsync(redirectUrl);
        if (context is not null && context.IsNativeClient())
        {
            // The client is native, so this change in how to
            // return the response is for better UX for the end user.
            return this.LoadingPage("Redirect", redirectUrl);
        }

        return Redirect(redirectUrl);
    }
}