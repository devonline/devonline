﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin;

/// <summary>
/// 附件及文件操作类服务通用接口
/// </summary>
[Authorize]
[ApiController]
[Route("api/[controller]")]
[Description("附件服务")]
[AccessAuthorize(Code = "AS0XAttachment", Name = "附件服务", Content = "/api/Attachments", ResourceType = ResourceType.Service)]
public class AttachmentsController(
    ILogger<AttachmentsController> logger,
    AspNetCore.IDataService<IdentityDbContext, Attachment> dataService,
    IAttachmentService attachmentService,
    IFileService fileService) :
    AttachmentServiceController<IdentityDbContext>(logger, dataService, attachmentService, fileService)
{
    [HttpPost("GetFiles")]
    [AccessAuthorize(Code = "AS0XAttachmentIGetFileByPath")]
    [DisplayName("获取固定路径的附件集合")]
    public IActionResult GetFilesAsync([FromForm] string path)
    {
        var atts = _dataService.GetQueryable(x => x.Path.Contains(path)).Select(x => new AttachmentViewModel
        {
            Id = x.Id,
            Name = x.Path,
            Path = x.Name,
            Type = x.ContentType
        }).ToList();
        return Ok(atts);
    }
}