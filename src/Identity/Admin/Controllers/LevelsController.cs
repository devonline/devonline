﻿using System.ComponentModel;
using Devonline.AspNetCore.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin.Controllers;

[Route("api/[controller]")]
[ApiController]
[SecurityHeaders]
[DisplayName("级别管理")]
[Authorize(Roles = GROUP_MAINTAINERS)]
[AccessAuthorize(Code = "AS0XLevel", Name = "级别管理", Content = "/api/Levels", ResourceType = ResourceType.Service)]
public class LevelsController(
    ILogger<LevelsController> logger,
    IDataWithAttachmentService<Level> dataService,
    IExcelExportService excelExportService) :
    ODataExportServiceController<IdentityDbContext, Level>(logger, dataService, excelExportService);