﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PluralizeService.Core;

namespace Devonline.Identity.Admin.Controllers;

/// <summary>
/// 系统初始化控制器
/// </summary>
[Route("api/[controller]")]
[ApiController]
[AccessAuthorize(Code = "AS0XInitial", Content = "/Initial", ResourceType = ResourceType.Service)]
public class InitialController(
    ILogger<InitialController> logger,
    IDataService<Parameter> parameterService,
    AdminSetting appSetting,
    UserManager<User> userManager,
    RoleManager<Role> roleManager,
    SignInManager<User> signInManager,
    IdentityDbContext context,
    AuthorizationService authorizationService,
    IDataService<User> userService,
    IDataService<Role> roleService,
    IDataService<Resource> resourceService,
    IDataService<AccessRule> accessRuleService
    ) : ControllerBase
{
    private readonly ILogger<InitialController> _logger = logger;
    private readonly IDataService<Parameter> _parameterService = parameterService;
    private readonly AdminSetting _appSetting = appSetting;
    private readonly UserManager<User> _userManager = userManager;
    private readonly RoleManager<Role> _roleManager = roleManager;
    private readonly SignInManager<User> _signInManager = signInManager;
    private readonly IdentityDbContext _context = context;
    private readonly AuthorizationService _authorizationService = authorizationService;
    private readonly IDataService<User> _userService = userService;
    private readonly IDataService<Role> _roleService = roleService;
    private readonly IDataService<Resource> _resourceService = resourceService;
    private readonly IDataService<AccessRule> _accessRuleService = accessRuleService;

    [AllowAnonymous]
    [HttpGet("InitSystem")]
    [Display(Name = "初始化")]
    [AccessAuthorize(Code = "AS0XInitialIInitSystem")]
    public async Task<IActionResult> InitSystemAsync()
    {
        _logger.LogInformation("系统初始化!");

        await AutoMigrationAsync();
        await InitAccountAsync();
        await InitAccessAuthorizesAsync();
        await InitParameter();

        _logger.LogInformation("系统初始化完成!");
        return Ok();
    }

    [AllowAnonymous]
    [HttpGet("AutoMigration")]
    [Display(Name = "数据迁移")]
    [AccessAuthorize(Code = "AS0XInitialIAutoMigration")]
    public async Task<IActionResult> AutoMigrationAsync()
    {
        await _context.AutoMigrationAsync();
        _logger.LogInformation("数据迁移完成!");
        return Ok();
    }

    [AllowAnonymous]
    [HttpGet("InitAccount")]
    [Display(Name = "初始化账户")]
    [AccessAuthorize(Code = "AS0XInitialIInitAccount")]
    public async Task<IActionResult> InitAccountAsync()
    {
        _logger.LogInformation("初始化账户开始!");

        #region 创建内置角色
        await _context.Users.ExecuteDeleteAsync();
        await _context.Roles.ExecuteDeleteAsync();
        await _context.UserRoles.ExecuteDeleteAsync();
        await _context.UserClaims.ExecuteDeleteAsync();

        var authTypes = Enum.GetValues<AuthorizeType>();
        foreach (var authType in authTypes)
        {
            var roleName = authType.ToString().ToCamelCase();
            roleName = PluralizationProvider.Pluralize(roleName);
            var displayName = authType.GetDisplayName();
            var role = new Role
            {
                Id = authType.ToString("D"),
                Name = roleName,
                Alias = displayName,
                Type = authType,
                Description = displayName,
                CreatedBy = USER_SYSTEM,
                UpdatedBy = USER_SYSTEM
            };

            _roleService.Create(role);
            _roleService.Update(role);
            await _roleManager.CreateAsync(role);
        }
        #endregion

        #region 创建内置用户, 并给内置用户分配角色
        ArgumentException.ThrowIfNullOrWhiteSpace(_appSetting.DefaultPassword);

        //默认超管
        var user = new User
        {
            Id = AuthorizeType.Administrator.ToString("D"),
            Name = "超级管理员",
            UserName = USER_ADMINISTRATOR,
            Alias = "超级管理员",
            Type = AuthorizeType.Administrator,
            Description = "超级管理员",
            CreatedBy = USER_SYSTEM,
            UpdatedBy = USER_SYSTEM
        };

        _userService.Create(user);
        _userService.Update(user);
        await _userManager.CreateAsync(user);
        await _userManager.AddPasswordAsync(user, _appSetting.DefaultPassword);
        await _userManager.AddToRoleAsync(user, GROUP_ADMINISTRATOR);
        await _userManager.InitUserClaimsAsync(user);

        //默认开发者
        user = new User
        {
            Id = AuthorizeType.Developer.ToString("D"),
            Name = "开发者",
            UserName = USER_DEVELOPER,
            Alias = "开发者",
            Type = AuthorizeType.Developer,
            Description = "系统开发者"
        };

        _userService.Create(user);
        _userService.Update(user);
        await _userManager.CreateAsync(user);
        await _userManager.AddPasswordAsync(user, _appSetting.DefaultPassword);
        await _userManager.AddToRoleAsync(user, GROUP_DEVELOPER);
        await _userManager.InitUserClaimsAsync(user);

        //系统本身
        user = new User
        {
            Id = AuthorizeType.System.ToString("D"),
            Name = "系统",
            UserName = USER_SYSTEM,
            Alias = "系统",
            Type = AuthorizeType.System,
            Description = "系统本身"
        };

        _userService.Create(user);
        _userService.Update(user);
        await _userManager.CreateAsync(user);
        await _userManager.AddToRoleAsync(user, GROUP_SYSTEM);
        await _userManager.InitUserClaimsAsync(user);

        //匿名用户
        user = new User
        {
            Id = AuthorizeType.Anonymous.ToString("D"),
            Name = "匿名用户",
            UserName = USER_ANONYMOUS,
            Alias = "匿名用户",
            Type = AuthorizeType.Anonymous,
            Description = "匿名用户"
        };

        _userService.Create(user);
        _userService.Update(user);
        await _userManager.CreateAsync(user);
        await _userManager.AddToRoleAsync(user, GROUP_ANONYMOUS);
        await _userManager.InitUserClaimsAsync(user);
        #endregion

        _logger.LogInformation("初始化账户完成!");
        return Ok();
    }

    [AllowAnonymous]
    [HttpGet("InitAccessAuthorizes")]
    [Display(Name = "初始化访问授权")]
    [AccessAuthorize(Code = "AS0XInitialIInitAccessAuthorizes")]
    public async Task<IActionResult> InitAccessAuthorizesAsync()
    {
        _logger.LogInformation("初始化访问授权开始!");
        var assembly = System.Reflection.Assembly.GetExecutingAssembly();
        var assemblyName = "统一认证与授权中心";
        var resource = new Resource
        {
            Id = KeyGenerator.GetStringKey(),
            Code = "AS0",
            Name = assemblyName,
            Title = assemblyName,
            Alias = assemblyName,
            Content = _appSetting.Copyright!
        };
        await _authorizationService.InitialAccessAuthorizesAsync(assembly, resource);
        _logger.LogInformation("初始化访问授权完成!");
        return Ok();
    }

    /// <summary>
    /// 初始化基础数据
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet("InitParameter")]
    [DisplayName("初始化基础数据")]
    [AccessAuthorize(Code = "AS0XInitialIInitParameter")]
    public async Task<IActionResult> InitParameter()
    {
        var parameter = await _parameterService.GetQueryable().FirstOrDefaultAsync(x => x.Key == "root");
        if (parameter != null)
        {
            return Ok("基础数据已经初始化过了!");
        }

        parameter = new Parameter
        {
            Id = "0",
            Index = 0,
            Key = "root",
            Value = "Root",
            Text = "基础数据根节点"
        };
        parameter.Create();
        var list = new List<Parameter> { parameter };
        var types = typeof(AppSetting).Assembly.GetTypes().Where(x => x.IsEnum);
        if (types.Any())
        {
            var index = 101;
            var extensionType = typeof(AspNetCoreExtensions);
            var toLowerTypes = new string[] { nameof(DatabaseType), nameof(EncryptionAlgorithm), nameof(HashAlgorithm), nameof(SymmetricAlgorithm), nameof(Currency) };
            var boolTypes = new string[] { nameof(YesOrNo), nameof(HaveOrNot), nameof(AgreeOrNot) };
            foreach (var type in types.Where(x => !boolTypes.Contains(x.Name)))
            {
                var parameters = extensionType.InvokeGenericMethod<List<Parameter>>(nameof(AspNetCoreExtensions.GetParametersFromEnum), index++, [toLowerTypes.Contains(type.Name)], type);
                if (parameters is not null && parameters.Count != 0)
                {
                    list.AddRange(parameters);
                }
            }

            index = 201;
            foreach (var type in types.Where(x => boolTypes.Contains(x.Name)))
            {
                var parameters = extensionType.InvokeGenericMethod<List<Parameter>>(nameof(AspNetCoreExtensions.GetBoolParametersFromEnum), index++, [toLowerTypes.Contains(type.Name)], type);
                if (parameters is not null && parameters.Count != 0)
                {
                    list.AddRange(parameters);
                }
            }
        }

        _parameterService.SetAutoSaveChanges(false);
        foreach (var item in list)
        {
            await _parameterService.AddAsync(item);
        }

        await _parameterService.SaveChangesAsync();
        return Ok("基础数据初始化完成!");
    }
}