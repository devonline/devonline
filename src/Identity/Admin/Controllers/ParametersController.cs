﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin.Controllers;

/// <summary>
/// 基础数据管理
/// </summary>    
[Route("api/[controller]")]
[ApiController]
[SecurityHeaders]
[DisplayName("基础数据管理")]
[Authorize(Roles = GROUP_MAINTAINERS)]
[AccessAuthorize(Code = "AS0XParameter", Name = "基础数据管理", Content = "/api/Parameters", ResourceType = ResourceType.Service)]
public class ParametersController(
    ILogger<ParametersController> logger,
    IDataService<Parameter> dataService) :
    ODataModelServiceController<Parameter, ParameterViewModel>(logger, dataService);