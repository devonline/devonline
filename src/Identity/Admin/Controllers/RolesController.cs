﻿using System.ComponentModel;
using System.Data;
using System.Security.Claims;
using Devonline.Communication.Messages;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;

namespace Devonline.Identity.Admin.Controllers;

/// <summary>
/// 角色
/// </summary>    
[Route("api/[controller]")]
[ApiController]
[SecurityHeaders]
[Description("角色管理")]
[Authorize(Roles = GROUP_MAINTAINERS)]
[AccessAuthorize(Code = "AS0XRole", Content = "/api/Roles", ResourceType = ResourceType.Service)]
public class RolesController(
    AdminSetting appSetting,
    RoleManager<Role> roleManager,
    AuthorizationService authorizationService,
    ILogger<RolesController> logger,
    IDataWithAttachmentService<Role> dataService,
    IMessageCommunicator communicator) : ControllerBase
{
    private readonly AdminSetting _appSetting = appSetting;
    private readonly RoleManager<Role> _roleManager = roleManager;
    private readonly AuthorizationService _authorizationService = authorizationService;
    private readonly ILogger<RolesController> _logger = logger;
    private readonly IDataWithAttachmentService<Role> _dataService = dataService;
    private readonly IMessageCommunicator _communicator = communicator;

    [HttpGet, EnableQuery]
    [AccessAuthorize(Code = "AS0XRoleIGet")]
    [Description("查询角色")]
    public IActionResult Get() => Ok(_dataService.GetQueryable());
    /// <summary>
    /// 添加角色
    /// </summary>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    [HttpPost]
    [AccessAuthorize(Code = "AS0XRoleICreate")]
    [Description("新增角色")]
    public async Task<IActionResult> CreateAsync(IdentityViewModel viewModel)
    {
        _logger.LogInformation("user {user} will add the role {role}", _dataService.UserName, viewModel.Name);
        if (string.IsNullOrWhiteSpace(viewModel.Name))
        {
            return BadRequest("角色名不可为空!");
        }

        if (await _roleManager.RoleExistsAsync(viewModel.Name))
        {
            return BadRequest($"角色 {viewModel.Name} 已经存在!");
        }

        if (GetBuildinRoles().Contains(viewModel.Name))
        {
            return BadRequest($"不可创建内置角色 {viewModel.Name} !");
        }

        var role = new Role
        {
            State = viewModel.State,
            Name = viewModel.Name,
            Alias = viewModel.Alias,
            Image = viewModel.Image,
            Type = viewModel.Type,
            NormalizedName = viewModel.Name,
            Description = viewModel.Description
        };

        _dataService.Create(role);
        if (viewModel.Attachments != null && viewModel.Attachments.Count != 0)
        {
            await _dataService.AddAttachmentsAsync(role, viewModel.Attachments, nameof(Role));
        }

        var result = await _roleManager.CreateAsync(role);
        if (result.Succeeded)
        {
            _logger.LogWarning("user {user} success to add the role {role}", _dataService.UserName, role.Name);
            return Ok();
        }

        var errorMessage = string.Empty;
        if (result.Errors.IsNotNullOrEmpty())
        {
            errorMessage = result.Errors.Select(x => $"{x.Code}: {x.Description}").ToString<string>();
            _logger.LogWarning("user {user} fail to add the role {role}, error message is: {errorMessage}", _dataService.UserName, role.Name, errorMessage);
        }

        return BadRequest(errorMessage);
    }
    /// <summary>
    /// 修改
    /// </summary>
    /// <param name="viewModel"></param>
    /// <returns></returns>
    [HttpPut]
    [AccessAuthorize(Code = "AS0XRoleIUpdate")]
    [Description("修改角色")]
    public async Task<IActionResult> UpdateAsync(IdentityViewModel viewModel)
    {
        _logger.LogInformation("user {user} will update the user {role}", _dataService.UserName, viewModel);
        var role = await _roleManager.FindByIdAsync(viewModel.Id);
        if (role is null)
        {
            return BadRequest($"角色 {viewModel.Name} 不存在!");
        }

        //不可修改内置角色名
        if ((role.Name != viewModel.Name || role.Type != viewModel.Type) && GetBuildinRoles().Contains(role.Name))
        {
            return BadRequest($"角色 {viewModel.Name} 为内置角色, 不可修改角色名和角色类型!");
        }

        role.Name = viewModel.Name;
        role.Alias = viewModel.Alias;
        role.Image = viewModel.Image;
        role.Type = viewModel.Type;
        role.Description = viewModel.Description;
        role.Update(_dataService.UserName);
        await _dataService.UpdateAttachmentsAsync(role, viewModel.Attachments, nameof(Role));
        var result = await _roleManager.UpdateAsync(role);
        if (result.Succeeded)
        {
            _logger.LogWarning("user {user} success to update the role {role}", _dataService.UserName, viewModel.Name);
            await _authorizationService.RefreshUserInfoAsync(_communicator, IdentityType.Role, role.Id);
            return Ok();
        }

        var errorMessage = string.Empty;
        if (result.Errors.IsNotNullOrEmpty())
        {
            errorMessage = result.Errors.Select(x => $"{x.Code}: {x.Description}").ToString<string>();
            _logger.LogWarning("user {user} fail to update the role {role}, error message is: {errorMessage}", _dataService.UserName, viewModel.Name, errorMessage);
        }

        return BadRequest(errorMessage);
    }
    /// <summary>
    /// 删除
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    [AccessAuthorize(Code = "AS0XRoleIDelete")]
    [Description("删除角色")]
    public async Task<IActionResult> DeleteAsync(string id)
    {
        _logger.LogInformation("user {user} will delete the role {role}", _dataService.UserName, id);
        var role = await _roleManager.FindByIdAsync(id);
        if (role is null)
        {
            return BadRequest($"角色 {id} 不存在!");
        }

        //不可删除内置角色
        if (GetBuildinRoles().Contains(role.Name))
        {
            return BadRequest($"内置角色 {role.Name} 不可删除!");
        }

        //此处是逻辑删除, 因此删除后的数据要查询需要带状态条件
        role.State = DataState.Deleted;
        var result = await _roleManager.UpdateAsync(role);
        if (result.Succeeded)
        {
            _logger.LogWarning("user {user} success to delete the role {role}", _dataService.UserName, role.Name);
            await _authorizationService.RefreshUserInfoAsync(_communicator, IdentityType.Role, role.Id);
            return Ok();
        }

        var errorMessage = string.Empty;
        if (result.Errors.IsNotNullOrEmpty())
        {
            errorMessage = result.Errors.Select(x => $"{x.Code}: {x.Description}").ToString<string>();
            _logger.LogWarning("user {user} fail to delete the role {role}, error message is: {errorMessage}", _dataService.UserName, role.Name, errorMessage);
        }

        return BadRequest(errorMessage);
    }

    /// <summary>
    /// 添加角色的一个 Claim
    /// </summary>
    /// <param name="id"></param>
    /// <param name="claim"></param>
    /// <returns></returns>
    [HttpPost("AddClaim/{id}")]
    [AccessAuthorize(Code = "AS0XRoleIAddClaim")]
    [Description("添加角色的一个Claim")]
    public async Task<ActionResult> AddClaimAsync(string id, Claim claim)
    {
        _logger.LogInformation("user {user} will add claim to role {role}", _dataService.UserName, id);
        var role = await _roleManager.FindByIdAsync(id);
        if (role == null)
        {
            return BadRequest($"角色 {id} 不存在!");
        }

        var result = await _roleManager.AddClaimAsync(role, claim);
        if (result.Succeeded)
        {
            _logger.LogWarning("user {user} success to add claim to role {role}", _dataService.UserName, role.Name);
            return Ok();
        }

        var errorMessage = string.Empty;
        if (result.Errors.IsNotNullOrEmpty())
        {
            errorMessage = result.Errors.Select(x => $"{x.Code}: {x.Description}").ToString<string>();
            _logger.LogWarning("user {user} fail to add claim to role {role}, error message is: {errorMessage}", _dataService.UserName, role.Name, errorMessage);
        }

        return BadRequest(errorMessage);
    }
    /// <summary>
    /// 移除角色的一个 Claim
    /// </summary>
    /// <param name="id"></param>
    /// <param name="claim"></param>
    /// <returns></returns>
    [HttpPost("RemoveClaim/{id}")]
    [AccessAuthorize(Code = "AS0XRoleIRemoveClaim")]
    [Description("移除角色的一个Claim")]
    public async Task<ActionResult> RemoveClaimAsync(string id, Claim claim)
    {
        _logger.LogInformation("user {user} will remove claim to role {role}", _dataService.UserName, id);
        var role = await _roleManager.FindByNameAsync(id);
        if (role == null)
        {
            return BadRequest($"角色 {id} 不存在!");
        }

        var result = await _roleManager.RemoveClaimAsync(role, claim);
        if (result.Succeeded)
        {
            _logger.LogWarning("user {user} success to remove claim to role {role}", _dataService.UserName, role.Name);
            return Ok();
        }

        var errorMessage = string.Empty;
        if (result.Errors.IsNotNullOrEmpty())
        {
            errorMessage = result.Errors.Select(x => $"{x.Code}: {x.Description}").ToString<string>();
            _logger.LogWarning("user {user} fail to remove claim to role {role}, error message is: {errorMessage}", _dataService.UserName, role.Name, errorMessage);
        }

        return BadRequest(errorMessage);
    }

    /// <summary>
    /// 获取内置角色
    /// </summary>
    /// <returns></returns>
    private string[] GetBuildinRoles()
    {
        var results = BUILDIN_ROLES.Split(CHAR_COMMA, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        var roles = _appSetting.BuiltInRoles.Split(CHAR_COMMA, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
        return results.Concat(roles).Distinct().ToArray();
    }
}