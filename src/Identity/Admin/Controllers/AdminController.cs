﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Devonline.Identity.Admin.Controllers;

/// <summary>
/// 系统管理的功能页面
/// </summary>
[DisplayName("系统管理")]
[Authorize(Roles = GROUP_MAINTAINERS)]
[AccessAuthorize(Code = "AS0XAdmin", Content = "/Admin", ResourceType = ResourceType.Page)]
public class AdminController : Controller
{
    /// <summary>
    /// 主页
    /// </summary>
    /// <returns></returns>
    [DisplayName("主页"), AccessAuthorize(Code = "AS0XAdminIndex")]
    public IActionResult Index()
    {
        return View();
    }
    /// <summary>
    /// 基础数据
    /// </summary>
    /// <returns></returns>
    [DisplayName("基础数据"), AccessAuthorize(Code = "AS0PParameter")]
    public IActionResult Parameter()
    {
        return View();
    }
    /// <summary>
    /// 用户
    /// </summary>
    /// <returns></returns>
    [DisplayName("用户"), AccessAuthorize(Code = "AS0PUser")]
    public new IActionResult User()
    {
        return View();
    }
    /// <summary>
    /// 组织单位
    /// </summary>
    /// <returns></returns>
    [DisplayName("组织单位"), AccessAuthorize(Code = "AS0PGroup")]
    public IActionResult Group()
    {
        return View();
    }
    /// <summary>
    /// 级别
    /// </summary>
    /// <returns></returns>
    [DisplayName("级别"), AccessAuthorize(Code = "AS0P04")]
    public IActionResult Level()
    {
        return View();
    }
    /// <summary>
    /// 角色
    /// </summary>
    /// <returns></returns>
    [DisplayName("角色"), AccessAuthorize(Code = "AS0PRole")]
    public IActionResult Role()
    {
        return View();
    }
    /// <summary>
    /// 用户角色
    /// </summary>
    /// <returns></returns>
    [DisplayName("用户角色"), AccessAuthorize(Code = "AS0PUserRole")]
    public IActionResult UserRole()
    {
        return View();
    }
    /// <summary>
    /// 用户组织
    /// </summary>
    /// <returns></returns>
    [DisplayName("用户组织"), AccessAuthorize(Code = "AS0PUserGroup")]
    public IActionResult UserGroup()
    {
        return View();
    }
    /// <summary>
    /// 资源
    /// </summary>
    /// <returns></returns>
    [DisplayName("资源"), AccessAuthorize(Code = "AS0PResource")]
    public IActionResult Resource()
    {
        return View();
    }
    /// <summary>
    /// 访问规则
    /// </summary>
    /// <returns></returns>
    [DisplayName("访问规则"), AccessAuthorize(Code = "AS0PAccessRule")]
    public IActionResult AccessRule()
    {
        return View();
    }
    /// <summary>
    /// 访问申请
    /// </summary>
    /// <returns></returns>
    [DisplayName("访问申请"), AccessAuthorize(Code = "AS0PAccessApply")]
    public IActionResult AccessApply()
    {
        return View();
    }
    /// <summary>
    /// 访问记录
    /// </summary>
    /// <returns></returns>
    [DisplayName("访问记录"), AccessAuthorize(Code = "AS0PAccessRecord")]
    public IActionResult AccessRecord()
    {
        return View();
    }
    /// <summary>
    /// 用户可访问资源查询
    /// </summary>
    /// <returns></returns>
    [DisplayName("用户可访问资源查询"), AccessAuthorize(Code = "AS0PUserResource")]
    public IActionResult UserResource()
    {
        return View();
    }
    /// <summary>
    /// 角色可访问资源
    /// </summary>
    /// <returns></returns>
    [DisplayName("角色可访问资源"), AccessAuthorize(Code = "AS0PRoleResource")]
    public IActionResult RoleResource()
    {
        return View();
    }
    /// <summary>
    /// 组织机构可访问资源
    /// </summary>
    /// <returns></returns>
    [DisplayName("组织机构可访问资源"), AccessAuthorize(Code = "AS0PGroupResource")]
    public IActionResult GroupResource()
    {
        return View();
    }
}