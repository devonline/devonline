﻿using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace Devonline.Entity;

/// <summary>
/// 基础数据, 字符串类型的默认实现
/// </summary>
//[Cacheable(LocalCache = true)]
[Table("parameter"), DisplayName("基础数据"), Index(nameof(Index), IsUnique = true)]
public class Parameter : Parameter<string>, IKeyValuePair, IParent, IEntitySet, IEntitySetWithCreate
{
    /// <summary>
    /// 构造方法给 Id 赋值
    /// </summary>
    public Parameter() => Id = KeyGenerator.GetStringKey();

    /// <summary>
    /// 父项基础数据
    /// </summary>
    public virtual Parameter? Parent { get; set; }

    /// <summary>
    /// 子项基础数据
    /// </summary>
    public virtual ICollection<Parameter>? Children { get; set; }
}

/// <summary>
/// 基础数据
/// </summary>
[Table("parameter"), DisplayName("基础数据"), Index(nameof(Index), IsUnique = true)]
public abstract class Parameter<TKey> : EntitySetWithCreate<TKey>, IKeyValuePair, IParent<TKey>, IEntitySet<TKey>, IEntitySetWithCreate<TKey> where TKey : IConvertible
{
    /// <summary>
    /// 序号
    /// </summary>
    [Column("index"), DisplayName("序号"), Unique, Excel]
    public virtual int Index { get; set; }
    /// <summary>
    /// 键
    /// </summary>
    [Column("key"), DisplayName("键"), Required, Unique, MaxLength(128), Excel]
    public virtual string Key { get; set; } = null!;
    /// <summary>
    /// 值
    /// </summary>
    [Column("value"), DisplayName("值"), Required, MaxLength(255), Excel]
    public virtual string Value { get; set; } = null!;
    /// <summary>
    /// 基础数据项显示的文本
    /// </summary>
    [Column("text"), DisplayName("文本"), Required, MaxLength(128), Excel]
    public virtual string Text { get; set; } = null!;
    /// <summary>
    /// 父项编号
    /// </summary>
    [Column("parent_id"), DisplayName("父级编号"), MaxLength(36), Excel]
    public virtual TKey? ParentId { get; set; }
}